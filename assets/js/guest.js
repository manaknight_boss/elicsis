console.log = function() {};
function initAutocomplete() {
  if (document.getElementById("address_string")) {
    autocomplete = new google.maps.places.Autocomplete(
      document.getElementById("address_string"),
      {
        types: ["geocode", "establishment"],
        componentRestrictions: { country: "do" }
      }
    );
    autocomplete.setFields(["address_components", "geometry"]);
    // var bounds = new google.maps.LatLngBounds(
    //   new google.maps.LatLng(19, -70.666667)
    // );
    // autocomplete.setBounds(bounds);
    autocomplete.addListener("place_changed", fillInAddress);
  }
}

function fillInAddress() {
  var place = autocomplete.getPlace();
  console.log(place);
  var componentForm = {
    street_number: "short_name",
    route: "long_name",
    locality: "long_name",
    administrative_area_level_1: "short_name",
    country: "long_name",
    postal_code: "short_name"
  };

  var addressObject = {
    street_number: "",
    route: "long_name",
    locality: "long_name", //city
    administrative_area_level_1: "", //state
    country: "",
    postal_code: "",
    lat: place.geometry.location.lat(),
    lng: place.geometry.location.lng()
  };

  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      addressObject[addressType] = val;
    }
  }
  addressObject.full_address = document.getElementById("address_string").value;
  // console.log(addressObject);
  xhr = new XMLHttpRequest();

  xhr.open("POST", "/v1/api/search");
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
    if (xhr.status === 200 && xhr.responseText) {
      var data = JSON.parse(xhr.responseText);
      if (data.success) {
        window.location.href = "/search";
      }
    } else if (xhr.status !== 200) {
      console.log(xhr.status);
    }
  };

  xhr.send(param(addressObject));
}

function param(object) {
  var encodedString = "";
  for (var prop in object) {
    if (object.hasOwnProperty(prop)) {
      if (encodedString.length > 0) {
        encodedString += "&";
      }
      encodedString += encodeURI(prop + "=" + object[prop]);
    }
  }
  return encodedString;
}

$(window).scroll(function() {
  var nav = $("#navbarMain");
  var top = nav.height();
  if ($(window).scrollTop() >= top) {
    nav.addClass("scrolled");
  } else {
    nav.removeClass("scrolled");
  }
});

$(document).ready(function() {
  $(".contact-us-modal__submit").click(function() {
    var email = $("#contact_us_form_email").val();
    var name = $("#contact_us_form_name").val();
    var comment = $("#contact_us_form_comments").val();
    if (
      email &&
      email.length > 0 &&
      name &&
      name.length > 0 &&
      comment &&
      comment.length > 0
    ) {
      $.ajax({
        type: "POST",
        url: "/v1/api/contact",
        data: {
          email: email,
          name: name,
          comment: comment
        },
        contentType: "application/x-www-form-urlencoded; charset=UTF-8;"
      }).done(
        function(data) {
          $(".contact-form-success").removeClass("d-none");
          $(".contact-form-error").addClass("d-none");
        },
        function(error) {}
      );
    } else {
      $(".contact-form-error").removeClass("d-none");
    }
  });

  $(".more-preference-option").click(function() {
    $(".less-preference-option").removeClass("d-none");
    $(".more-preference-option").addClass("d-none");
    $(".preference-more").removeClass("d-none");
  });
  $(".less-preference-option").click(function() {
    $(".more-preference-option").removeClass("d-none");
    $(".less-preference-option").addClass("d-none");
    $(".preference-more").addClass("d-none");
  });

  $("#filter-search").click(function(e) {});

  jQuery.datetimepicker.setLocale("es");
  if ($('[type="date"]').prop("type") != "date") {
    $('[type="date"]').datetimepicker({
      minDate: 0,
      timepicker: false,
      format: "Y-m-d"
    });
  }

  if ($("#filter-date").val() != undefined) {
    if (
      $("#filter-date").attr("data-date") &&
      $("#filter-date").attr("data-date").length > 0
    ) {
      var defaultDate = $("#filter-date").attr("data-date");
      $("#filter-date").val(defaultDate);
    }

    jQuery("#filter-date").datetimepicker({
      minDate: 0,
      timepicker: false,
      format: "Y-m-d",
      onChangeDateTime: function(dp, $input) {
        var dateSelected = new Date($input.val());
        var month =
          dateSelected.getMonth() + 1 < 10
            ? "0" + (dateSelected.getMonth() + 1)
            : dateSelected.getMonth() + 1;
        var date =
          dateSelected.getUTCDate() < 10
            ? "0" + dateSelected.getUTCDate()
            : dateSelected.getUTCDate();
        mkd_events.publish("update_date", {
          date: dateSelected.getFullYear() + "-" + month + "-" + date,
          day: dateSelected.getDay()
        });
      }
    });
  }

  if ($("#rating").val() != undefined) {
    $("#rating").starRating({
      starSize: 25,
      useFullStars: true,
      callback: function(currentRating, $el) {
        $("#rating_value").val(currentRating);
      }
    });
  }
  if ($("#create_account_type")) {
    $(".login-step-1").hide();
    $(".register-step-1").hide();

    $("#return-customer").click(function() {
      $(".login-step-1").show();
      $(".register-step-1").hide();
    });

    $("#new-customer").click(function() {
      $(".login-step-1").hide();
      $(".register-step-1").show();
    });
    $("#google-customer").click(function() {
      window.location.href = $("#google_url").val();
    });
    $("#new-customer").click();
  }
});
