console.log = function() {};

function configure_menu_addon_form(index, menu_property_list) {
  $("serving-form-component").addClass("d-none");
  $("price-form-component").addClass("d-none");
  $("info-form-component").addClass("d-none");
  $("preference-form-component").addClass("d-none");
  $("#serving").val("");
  $("#price").val("");
  $("#info").val("");

  if (menu_property_list[index].is_serving == 1) {
    $(".serving-form-component").removeClass("d-none");
    $(".serving-form-component").removeClass("d-none");
  }
  if (menu_property_list[index].is_price == 1) {
    $(".price-form-component").removeClass("d-none");
    $(".price-form-component").removeClass("d-none");
  }
  if (menu_property_list[index].is_info == 1) {
    $(".info-form-component").removeClass("d-none");
    $(".info-form-component").removeClass("d-none");
  }
  if (menu_property_list[index].is_preference == 1) {
    $(".preference-form-component").removeClass("d-none");
    $(".preference-form-component").removeClass("d-none");
  }
}

function configure_menu_addon_edit_form(index, menu_property_list) {
  $("serving-form-component").addClass("d-none");
  $("price-form-component").addClass("d-none");
  $("info-form-component").addClass("d-none");
  $("preference-form-component").addClass("d-none");

  if (menu_property_list[index].is_serving == 1) {
    $(".serving-form-component").removeClass("d-none");
    $(".serving-form-component").removeClass("d-none");
  }
  if (menu_property_list[index].is_price == 1) {
    $(".price-form-component").removeClass("d-none");
    $(".price-form-component").removeClass("d-none");
  }
  if (menu_property_list[index].is_info == 1) {
    $(".info-form-component").removeClass("d-none");
    $(".info-form-component").removeClass("d-none");
  }
  if (menu_property_list[index].is_preference == 1) {
    $(".preference-form-component").removeClass("d-none");
    $(".preference-form-component").removeClass("d-none");
  }
}

function callbackAddress() {
  autocomplete = new google.maps.places.Autocomplete(
    document.getElementById("google_address"),
    {
      types: ["geocode", "establishment"],
      componentRestrictions: { country: "do" }
    }
  );
  autocomplete.setFields(["address_components", "geometry"]);
  // var bounds = new google.maps.LatLngBounds(
  //   new google.maps.LatLng(19, -70.666667)
  // );
  // autocomplete.setBounds(bounds);
  autocomplete.addListener("place_changed", fillInRestaurantAddress);
}

function fillInRestaurantAddress() {
  var place = autocomplete.getPlace();

  var componentForm = {
    street_number: "short_name",
    route: "long_name",
    locality: "long_name",
    administrative_area_level_1: "short_name",
    country: "long_name",
    postal_code: "short_name"
  };

  var addressObject = {
    street_number: "",
    route: "",
    locality: "", //city
    administrative_area_level_1: "", //state
    country: "",
    postal_code: "",
    lat: place.geometry.location.lat(),
    lng: place.geometry.location.lng()
  };

  console.log(addressObject);

  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      addressObject[addressType] = val;
    }
  }

  console.info(addressObject);

  document.getElementById("address").value = addressObject.route;
  document.getElementById("city").value = addressObject.locality;
  document.getElementById("state").value =
    addressObject.administrative_area_level_1;
  document.getElementById("zip").value = addressObject.postal_code;
  document.getElementById("lat").value = addressObject.lat;
  document.getElementById("long").value = addressObject.lng;

  document.getElementById("google_city").value = addressObject.locality;
  document.getElementById("google_state").value =
    addressObject.administrative_area_level_1;
  document.getElementById("google_zip").value = addressObject.postal_code;
}

function readFileLogo(input, cropObject) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      document.getElementById("upload_logo_container_wrapper").className = "";
      document.getElementById("preview_logo_container_wrapper").className =
        "d-none";
      cropObject.bind({
        url: e.target.result
      });
    };
    reader.readAsDataURL(input.files[0]);
  } else {
    console.error("Your browser cannot read images");
  }
}

function readFileThumbnail(input, cropObject) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      document.getElementById("upload_thumbnail_container_wrapper").className =
        "";
      document.getElementById("preview_thumbnail_container_wrapper").className =
        "d-none";
      cropObject.bind({
        url: e.target.result
      });
    };
    reader.readAsDataURL(input.files[0]);
  } else {
    console.error("Your browser cannot read images");
  }
}

function readFileGeneric(input, cropObject, type) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      document.getElementById(
        "upload_" + type + "_container_wrapper"
      ).className = "";
      if (document.getElementById("preview_" + type + "_container_wrapper")) {
        document.getElementById(
          "preview_" + type + "_container_wrapper"
        ).className = "d-none";
      }
      cropObject.bind({
        url: e.target.result
      });
    };
    reader.readAsDataURL(input.files[0]);
  } else {
    console.error("Your browser cannot read images");
  }
}

jQuery(document).ready(function($) {
  "use strict";

  // ==============================================================
  // Sidebar scrollnavigation
  // ==============================================================

  if ($(".sidebar-nav-fixed a").length) {
    $(".sidebar-nav-fixed a")
      // Remove links that don't actually link to anything

      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length
            ? target
            : $("[name=" + this.hash.slice(1) + "]");
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $("html, body").animate(
              {
                scrollTop: target.offset().top - 90
              },
              1000,
              function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) {
                  // Checking if the target was focused
                  return false;
                } else {
                  $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                  $target.focus(); // Set focus again
                }
              }
            );
          }
        }
        $(".sidebar-nav-fixed a").each(function() {
          $(this).removeClass("active");
        });
        $(this).addClass("active");
      });
  }

  // ==============================================================
  // tooltip
  // ==============================================================
  if ($('[data-toggle="tooltip"]').length) {
    $('[data-toggle="tooltip"]').tooltip();
  }

  // ==============================================================
  // popover
  // ==============================================================
  if ($('[data-toggle="popover"]').length) {
    $('[data-toggle="popover"]').popover();
  }
}); // AND OF JQUERY

$(document).ready(function() {
  $("#google_address").change(function(e) {
    $("#address").val($("#google_address").val());
  });
  $("#google_zip").change(function(e) {
    $("#zip").val($("#google_zip").val());
  });
  $("#google_city").change(function(e) {
    $("#city").val($("#google_city").val());
  });
  $("#google_state").change(function(e) {
    $("#state").val($("#google_state").val());
  });

  jQuery.datetimepicker.setLocale("es");
  if ($('[type="date"]').prop("type") != "date") {
    $('[type="date"]').datetimepicker({
      minDate: 0,
      timepicker: false,
      format: "Y-m-d"
    });
  }
  if ($("#chart_on_time_accept").html() != undefined) {
    var json_graph_accept = $("#chart_on_time_accept")
      .attr("data-graph")
      .replace(/'/g, '"');
    var json_accept = JSON.parse(json_graph_accept);
    var chart = c3.generate({
      bindto: "#chart_on_time_accept",
      data: {
        columns: [
          ["Perfecto", json_accept[100]],
          ["Estándar", json_accept[75]],
          ["Pobre", json_accept[30]],
          ["Tarde", json_accept[0]]
        ],
        type: "bar"
      },
      bar: {
        width: {
          ratio: 1 // this makes bar width 50% of length between ticks
        }
      }
    });
  }
  if ($("#chart_on_time_delivery").html() != undefined) {
    var json_graph_delivery = $("#chart_on_time_delivery")
      .attr("data-graph")
      .replace(/'/g, '"');
    var json_delivery = JSON.parse(json_graph_delivery);
    var chart = c3.generate({
      bindto: "#chart_on_time_delivery",
      data: {
        columns: [
          ["Perfecto", json_delivery[100]],
          ["Estándar", json_delivery[75]],
          ["Pobre", json_delivery[30]],
          ["Tarde", json_delivery[0]]
        ],
        type: "bar"
      },
      bar: {
        width: {
          ratio: 1 // this makes bar width 50% of length between ticks
        }
      }
    });
  }

  $("#menu_property_add_preference").bsMultiSelect();
  $("#preference").bsMultiSelect();

  // $(":input").inputmask({
  //   rightAlign: false
  // });
  $("#google_address").inputmask("remove");

  if (document.getElementById("restaurant_logo")) {
    var el = document.getElementById("upload_logo_container");
    var uploadLogoCrop = new Croppie(el, {
      enableExif: true,
      viewport: {
        width: 240,
        height: 100
      },
      boundary: {
        width: 300,
        height: 300
      }
    });

    $("#restaurant_logo").on("change", function() {
      readFileGeneric(this, uploadLogoCrop, "logo");
    });

    $("#save_restaurant_logo").click(function(e) {
      uploadLogoCrop
        .result({
          type: "blob",
          format: "png"
        })
        .then(function(blob) {
          var form = new FormData();
          form.append("attachments", blob, "featured.png");
          $.ajax({
            type: "POST",
            url: "/v1/api/attachment/upload/blob",
            data: form,
            processData: false,
            contentType: false
          }).done(function(data) {
            if (data.url) {
              $.ajax({
                type: "POST",
                url: "/v1/api/restaurant/upload/logo",
                data: {
                  logo: data.url,
                  id: $("#restaurant_id").val()
                }
              }).done(function(result) {
                if (!result.error) {
                  $.toast({
                    text: "Foto Actualizada",
                    position: "top-right",
                    stack: false,
                    loader: false
                  });
                } else {
                  $.toast({
                    text: "Foto Actualizada Fallada",
                    position: "top-right",
                    stack: false,
                    loader: false
                  });
                }
              });
            }
          });
        });
    });
  }

  if (document.getElementById("menu_image")) {
    var el = document.getElementById("upload_menu_container");
    var uploadMenuCrop = new Croppie(el, {
      enableExif: true,
      viewport: {
        width: 600,
        height: 300
      },
      boundary: {
        width: 800,
        height: 400
      }
    });
    $("#menu_image").on("change", function() {
      readFileGeneric(this, uploadMenuCrop, "menu");
    });
    $("#save_menu_image").click(function(e) {
      uploadMenuCrop
        .result({
          type: "blob",
          format: "png"
        })
        .then(function(blob) {
          var form = new FormData();
          form.append("attachments", blob, "featured.png");
          $.ajax({
            type: "POST",
            url: "/v1/api/attachment/upload/blob",
            data: form,
            processData: false,
            contentType: false
          }).done(function(data) {
            if (data.url) {
              $.ajax({
                type: "POST",
                url: "/v1/api/menus/upload",
                data: {
                  image: data.url,
                  id: $("#menu_id").val()
                }
              }).done(function(result) {
                if (!result.error) {
                  $.toast({
                    text: "Foto Actualizada",
                    position: "top-right",
                    stack: false,
                    loader: false
                  });
                } else {
                  $.toast({
                    text: "Foto Actualizada Fallada",
                    position: "top-right",
                    stack: false,
                    loader: false
                  });
                }
              });
            }
          });
        });
    });
  }

  if (document.getElementById("restaurant_thumbnail")) {
    var el = document.getElementById("upload_thumbnail_container");
    var uploadThumbnailCrop = new Croppie(el, {
      enableExif: true,
      viewport: {
        width: 600,
        height: 300
      },
      boundary: {
        width: 800,
        height: 400
      }
    });

    $("#restaurant_thumbnail").on("change", function() {
      readFileGeneric(this, uploadThumbnailCrop, "thumbnail");
    });

    $("#save_restaurant_thumbnail").click(function(e) {
      uploadThumbnailCrop
        .result({
          type: "blob",
          format: "png"
        })
        .then(function(blob) {
          var form = new FormData();
          form.append("attachments", blob, "featured.png");
          $.ajax({
            type: "POST",
            url: "/v1/api/attachment/upload/blob",
            data: form,
            processData: false,
            contentType: false
          }).done(function(data) {
            if (data.url) {
              $.ajax({
                type: "POST",
                url: "/v1/api/restaurant/upload/image",
                data: {
                  image: data.url,
                  id: $("#restaurant_id").val()
                }
              }).done(function(result) {
                if (!result.error) {
                  $.toast({
                    text: "Foto Actualizada",
                    position: "top-right",
                    stack: false,
                    loader: false
                  });
                } else {
                  $.toast({
                    text: "Foto Actualizada Fallada",
                    position: "top-right",
                    stack: false,
                    loader: false
                  });
                }
              });
            }
          });
        });
    });
  }

  if (document.getElementById("restaurant_banner")) {
    var el = document.getElementById("upload_banner_container");
    var uploadBannerCrop = new Croppie(el, {
      enableExif: true,
      viewport: {
        width: 600,
        height: 300
      },
      boundary: {
        width: 800,
        height: 400
      }
    });

    $("#restaurant_banner").on("change", function() {
      readFileGeneric(this, uploadBannerCrop, "banner");
    });

    $("#save_restaurant_banner").click(function(e) {
      uploadBannerCrop
        .result({
          type: "blob",
          format: "png"
        })
        .then(function(blob) {
          var form = new FormData();
          form.append("attachments", blob, "featured.png");
          $.ajax({
            type: "POST",
            url: "/v1/api/attachment/upload/blob",
            data: form,
            processData: false,
            contentType: false
          }).done(function(data) {
            if (data.url) {
              $.ajax({
                type: "POST",
                url: "/v1/api/restaurant/upload/banner_image",
                data: {
                  banner_image: data.url,
                  id: $("#restaurant_id").val()
                }
              }).done(function(result) {
                if (!result.error) {
                  $.toast({
                    text: "Foto Actualizada",
                    position: "top-right",
                    stack: false,
                    loader: false
                  });
                } else {
                  $.toast({
                    text: "Foto Actualizada Fallada",
                    position: "top-right",
                    stack: false,
                    loader: false
                  });
                }
              });
            }
          });
        });
    });
  }

  setTimeout(function() {
    if (
      $("#menu_property_list") &&
      $("#menu_property_list").val() != undefined
    ) {
      var index = $("#menu_property_list").val();
      for (let i = 0; i < menu_property_list.length; i++) {
        if (index == menu_property_list[i].id) {
          index = i;
        }
      }
      configure_menu_addon_form(index, menu_property_list);
    }

    if ($("#menu_addon_edit") && $("#menu_addon_edit").val() != undefined) {
      var index = -1;
      var id = $("#menu_property_id").val();
      for (let i = 0; i < menu_property_list.length; i++) {
        if (id == menu_property_list[i].id) {
          index = i;
        }
      }
      configure_menu_addon_edit_form(index, menu_property_list);
    }
  }, 500);

  $("#menu_property_list").change(function(e) {
    var id = $("#menu_property_list")
      .children("option:selected")
      .val();
    $("#menu_property_id").val(id);
    var index = -1;
    for (var i = 0; i < menu_property_list.length; i++) {
      if (id == menu_property_list[i].id) {
        index = i;
      }
    }
    configure_menu_addon_form(index, menu_property_list);
  });
});
