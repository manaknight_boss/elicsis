console.log = function() {};
var mkd_events = (function() {
  var topics = {};
  var hOP = topics.hasOwnProperty;

  return {
    subscribe: function(topic, listener) {
      // Create the topic's object if not yet created
      if (!hOP.call(topics, topic)) topics[topic] = [];

      // Add the listener to queue
      var index = topics[topic].push(listener) - 1;

      // Provide handle back for removal of topic
      return {
        remove: function() {
          delete topics[topic][index];
        }
      };
    },
    publish: function(topic, info) {
      // If the topic doesn't exist, or there's no listeners in queue, just leave
      if (!hOP.call(topics, topic)) return;

      // Cycle through topics queue, fire!
      topics[topic].forEach(function(item) {
        item(info != undefined ? info : {});
      });
    }
  };
})();

function initAutocompleteFilter() {
  if (document.getElementById("address_string")) {
    autocomplete = new google.maps.places.Autocomplete(
      document.getElementById("address_string"),
      {
        types: ["geocode", "establishment"],
        componentRestrictions: { country: "do" }
      }
    );
    autocomplete.setFields(["address_components", "geometry"]);
    // var bounds = new google.maps.LatLngBounds(
    //     new google.maps.LatLng(19, -70.666667)
    // );
    // autocomplete.setBounds(bounds);
    autocomplete.addListener("place_changed", fillInAddressFilter);
  }
}

function fillInAddressFilter() {
  var place = autocomplete.getPlace();
  var componentForm = {
    street_number: "short_name",
    route: "long_name",
    locality: "long_name",
    administrative_area_level_1: "short_name",
    country: "long_name",
    postal_code: "short_name"
  };

  var addressObject = {
    street_number: "",
    route: "long_name",
    locality: "long_name", //city
    administrative_area_level_1: "", //state
    country: "",
    postal_code: "",
    lat: place.geometry.location.lat(),
    lng: place.geometry.location.lng()
  };

  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      addressObject[addressType] = val;
    }
  }

  addressObject.full_address = document.getElementById("address_string").value;
  // window.config.lat = place.geometry.location.lat();
  // window.config.lng = place.geometry.location.lng();
  mkd_events.publish("update_lat_lng", {
    address: document.getElementById("address_string").value,
    lat: place.geometry.location.lat(),
    lng: place.geometry.location.lng()
  });
}

var app = angular.module("app", ["ngSanitize"]);

/**
 * 1.Get Config Object
 * 2.Bind all controls to everything
 * 3.Make a GET API Call to get results
 * 4.Bind Paginate buttons at bottom
 * 5.If User clicks link, jump to that page
 * 6.Make API Call for paginate clicks
 */
app.controller("SearchController", [
  "$scope",
  "$window",
  "http.service",
  "$sce",
  function($scope, $window, httpService, $sce) {
    //1
    $scope.filterObj = $window.c()[0];
    $scope.filterObj.search = $scope.filterObj.search
      ? $scope.filterObj.search
      : "";
    $scope.full_address_display = $window.c()[0].address_full;
    $scope.num_restaurant = $window.c()[1];
    $scope.page = 0;
    $scope.num_items = 0;
    $scope.num_pages = 0;
    $scope.items = [];
    $scope.pageNumList = [];
    $scope.ready = false;
    $scope.sidemenu = false;
    $scope.filterChanged = false;
    $scope.pageChanged = true;
    $scope.hasPrev = false;
    $scope.hasNext = false;

    $scope.restaurantUrl = "/restaurant/";
    $scope.debugLog = function(field, data) {
      //   console.log(field, data);
    };

    if ($scope.filterObj.date.length > 0) {
      document
        .getElementById("filter-date")
        .setAttribute("data-date", $scope.filterObj.date.replace(/-/g, "/"));
      $scope.filterObj.day = new Date(
        $scope.filterObj.date.replace(/-/g, "/")
      ).getDay();
    }

    if ($scope.filterObj.time) {
      $scope.filterObj.time = $scope.filterObj.time.toString();
    }

    $scope.debugLog("Config: ", $scope.filterObj);

    $scope.subscription = mkd_events.subscribe("update_date", function(obj) {
      $scope.debugLog("UPDATE DATE: ", obj);
      $scope.filterObj.date = obj.date;
      $scope.filterObj.day = obj.day;
      $scope.filterChanged = true;
      $scope.pageChanged = true;
      $scope.filterResults();
    });

    $scope.subscription = mkd_events.subscribe("update_lat_lng", function(obj) {
      $scope.filterObj.lat = obj.lat;
      $scope.filterObj.lng = obj.lng;
      $scope.full_address_display = obj.address;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    });

    $scope.toggleSideMenu = function() {
      $scope.sidemenu = !$scope.sidemenu;
    };

    $scope.am_or_pm = function(n) {
      var minute_format = n % 1 == 0 ? ":00" : ":30";
      var integer_n = Number(Math.floor(n));
      if (integer_n == 24 || integer_n == 0) {
        return "12" + minute_format + " AM";
      }
      if (integer_n == 12) {
        return "12" + minute_format + " PM";
      }
      if (integer_n < 12) {
        return integer_n + minute_format + " AM";
      }

      if (integer_n > 12) {
        return (integer_n % 12) + minute_format + " PM";
      }
    };

    $scope.hour_key = [];
    $scope.hour_value = [];

    for (i = 0; i < 24; i++) {
      var v = i < 13 ? i + ":00 AM" : (i % 12) + ":00 PM";
      if (i == 12) {
        v = "12:00 PM";
      }
      if (i == 0) {
        v = "12:00 AM";
      }
      var v2 = v.replace("00", "30");
      $scope.hour_key.push(i.toString());
      $scope.hour_key.push((i + 0.5).toString());
      $scope.hour_value.push(v);
      $scope.hour_value.push(v2);
    }
    //2
    $scope.orderTypeChange = function(val) {
      $scope.debugLog("Order Type Change: ", val);
      $scope.filterObj.order_type = val;
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.dateChange = function(val) {
      $scope.debugLog("Date Change: ", $scope.filterObj.date);
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.timeChange = function(val) {
      $scope.debugLog("Time Change: ", $scope.filterObj.time);
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.servingChange = function() {
      $scope.debugLog("Serving Change: ", $scope.filterObj.serving);
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.searchChange = function() {
      $scope.debugLog("Search Change: ", $scope.filterObj.search);
      if ($scope.filterObj.search.length > 4) {
        $scope.page = 0;
        $scope.pageChanged = true;
        $scope.filterChanged = true;
        $scope.filterResults();
      }
    };

    $scope.cuisineChange = function(val) {
      $scope.debugLog("Cuisine Change: ", val);
      $scope.filterObj.cuisine_id = val;
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.priceChange = function(val) {
      $scope.debugLog("Price Change: ", val);
      $scope.filterObj.price = val;
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.deliveryChange = function(val) {
      $scope.debugLog("Delivery Change: ", val);
      $scope.filterObj.delivery = val;
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.distanceChange = function(val) {
      $scope.debugLog("Distance Change: ", val);
      $scope.filterObj.distance = val;
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.ratingChange = function(val) {
      $scope.debugLog("Rating Change: ", val);
      $scope.filterObj.rating = val;
      $scope.page = 0;
      $scope.pageChanged = true;
      $scope.filterChanged = true;
      $scope.filterResults();
    };

    $scope.range = function(start, end) {
      return Array(end - start + 1)
        .fill()
        .map((_, idx) => start + idx);
    };

    $scope.resetPaginationButton = function() {
      if ($scope.page == 0 && $scope.num_pages == 0) {
      } else {
        if ($scope.page != 0) {
          $scope.hasPrev = true;
        } else {
          $scope.hasPrev = false;
        }
        if ($scope.page + 1 < $scope.num_pages) {
          $scope.hasNext = true;
        } else {
          $scope.hasNext = false;
        }
      }
    };

    //3
    $scope.filterResults = function() {
      if (!$scope.pageChanged) {
        return false;
      }

      if ($scope.filterObj.serving == null) {
        $scope.filterObj.serving = 0;
      }
      if ($scope.filterObj.search == null) {
        $scope.filterObj.search = "";
      }

      var result = $scope.filterObj;
      result.page = $scope.page;
      $scope.ready = false;
      $scope.debugLog("Filter Data: ", result);

      httpService
        .doPost("filter", result)
        .then(function(data) {
          $scope.debugLog("Filter Result :", data);
          $scope.pageChanged = false;
          $scope.num_restaurant = data.num_items;
          $scope.num_items = data.num_items;
          $scope.num_pages = data.num_pages;
          $scope.ready = true;
          $scope.resetPaginationButton();
          $scope.pageNumList = $scope.range(0, $scope.num_pages - 1);
          $scope.debugLog(
            "Created Next: ",
            $scope.pageNumList.length > 0 && $scope.page != $scope.num_pages
          );
          $scope.debugLog(
            "Created Prev: ",
            $scope.pageNumList.length > 0 && $scope.page != 0
          );

          $scope.items = data.items.map(function(element) {
            var rating_star = "";
            if (element.num_review < 2) {
              element.rating = 5;
              element.on_time_rating = 100;
              element.on_delivery_rating = 100;
              element.num_review = 1;
            }
            element.rating = Math.floor(element.rating);
            for (var i = 0; i < 5; i++) {
              if (i < element.rating) {
                rating_star = rating_star + '<i class="fas fa-star fa-xs"></i>';
              } else {
                rating_star = rating_star + '<i class="far fa-star fa-xs"></i>';
              }
            }
            element.rating_star = $sce.trustAsHtml(rating_star);
            return element;
          });
        })
        .catch(function(e) {
          $scope.pageChanged = false;
          $scope.debugLog("Filter Error: ", e);
        });
    };

    //4
    $scope.prev = function() {
      $scope.pageChanged = false;
      var oldPage = $scope.page;
      $scope.debugLog("Old: ", oldPage);
      if ($scope.page - 1 > 0) {
        $scope.page--;
      } else {
        $scope.page = 0;
      }

      if (oldPage != $scope.page) {
        $scope.pageChanged = true;
        $scope.filterResults();
      }
      $scope.resetPaginationButton();
      $scope.debugLog("Num Page: ", $scope.num_pages);
      $scope.debugLog("Prev: ", $scope.page);
      $scope.debugLog("Prev: ", oldPage);
      $scope.debugLog("Prev: ", $scope.pageChanged);
    };

    $scope.next = function(value) {
      $scope.pageChanged = false;
      var oldPage = $scope.page;
      $scope.debugLog("Old: ", oldPage);
      if ($scope.page + 1 < $scope.num_pages) {
        $scope.page++;
      } else {
        $scope.page = $scope.num_pages - 1;
      }

      $scope.resetPaginationButton();

      if (oldPage != $scope.page) {
        $scope.pageChanged = true;
        $scope.filterResults();
      }

      $scope.debugLog("Num Page: ", $scope.num_pages);
      $scope.debugLog("Next: ", $scope.page);
      $scope.debugLog("Next: ", oldPage);
      $scope.debugLog("Next: ", $scope.pageChanged);
    };

    $scope.choosePage = function(newPage) {
      $scope.debugLog("New Page: ", newPage);
      $scope.debugLog("New Page Result: ", newPage != $scope.page);
      // if (newPage != $scope.page) {
      $scope.page = newPage;
      $scope.pageChanged = true;
      // $scope.resetPaginationButton();
      $scope.filterResults();
      // }
    };

    //5
    $scope.openRestaurant = function(restaurant) {
      $scope.debugLog("Open Restaurant: ", restaurant);
      httpService
        .doPost("search/save", $scope.filterObj)
        .then(function(data) {
          $scope.debugLog("Search Saved :", data);
          window.location.href = $scope.restaurantUrl + restaurant.id;
        })
        .catch(function(e) {
          $scope.debugLog("Search Saved Error: ", e);
        });
    };

    $scope.clear = function() {
      $scope.filterChanged = false;
      $scope.page = 0;
      $scope.num_items = 0;
      $scope.num_pages = 0;
      $scope.items = [];
      $scope.pageNumList = [];
      $scope.ready = false;
      $scope.pageChanged = true;
      $scope.hasPrev = false;
      $scope.hasNext = false;
      $scope.filterObj.cuisine_id = 0;
      $scope.filterObj.date = "";
      $scope.filterObj.delivery = 0;
      $scope.filterObj.distance = 0;
      $scope.filterObj.order_type = "delivery";
      $scope.filterObj.price = "";
      $scope.filterObj.rating = 0;
      $scope.filterObj.search = "";
      $scope.filterObj.serving = 1;
      $scope.filterObj.time = "";
      $scope.filterResults();
    };

    $scope.filterResults();
    $("#filterModal").modal("show");
  }
]);

/**
 * 1.Get Config Object
 * 2.Bind all controls to everything
 * 3.Make a GET API Call to get results
 * 4.Bind Paginate buttons at bottom
 * 5.If User clicks link, jump to that page
 * 6.Make API Call for paginate clicks
 */
app.controller("RestaurantController", [
  "$scope",
  "$window",
  "http.service",
  "$sce",
  "$location",
  "$anchorScroll",
  function($scope, $window, httpService, $sce, $location, $anchorScroll) {
    $anchorScroll.yOffset = 150;
    $scope.filterObj = $window.c()[0];
    $scope.restaurant_config = $window.c()[2];
    $scope.search = $window.c()[3];
    $scope.schedule = $window.c()[4];
    $scope.catering_menus = $scope.restaurant_config.catering_menu;
    $scope.menu = $scope.restaurant_config.menu;
    $scope.reviews = [];
    $scope.hasMoreReviews = true;
    $scope.bypass_cart_popup = false;
    $scope.reviewPage = 0;
    $scope.total_serving = 0;
    $scope.cart_item_index = -1;
    $scope.cart = {
      items: [],
      tip_option: false,
      delivery_fee: $scope.restaurant_config.delivery_fee,
      is_delivery: true,
      restaurant_id: $scope.restaurant_config.id,
      total: 0,
      tips: 0,
      real_tips: 0,
      tax: 0
    };

    if ($scope.search.date.length > 0) {
      $scope.filterObj.date = $scope.search.date;
      document
        .getElementById("filter-date")
        .setAttribute("data-date", $scope.search.date.replace(/-/g, "/"));
    }

    if ($scope.search.time) {
      $scope.filterObj.time = $scope.search.time;
    }

    if ($scope.restaurant_config.delivery_type == 2) {
      $scope.restaurant_config.delivery_fee = 0;
      $scope.filterObj.order_type = "takeout";
      $scope.cart.is_delivery = false;
      $scope.cart.delivery_fee = 0;
    }

    if ($scope.restaurant_config.delivery_type == 1) {
      $scope.filterObj.order_type = "delivery";
      $scope.cart.is_delivery = true;
      $scope.cart.delivery_fee = $scope.restaurant_config.delivery_fee;
    }

    $scope.menuPopup = {
      isSpecialInstruction: false,
      specialInstruction: "",
      image: "",
      total: 0,
      price: 0,
      serving: 1,
      quantity: 1,
      quantity_select: 0,
      quantity_range: [],
      addons: [],
      cart: [],
      addon_keys: [],
      title: "",
      description: ""
    };

    $scope.debugLog = function(field, data) {
      console.log(field, data);
    };

    $scope.debugLog("Config: ", $scope.filterObj);

    $scope.subscription = mkd_events.subscribe("update_date", function(obj) {
      $scope.filterObj.date = obj.date;
      $scope.filterObj.day = obj.day;
      // $scope.filterObj.time = obj.time;
      $scope.debugLog("Update Date: ", obj);
    });

    $scope.hour_key = [];
    $scope.hour_value = [];

    for (i = 0; i < 24; i++) {
      var v = i < 13 ? i + ":00 AM" : (i % 12) + ":00 PM";
      if (i == 12) {
        v = "12:00 PM";
      }
      if (i == 0) {
        v = "12:00 AM";
      }
      var v2 = v.replace("00", "30");
      $scope.hour_key.push(i.toString());
      $scope.hour_key.push((i + 0.5).toString());
      $scope.hour_value.push(v);
      $scope.hour_value.push(v2);
    }

    $scope.am_or_pm = function(n) {
      var minute_format = n % 1 == 0 ? ":00" : ":30";
      var integer_n = Number(Math.floor(n));
      if (integer_n == 24 || integer_n == 0) {
        return "12" + minute_format + " AM";
      }
      if (integer_n == 12) {
        return "12" + minute_format + " PM";
      }
      if (integer_n < 12) {
        return integer_n + minute_format + " AM";
      }

      if (integer_n > 12) {
        return (integer_n % 12) + minute_format + " PM";
      }
    };

    $scope.check_restaurant_open = function() {
      if (
        $scope.filterObj.date.length > 0 &&
        $scope.filterObj.time != undefined
      ) {
        var event_date = new Date($scope.filterObj.date.replace(/\-/g, "/"));
        var event_day = event_date.getDay();
        var schedule_of_day = $scope.schedule[event_day];

        if (schedule_of_day.active != 1) {
          alert(
            "Lo Sentimos, el Restaurante no está abierto para la fecha seleccionada. Por favor considere una fecha diferente u otro restaurnate en la zona!"
          );
          return false;
        } else if (Number(schedule_of_day.open) > $scope.filterObj.time) {
          alert(
            "Lo sentimos, la hora de entrega seleccionada es antes de la hora de apertura del restaurante. Por favor considere una hora de entrega más tarde u otro restaurante en la zona"
          );
          return false;
        } else if ($scope.filterObj.time > Number(schedule_of_day.close)) {
          alert(
            "Lo Sentimos, el restaurante cierra antes de la hora de entrega seleccionada. Por favor considere una hora de entrega más temprano u otro restaurante en la zona!"
          );
          return false;
        }
      } else {
        if ($scope.cart.items.length > 0) {
          alert(
            "Por favor elija una fecha y hora de entrega antes de continuar."
          );
          return false;
        }
      }
      return true;
    };

    $scope.orderTypeChange = function(val) {
      $scope.debugLog("Order Type Change: ", val);
      if ($scope.restaurant_config.delivery_type == 0) {
        $scope.filterObj.order_type = val;
        if ($scope.filterObj.order_type != 1) {
          $scope.cart.is_delivery = false;
        } else {
          $scope.cart.is_delivery = true;
        }
        $scope.cartRecalculate();
      }
    };

    $scope.getReview = function() {
      httpService
        .doGet("reviews/" + $window.c()[1] + "/page/" + $scope.reviewPage)
        .then(function(data) {
          $scope.debugLog("Review Result :", data);
          $scope.reviews = $scope.reviews.concat(
            data.items.map(function(element) {
              element.date_format = new Date(
                element.created_at.replace(/\-/g, "/")
              );
              element.star = "";
              for (let i = 0; i < Math.floor(element.rating); i++) {
                element.star += "★";
              }
              return element;
            })
          );
          $scope.hasMoreReviews = data.has_more;

          if ($scope.hasMoreReviews) {
            $scope.reviewPage++;
          }
        })
        .catch(function(e) {
          $scope.debugLog("Review Error: ", e);
        });
    };

    $scope.scrollToMenu = function(cateringMenu) {
      $scope.debugLog("Catering Menu Selected :", cateringMenu);
      $location.hash(cateringMenu);
      $anchorScroll();
    };

    $scope.range = function(start, count) {
      return Array.apply(0, Array(count)).map(function(element, index) {
        return index + start;
      });
    };
    /**
     * Steps:
     * 1.Open Modal
     * 2.Load in basic info
     * 3.Create Quantity Select box
     * 4.ng repeat the addons
     * 5.Add button for add to cart and bind to event addToCart
     * 6.Add Special instruction Section
     * 7.On change of quantity, change price and price in button
     * 8.Add to cart after done
     * 9.Close Modal
     */
    $scope.openMenu = function(menu) {
      $scope.debugLog("Open Menu", menu);
      $scope.cart_item_index = -1;
      $scope.menuPopup.isSpecialInstruction = false;
      $scope.menuPopup.preference = menu.preference;
      $scope.menuPopup.sold_by = menu.sold_by;
      $scope.menuPopup.image = menu.image;
      $scope.menuPopup.specialInstruction = "";
      $scope.menuPopup.price = Number(menu.price);
      $scope.menuPopup.total = Number(menu.price) * Number(menu.quantity_min);
      $scope.menuPopup.quantity = Number(menu.quantity_min);
      $scope.menuPopup.serving = Number(menu.serving);
      $scope.menuPopup.quantity_range = $scope
        .range(
          Number(menu.quantity_min),
          Number(menu.quantity_max) - Number(menu.quantity_min)
        )
        .map(function(n) {
          return n.toString();
        });
      $scope.menuPopup.id = menu.id;
      $scope.menuPopup.quantity_select = menu.quantity_min;
      angular.copy(menu.addon, $scope.menuPopup.addons);
      $scope.menuPopup.cart = [];

      $scope.menuPopup.addon_keys = Object.keys(menu.addon);
      $scope.menuPopup.title = menu.name;
      $scope.menuPopup.description = menu.description;
      $scope.debugLog("Menu Popup", $scope.menuPopup);
      $("#menuPopup").modal("show");

      $(document).ready(function() {
        $(".tooltips").tooltip();
      });
    };

    $scope.findMenu = function(id) {
      var keys = Object.keys($scope.menu);
      for (var i = 0; i < keys.length; i++) {
        var menu_list = $scope.menu[keys[i]];
        for (var j = 0; j < menu_list.length; j++) {
          if (menu_list[j].id == id) {
            return menu_list[j];
          }
        }
      }
      return null;
    };

    $scope.editMenu = function(index) {
      $scope.cart_item_index = index;
      var menu = $scope.cart.items[$scope.cart_item_index];
      var menu_settings = $scope.findMenu(menu.id);

      $scope.debugLog("Open Edit Menu", menu);
      $scope.debugLog("Open Edit Menu Setting", menu_settings);

      if (!menu_settings) {
        return false;
      }

      $scope.menuPopup.isSpecialInstruction =
        menu.specialInstruction.length > 0;
      $scope.menuPopup.preference = menu_settings.preference;
      $scope.menuPopup.sold_by = menu_settings.sold_by;
      $scope.menuPopup.image = menu_settings.image;
      $scope.menuPopup.specialInstruction = menu.specialInstruction;
      $scope.menuPopup.price = Number(menu_settings.price);
      $scope.menuPopup.total =
        Number(menu_settings.price) * Number(menu_settings.quantity_min);
      $scope.menuPopup.quantity = Number(menu_settings.quantity_min);
      $scope.menuPopup.serving = Number(menu_settings.serving);
      $scope.menuPopup.quantity_range = $scope
        .range(
          Number(menu_settings.quantity_min),
          Number(menu_settings.quantity_max) -
            Number(menu_settings.quantity_min)
        )
        .map(function(n) {
          return n.toString();
        });
      $scope.menuPopup.quantity_select = menu.quantity;
      $scope.menuPopup.id = menu_settings.id;
      angular.copy(menu_settings.addon, $scope.menuPopup.addons);
      $scope.menuPopup.cart = [];

      $scope.menuPopup.addon_keys = Object.keys(menu_settings.addon);
      $scope.menuPopup.title = menu_settings.name;
      $scope.menuPopup.description = menu_settings.description;

      $scope.menuPopupTotalRecalculate();

      for (var i = 0; i < $scope.menuPopup.addon_keys.length; i++) {
        var addon_list = menu_settings.addon[$scope.menuPopup.addon_keys[i]];
        for (var j = 0; j < addon_list.length; j++) {
          for (var k = 0; k < menu.addons.length; k++) {
            if (menu.addons[k].id == addon_list[j].id) {
              $scope.addonSelected(
                $scope.menuPopup.addons[$scope.menuPopup.addon_keys[i]][j]
              );
              $scope.menuPopup.addons[$scope.menuPopup.addon_keys[i]][
                j
              ].checked = true;
            }
          }
        }
      }

      $("#menuPopupEdit").modal("show");

      $(document).ready(function() {
        $(".tooltips").tooltip();
      });
    };

    /**
     * Steps:
     * 1.Set serving
     * 2.Calculate Serving
     * 3.If Calculated Serving < serving return true
     */
    $scope.haveEnoughServing = function() {
      var serving = $scope.filterObj.serving;
      if (!$scope.filterObj.serving) {
        serving = 100;
      }

      var items = $scope.cart.items;
      var length = items.length;
      $scope.total_serving = 0;
      for (var i = 0; i < length; i++) {
        $scope.total_serving += items[i].quantity * items[i].serving;
      }
      $scope.debugLog("Enough Serving", serving);
      $scope.debugLog("Enough Serving Total", $scope.total_serving);
      return $scope.total_serving >= serving;
    };

    $scope.openCartPopup = function(cart) {
      $scope.bypass_cart_popup = false;
      $scope.debugLog("Open Cart Popup", cart);
      $("#cartPopup").modal("show");
    };

    $scope.menuPopupSelected = function() {
      $scope.debugLog("Selected Menu", "selected");
      $scope.menuPopup.total =
        $scope.menuPopup.price * $scope.menuPopup.quantity_select;
      $scope.menuPopupTotalRecalculate();
    };

    $scope.isMostOrdered = function(order) {
      return order == "1";
    };

    $scope.isMostPopular = function(popular) {
      return popular == "1";
    };

    $scope.cartRecalculate = function() {
      $scope.cart["total"] = 0;
      $scope.cart["tax"] = 0;
      $scope.cart["real_tips"] = 0;
      var delivery_fee = $scope.cart.is_delivery
        ? Number($scope.restaurant_config.delivery_fee)
        : 0;
      $scope.cart.delivery_fee = delivery_fee;
      for (var i = 0; i < $scope.cart.items.length; i++) {
        var item = $scope.cart.items[i];
        $scope.cart.total += item.total;
      }

      $scope.cart.tax =
        ($scope.restaurant_config.tax / 100) * $scope.cart.total;

      $scope.cart.real_tips =
        ($scope.cart.tips / 100) *
        ($scope.cart.total + delivery_fee + $scope.cart.tax);
      console.log(
        $scope.cart.is_delivery,
        $scope.cart.total,
        $scope.cart.real_tips,
        $scope.cart.delivery_fee,
        $scope.cart.tax
      );
    };

    $scope.updateTip = function(tip, tipClass) {
      $scope.debugLog("Update Tip", tip);
      $scope.debugLog("Update Tip", tipClass);
      var delivery_fee = $scope.cart.is_delivery
        ? Number($scope.restaurant_config.delivery_fee)
        : 0;
      $scope.cart.tip_option = tipClass;
      $scope.cart.tips = tip;
      $scope.cart.real_tips = tip;
      if (tipClass != "tip-custom") {
        $scope.cart.real_tips =
          ($scope.cart.tips / 100) *
          ($scope.cart.total + delivery_fee + $scope.cart.tax);
      }
    };

    $scope.updateTipCustom = function(tip, tipClass) {
      $scope.debugLog("Update Tip", tip);
      $scope.debugLog("Update Tip", tipClass);
      var amount = prompt("Que porcentaje de Propinas deseas aportar?");

      if (amount != null) {
        $scope.updateTip(Number(amount), tipClass);
      }
    };

    /**
     * Steps:
     * 1.Transfer cart to global cart
     * 2.draw new cart with ng-repeat
     * 3.add click event to remove from cart
     * 4.implement cart recalculate event
     * 5.manage tips ui
     * 6.checkout button confirm event and time is set up
     */
    $scope.addToCart = function(menuPopup) {
      $scope.debugLog("Add To Cart", menuPopup);

      $scope.cart.items.push({
        id: $scope.menuPopup.id,
        addons: $scope.menuPopup.cart,
        title: $scope.menuPopup.title,
        specialInstruction: $scope.menuPopup.specialInstruction,
        price: $scope.menuPopup.price,
        serving: $scope.menuPopup.serving,
        quantity: $scope.menuPopup.quantity_select,
        total: $scope.menuPopup.total
      });

      $scope.cartRecalculate();
      $("#menuPopup").modal("hide");
      $scope.debugLog("New Cart", $scope.cart);
    };

    $scope.editToCart = function(menuPopup) {
      $scope.debugLog("Edit To Cart Popup", menuPopup);
      $scope.cart.items[$scope.cart_item_index].id = $scope.menuPopup.id;
      $scope.cart.items[$scope.cart_item_index].addons = $scope.menuPopup.cart;
      $scope.cart.items[$scope.cart_item_index].title = $scope.menuPopup.title;
      $scope.cart.items[$scope.cart_item_index].specialInstruction =
        $scope.menuPopup.specialInstruction;
      $scope.cart.items[$scope.cart_item_index].price = $scope.menuPopup.price;
      $scope.cart.items[$scope.cart_item_index].serving =
        $scope.menuPopup.serving;
      $scope.cart.items[$scope.cart_item_index].quantity =
        $scope.menuPopup.quantity_select;
      $scope.cart.items[$scope.cart_item_index].total = $scope.menuPopup.total;

      $scope.debugLog(
        "Edited Cartitem",
        $scope.cart.items[$scope.cart_item_index]
      );
      $scope.cartRecalculate();
      $("#menuPopupEdit").modal("hide");
      $scope.debugLog("Edited Cart", $scope.cart);
    };

    $scope.removeCartItem = function(index, item) {
      $scope.debugLog("Remove Cart Item", index);
      $scope.debugLog("Remove Cart Item", item);
      $scope.cart.items.splice(index, 1);
      $scope.cartRecalculate();
    };

    $scope.menuPopupTotalRecalculate = function() {
      var total = $scope.menuPopup.price * $scope.menuPopup.quantity_select;

      $scope.debugLog("Old Recalculate Cart Total", total);

      for (var i = 0; i < $scope.menuPopup.cart.length; i++) {
        var element = $scope.menuPopup.cart[i];
        $scope.debugLog("Cart Item", element);
        if (element.price) {
          total += Number(element.price) * $scope.menuPopup.quantity_select;
        }
      }

      $scope.debugLog("Recalculate Cart Total", total);

      $scope.menuPopup.total = total;
    };

    $scope.addonSelected = function(addon) {
      addon.checked = !addon.checked;
      $scope.debugLog("Addon Selected", addon);
      var exist = false;
      for (var i = 0; i < $scope.menuPopup.cart.length; i++) {
        var element = $scope.menuPopup.cart[i];
        if (element.id == addon.id) {
          exist = i;
        }
      }

      if (exist !== false) {
        $scope.debugLog("Addon Exist in Cart", $scope.menuPopup.cart);
        $scope.menuPopup.cart.splice(exist, 1);
      } else {
        $scope.menuPopup.cart.push(addon);
        $scope.debugLog("Addon DNE in Cart", $scope.menuPopup.cart);
      }

      $scope.menuPopupTotalRecalculate();
    };

    $scope.checkout = function() {
      $scope.debugLog("Checkout: ", $scope.cart);
      var cleanCart = $scope.cart;
      cleanCart.is_delivery = cleanCart.is_delivery ? 1 : 0;
      if ($scope.cart.items.length > 0) {
        if (!$scope.check_restaurant_open()) {
          return false;
        }
        $scope.debugLog("enough serving", $scope.haveEnoughServing());
        $scope.debugLog("enough serving bypass", $scope.bypass_cart_popup);
        if (!$scope.haveEnoughServing() && !$scope.bypass_cart_popup) {
          $scope.openCartPopup($scope.cart);
          return false;
        }

        if (
          ($scope.cart.total >= $scope.restaurant_config.food_minimum &&
            $scope.cart.is_delivery == 1) ||
          $scope.cart.is_delivery == 0
        ) {
          $scope.cart.date = $scope.filterObj.date;
          $scope.cart.time = $scope.filterObj.time;

          httpService
            .doPost("checkout", $scope.cart)
            .then(function(data) {
              $scope.debugLog("Checkout Saved :", data);
              window.location.href = "/checkout/1/" + data.id;
            })
            .catch(function(e) {
              $scope.debugLog("Checkout Error: ", e);
            });
        } else {
          alert(
            "por favor compre al menos DOP" +
              $scope.restaurant_config.food_minimum +
              " de comida para entrega"
          );
        }
      }
    };

    $scope.continueCheckout = function() {
      $scope.bypass_cart_popup = true;
      $scope.checkout();
    };

    $scope.closeCartPopup = function() {
      $("#cartPopup").modal("hide");
    };

    $scope.check_restaurant_open();
    $scope.getReview();
  }
]);

app.service("http.service", [
  "$http",
  "$q",
  function($http, $q) {
    this._baseUrl = "/v1/api/";
    this._q = $q;
    this._http = $http;

    this.getDeferred = function() {
      return this._q.defer();
    };

    this.doGet = function(url, cache) {
      var deferred = this.getDeferred(),
        promise;

      if (cache) {
        promise = this._http.get(this._baseUrl + url, {
          cache: true
        });
      } else {
        promise = this._http.get(this._baseUrl + url);
      }

      promise.then(
        function(result) {
          if (result.data) {
            result = result.data;
          }
          deferred.resolve(result);
        },
        function(err) {
          deferred.reject(err);
        }
      );

      return deferred.promise;
    };

    this.doPost = function(url, data) {
      var deferred = this.getDeferred();
      this._http.post(this._baseUrl + url, data).then(
        function(result) {
          if (result.data) {
            result = result.data;
          }
          deferred.resolve(result);
        },
        function(err) {
          deferred.reject(err);
        }
      );
      return deferred.promise;
    };
    return this;
  }
]);

app.filter("timeago", function() {
  return function(input, p_allowFuture) {
    var substitute = function(stringOrFunction, number, strings) {
        var string = angular.isFunction(stringOrFunction)
          ? stringOrFunction(number, dateDifference)
          : stringOrFunction;
        var value = (strings.numbers && strings.numbers[number]) || number;
        return string.replace(/%d/i, value);
      },
      nowTime = new Date().getTime(),
      date = new Date(input).getTime(),
      //refreshMillis= 6e4, //A minute
      allowFuture = p_allowFuture || false,
      strings = {
        prefixAgo: "Hace",
        prefixFromNow: "Hace",
        suffixAgo: "atrás",
        suffixFromNow: "desde ahora",
        seconds: "menos de un minuto",
        minute: "como un minuto",
        minutes: "%d minutos",
        hour: "como una hora",
        hours: "como %d horas",
        day: "un día",
        days: "%d días",
        month: "como un mes",
        months: "%d meses",
        year: "como un año",
        years: "%d años"
      },
      // strings = {
      //     prefixAgo: "",
      //     prefixFromNow: "",
      //     suffixAgo: "ago",
      //     suffixFromNow: "from now",
      //     seconds: "less than a minute",
      //     minute: "about a minute",
      //     minutes: "%d minutes",
      //     hour: "about an hour",
      //     hours: "about %d hours",
      //     day: "a day",
      //     days: "%d days",
      //     month: "about a month",
      //     months: "%d months",
      //     year: "about a year",
      //     years: "%d years"
      // },
      dateDifference = nowTime - date,
      words,
      seconds = Math.abs(dateDifference) / 1000,
      minutes = seconds / 60,
      hours = minutes / 60,
      days = hours / 24,
      years = days / 365,
      separator =
        strings.wordSeparator === undefined ? " " : strings.wordSeparator,
      prefix = strings.prefixAgo,
      suffix = strings.suffixAgo;

    if (allowFuture) {
      if (dateDifference < 0) {
        prefix = strings.prefixFromNow;
        suffix = strings.suffixFromNow;
      }
    }

    words =
      (seconds < 45 &&
        substitute(strings.seconds, Math.round(seconds), strings)) ||
      (seconds < 90 && substitute(strings.minute, 1, strings)) ||
      (minutes < 45 &&
        substitute(strings.minutes, Math.round(minutes), strings)) ||
      (minutes < 90 && substitute(strings.hour, 1, strings)) ||
      (hours < 24 && substitute(strings.hours, Math.round(hours), strings)) ||
      (hours < 42 && substitute(strings.day, 1, strings)) ||
      (days < 30 && substitute(strings.days, Math.round(days), strings)) ||
      (days < 45 && substitute(strings.month, 1, strings)) ||
      (days < 365 &&
        substitute(strings.months, Math.round(days / 30), strings)) ||
      (years < 1.5 && substitute(strings.year, 1, strings)) ||
      substitute(strings.years, Math.round(years), strings);
    // console.log(prefix + words + suffix + separator);
    prefix.replace(/ /g, "");
    words.replace(/ /g, "");
    suffix.replace(/ /g, "");
    return prefix + " " + words + " " + suffix + " " + separator;
  };
});
app.run(function($rootScope) {});

angular.element(function() {
  angular.bootstrap(document, ["app"]);
});
