<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';
class Payment extends Catering_Controller
{
    protected $model_file = 'payments';
    public $_page_name = 'payment';


	public function index($page)
	{
		/**
		 * Steps:
		 * 1.Day of week is 0(sun) - 6(sat)
		 * 2.Paginate get all payouts
		 * 3.Get Last Payout and fill in that payout information.
		 */
		$this->_data['base_url'] = '/catering/payments/';
        $this->_data['total_rows'] = $this->payments->num_restaurant_payments($_SESSION['user_id']);
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$this->_data['list'] = $this->payments->get_paginated_payments($page, $this->_data['per_page'], [
			'restaurant_id' => $_SESSION['user_id']
		]);
		$this->_data['links'] = $this->pagination->create_links();
		$this->_get_last_payout();
		$this->render('Catering/Payment', $this->_data);
	}

	/**
	 * Steps:
	 * 1.Get id of payment
	 * 2.Check if payment exist
	 * 3.Look for all orders between this period
	 * 4.Show user these orders.
	 * 5.Also show user the notes left.
	 */
	public function view($id)
	{
		$payment = $this->payments->get_payment($id);

		if (!$payment)
		{
			$this->error('Error');
			return redirect('/catering/payments/0');
		}

		if ($payment->restaurant_id != $_SESSION['user_id'])
		{
			$this->error('Error');
			return redirect('/catering/payments/0');
		}

		$this->load->model('orders');

		$this->_data['model'] = $payment;
		$this->_data['payout_day'] = $this->_data['model']->payment_date;
		$this->_data['previous_payout_day'] = $this->get_previous_payout_date($this->_data['payout_day']);
		$this->_data['mapping'] = $this->orders->mapping();
        $this->_data['delivery_mapping'] = $this->orders->delivery_type_mapping();
		$this->_data['list'] = $this->orders->get_orders_by_date_range($this->_data['payout_day'],
		$this->_data['previous_payout_day'],
		$_SESSION['user_id']);
		$this->render('Catering/PaymentView', $this->_data);
	}

	/**
	 * get previous payout date
	 *
	 * @param date $payout_day
	 * @return date
	 */
    protected function get_previous_payout_date($payout_day)
    {
		$date = new DateTime();
		$parts = explode('-', $payout_day);
		$date->setDate($parts[0], $parts[1], $parts[2]);
		$date = $date->modify('- 7 days');
		return $date->format('Y-m-d');
    }

	/**
	 * Steps:
	 *
	 * 1. Get last payment for user
	 * 2. If exist, take date and amount
	 * 3. Set it to variable
	 * 4. If money is negative its negative
	 *
	 * @return void
	 */
	protected function _get_last_payout ()
	{
		$this->_data['payout_period'] = 'N/A';
		$this->_data['payout_amount'] = 'N/A';

		$payment = $this->payments->get_last_payment_by_restaurant($_SESSION['user_id']);

		if ($payment)
		{
			$this->_data['payout_period'] = $payment->payment_date;
			$this->_data['payout_amount'] = $payment->total - $payment->commission + $payment->adjustment;
		}
	}
}