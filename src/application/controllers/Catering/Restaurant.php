<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';
class Restaurant extends Catering_Controller
{
    protected $model_file = 'restaurants';
	public $_page_name = 'restaurant';

	public function index()
	{
		$this->load->model('cuisines');
		$this->load->model('settings');

		$id = $_SESSION['user_id'];
		$restaurants = $this->restaurants->get_restaurant($id);
		$this->_data['model'] = $restaurants;
		$this->_data['cuisines'] = $this->cuisines->get_cuisines(TRUE);
		$this->_data['setting'] = $this->settings->get_config_settings();
		$this->_data['enable_google_api'] = TRUE;
		$this->_data['google_key'] = $this->config->item('google_key');
		$this->_data['google_api_callback'] = 'callbackAddress';
		$this->_data['type'] = $this->restaurants->delivery_type_mapping();

		$this->form_validation->set_rules('title', 'Nombre', 'required');
		$this->form_validation->set_rules('description', 'Descripción', 'required');
		$this->form_validation->set_rules('about', 'Detalles', 'required');
		$this->form_validation->set_rules('delivery_fee', 'Delivery', 'required|numeric');
		$this->form_validation->set_rules('type', 'Tipo de Restaurante', 'required|numeric');
		$this->form_validation->set_rules('food_minimum', 'Consumo Minimo', 'required|integer');
		$this->form_validation->set_rules('cuisines_id', 'Gastronomia ', 'required');
		$this->form_validation->set_rules('price_range', 'Rango de Precios', 'required');
		$this->form_validation->set_rules('address', 'Dirección', 'required');
		$this->form_validation->set_rules('city', 'Ciudad', '');
		$this->form_validation->set_rules('state', 'Provincia', '');
		$this->form_validation->set_rules('zip', 'Codigo Postal', '');
		$this->form_validation->set_rules('lat', 'Latitude', 'required|numeric');
		$this->form_validation->set_rules('long', 'Longitude', 'required|numeric');
		$this->form_validation->set_rules('rnc', 'RNC #', 'required');
		$this->form_validation->set_rules('notes', 'notas #', '');
		$this->form_validation->set_rules('phone', 'Telefono', 'required');
		$this->form_validation->set_rules('serving', 'No. Personas Max/Orden', 'required');
		$this->form_validation->set_rules('started_at', 'Abierto desde', 'required|date');
		$this->form_validation->set_rules('u', 'Sunday', 'integer');
		$this->form_validation->set_rules('m', 'Monday', 'integer');
		$this->form_validation->set_rules('t', 'Tuesday', 'integer');
		$this->form_validation->set_rules('w', 'Wednesday', 'integer');
		$this->form_validation->set_rules('r', 'Thursday', 'integer');
		$this->form_validation->set_rules('f', 'Friday', 'integer');
		$this->form_validation->set_rules('s', 'Saturday', 'integer');
		$this->form_validation->set_rules('u_open', 'Domingo Horas Abierto', 'numeric');
		$this->form_validation->set_rules('m_open', 'Lunes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('t_open', 'Martes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('w_open', 'Miercoles Horas Abierto', 'numeric');
		$this->form_validation->set_rules('r_open', 'Jueves Horas Abierto', 'numeric');
		$this->form_validation->set_rules('f_open', 'Viernes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('s_open', 'Sabado Horas Abierto', 'numeric');
		$this->form_validation->set_rules('u_close', 'Domingo Horas Cerrado', 'numeric|greater_than[' . $this->input->post('u_open') . ']');
		$this->form_validation->set_rules('m_close', 'Lunes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('m_open') . ']');
		$this->form_validation->set_rules('t_close', 'Martes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('t_open') . ']');
		$this->form_validation->set_rules('w_close', 'Miercoles Horas Cerrado', 'numeric|greater_than[' . $this->input->post('w_open') . ']');
		$this->form_validation->set_rules('r_close', 'Jueves Horas Cerrado', 'numeric|greater_than[' . $this->input->post('r_open') . ']');
		$this->form_validation->set_rules('f_close', 'Viernes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('f_open') . ']');
		$this->form_validation->set_rules('s_close', 'Sabado Horas Cerrado', 'numeric|greater_than[' . $this->input->post('s_open') . ']');

		if ($this->form_validation->run() === FALSE)
        {
			$this->render('Catering/Restaurant', $this->_data);
		}
        else
        {
    		$title = $this->input->post('title', TRUE);
			$description = $this->input->post('description', TRUE);
			$about = $this->input->post('about', TRUE);
			$type = $this->input->post('type', TRUE);
			$delivery_fee = $this->input->post('delivery_fee', TRUE);
			$food_minimum = $this->input->post('food_minimum', TRUE);
			$cuisines_id = $this->input->post('cuisines_id', TRUE);
			$price_range = $this->input->post('price_range', TRUE);
			$address = $this->input->post('address', TRUE);
			$city = $this->input->post('city', TRUE);
			$state = $this->input->post('state', TRUE);
			$zip = $this->input->post('zip', TRUE);
			$lat = $this->input->post('lat', TRUE);
			$long = $this->input->post('long', TRUE);
			$rnc = $this->input->post('rnc', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$serving = $this->input->post('serving', TRUE);
			$started_at = $this->input->post('started_at', TRUE);

			if ($this->restaurants->edit_restaurant([
				'title' => $title,
				'description' => $description,
				'about' => $about,
				'delivery_fee' => $delivery_fee,
				'type' => $type,
				'food_minimum' => $food_minimum,
				'cuisines_id' => $cuisines_id,
				'price_range' => $price_range,
				'address' => $address,
				'city' => $city,
				'state' => $state,
				'zip' => $zip,
				'lat' => $lat,
				'long' => $long,
				'rnc' => $rnc,
				'phone' => $phone,
				'serving' => $serving,
				'started_at' => $started_at,
				'u' => $this->input->post('u', TRUE),
				'u_open' => (int)$this->input->post('u_open', TRUE),
				'u_close' => (int)$this->input->post('u_close', TRUE),
				'm' => $this->input->post('m', TRUE),
				'm_open' => (int)$this->input->post('m_open', TRUE),
				'm_close' => (int)$this->input->post('m_close', TRUE),
				't' => $this->input->post('t', TRUE),
				't_open' => (int)$this->input->post('t_open', TRUE),
				't_close' => (int)$this->input->post('t_close', TRUE),
				'w' => $this->input->post('w', TRUE),
				'w_open' => (int)$this->input->post('w_open', TRUE),
				'w_close' => (int)$this->input->post('w_close', TRUE),
				'r' => $this->input->post('r', TRUE),
				'r_open' => (int)$this->input->post('r_open', TRUE),
				'r_close' => (int)$this->input->post('r_close', TRUE),
				'f' => $this->input->post('f', TRUE),
				'f_open' => (int)$this->input->post('f_open', TRUE),
				'f_close' => (int)$this->input->post('f_close', TRUE),
				's' => $this->input->post('s', TRUE),
				's_open' => (int)$this->input->post('s_open', TRUE),
				's_close' => (int)$this->input->post('s_close', TRUE),
				'u_open_min' => ($this->is_decimal($this->input->post('u_open', TRUE))? 30 : 0),
				'u_close_min' => ($this->is_decimal($this->input->post('u_close', TRUE))? 30 : 0),
				'm_open_min' => ($this->is_decimal($this->input->post('m_open', TRUE))? 30 : 0),
				'm_close_min' => ($this->is_decimal($this->input->post('m_close', TRUE))? 30 : 0),
				't_open_min' => ($this->is_decimal($this->input->post('t_open', TRUE))? 30 : 0),
				't_close_min' => ($this->is_decimal($this->input->post('t_close', TRUE))? 30 : 0),
				'w_open_min' => ($this->is_decimal($this->input->post('w_open', TRUE))? 30 : 0),
				'w_close_min' => ($this->is_decimal($this->input->post('w_close', TRUE))? 30 : 0),
				'r_open_min' => ($this->is_decimal($this->input->post('r_open', TRUE))? 30 : 0),
				'r_close_min' => ($this->is_decimal($this->input->post('r_close', TRUE))? 30 : 0),
				'f_open_min' => ($this->is_decimal($this->input->post('f_open', TRUE))? 30 : 0),
				'f_close_min' => ($this->is_decimal($this->input->post('f_close', TRUE))? 30 : 0),
				's_open_min' => ($this->is_decimal($this->input->post('s_open', TRUE))? 30 : 0),
				's_close_min' => ($this->is_decimal($this->input->post('s_close', TRUE)? 30 : 0))
            ], $id))
            {
				redirect('/catering/restaurant', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar restaurante.';
				$this->render('Catering/Restaurant', $this->_data);
			}
		}
	}

	private function is_decimal( $val )
	{
		return is_numeric( $val ) && floor( $val ) != $val;
	}
}