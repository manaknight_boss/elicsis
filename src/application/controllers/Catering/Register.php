<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends Manaknight_Controller
{
	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		// create the data object
		$data = [];
		$this->load->model('users');
		$this->load->model('emails');
		$this->load->library('mail_service');
		$this->load->model('analytics');

		// set validation rules
		$this->form_validation->set_rules('email', 'Corréo', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', 'Confrimar Contraseña', 'trim|required|min_length[6]|matches[password]');

		if ($this->form_validation->run() === false)
		{
			$this->load->view('Member/Register', $data);
		}
		else
		{
			$email    = $this->input->post('email');
			$password = $this->input->post('password');

			if ($this->users->create_user($email, $password, 1, 'Guest'))
			{
				$this->mail_service->set_adapter('mailgun');
				$this->mail_service->setDomain('manaknightdigital.com');
				$email_template = $this->email->get_template('register', [
					'email' => $email
				]);

				if ($email_template)
				{
					$this->mail_service->send('admin@manaknightdigital.com', $email, $email_template->subject, $email_template->html);
				}
				else
				{
					$this->notify_error('Corréo no se esta enviando durante el registro');
				}

				$user_id = $this->users->get_user_id_from_email($email);
				$user    = $this->users->get_user($user_id);

				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['role']     = $user->role_id;

				$affilate_id = (isset($_SESSION['affilate_id'])) ? $_SESSION['affilate_id'] : 0;

				redirect('/member/dashboard', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$data['error'] = 'Se produjo un error al crear su cuenta, por favor intentelo de nuevo.';
				$this->load->view('Member/Register', $data);
			}
		}
	}

}