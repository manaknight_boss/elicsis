<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';
class Reviews extends Catering_Controller
{
    protected $model_file = 'review';
    public $_page_name = 'review';

    public function index($page)
	{
        $this->load->library('pagination');
		$this->_data['base_url'] = '/catering/review/';
        $this->_data['total_rows'] = $this->review->num_reviews($_SESSION['user_id']);
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->process_data($this->review->get_paginated_reviews($_SESSION['user_id'], $page, $this->_data['per_page']));
		$this->_data['links'] = $this->pagination->create_links();
		$this->render('Catering/Review', $this->_data);
	}

	public function edit($id)
	{
		$review = $this->review->get_review($id);
		$this->_data['model'] = $review;
		if (!$review)
		{
			$this->error('La calificación no existe');
			redirect('/catering/reviews/0');
		}
		$this->form_validation->set_rules('user_id', 'ID Usuario', 'required|integer');
		$this->form_validation->set_rules('order_id', 'ID Pedido', 'required|integer');
		$this->form_validation->set_rules('restaurant_id', 'ID Restaurante', 'required|integer');
		$this->form_validation->set_rules('rating', 'Calificación', 'required|integer');
		$this->form_validation->set_rules('message', 'Mensaje', 'required');
		$this->form_validation->set_rules('created_at', 'Fecha de creación', 'required|date');

		if ($this->form_validation->run() === false)
        {
			$this->render('Catering/ReviewEdit', $this->_data);
		}
        else
        {
    		$user_id = $this->input->post('user_id', TRUE);
			$order_id = $this->input->post('order_id', TRUE);
			$restaurant_id = $this->input->post('restaurant_id', TRUE);
			$rating = $this->input->post('rating', TRUE);
			$message = $this->input->post('message', TRUE);
			$created_at = $this->input->post('created_at', TRUE);

			if ($this->review->edit_review([
				'user_id' => $user_id,
				'order_id' => $order_id,
				'restaurant_id' => $restaurant_id,
				'rating' => $rating,
				'message' => $message,
				'created_at' => $created_at,

            ], $id))
            {
				redirect('/catering/reviews/0', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar calificacion.';
				$this->render('Catering/ReviewEdit', $this->_data);
			}
		}
	}

	public function view($id)
	{
		$review = $this->review->get_review($id);
		$this->_data['model'] = $review;
		if (!$review)
		{
			$this->error('La calificaión no existe');
			redirect('/catering/reviews/0');
		}

		$this->render('Catering/ReviewView', $this->_data);
	}

	public function process_data ($data)
	{
		$this->load->model('users');
		$this->load->model('orders');
		foreach ($data as $key => $review)
		{
			$user = $this->users->get_user($review->user_id);
			$order = $this->orders->get_order($review->order_id);
			$data[$key]->user = $user;
			$data[$key]->order = $order;
		}
		return $data;
	}
}