<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';

class Reports extends Catering_Controller
{
    protected $model_file = 'orders';
    public $_page_name = 'report';


	public function order_report()
	{
		$this->load->library('report_service');
		$this->_data['mapping'] = $this->orders->mapping();
		$this->form_validation->set_rules('start_date', 'fecha de inicio', 'required|date');
		$this->form_validation->set_rules('end_date', 'Fecha Final', 'required|date');
		$this->form_validation->set_rules('status', 'Estado', 'required|integer');

		if ($this->form_validation->run() === false)
		{
			$this->render('Catering/Report', $this->_data);
		}
		else
		{
			$start_date = $this->input->post('start_date', TRUE);
			$end_date = $this->input->post('end_date', TRUE);
			$status = $this->input->post('status', TRUE);
			$orders = $this->orders->generate_order_report($start_date, $end_date, $_SESSION['user_id'], $status);

			$this->report_service->generate_csv([
				'Pedido #',
				'Fecha',
				'Horas',
				'Minutos',
				'Método de pago',
				'Monto',
				'Delivery',
				'Descuento',
				'Ajustes',
				'ITBIS',
				'Total',
				'Estado',
			], $orders, 'informe_' . $start_date . '_' . $end_date . '.csv');
		}
	}

	public function item_order_report()
	{
		$this->load->library('report_service');
		$this->_data['mapping'] = $this->orders->mapping();
		$this->form_validation->set_rules('start_date', 'fecha de inicio', 'required|date');
		$this->form_validation->set_rules('end_date', 'Fecha de Fin', 'required|date');
		$this->form_validation->set_rules('status', 'Estado', 'required|integer');

		if ($this->form_validation->run() === false)
		{
			$this->render('Catering/ItemReport', $this->_data);
		}
		else
		{
			$start_date = $this->input->post('start_date', TRUE);
			$end_date = $this->input->post('end_date', TRUE);
			$status = $this->input->post('status', TRUE);
			$orders = $this->orders->generate_item_order_report($start_date, $end_date, $_SESSION['user_id'], $status);

			$this->report_service->generate_csv([
				'Pedido #',
				'Fecha',
				'Producto',
				'Complemento',
				'Porciones',
				'Precio',
				'Estado',
			], $orders, 'informe_' . $start_date . '_' . $end_date . '.csv');
		}
	}
}