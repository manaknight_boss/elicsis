<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';

class Invoice extends Member_Controller 
{
    protected $model_file = 'invoices';
    public $_user_id;

    public function __construct() 
    {
        parent::__construct();
        $this->_user_id = $_SESSION['user_id'];
        $this->load->helper('date');
    }

    public function view($invoice_id) 
    {
        $invoice_data = $this->invoices->get_invoice_by_id($invoice_id);

        if (count($invoice_data) < 1) 
		{
			$this->error('Factura no existe');
            redirect('/member/billing');
            exit;
        }
        
        $this->_data['model'] = $invoice_data[0];
        $this->render('Member/InvoiceView', $this->_data);
    }

}
