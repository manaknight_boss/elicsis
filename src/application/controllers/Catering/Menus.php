<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';
class Menus extends Catering_Controller
{
	protected $model_file = 'menu';
	public $_page_name = 'menu';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('catering_menu');
		$this->load->model('preferences');
	}

	public function index()
	{
		$this->load->helper('date');
		$this->_data['list'] = $this->menu->get_menus_by_restaurant($_SESSION['user_id']);
		$this->_data['catering_menu'] = $this->catering_menu->get_catering_menus(TRUE);
		$this->_data['preferences'] = $this->preferences->get_preferences(TRUE);
		$this->_data['status'] = $this->menu->mapping();
		$this->render('Catering/Menu', $this->_data);
	}

	public function add()
	{
		$this->load->model('restaurants');
		$this->_data['catering_menu'] = $this->catering_menu->get_catering_menus(TRUE);
		$this->_data['preferences'] = $this->preferences->get_preferences(TRUE);
		$this->_data['status'] = $this->menu->mapping();
		$this->_data['sold_by_mapping'] = $this->menu->sold_by_mapping();
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('catering_menu_id', 'Menú', 'required|integer');
		$this->form_validation->set_rules('most_ordered', 'Más Pedido', 'integer');
		$this->form_validation->set_rules('popular', 'Popular', 'integer');
		$this->form_validation->set_rules('quantity_min', 'Consumo Minimo', 'required|integer');
		$this->form_validation->set_rules('quantity_max', 'Consumo Maximo', 'required|integer|greater_than['.$this->input->post('quantity_min').']');
		$this->form_validation->set_rules('serving', 'Porción', 'required|integer');
		$this->form_validation->set_rules('price', 'Precio', 'required|numeric');
		$this->form_validation->set_rules('order', 'Pedido', 'required|integer');
		$this->form_validation->set_rules('sold_by', 'Pedido', 'required|integer');

		if ($this->form_validation->run() === false)
		{
			$this->render('Catering/MenuAdd', $this->_data);
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$description = $this->input->post('description', TRUE);
			$image = '';
			$restaurant_id = $_SESSION['user_id'];
			$catering_menu_id = $this->input->post('catering_menu_id', TRUE);
			$most_ordered = ($this->input->post('most_ordered', TRUE) == 1) ? 1 : 0;
			$popular = ($this->input->post('popular', TRUE) == 1) ? 1 : 0;
			$quantity_min = $this->input->post('quantity_min', TRUE);
			$quantity_max = $this->input->post('quantity_max', TRUE);
			$serving = $this->input->post('serving', TRUE);
			$price = $this->input->post('price', TRUE);
			$order = $this->input->post('order', TRUE);
			$sold_by = $this->input->post('sold_by', TRUE);
			$preference = $this->input->post('preference', TRUE);
			$data = '[]';
			$status = 1;

			if ($this->menu->create_menu([
				'name' => $name,
				'description' => $description,
				'image' => $image,
				'restaurant_id' => $restaurant_id,
				'catering_menu_id' => $catering_menu_id,
				'most_ordered' => $most_ordered,
				'popular' => $popular,
				'quantity_min' => $quantity_min,
				'quantity_max' => $quantity_max,
				'serving' => $serving,
				'sold_by' => $sold_by,
				'price' => $price,
				'order' => $order,
				'preference' => json_encode(array_map(function($element){
					return (int)$element;
				}, $preference ? $preference : [])),
				'data' => $data,
				'status' => $status
			]))
			{
				redirect('/catering/menu', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo añadir Menu.';
				$this->render('Catering/MenuAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$this->load->model('restaurants');
		$menu = $this->menu->get_menu($id);
		$this->_data['model'] = $menu;
		if (!$menu)
		{
			$this->error('Menú no existe');
			redirect('/catering/menu');
		}

		$this->_data['catering_menu'] = $this->catering_menu->get_catering_menus(TRUE);
		$this->_data['preferences'] = $this->preferences->get_preferences(TRUE);
		$this->_data['status'] = $this->menu->mapping();
		$this->_data['sold_by_mapping'] = $this->menu->sold_by_mapping();
		$this->_data['processed_preference'] = json_decode($this->_data['model']->preference, TRUE);
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('catering_menu_id', ' Menu', 'required|integer');
		$this->form_validation->set_rules('most_ordered', 'Mas Pedido', 'integer');
		$this->form_validation->set_rules('popular', 'Popular', 'integer');
		$this->form_validation->set_rules('quantity_min', 'Consumo Minimo', 'required|integer');
		$this->form_validation->set_rules('quantity_max', 'Consumo Maximo', 'required|integer|greater_than['.$this->input->post('quantity_min').']');
		$this->form_validation->set_rules('serving', 'Porcion', 'required|integer');
		$this->form_validation->set_rules('price', 'Precio', 'required|numeric');
		$this->form_validation->set_rules('order', 'Pedido', 'required|integer');
		$this->form_validation->set_rules('sold_by', 'Pedido', 'required|integer');
		$this->form_validation->set_rules('preference', 'Preferencia', '');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[0,1]');

		if ($this->form_validation->run() === false)
    {
			$this->render('Catering/MenuEdit', $this->_data);
		}
		else
		{
		  $name = $this->input->post('name', TRUE);
			$description = $this->input->post('description', TRUE);
			$catering_menu_id = $this->input->post('catering_menu_id', TRUE);
			$most_ordered = $this->input->post('most_ordered', TRUE);
			$popular = $this->input->post('popular', TRUE);
			$quantity_min = $this->input->post('quantity_min', TRUE);
			$quantity_max = $this->input->post('quantity_max', TRUE);
			$serving = $this->input->post('serving', TRUE);
			$sold_by = $this->input->post('sold_by', TRUE);
			$price = $this->input->post('price', TRUE);
			$order = $this->input->post('order', TRUE);
			$preference = $this->input->post('preference', TRUE);
			$status = $this->input->post('status', TRUE);

			if ($preference == NULL)
			{
				$preference = [];
			}

			if ($this->menu->edit_menu([
				'name' => $name,
				'description' => $description,
				'catering_menu_id' => $catering_menu_id,
				'most_ordered' => $most_ordered,
				'popular' => $popular,
				'quantity_min' => $quantity_min,
				'quantity_max' => $quantity_max,
				'serving' => $serving,
				'sold_by' => $sold_by,
				'price' => $price,
				'order' => $order,
				'preference' => json_encode(array_map(function($element){
					return (int)$element;
				}, $preference)),
				'status' => $status
            ], $id))
			{
				redirect('/catering/menu', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar el menú.';
				$this->render('Catering/MenuEdit', $this->_data);
			}
		}
	}

	public function remove($id)
	{
		$menu = $this->menu->get_menu($id);
		$this->_data['model'] = $menu;
		if (!$menu)
		{
			$this->error('Menu no existe');
			redirect('/catering/menu');
		}

		if ($this->menu->edit_menu([
			'status' => 0
		], $id))
		{
			redirect('/catering/menu', 'refresh');
		} else {
			$this->error('No se pudo actualizar el menú');
			redirect('/catering/menu');
		}
	}
}