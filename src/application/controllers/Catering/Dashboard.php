<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';
class Dashboard extends Catering_Controller
{
	public $_page_name = 'dashboard';

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{

		$this->load->model('orders');
		$this->load->model('users');
		$this->load->model('restaurants');
		$this->load->model('customer_restaurant');

		$this->_data['mapping'] = $this->orders->mapping();
		$this->_data['delivery_mapping'] = $this->orders->delivery_type_mapping();
		$this->_data['per_page'] = 100;
		$this->_data['restaurant_id'] = $_SESSION['user_id'];
		$this->_data['list'] = $this->orders->get_new_event_restaurant_orders($_SESSION['user_id']);
		$this->_data['restaurant'] = $this->restaurants->get_restaurant($_SESSION['user_id']);
		$this->_data['total_review'] = $this->_data['restaurant']->num_review;
		$this->_data['avg_review'] = $this->_data['restaurant']->rating;
		$this->_data['repeat_customer'] = $this->customer_restaurant->num_repeat_customer($_SESSION['user_id']);
		$this->_data['total_sales'] = $this->orders->total_restaurant_orders_delivered($_SESSION['user_id']);
		$this->_data['total_orders'] = $this->orders->num_restaurant_orders_delivered($_SESSION['user_id']);
		$this->_data['avg_order_value'] = number_format((float)$this->_data['total_sales'] / (float)$this->_data['total_orders'], 2);
		$this->_data['on_time_delivery_rating'] = $this->_data['restaurant']->on_delivery_rating;
		$this->_data['on_time_accept_rating'] = $this->_data['restaurant']->on_time_rating;
		$this->_data['on_time_delivery'] = [
			'100' => $this->orders->calculate_on_delivery_rating_restaurant_last_30_days_by_score($_SESSION['user_id'], 100),
			'75' => $this->orders->calculate_on_delivery_rating_restaurant_last_30_days_by_score($_SESSION['user_id'], 75),
			'30' => $this->orders->calculate_on_delivery_rating_restaurant_last_30_days_by_score($_SESSION['user_id'], 30),
			'0' => $this->orders->calculate_on_delivery_rating_restaurant_last_30_days_by_score($_SESSION['user_id'], 0)
		];
		$this->_data['on_time_accept'] = [
			'100' => $this->orders->calculate_on_time_rating_restaurant_last_30_days_by_score($_SESSION['user_id'], 100),
			'75' => $this->orders->calculate_on_time_rating_restaurant_last_30_days_by_score($_SESSION['user_id'], 75),
			'30' => $this->orders->calculate_on_time_rating_restaurant_last_30_days_by_score($_SESSION['user_id'], 30),
			'0' => $this->orders->calculate_on_time_rating_restaurant_last_30_days_by_score($_SESSION['user_id'], 0)
		];
		$this->render('Catering/Dashboard', $this->_data);
	}

}