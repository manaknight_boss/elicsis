<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends Manaknight_Controller {

	protected $_redirect = '/catering/dashboard';

	public function __construct()
    {
        parent::__construct();
    }

	public function index ()
	{
		$this->_login();
	}

	protected function _login ()
	{
		$this->load->model('restaurants');
		$this->form_validation->set_rules('email', 'Corréo', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Contraseña', 'required');

		if ($this->form_validation->run() == false)
		{
			$this->_render_strategy('Catering/Login', $this->_data);
		}
		else
		{
			// set variables from the form
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			if ($this->restaurants->authenticate($email, $password))
			{
				$user_id = $this->restaurants->get_restaurant_id_from_email($email);
				$user    = $this->restaurants->get_restaurant($user_id);

				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['restaurant_name'] = $user->title;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['role']     = 1;
				redirect($this->_redirect);
			}
			else
			{
				$this->_data['error'] = 'Corréo o Contraseña invalidos.';
				$this->_render_strategy('Catering/Login', $this->_data);
			}
		}
	}

	public function forgot ()
	{
		$this->load->library('mail_service');
		$this->load->model('emails');
		$this->load->model('restaurants');
		$this->mail_service->set_adapter('smtp');

		$this->_data = [];

		$this->form_validation->set_rules('email', 'Corréo', 'trim|required|valid_email');

		if ($this->form_validation->run() == FALSE)
		{
			$this->_render_strategy('Catering/Forgot', $this->_data);
		}
		else
		{
			// set variables from the form
			$email = $this->input->post('email');
			$restaurant = $this->restaurants->get_restaurant_from_email($email);

			if ($restaurant && $this->is_valid_restaurant($restaurant))
			{
				$token = $this->restaurants->reset_password($restaurant->id);
				$from = $this->config->item('from_email');
				$to = $email;
				$template = $this->emails->get_template('reset-password', [
					'email' => $email,
					'reset_token' => $token,
					'link' => base_url() . 'catering/reset'
				]);

				$this->mail_service->send($from, $to, $template->subject, $template->html);
				$this->_data['success'] = 'Hemos enviado un enlace a su corréo para actualizar su contraseña';
				$this->_render_strategy('Catering/Forgot', $this->_data);
			}
			else
			{
				$this->_data['error'] = 'Corréo no existe';
				$this->_render_strategy('Catering/Forgot', $this->_data);
			}
		}
	}

	public function reset ($token)
	{
		$this->load->model('restaurants');
		$this->_data = [];

		$user = $this->restaurants->get_restaurant_from_reset_token($token);

		if (!$user)
		{
			$this->error('El token proveido no es valido');
			redirect('/catering/forgot');
			exit;
		}

		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('confirm_password', 'Confirmar Contraseña', 'required|matches[password]');

		if ($this->form_validation->run() == FALSE)
		{
			$this->_render_strategy('Catering/Reset', $this->_data);
		}
		else
		{
			$password = $this->input->post('password');

			if ($this->restaurants->edit_profile($user->id, ['password' => $password]))
			{
				$this->success('Contraseña Actualizada exitosamente.');
				redirect('/catering/login');
			}
			else
			{
				$this->_data['error'] = 'No se pudo acutalizar la contraseña';
				$this->_render_strategy('Catering/Reset', $this->_data);
			}
		}
	}

	protected function _render_strategy ($view_name, $data)
	{
		$this->load->view($view_name, $data);
	}

	public function logout()
	{
		$_SESSION = [];
		unset($_SESSION);
		redirect('/catering/login', 'refresh');
	}

	private function is_valid_restaurant($restaurant)
	{
		return ($restaurant && $restaurant->status == 1);
	}
}