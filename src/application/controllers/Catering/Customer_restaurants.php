<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';
class Customer_restaurants extends Catering_Controller
{
    protected $model_file = 'customer_restaurant';
	public $_page_name = 'customer';

	public function index($page)
	{
		$this->load->library('pagination');
		$this->load->model('users');
		$this->_data['email'] = ($this->input->post('email', TRUE)) ? $this->input->post('email', TRUE) : '';
		$this->_data['num_order'] = ($this->input->post('num_order', TRUE)) ? $this->input->post('num_order', TRUE) : '';
		$where = [];

		if (strlen($this->_data['email']) > 0)
		{
			$user_id = $this->users->get_user_id_from_email($this->_data['email']);
			if ($user_id)
			{
				$where['user_id'] = $user_id;
			}
			else
			{
				$where['user_id'] = 0;
			}
		}

		if (strlen($this->_data['num_order']) > 0)
		{
			$where['num_order'] = $this->_data['num_order'];
		}

		$this->_data['base_url'] = '/admin/user/';
        $this->_data['total_rows'] = $this->customer_restaurant->num_customer_restaurants($_SESSION['user_id']);
        $this->_data['mapping'] = $this->customer_restaurant->mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->process_data($this->customer_restaurant->get_paginated_customer_restaurants($page, $this->_data['per_page'], $where));
		$this->_data['links'] = $this->pagination->create_links();
		$this->render('Catering/Customer_restaurant', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('restaurant_id', 'ID Restaurante', 'required|integer');
		$this->form_validation->set_rules('user_id', 'ID Usuario', 'required|integer');
		$this->form_validation->set_rules('num_order', '# Pedidos', 'required|integer');
		$this->form_validation->set_rules('first_order_date', 'Fehca Primer Pedido', 'required|date');

		if ($this->form_validation->run() === false)
		{
			$this->render('Catering/Customer_restaurantAdd', $this->_data);
		}
		else
		{
    		$restaurant_id = $this->input->post('restaurant_id', TRUE);
			$user_id = $this->input->post('user_id', TRUE);
			$num_order = $this->input->post('num_order', TRUE);
			$first_order_date = $this->input->post('first_order_date', TRUE);

			if ($this->customer_restaurant->create_customer_restaurant([
				'restaurant_id' => $restaurant_id,
				'user_id' => $user_id,
				'num_order' => $num_order,
				'first_order_date' => $first_order_date,

			]))
			{
				redirect('/catering/customer_restaurants', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo añadir cliente de restaurante.';
				$this->render('Catering/Customer_restaurantAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$customer_restaurant = $this->customer_restaurant->get_customer_restaurant($id);
		$this->_data['model'] = $customer_restaurant;
		if (!$customer_restaurant)
		{
			$this->error('El cliente no existe');
			redirect('/catering/customer_restaurants');
		}
		$this->form_validation->set_rules('restaurant_id', 'ID Restaurante', 'required|integer');
		$this->form_validation->set_rules('user_id', 'ID Usuario', 'required|integer');
		$this->form_validation->set_rules('num_order', '# Pedidos', 'required|integer');
		$this->form_validation->set_rules('first_order_date', 'Fecha Primer Pedido', 'required|date');

		if ($this->form_validation->run() === false)
        {
			$this->render('Catering/Customer_restaurantEdit', $this->_data);
		}
        else
        {
    		$restaurant_id = $this->input->post('restaurant_id', TRUE);
			$user_id = $this->input->post('user_id', TRUE);
			$num_order = $this->input->post('num_order', TRUE);
			$first_order_date = $this->input->post('first_order_date', TRUE);

			if ($this->customer_restaurant->edit_customer_restaurant([
				'restaurant_id' => $restaurant_id,
				'user_id' => $user_id,
				'num_order' => $num_order,
				'first_order_date' => $first_order_date,

            ], $id))
            {
				redirect('/catering/customer_restaurants', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar cliente.';
				$this->render('Catering/Customer_restaurantEdit', $this->_data);
			}
		}
	}

	public function process_data ($data)
	{
		$this->load->model('users');
		foreach ($data as $key => $customer_restaurant)
		{
			$user = $this->users->get_user($customer_restaurant->user_id);
			$data[$key]->user = $user;
		}
		return $data;
	}
}