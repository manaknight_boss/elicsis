<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';
class Ranking extends Catering_Controller
{
    protected $model_file = 'metric';
    public $_page_name = 'ranking';


	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->metric->get_metric($_SESSION['user_id']);
		$this->render('Catering/Metric', $this->_data);
	}
}