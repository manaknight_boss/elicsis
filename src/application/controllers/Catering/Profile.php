<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';

class Profile extends Catering_Controller
{
	protected $model_file = 'restaurants';
	protected $_redirect = '/catering/dashboard';
	public $_page_name ='profile';

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		$user = $this->restaurants->get_profile($_SESSION['user_id']);

		$this->_data['model'] = $user;

		if (!$user)
		{
			$this->set_message('El perfil no existe', 'error');
			redirect($this->_redirect);
		}

		$email_validation_rules = 'required|valid_email';

		if ($this->input->post('email') != $_SESSION['email'])
		{
			$email_validation_rules .= '|is_unique[restaurants.email]';
		}

		$this->form_validation->set_rules('email', 'Corréo', $email_validation_rules);
		$this->form_validation->set_rules('password', 'Contraseña', '');

		if ($this->form_validation->run() === false)
		{
			return $this->render('Catering/Profile', $this->_data);
		}
		else
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			if ($this->restaurants->edit_profile($_SESSION['user_id'], [
				'email' => $email,
				'password' => $password
			]))
			{
				$_SESSION['email'] = $email;
				redirect($this->_redirect, 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar el perfil.';
				return $this->render('Catering/Profile', $this->_data);
			}

		}
	}
}