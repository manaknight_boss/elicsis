<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Catering_Controller.php';
class Menu_addons extends Catering_Controller
{
    protected $model_file = 'menu_addon';
    public $_page_name = 'menu';

	public function __construct()
    {
		parent::__construct();
		$this->load->model('menu');
		$this->load->model('menu_property');
		$this->load->model('preferences');
		$this->_data['status'] = $this->menu_addon->mapping();
		$this->_data['preferences'] = $this->preferences->get_preferences(TRUE);
    }

	public function index($menu_id)
	{
		$menu = $this->menu->get_menu($menu_id);
		if (!$menu)
		{
			$this->error('Complemento de Menú no existe');
			redirect('/catering/menu');
		}

		$this->_data['model'] = $menu;
		$this->_data['list'] = $this->menu_addon->get_menu_addons($menu_id);
		$this->_data['menu_property_list'] = $this->menu_property->get_menu_properties(TRUE);
		$this->render('Catering/Menu_addon', $this->_data);
	}

	public function add($menu_id)
	{
		$menu = $this->menu->get_menu($menu_id);
		if (!$menu)
		{
			$this->error('Complemento de Menú no existe');
			redirect('/catering/menu');
		}

		$this->_data['model'] = $menu;
		$this->_data['menu_property_list'] = $this->menu_property->get_menu_properties();
		$this->_data['preferences'] = $this->preferences->get_preferences(TRUE);

		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('menu_property_id', 'Tipo de Complemento', 'required');
		$this->form_validation->set_rules('serving', 'Porciones', 'integer');
		$this->form_validation->set_rules('price', 'Precio', 'numeric');
		$this->form_validation->set_rules('info', 'Detalles', '');
		$this->form_validation->set_rules('preference', 'Preferencia', '');

		if ($this->form_validation->run() === false)
		{
			$this->render('Catering/Menu_addonAdd', $this->_data);
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$serving = $this->input->post('serving', TRUE);
			$price = $this->input->post('price', TRUE);
			$info = $this->input->post('info', TRUE);
			$preference = $this->input->post('preference', TRUE);
			$status = 1;
			$menu_property_id = $this->input->post('menu_property_id', TRUE);
			$payload = $this->create_payload($this->_data['menu_property_list'], $menu_property_id, $name, $serving, $price, $info, $preference, $status);
			$payload['menu_id'] = $menu_id;
			$payload['menu_property_id'] = $menu_property_id;
			if ($this->menu_addon->create_menu_addon($payload))
			{
				redirect('/catering/menu/addon/' . $menu_id, 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo añadir complemento.';
				$this->render('Catering/Menu_addonAdd', $this->_data);
			}
		}
	}

	public function edit($menu_addon_id)
	{
		$menu_addon = $this->menu_addon->get_menu_addon($menu_addon_id);

		if (!$menu_addon)
		{
			$this->error('Complemento de Menú no existe');
			redirect('/catering/menu');
		}

		$this->_data['model'] = $menu_addon;
		$this->_data['menu_property_list'] = $this->menu_property->get_menu_properties();
		$this->_data['preferences'] = $this->preferences->get_preferences(TRUE);
		$this->_data['processed_preference'] = json_decode($this->_data['model']->preference, TRUE);

		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('serving', 'Porciones', 'integer');
		$this->form_validation->set_rules('price', 'Precio', 'integer');
		$this->form_validation->set_rules('info', 'Detalles', '');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
		{
			$this->render('Catering/Menu_addonEdit', $this->_data);
		}
		else
		{
			$name = $this->input->post('name', TRUE);
			$serving = $this->input->post('serving', TRUE);
			$price = $this->input->post('price', TRUE);
			$info = $this->input->post('info', TRUE);
			$preference = $this->input->post('preference', TRUE);
			$status = $this->input->post('status', TRUE);
			$menu_property_id = $menu_addon->menu_property_id;
			$payload = $this->create_payload($this->_data['menu_property_list'], $menu_property_id, $name, $serving, $price, $info, $preference, $status);

			if ($this->menu_addon->edit_menu_addon($payload, $menu_addon_id))
			{
				redirect('/catering/menu/addon/' . $menu_addon->menu_id, 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar complemento.';
				$this->render('Catering/Menu_addonEdit', $this->_data);
			}
		}
	}

	private function create_payload($menu_property_list, $menu_property_id, $name, $serving, $price, $info, $preference, $status)
	{
		$index = 0;
		$payload = [];
		$payload['name'] = $name;
		foreach ($menu_property_list as $key => $value)
		{
			if ($menu_property_id == $value->id)
			{

				foreach ($value as $allow_field => $allow_field_value) {
					if ($allow_field_value == 1) {
						if ($allow_field == 'is_serving')
						{
							$payload['serving'] = $serving;
						}
						if ($allow_field == 'is_price')
						{
							$payload['price'] = $price;
						}
						if ($allow_field == 'is_info')
						{
							$payload['info'] = $info;
						}
						if ($allow_field == 'is_preference')
						{
							$payload['preference'] = '[]';

							if (is_array($preference))
							{
								$payload['preference'] = json_encode(array_map(function($element){
									return (int)$element;
								}, $preference));
							}
						}
					}
				}

			}
		}
		$payload['status'] = $status;
		return $payload;
	}

	private function process_preferences_string($source, $input)
	{
		$results = [];
		$input = json_decode($input, TRUE);
		foreach ($input as $value)
		{
			foreach ($source as $key => $src_name)
			{
				if ($value == $src_name)
				{
					$results[] = $src_name;
				}
			}
		}
		return json_encode($results);
	}

	private function process_preferences_key_to_string($source, $input)
	{
		$results = [];
		$input = json_decode($input, TRUE);
		foreach ($input as $value)
		{
			foreach ($source as $key => $src_name)
			{
				if ($value == $key)
				{
					$results[] = $src_name;
				}
			}
		}
		return json_encode($results);
	}
	private function process_preferences_int($source, $input)
	{
		$results = [];
		$input = json_decode($input, TRUE);
		foreach ($input as $value)
		{
			foreach ($source as $key => $src_name)
			{
				if ($value == $src_name)
				{
					$results[] = $key;
				}
			}
		}
		return json_encode($results);
	}
}