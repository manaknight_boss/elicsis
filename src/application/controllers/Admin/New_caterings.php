<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class New_caterings extends Admin_Controller
{
    protected $model_file = 'new_catering';
	public $_page_name = 'catering';

	public function index($page)
	{
        $this->load->library('pagination');
		$this->_data['base_url'] = '/admin/catering/new/';
        $this->_data['total_rows'] = $this->new_catering->num_new_catering();
        $this->_data['mapping'] = $this->new_catering->mapping();
        $this->_data['type_mapping'] = $this->new_catering->type_mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->new_catering->get_paginated_new_caterings((int)$page, (int)$this->_data['per_page']);
		$this->_data['links'] = $this->pagination->create_links();
		$this->render('Admin/New_catering', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('first_name', 'Nombres', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');
		$this->form_validation->set_rules('business_name', 'Nombre de Negocio', 'required');
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('comment', 'Comentario', 'required');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/New_cateringAdd', $this->_data);
		}
		else
		{
    		$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
			$business_name = $this->input->post('business_name', TRUE);
			$email = $this->input->post('email', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$comment = $this->input->post('comment', TRUE);

			if ($this->new_catering->create_new_catering([
				'first_name' => $first_name,
				'last_name' => $last_name,
				'business_name' => $business_name,
				'email' => $email,
				'phone' => $phone,
				'comment' => $comment
			]))
			{
				redirect('/admin/new_catering', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo agregar Afiliado.';
				$this->render('Admin/New_cateringAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$new_catering = $this->new_catering->get_new_caterin($id);
		$this->_data['model'] = $new_catering;
		if (!$new_catering)
		{
			$this->error('Afiliado no existe');
			redirect('/admin/new_catering');
		}
		$this->form_validation->set_rules('first_name', 'Nombres', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');
		$this->form_validation->set_rules('business_name', 'Nombre de Negocio', 'required');
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('comment', 'Comentario', 'required');

		if ($this->form_validation->run() === false)
        {
			$this->render('Admin/New_cateringEdit', $this->_data);
		}
        else
        {
    		$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
			$business_name = $this->input->post('business_name', TRUE);
			$email = $this->input->post('email', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$comment = $this->input->post('comment', TRUE);

			if ($this->new_catering->edit_new_catering([
				'first_name' => $first_name,
				'last_name' => $last_name,
				'business_name' => $business_name,
				'email' => $email,
				'phone' => $phone,
				'comment' => $comment
            ], $id))
            {
				redirect('/admin/new_catering', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar afiliado.';
				$this->render('Admin/New_cateringEdit', $this->_data);
			}
		}
	}
}