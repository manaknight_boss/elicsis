<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Coupon extends Admin_Controller 
{
    protected $model_file = 'coupons';
    public $_plans_list;

    public function __construct() 
    {
        parent::__construct();
        $stripe_params = [
            'stripe_secret_key' => $this->config->item('stripe_secret_key')
        ];
        $this->load->library('stripe_payment', $stripe_params);        
        $this->_plans_list = $this->get_plan_list();
    }

    public function index() 
    {
        $this->_data['list'] = $this->coupons->get_coupons();
        $this->_data['plans_list'] = $this->_plans_list;
        $this->render('Admin/Coupons', $this->_data);
    }

    public function add() 
    {
        $this->form_validation->set_rules('coupon_code', 'Código de Cupón', 'required');
        $this->form_validation->set_rules('discount_type', 'Descuento', 'required|in_list[percent,fixed]');
        $this->form_validation->set_rules('coupon_amount', 'Monto de Cupón', 'required|integer');
        $this->form_validation->set_rules('customer_email', 'Corréo del Cliente', 'valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('usage_limit', 'Límite de uso', 'integer');
        $this->form_validation->set_rules('usage_limit_per_user', 'Límite por Usuario', 'integer');
        $this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');
        $this->form_validation->set_rules('expiry_date', 'Fecha Expira', 'required|date');

        $this->_data['plans_list'] = $this->_plans_list;

        if ($this->form_validation->run() === false) 
        {
            return $this->render('Admin/CouponsAdd', $this->_data);
        } 
        else 
        {
            $coupon_code = $this->input->post('coupon_code', TRUE);
            $discount_type = $this->input->post('discount_type', TRUE);
            $coupon_amount = $this->input->post('coupon_amount', TRUE);
            $plan_id = $this->input->post('plan_id', TRUE);
            $customer_email = $this->input->post('customer_email', TRUE);
            $usage_limit = ($this->input->post('usage_limit', TRUE) > 0) ? $this->input->post('usage_limit', TRUE) : NULL;
            $status = $this->input->post('status', TRUE);
            $expiry_date = $this->input->post('expiry_date', TRUE);

            $stripe_coupon = $this->stripe_payment->create_coupon($coupon_code, 'usd', $coupon_amount, $discount_type, $usage_limit);

            if (!$stripe_coupon) 
            {
                $this->error($this->stripe_payment->get_error_message());
                redirect('/admin/coupon', 'refresh');
            }

            $stripe_coupon_code = $stripe_coupon->id;

            if ($this->coupons->create_coupon([
                'coupon_code' => $coupon_code,
                'discount_type' => $discount_type,
                'coupon_amount' => $coupon_amount,
                'plan_id' => $plan_id,
                'customer_email' => $customer_email,
                'usage_limit' => $usage_limit,
                'stripe_coupon_code' => $stripe_coupon_code,
                'status' => $status,
                'expiry_date' => $expiry_date
            ])) 
            {
                $this->success('El cupón fue creado exitosamente');
                redirect('/admin/coupon', 'refresh');
            } 
            else 
            {
                // user creation failed, this should never happen
                $this->_data['error'] = 'No se Añadió el cupón.';
                return $this->render('Admin/CouponsAdd', $this->_data);
            }
        }
    }

    public function edit($id) 
    {
        $coupons = $this->coupons->get_coupon($id);

        if (!$coupons) 
        {
            $this->error('Los Cupones no existen');
            redirect('/admin/coupons');
        }

        $this->_data['plans_list'] = $this->_plans_list;
        $this->_data['model'] = $coupons;

        $this->form_validation->set_rules('coupon_code', 'Código de Cupón', 'required');
        $this->form_validation->set_rules('discount_type', 'Descuento', 'required|in_list[percent,fixed]');
        $this->form_validation->set_rules('coupon_amount', 'Monto de Cupón', 'required|integer');
        $this->form_validation->set_rules('customer_email', 'Corréo del Cliente', 'valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('usage_limit', 'Límite de uso', 'integer');
        $this->form_validation->set_rules('usage_limit_per_user', 'Límite por Usuario', 'integer');
        $this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');
        $this->form_validation->set_rules('expiry_date', 'Fecha Expira', 'required|date');

        if ($this->form_validation->run() === false) 
        {
            return $this->render('Admin/CouponsEdit', $this->_data);
        } 
        else 
        {
            $coupon_code = $this->input->post('coupon_code', TRUE);
            $discount_type = $this->input->post('discount_type', TRUE);
            $coupon_amount = $this->input->post('coupon_amount', TRUE);
            $plan = $this->input->post('plan', TRUE);
            $customer_email = $this->input->post('customer_email', TRUE);
            $usage_limit = $this->input->post('usage_limit', TRUE);
            $usage_limit_per_user = $this->input->post('usage_limit_per_user', TRUE);
            $status = $this->input->post('status', TRUE);
            $expiry_date = $this->input->post('expiry_date', TRUE);

            if ($this->coupons->edit_coupon([
                'coupon_code' => $coupon_code,
                'discount_type' => $discount_type,
                'coupon_amount' => $coupon_amount,
                'plan' => $plan,
                'customer_email' => $customer_email,
                'usage_limit' => $usage_limit,
                'usage_limit_per_user' => $usage_limit_per_user,
                'status' => $status,
                'expiry_date' => $expiry_date
            ], $id)) 
            {
                $this->success('El cupón fue editado exitosamente');
                redirect('/admin/coupon', 'refresh');
            } 
            else 
            {
                // user creation failed, this should never happen
                $this->_data['error'] = 'No se pudo editar el cupón.';
                return $this->render('Admin/CouponsEdit', $this->_data);
            }
        }
    }

    protected function get_plan_list() 
    {
        $this->load->model('plans', 'plans');
        $result = $this->plans->get_plans_list();
        $plans = [];
        if (isset($result) && count($result) > 0) 
        {
            foreach ($result as $plan) 
            {
                $plans[$plan->id] = $plan->display_name . ' (' . $plan->name . ')';
            }
        }
        return $plans;
    }

}
