<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Restaurant extends Admin_Controller
{
    protected $model_file = 'restaurants';
	public $_page_name = 'restaurant';

	public function index($page)
	{
		$this->load->library('pagination');
		$this->load->model('cuisines');
		$this->_data['base_url'] = '/admin/restaurants/';
        $this->_data['total_rows'] = $this->restaurants->num_restaurants($_SESSION['user_id']);
        $this->_data['cuisines'] = $this->cuisines->get_cuisines_key_value();
        $this->_data['status'] = $this->restaurants->mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$this->_data['list'] = $this->restaurants->get_paginated_restaurants($page, $this->_data['per_page']);
		$this->_data['links'] = $this->pagination->create_links();
		$this->render('Admin/Restaurants', $this->_data);
	}

	public function add()
	{
		$this->load->model('cuisines');
		$this->load->model('settings');
		$this->load->model('metric');

		$this->_data['cuisines'] = $this->cuisines->get_cuisines(TRUE);
		$this->_data['setting'] = $this->settings->get_config_settings();
		$this->_data['type'] = $this->restaurants->delivery_type_mapping();
		$this->_data['enable_google_api'] = TRUE;
		$this->_data['google_key'] = $this->config->item('google_key');
		$this->_data['google_api_callback'] = 'callbackAddress';

		$this->form_validation->set_rules('title', 'Nombre', 'required');
		$this->form_validation->set_rules('description', 'Descripción', 'required');
		$this->form_validation->set_rules('about', 'Detalles', 'required');
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email|is_unique[restaurants.email]');
		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('delivery_fee', 'Delivery', 'required|numeric');
		$this->form_validation->set_rules('food_minimum', 'Consumo Mínimo', 'required|integer');
		$this->form_validation->set_rules('type', 'Tipo de Restaurante', 'required|integer');
		$this->form_validation->set_rules('cuisines_id', 'Gastronomia', 'required');
		$this->form_validation->set_rules('price_range', 'Rango de Precios', 'required');
		$this->form_validation->set_rules('address', 'Dirección', 'required');
		$this->form_validation->set_rules('city', 'Ciudad', '');
		$this->form_validation->set_rules('state', 'Provincia', '');
		$this->form_validation->set_rules('zip', 'Codigo Postal', '');
		$this->form_validation->set_rules('lat', 'Latitude', 'required|numeric');
		$this->form_validation->set_rules('long', 'Longitude', 'required|numeric');
		$this->form_validation->set_rules('started_at', 'Abierto desde', 'required|date');
		$this->form_validation->set_rules('u', 'Domingo', 'integer');
		$this->form_validation->set_rules('m', 'Lunes', 'integer');
		$this->form_validation->set_rules('t', 'Martes', 'integer');
		$this->form_validation->set_rules('w', 'Miercoles', 'integer');
		$this->form_validation->set_rules('r', 'Jueves', 'integer');
		$this->form_validation->set_rules('f', 'Viernes', 'integer');
		$this->form_validation->set_rules('s', 'Sabado', 'integer');
		$this->form_validation->set_rules('rnc', 'RNC #', 'required');
		$this->form_validation->set_rules('notes', 'notes #', '');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('serving', 'No. Personas Max/Orden', 'required');
		$this->form_validation->set_rules('u_open', 'Domingo Horas Abierto', 'numeric');
		$this->form_validation->set_rules('m_open', 'Lunes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('t_open', 'Martes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('w_open', 'Miercoles Horas Abierto', 'numeric');
		$this->form_validation->set_rules('r_open', 'Jueves Horas Abierto', 'numeric');
		$this->form_validation->set_rules('f_open', 'Viernes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('s_open', 'Sabado Horas Abierto', 'numeric');
		$this->form_validation->set_rules('u_close', 'Domingo Horas Cerrado', 'numeric|greater_than[' . $this->input->post('u_open') . ']');
		$this->form_validation->set_rules('m_close', 'Lunes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('m_open') . ']');
		$this->form_validation->set_rules('t_close', 'Martes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('t_open') . ']');
		$this->form_validation->set_rules('w_close', 'Miercoles Horas Cerrado', 'numeric|greater_than[' . $this->input->post('w_open') . ']');
		$this->form_validation->set_rules('r_close', 'Jueves Horas Cerrado', 'numeric|greater_than[' . $this->input->post('r_open') . ']');
		$this->form_validation->set_rules('f_close', 'Viernes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('f_open') . ']');
		$this->form_validation->set_rules('s_close', 'Sabado Horas Cerrado', 'numeric|greater_than[' . $this->input->post('s_open') . ']');

		if ($this->form_validation->run() === FALSE)
        {
			$this->render('Admin/RestaurantAdd', $this->_data);
		}
        else
        {
    		$title = $this->input->post('title', TRUE);
    		$email = $this->input->post('email', TRUE);
    		$password = $this->input->post('password', TRUE);
			$description = $this->input->post('description', TRUE);
			$about = $this->input->post('about', TRUE);
			$delivery_fee = $this->input->post('delivery_fee', TRUE);
			$food_minimum = $this->input->post('food_minimum', TRUE);
			$cuisines_id = $this->input->post('cuisines_id', TRUE);
			$price_range = $this->input->post('price_range', TRUE);
			$type = $this->input->post('type', TRUE);
			$address = $this->input->post('address', TRUE);
			$city = $this->input->post('city', TRUE);
			$state = $this->input->post('state', TRUE);
			$zip = $this->input->post('zip', TRUE);
			$rnc = $this->input->post('rnc', TRUE);
			$lat = $this->input->post('lat', TRUE);
			$long = $this->input->post('long', TRUE);
			$notes = $this->input->post('notes', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$serving = $this->input->post('serving', TRUE);
			$started_at = $this->input->post('started_at', TRUE);

			if ($this->restaurants->create_restaurant([
				'title' => $title,
				'description' => $description,
				'about' => $about,
				'u' => $this->input->post('u', TRUE),
				'u_open' => (int)$this->input->post('u_open', TRUE),
				'u_close' => (int)$this->input->post('u_close', TRUE),
				'm' => $this->input->post('m', TRUE),
				'm_open' => (int)$this->input->post('m_open', TRUE),
				'm_close' => (int)$this->input->post('m_close', TRUE),
				't' => $this->input->post('t', TRUE),
				't_open' => (int)$this->input->post('t_open', TRUE),
				't_close' => (int)$this->input->post('t_close', TRUE),
				'w' => $this->input->post('w', TRUE),
				'w_open' => (int)$this->input->post('w_open', TRUE),
				'w_close' => (int)$this->input->post('w_close', TRUE),
				'r' => $this->input->post('r', TRUE),
				'r_open' => (int)$this->input->post('r_open', TRUE),
				'r_close' => (int)$this->input->post('r_close', TRUE),
				'f' => $this->input->post('f', TRUE),
				'f_open' => (int)$this->input->post('f_open', TRUE),
				'f_close' => (int)$this->input->post('f_close', TRUE),
				's' => $this->input->post('s', TRUE),
				's_open' => (int)$this->input->post('s_open', TRUE),
				's_close' => (int)$this->input->post('s_close', TRUE),
				'u_open_min' => ($this->is_decimal($this->input->post('u_open', TRUE))? 30 : 0),
				'u_close_min' => ($this->is_decimal($this->input->post('u_close', TRUE))? 30 : 0),
				'm_open_min' => ($this->is_decimal($this->input->post('m_open', TRUE))? 30 : 0),
				'm_close_min' => ($this->is_decimal($this->input->post('m_close', TRUE))? 30 : 0),
				't_open_min' => ($this->is_decimal($this->input->post('t_open', TRUE))? 30 : 0),
				't_close_min' => ($this->is_decimal($this->input->post('t_close', TRUE))? 30 : 0),
				'w_open_min' => ($this->is_decimal($this->input->post('w_open', TRUE))? 30 : 0),
				'w_close_min' => ($this->is_decimal($this->input->post('w_close', TRUE))? 30 : 0),
				'r_open_min' => ($this->is_decimal($this->input->post('r_open', TRUE))? 30 : 0),
				'r_close_min' => ($this->is_decimal($this->input->post('r_close', TRUE))? 30 : 0),
				'f_open_min' => ($this->is_decimal($this->input->post('f_open', TRUE))? 30 : 0),
				'f_close_min' => ($this->is_decimal($this->input->post('f_close', TRUE))? 30 : 0),
				's_open_min' => ($this->is_decimal($this->input->post('s_open', TRUE))? 30 : 0),
				's_close_min' => ($this->is_decimal($this->input->post('s_close', TRUE)? 30 : 0)),
				'rating' => 0,
				'food_minimum' => $food_minimum,
				'delivery_fee' => $delivery_fee,
				'cuisines_id' => $cuisines_id,
				'price_range' => $price_range,
				'type' => $type,
				'status' => 0,
				'serving' => $serving,
				'reset_token' => '',
				'address' => $address,
				'city' => $city,
				'state' => $state,
				'zip' => $zip,
				'lat' => $lat,
				'long' => $long,
				'email' => $email,
				'password' => $password,
				'phone' => $phone,
				'notes' => $notes,
				'rnc' => $rnc,
				'logo' => '',
				'image' => '',
				'banner_image' => '',
				'on_time_rating' => 0,
				'on_delivery_rating' => 0,
				'num_review' => 0,
				'started_at' => $started_at
            ]))
            {
				$this->metric->update_metric('restaurants');
				redirect('/admin/restaurants/0', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo agregar Restaurante.';
				$this->render('Admin/RestaurantAdd', $this->_data);
			}
		}
	}

	public function edit($restaurant_id)
	{
		$this->load->model('metric');
		$this->load->model('cuisines');
		$this->load->model('settings');

        $restaurants = $this->restaurants->get_restaurant($restaurant_id);
		$this->_data['model'] = $restaurants;
		$this->_data['type'] = $this->restaurants->delivery_type_mapping();
		$this->_data['cuisines'] = $this->cuisines->get_cuisines(TRUE);
		$this->_data['setting'] = $this->settings->get_config_settings();
		$this->_data['enable_google_api'] = TRUE;
		$this->_data['google_key'] = $this->config->item('google_key');
		$this->_data['google_api_callback'] = 'callbackAddress';


		$email_validation_rules = 'required|valid_email';

		if ($this->input->post('email') != $restaurants->email)
		{
			$email_validation_rules .= '|is_unique[restaurants.email]';
		}

		$this->form_validation->set_rules('email', 'Corréo', $email_validation_rules);
		$this->form_validation->set_rules('password', 'Contraseña', '');
		$this->form_validation->set_rules('title', 'Nombre', 'required');
		$this->form_validation->set_rules('description', 'Descripcion', 'required');
		$this->form_validation->set_rules('about', 'Detalles', 'required');
		$this->form_validation->set_rules('u', 'Domingo', 'integer');
		$this->form_validation->set_rules('m', 'Lunes', 'integer');
		$this->form_validation->set_rules('t', 'Martes', 'integer');
		$this->form_validation->set_rules('w', 'Miercoles', 'integer');
		$this->form_validation->set_rules('r', 'Jueves', 'integer');
		$this->form_validation->set_rules('f', 'Viernes', 'integer');
		$this->form_validation->set_rules('s', 'Sabado', 'integer');
		$this->form_validation->set_rules('u_open', 'Domingo Horas Abierto', 'numeric');
		$this->form_validation->set_rules('m_open', 'Lunes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('t_open', 'Martes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('w_open', 'Miercoles Horas Abierto', 'numeric');
		$this->form_validation->set_rules('r_open', 'Jueves Horas Abierto', 'numeric');
		$this->form_validation->set_rules('f_open', 'Viernes Horas Abierto', 'numeric');
		$this->form_validation->set_rules('s_open', 'Sabado Horas Abierto', 'numeric');
		$this->form_validation->set_rules('u_close', 'Domingo Horas Cerrado', 'numeric|greater_than[' . $this->input->post('u_open') . ']');
		$this->form_validation->set_rules('m_close', 'Lunes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('m_open') . ']');
		$this->form_validation->set_rules('t_close', 'Martes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('t_open') . ']');
		$this->form_validation->set_rules('w_close', 'Miercoles Horas Cerrado', 'numeric|greater_than[' . $this->input->post('w_open') . ']');
		$this->form_validation->set_rules('r_close', 'Jueves Horas Cerrado', 'numeric|greater_than[' . $this->input->post('r_open') . ']');
		$this->form_validation->set_rules('f_close', 'Viernes Horas Cerrado', 'numeric|greater_than[' . $this->input->post('f_open') . ']');
		$this->form_validation->set_rules('s_close', 'Sabado Horas Cerrado', 'numeric|greater_than[' . $this->input->post('s_open') . ']');
		$this->form_validation->set_rules('food_minimum', 'Consumo Mínimo', 'required|integer');
		$this->form_validation->set_rules('delivery_fee', 'Delivery', 'required|numeric');
		$this->form_validation->set_rules('cuisines_id', 'Gastronomia', 'required');
		$this->form_validation->set_rules('price_range', 'Rango de Precios', 'required');
		$this->form_validation->set_rules('type', 'Tipo de Restaurante', 'required|integer');
		$this->form_validation->set_rules('status', 'Estado', 'required|integer');
		$this->form_validation->set_rules('address', 'Dirección', 'required');
		$this->form_validation->set_rules('city', 'Ciudad', '');
		$this->form_validation->set_rules('state', 'Estado', '');
		$this->form_validation->set_rules('zip', 'Codigo Postal', '');
		$this->form_validation->set_rules('lat', 'Latitude', 'required|numeric');
		$this->form_validation->set_rules('long', 'Longitude', 'required|numeric');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('serving', 'No. Personas Max/Orden', 'required');
		$this->form_validation->set_rules('notes', 'notas', '');
		$this->form_validation->set_rules('rnc', 'RNC #', 'required');
		$this->form_validation->set_rules('started_at', 'Abiertos desde', 'required|date');

		if ($this->form_validation->run() === FALSE)
        {
			$this->render('Admin/RestaurantEdit', $this->_data);
		}
        else
        {
    		$title = $this->input->post('title', TRUE);
			$description = $this->input->post('description', TRUE);
			$about = $this->input->post('about', TRUE);
			$food_minimum = $this->input->post('food_minimum', TRUE);
			$delivery_fee = $this->input->post('delivery_fee', TRUE);
			$cuisines_id = $this->input->post('cuisines_id', TRUE);
			$price_range = $this->input->post('price_range', TRUE);
			$type = $this->input->post('type', TRUE);
			$status = $this->input->post('status', TRUE);
			$password = $this->input->post('password', TRUE);
			$email = $this->input->post('email', TRUE);
			$address = $this->input->post('address', TRUE);
			$city = $this->input->post('city', TRUE);
			$state = $this->input->post('state', TRUE);
			$zip = $this->input->post('zip', TRUE);
			$lat = $this->input->post('lat', TRUE);
			$long = $this->input->post('long', TRUE);
			$rnc = $this->input->post('rnc', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$serving = $this->input->post('serving', TRUE);
			$notes = $this->input->post('notes', TRUE);
			$started_at = $this->input->post('started_at', TRUE);

			if ($this->restaurants->edit_restaurant([
				'title' => $title,
				'description' => $description,
				'email' => $email,
				'password' => $password,
				'about' => $about,
				'status' => $status,
				'delivery_fee' => $delivery_fee,
				'food_minimum' => $food_minimum,
				'type' => $type,
				'cuisines_id' => $cuisines_id,
				'price_range' => $price_range,
				'address' => $address,
				'city' => $city,
				'state' => $state,
				'zip' => $zip,
				'lat' => $lat,
				'long' => $long,
				'rnc' => $rnc,
				'phone' => $phone,
				'serving' => $serving,
				'notes' => $notes,
				'started_at' => $started_at,
				'u' => $this->input->post('u', TRUE),
				'u_open' => (int)$this->input->post('u_open', TRUE),
				'u_close' => (int)$this->input->post('u_close', TRUE),
				'm' => $this->input->post('m', TRUE),
				'm_open' => (int)$this->input->post('m_open', TRUE),
				'm_close' => (int)$this->input->post('m_close', TRUE),
				't' => $this->input->post('t', TRUE),
				't_open' => (int)$this->input->post('t_open', TRUE),
				't_close' => (int)$this->input->post('t_close', TRUE),
				'w' => $this->input->post('w', TRUE),
				'w_open' => (int)$this->input->post('w_open', TRUE),
				'w_close' => (int)$this->input->post('w_close', TRUE),
				'r' => $this->input->post('r', TRUE),
				'r_open' => (int)$this->input->post('r_open', TRUE),
				'r_close' => (int)$this->input->post('r_close', TRUE),
				'f' => $this->input->post('f', TRUE),
				'f_open' => (int)$this->input->post('f_open', TRUE),
				'f_close' => (int)$this->input->post('f_close', TRUE),
				's' => $this->input->post('s', TRUE),
				's_open' => (int)$this->input->post('s_open', TRUE),
				's_close' => (int)$this->input->post('s_close', TRUE),
				'u_open_min' => ($this->is_decimal($this->input->post('u_open', TRUE))? 30 : 0),
				'u_close_min' => ($this->is_decimal($this->input->post('u_close', TRUE))? 30 : 0),
				'm_open_min' => ($this->is_decimal($this->input->post('m_open', TRUE))? 30 : 0),
				'm_close_min' => ($this->is_decimal($this->input->post('m_close', TRUE))? 30 : 0),
				't_open_min' => ($this->is_decimal($this->input->post('t_open', TRUE))? 30 : 0),
				't_close_min' => ($this->is_decimal($this->input->post('t_close', TRUE))? 30 : 0),
				'w_open_min' => ($this->is_decimal($this->input->post('w_open', TRUE))? 30 : 0),
				'w_close_min' => ($this->is_decimal($this->input->post('w_close', TRUE))? 30 : 0),
				'r_open_min' => ($this->is_decimal($this->input->post('r_open', TRUE))? 30 : 0),
				'r_close_min' => ($this->is_decimal($this->input->post('r_close', TRUE))? 30 : 0),
				'f_open_min' => ($this->is_decimal($this->input->post('f_open', TRUE))? 30 : 0),
				'f_close_min' => ($this->is_decimal($this->input->post('f_close', TRUE))? 30 : 0),
				's_open_min' => ($this->is_decimal($this->input->post('s_open', TRUE))? 30 : 0),
				's_close_min' => ($this->is_decimal($this->input->post('s_close', TRUE)? 30 : 0))
            ], $restaurant_id))
            {
				$this->metric->update_metric('restaurants');
				redirect('/admin/restaurants/0', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar el restaurante';
				$this->render('Admin/RestaurantEdit', $this->_data);
			}
		}
	}

	private function is_decimal( $val )
	{
		return is_numeric( $val ) && floor( $val ) != $val;
	}
}