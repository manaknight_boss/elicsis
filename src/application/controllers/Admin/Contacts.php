<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Contacts extends Admin_Controller
{
	protected $model_file = 'contact';
	public $_page_name = 'contacts';

	public function index($page)
	{
        $this->load->library('pagination');
		$this->_data['base_url'] = '/admin/catering/new/';
        $this->_data['total_rows'] = $this->contact->num_contacts();
        $this->_data['mapping'] = $this->contact->mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->contact->get_paginated_contacts($page, $this->_data['per_page']);
		$this->_data['links'] = $this->pagination->create_links();
		$this->render('Admin/Contact', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('first_name', 'Nombres', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');
		$this->form_validation->set_rules('email', 'Corréos', 'required');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('comment', 'Comentarios', 'required');
		$this->form_validation->set_rules('created_at', 'Fecha de creación', 'required|date');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/ContactAdd', $this->_data);
		}
		else
		{
    		$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
			$email = $this->input->post('email', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$comment = $this->input->post('comment', TRUE);
			$created_at = $this->input->post('created_at', TRUE);

			if ($this->contact->create_contac([
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'phone' => $phone,
				'comment' => $comment,
				'created_at' => $created_at,

			]))
			{
				redirect('/admin/contact', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo agregar el Contacto.';
				$this->render('Admin/ContactAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$contact = $this->contact->get_contac($id);
		$this->_data['model'] = $contact;
		if (!$contact)
		{
			$this->error('El Contacto no existe');
			redirect('/admin/contact');
		}
		$this->form_validation->set_rules('first_name', 'Nombres', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');
		$this->form_validation->set_rules('email', 'Corréos', 'required');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('comment', 'Comentario', 'required');
		$this->form_validation->set_rules('created_at', 'Fecha de creación', 'required|date');

		if ($this->form_validation->run() === false)
        {
			$this->render('Admin/ContactEdit', $this->_data);
		}
        else
        {
    		$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
			$email = $this->input->post('email', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$comment = $this->input->post('comment', TRUE);
			$created_at = $this->input->post('created_at', TRUE);

			if ($this->contact->edit_contac([
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'phone' => $phone,
				'comment' => $comment,
				'created_at' => $created_at,

            ], $id))
            {
				redirect('/admin/contact', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo Editar el Contacto';
				$this->render('Admin/ContactEdit', $this->_data);
			}
		}
	}
}