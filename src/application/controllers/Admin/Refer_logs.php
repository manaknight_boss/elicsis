<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Refer_logs extends Admin_Controller
{
    protected $model_file = 'refer_log';
    public $_page_name = 'refer_log';


	public function index()
	{
		$this->load->library('pagination');
		$this->_data['base_url'] = '/admin/refer/';
        $this->_data['total_rows'] = $this->refer_log->num_refer_logs();
        $this->_data['mapping'] = $this->refer_log->mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->process_users($this->refer_log->get_paginated_refer_log($page, $this->_data['per_page']));
		$this->_data['links'] = $this->pagination->create_links();
		return $this->render('Admin/Refer_log', $this->_data);
	}

	public function approve($id)
	{
		$refer_log = $this->refer_log->get_refer_log($id);
		$this->_data['model'] = $refer_log;

		if (!$refer_log)
		{
			$this->error('Numero de referencia invalido');
			return redirect('/admin/refer/0');

		}

		if ($this->refer_log->edit_refer_log([
			'status' => 1
		], $this->_data['model']->id))
		{
			redirect('/admin/refer/0', 'refresh');
		}
		else
		{
			$this->error('error');
			redirect('/admin/refer/0', 'refresh');
		}
	}

	public function decline($id)
	{
		$refer_log = $this->refer_log->get_refer_log($id);
		$this->_data['model'] = $refer_log;

		if (!$refer_log)
		{
			$this->error('Numero de ref. Invalido');
			return redirect('/admin/refer/0');

		}

		if ($this->refer_log->edit_refer_log([
			'status' => 2
		], $this->_data['model']->id))
		{
			redirect('/admin/refer/0', 'refresh');
		}
		else
		{
			$this->error('error');
			redirect('/admin/refer/0', 'refresh');
		}
	}

	private function process_users ($data)
	{
		$this->load->model('users');

		foreach ($data as $key => $refer)
		{
			$user_id = $refer->user_id;
			$refer_user_id = $refer->refer_id;
			$user = $this->users->get_user($user_id);
			$data[$key]->user = 'N\A';
			$data[$key]->refer_user = 'N\A';

			if ($user)
			{
				$data[$key]->user = $user->email;
			}

			$refer_user = $this->users->get_user($refer_user_id);

			if ($refer_user)
			{
				$data[$key]->refer_user = $refer_user->email;
			}
		}

		return $data;
	}
}