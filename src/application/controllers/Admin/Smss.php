<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Smss extends Admin_Controller
{
	protected $model_file = 'sms';
	public $_page_name = 'sms';

	public function index()
	{
		$this->_data['list'] = $this->sms->get_all_sms();
		return $this->render('Admin/Sms', $this->_data);
	}

	public function add()
	{
		// set validation rules
		$this->form_validation->set_rules('slug', 'Nombre', 'required|is_unique[sms.slug]');
		$this->form_validation->set_rules('tag', 'Etiquetas', 'required');
		$this->form_validation->set_rules('content', 'Contenido', 'required');

		if ($this->form_validation->run() === FALSE) {
			return $this->render('Admin/SmsAdd', $this->_data);
		}
		else
		{
			$slug = strtolower($this->input->post('slug'));
			$tag = $this->input->post('tag');
			$content = $this->input->post('content');

			if ($this->sms->create_sms([
				'slug' => $slug,
				'tags' => $tag,
				'content' => $content
			]))
			{
				redirect('/admin/sms', 'refresh');
			}
			else
			{
				$this->_data['error'] = 'No SMS.';
				return $this->render('Admin/SmsAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$sms = $this->sms->get_sms($id);
		$this->_data['model'] = $sms;

		if (!$sms)
		{
			$this->error('no se puede encontrar el SMS');
			redirect('/admin/sms');
		}

		// set validation rules
		$this->form_validation->set_rules('slug', 'Nombre', 'required');
		$this->form_validation->set_rules('content', 'Contenido', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/SmsEdit', $this->_data);
		}
		else
		{
			$content = $this->input->post('content');

			if ($this->sms->edit_sms([
				'content' => $content
			], $id))
			{
				redirect('/admin/sms', 'refresh');
			}
			else
			{
				$this->_data['error'] = 'No se pudo editar sms.';
				return $this->render('Admin/SmsEdit', $this->_data);
			}
		}
	}
}