<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Dashboard extends Admin_Controller
{

	public function index($page)
	{
		$this->load->model('issue');
		$this->load->model('metric');

		$metric = $this->metric->get_metric();
		$this->_data['num_order'] = number_format($metric->num_order, 0);
		$this->_data['num_user'] = number_format($metric->num_user, 0);
		$this->_data['num_catering'] = number_format($metric->num_catering, 0);
		$this->_data['total_sale'] = number_format($metric->total_sale, 2);
		$this->_data['total_delivered'] = number_format($metric->total_delivered, 2);
		$this->_data['total_pending'] = number_format($metric->total_pending, 2);
		$this->_data['total_cancel'] = number_format($metric->total_cancel, 2);
		$this->_data['total_refund'] = number_format($metric->total_refund, 2);
		$this->_data['total_issue'] = number_format($metric->total_issue, 2);

		$this->load->library('pagination');
		$this->_data['base_url'] = '/admin/dashboard/';
		$this->_data['total_rows'] = $this->issue->num_active_issues();
		$this->_data['mapping'] = $this->issue->mapping();
		$this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$this->_data['list'] = $this->issue->get_paginated_active_issues($page, $this->_data['per_page']);
		$this->_data['links'] = $this->pagination->create_links();
		return $this->render('Admin/Dashboard', $this->_data);
	}

}