<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Payment extends Admin_Controller
{
    protected $model_file = 'payments';
    public $_page_name = 'payment';


	/**
	 * Steps:
	 * 1.Get list of paginated payments. Display payment date, restaurant, # orders, final total, status, action edit/adjustment
	 * 2. View payment, shows same as catering but full payment information shown as well
	 */
	public function index($page)
	{
		$this->_data['restaurant_id'] = ($this->input->post('restaurant_id', TRUE)) ? $this->input->post('restaurant_id', TRUE) : '';
		$this->_data['id'] = ($this->input->post('id', TRUE)) ? $this->input->post('id', TRUE) : '';
		$this->_data['status'] = ($this->input->post('status', TRUE)) ? $this->input->post('status', TRUE) : '';

		$this->_data['base_url'] = '/admin/payments/';
        $this->_data['total_rows'] = $this->payments->num_payments();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$this->_data['list'] = $this->process_payments($this->payments->get_paginated_payments($page, $this->_data['per_page'], [
			'id' => $this->_data['id'],
			'status' => $this->_data['status'],
			'restaurant_id' => $this->_data['restaurant_id']
		]));
		$this->_data['links'] = $this->pagination->create_links();
		$this->_data['mapping'] = $this->payments->mapping();
		$this->render('Admin/Payment', $this->_data);
	}

	/**
	 * Steps:
	 * 1.Get id of payment
	 * 2.Check if payment exist
	 * 3.Look for all orders between this period
	 * 4.Show user these orders.
	 * 5.Also show user the notes left.
	 */
	public function view($id)
	{
		$payment = $this->payments->get_payment($id);

		if (!$payment)
		{
			$this->error('Error');
			return redirect('/admin/payments/0');
		}

		$this->load->model('orders');

		$this->_data['model'] = $payment;
		$this->_data['payout_day'] = $this->_data['model']->payment_date;
		$this->_data['previous_payout_day'] = $this->get_previous_payout_date($this->_data['payout_day']);
		$this->_data['mapping'] = $this->orders->mapping();
        $this->_data['delivery_mapping'] = $this->orders->delivery_type_mapping();
		$this->_data['list'] = $this->orders->get_orders_by_date_range($this->_data['payout_day'],
		$this->_data['previous_payout_day'],
		$this->_data['model']->restaurant_id);

		$this->_data['payment_list'] = $this->process_payments($this->payments->get_paginated_payments(0, 5, [
			'restaurant_id' => $this->_data['model']->restaurant_id,
			"(payment_date<='{$this->_data['payout_day']}')" => ''
		]));

		$this->form_validation->set_rules('adjustment', 'adjustment', 'required');
		$this->form_validation->set_rules('notes', 'notes', '');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/PaymentView', $this->_data);
		}
		else
		{
			$adjustment = $this->input->post('adjustment');
			$notes = $this->input->post('notes');

			if ($this->payments->edit_payment([
				'adjustment' => (float)$adjustment,
				'notes' => $notes
			], $id))
			{
				redirect('/admin/payments/view/' . $id);
				exit;
			}
		}
	}

	public function paid($id)
	{
		$payment = $this->payments->get_payment($id);
		$this->_data['model'] = $payment;

		if (!$payment)
		{
			$this->error('Error');
			redirect('/admin/payments/0');
		}

		if ($this->payments->edit_payment([
			'status' => 1
		], $id))
		{
			redirect('/admin/payments/0');
		}
	}

	/**
	 * get previous payout date
	 *
	 * @param date $payout_day
	 * @return date
	 */
    protected function get_previous_payout_date($payout_day)
    {
		$date = new DateTime();
		$parts = explode('-', $payout_day);
		$date->setDate($parts[0], $parts[1], $parts[2]);
		$date = $date->modify('- 7 days');
		return $date->format('Y-m-d');
	}

	protected function process_payments ($data)
	{
		$this->load->model('restaurants');
		foreach ($data as $key => $payment)
		{
			$restaurant = $this->restaurants->get_restaurant($payment->restaurant_id);
			$data[$key]->restaurant_title = $restaurant->title;
		}
		return $data;
	}
}