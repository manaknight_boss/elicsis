<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Catering_menus extends Admin_Controller
{
    protected $model_file = 'catering_menu';
	public $_page_name = 'catering_menu';

	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->catering_menu->get_catering_menus();
		$this->_data['status'] = $this->catering_menu->mapping();
		$this->render('Admin/Catering_menu', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/Catering_menuAdd', $this->_data);
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$status = $this->input->post('status', TRUE);

			if ($this->catering_menu->create_catering_menu([
				'name' => $name,
				'status' => $status,

			]))
			{
				redirect('/admin/catering_menu', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo Añadir el Menú.';
				$this->render('Admin/Catering_menuAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$catering_menu = $this->catering_menu->get_catering_menu($id);
		$this->_data['model'] = $catering_menu;
		if (!$catering_menu)
		{
			$this->error('El Menú no existe o fue removido');
			redirect('/admin/catering_menu');
		}
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
        {
			$this->render('Admin/Catering_menuEdit', $this->_data);
		}
        else
        {
    		$name = $this->input->post('name', TRUE);
			$status = $this->input->post('status', TRUE);

			if ($this->catering_menu->edit_catering_menu([
				'name' => $name,
				'status' => $status,

            ], $id))
            {
				redirect('/admin/catering_menu', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo Editar el Menú.';
				$this->render('Admin/Catering_menuEdit', $this->_data);
			}
		}
	}
}