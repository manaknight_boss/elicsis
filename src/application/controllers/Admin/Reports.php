<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Reports extends Admin_Controller
{
    protected $model_file = 'orders';
    public $_page_name = 'report';


	public function order_report()
	{
		$this->load->library('report_service');
		$this->_data['mapping'] = $this->orders->mapping();
		$this->form_validation->set_rules('start_date', 'fecha de inicio', 'required|date');
		$this->form_validation->set_rules('end_date', 'Fecha final', 'required|date');
		$this->form_validation->set_rules('status', 'Estado', 'required|integer');
		$this->form_validation->set_rules('restaurant_id', 'Restaurante', 'required|integer');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/Report', $this->_data);
		}
		else
		{
			$start_date = $this->input->post('start_date', TRUE);
			$end_date = $this->input->post('end_date', TRUE);
			$status = $this->input->post('status', TRUE);
			$restaurant_id = $this->input->post('restaurant_id', TRUE);
			$orders = $this->orders->generate_order_report($start_date, $end_date, $restaurant_id, $status);

			$this->report_service->generate_csv([
				'Pedido #',
				'Fecha',
				'Horas',
				'minutos',
				'Método de pago',
				'Monto',
				'Delivery',
				'Descuento',
				'Propinas',
				'ITBIS',
				'Total',
				'Estado',
			], $orders, 'informe_' . $start_date . '_' . $end_date . '.csv');
		}
	}

	public function earning_order_report()
	{
		$this->load->library('report_service');
		$this->load->model('settings');
		$this->_data['settings'] = $this->settings->get_config_settings();
		$this->_data['mapping'] = $this->orders->mapping();
		$this->form_validation->set_rules('start_date', 'fecha de inicio', 'required|date');
		$this->form_validation->set_rules('end_date', 'Fecha de Fin', 'required|date');
		$this->form_validation->set_rules('status', 'Estado', 'required|integer');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/EarningReport', $this->_data);
		}
		else
		{
			$start_date = $this->input->post('start_date', TRUE);
			$end_date = $this->input->post('end_date', TRUE);
			$status = $this->input->post('status', TRUE);
			$earning_percentage = ((float)$this->_data['settings']['earning_cut'])/100;
			$orders = $this->orders->generate_earning_report($start_date, $end_date, $status, $earning_percentage);

			$this->report_service->generate_csv([
				'Pedido #',
				'Restaurante #',
				'Fecha',
				'Método de pago',
				'Monto',
				'Delivery',
				'Descuento',
				'Propinas',
				'ITBIS',
				'Total',
				'Estado',
				'Ganancias',
			], $orders, 'informe_' . $start_date . '_' . $end_date . '.csv');
		}
	}

	public function item_order_report()
	{
		$this->load->library('report_service');
		$this->_data['mapping'] = $this->orders->mapping();
		$this->form_validation->set_rules('start_date', 'fecha de inicio', 'required|date');
		$this->form_validation->set_rules('end_date', 'Fecha Final', 'required|date');
		$this->form_validation->set_rules('status', 'Estado', 'required|integer');
		$this->form_validation->set_rules('restaurant_id', 'Restaurante', 'required|integer');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/Report', $this->_data);
		}
		else
		{
			$start_date = $this->input->post('start_date', TRUE);
			$end_date = $this->input->post('end_date', TRUE);
			$status = $this->input->post('status', TRUE);
			$restaurant_id = $this->input->post('restaurant_id', TRUE);
			$orders = $this->orders->generate_item_order_report($start_date, $end_date, $restaurant_id, $status);

			$this->report_service->generate_csv([
				'Pedido #',
				'Fecha',
				'Producto',
				'Complemento',
				'Porciones',
				'Precio',
				'Estado',
			], $orders, 'informe_' . $start_date . '_' . $end_date . '.csv');
		}
	}

	public function metric_order_report()
	{
		$this->load->library('report_service');
		$this->load->model('restaurants');
		$this->_data['mapping'] = $this->orders->mapping();

		$this->form_validation->set_rules('submit', 'Enviar', 'required');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/MetricReport', $this->_data);
		}
		else
		{
			$restaurant_results = $this->restaurants->generate_metric_report();

			$this->report_service->generate_csv([
				'Restaurante #',
				'Confirma Rapido',
				'Entrega a Tiempo',
				'Avg. Calificación',
				'Número de Calificaciones',
			], $restaurant_results, 'informe.csv');
		}
	}
}