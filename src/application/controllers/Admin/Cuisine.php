<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Cuisine extends Admin_Controller
{
    protected $model_file = 'cuisines';
	public $_page_name = 'cuisine';
	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->cuisines->get_cuisines();
		$this->_data['status'] = $this->cuisines->mapping();
		$this->render('Admin/Cuisines', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/CuisinesAdd', $this->_data);
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$status = $this->input->post('status', TRUE);

			if ($this->cuisines->create_cuisine([
				'name' => $name,
				'status' => $status,

			]))
			{
				redirect('/admin/cuisines', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo Añadir la Gastronomia.';
				$this->render('Admin/CuisinesAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$cuisines = $this->cuisines->get_cuisine($id);
		$this->_data['model'] = $cuisines;
		if (!$cuisines)
		{
			$this->error('Gastronomias no existen');
			redirect('/admin/cuisines');
		}
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
        {
			$this->render('Admin/CuisinesEdit', $this->_data);
		}
        else
        {
    		$name = $this->input->post('name', TRUE);
			$status = $this->input->post('status', TRUE);

			if ($this->cuisines->edit_cuisine([
				'name' => $name,
				'status' => $status,

            ], $id))
            {
				redirect('/admin/cuisines', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar la Gastronomia.';
				$this->render('Admin/CuisinesEdit', $this->_data);
			}
		}
	}
}