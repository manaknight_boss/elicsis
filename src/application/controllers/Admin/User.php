<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class User extends Admin_Controller
{
	protected $model_file = 'users';
	public $_page_name = 'users';

	public function index($page)
	{
		$this->load->library('pagination');
		$this->_data['base_url'] = '/admin/users/';
		$this->_data['total_rows'] = $this->users->num_users();
		$this->_data['mapping'] = $this->users->mapping();
		$this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->users->get_paginated_users($page, $this->_data['per_page']);
		$this->_data['links'] = $this->pagination->create_links();
		return $this->render('Admin/User', $this->_data);
	}

	public function add()
	{
		// set validation rules
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('first_name', 'Nombre', 'required');
		$this->form_validation->set_rules('last_name', 'Apellido', 'required');
		$this->form_validation->set_rules('profile_type', 'Tipo de perfil', 'required');
		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('role', 'Papel', 'required|numeric');
		$this->_data['mapping'] = $this->users->mapping();

		if ($this->form_validation->run() === false)
		{
			return $this->render('Admin/UserAdd', $this->_data);
		}
		else
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$profile_type = $this->input->post('profile_type');
			$first_name = (strlen($this->input->post('first_name')) > 0) ? $this->input->post('first_name') : '';
			$last_name = (strlen($this->input->post('last_name')) > 0) ? $this->input->post('last_name') : '';
			$rnc = (strlen($this->input->post('rnc')) > 0) ? $this->input->post('rnc') : '';
			$business_name = (strlen($this->input->post('business_name')) > 0) ? $this->input->post('business_name') : '';
			$role = $this->input->post('role');
			$phone = $this->input->post('phone');
			if ($this->users->create_user_by_admin([
				'email' => $email,
				'username' => $email,
				'password' => $password,
				'image' => '',
				'stripe_id' => '',
				'reset_token' => '',
				'role_id' => $role,
				'rnc' => $rnc,
				'business_name' => $business_name,
				'phone' => $phone,
				'refer' => uniqid(),
				'type' => 'n',
				'status' => 1,
				'profile_type' => $profile_type,
				'first_name' => $first_name,
				'last_name' => $last_name
				]))
			{
				redirect('/admin/users/0', 'refresh');
			}
			else
			{
				$this->_data['error'] = 'No se pudo agregar usuario.';
				return $this->render('Admin/UserAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$user = $this->users->get_user($id);
		$this->_data['model'] = $user;
		$this->_data['mapping'] = $this->users->mapping();

		if (!$user)
		{
			$this->set_message('El usuario no existe', 'error');
			redirect('/admin/users/0');
		}

		$email_validation_rules = 'required|valid_email';

		if ($this->input->post('email') != $user->email)
		{
			$email_validation_rules .= '|is_unique[users.email]';
		}

		$this->form_validation->set_rules('email', 'email', $email_validation_rules);
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('first_name', 'Nombre', 'required');
		$this->form_validation->set_rules('last_name', 'Apellido', 'required');
		$this->form_validation->set_rules('profile_type', 'Tipo de perfil', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required');
		$this->form_validation->set_rules('password', 'Contraseña', '');
		$this->form_validation->set_rules('role', 'Papel', 'required|numeric');
		if ($this->form_validation->run() === false)
		{
			return $this->render('Admin/UserEdit', $this->_data);
		}
		else
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$profile_type = $this->input->post('profile_type');
			$first_name = (strlen($this->input->post('first_name')) > 0) ? $this->input->post('first_name') : '';
			$last_name = (strlen($this->input->post('last_name')) > 0) ? $this->input->post('last_name') : '';
			$rnc = (strlen($this->input->post('rnc')) > 0) ? $this->input->post('rnc') : '';
			$business_name = (strlen($this->input->post('business_name')) > 0) ? $this->input->post('business_name') : '';
			$role = $this->input->post('role');
			$phone = $this->input->post('phone');
			$status = $this->input->post('status');
			$payload = [
				'email' => $email,
				'password' => $password,
				'role_id' => $role,
				'status' => $status,
				'profile_type' => $profile_type,
				'business_name' => $business_name,
				'rnc' => $rnc,
				'phone' => $phone,
				'first_name' => $first_name,
				'last_name' => $last_name
			];

			if ($this->users->edit_profile($id, $payload))
			{
				redirect('/admin/users/0', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar Perfil.';
				return $this->render('Admin/UserEdit', $this->_data);
			}
		}
	}
}