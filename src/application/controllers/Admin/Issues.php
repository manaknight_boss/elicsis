<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Issues extends Admin_Controller
{
    protected $model_file = 'issue';
	public $_page_name = 'dashboard';

	public function edit($id)
	{
		$this->load->model('orders');
		$this->load->model('users');
		$issue = $this->issue->get_issue($id);
		$this->_data['model'] = $issue;

		if (!$issue)
		{
			$this->error('Reclamación no existe');
			redirect('/admin/dashboard');
		}

		$this->form_validation->set_rules('resolution', 'Resolución', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[0,1]');

		if ($this->form_validation->run() === false)
        {
			$this->render('Admin/IssueEdit', $this->_data);
		}
        else
        {
			$resolution = $this->input->post('resolution', TRUE);
			$status = $this->input->post('status', TRUE);
			$order = $this->orders->get_order($issue->order_id);
			$user = $this->users->get_user($order->user_id);

			if ($this->issue->edit_issue([
				'resolution' => $resolution,
				'status' => $status
            ], $id))
            {
				$this->orders->edit_order(['status' => 7], $issue->order_id);

				$this->_send_email_notification('resolution',  [
					'id' => $issue->order_id
				], $user->email);

				$this->_send_sms_notification('resolution',  [
					'id' => $issue->order_id
				], $order->phone);
				redirect('/admin/dashboard', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar la reclamación.';
				$this->render('Admin/IssueEdit', $this->_data);
			}
		}
	}

	private function _send_email_notification($slug, $payload, $email)
    {
		$this->load->model('emails');
		$this->load->library('mail_service');
        $this->mail_service->set_adapter('smtp');
        $email_template = $this->emails->get_template($slug, $payload);

        if ($email_template)
        {
            $from = $this->config->item('from_email');
            $this->mail_service->send($from, $email, $email_template->subject, $email_template->html);
        }
    }

	private function _send_sms_notification($slug, $payload, $to)
    {
		$this->load->model('sms');
		$this->load->library('sms_service');
        $this->sms_service->set_adapter('sms');
        $sms_template = $this->sms->get_template($slug, $payload);

        if ($sms_template)
        {
            $this->sms_service->send($to, $sms_template->content);
        }
    }
}