<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Menu_properties extends Admin_Controller
{
    protected $model_file = 'menu_property';
	public $_page_name = 'menu_propertys';
	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->menu_property->get_menu_properties();
		$this->_data['status'] = $this->menu_property->mapping();
		$this->render('Admin/Menu_property', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('is_serving', 'Porción', 'integer');
		$this->form_validation->set_rules('is_price', 'Precio', 'integer');
		$this->form_validation->set_rules('is_info', 'Detalles', 'integer');
		$this->form_validation->set_rules('is_preference', 'Preferencia', 'integer');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/Menu_propertyAdd', $this->_data);
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$is_serving = (int)$this->input->post('is_serving', TRUE);
			$is_price = (int)$this->input->post('is_price', TRUE);
			$is_info = (int)$this->input->post('is_info', TRUE);
			$is_preference = (int)$this->input->post('is_preference', TRUE);
			$status = $this->input->post('status', TRUE);

			if ($this->menu_property->create_menu_property([
				'name' => $name,
				'is_serving' => $is_serving,
				'is_price' => $is_price,
				'is_info' => $is_info,
				'is_preference' => $is_preference,
				'status' => $status

			]))
			{
				redirect('/admin/menu_propertys', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo Añadir propiedad de menú.';
				$this->render('Admin/Menu_propertyAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$menu_property = $this->menu_property->get_menu_property($id);
		if (!$menu_property)
		{
			$this->error('Propiedad de menú no existe');
			redirect('/admin/menu_propertys');
		}


		$this->_data['model'] = $menu_property;

		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('is_serving', 'Porción', 'integer');
		$this->form_validation->set_rules('is_price', 'Precio', 'integer');
		$this->form_validation->set_rules('is_info', 'Detalles', 'integer');
		$this->form_validation->set_rules('is_preference', 'Preferencia', 'integer');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
        {
			$this->render('Admin/Menu_propertyEdit', $this->_data);
		}
        else
        {
    		$name = $this->input->post('name', TRUE);
			$is_serving = (int)$this->input->post('is_serving', TRUE);
			$is_price = (int)$this->input->post('is_price', TRUE);
			$is_info = (int)$this->input->post('is_info', TRUE);
			$is_preference = (int)$this->input->post('is_preference', TRUE);
			$status = $this->input->post('status', TRUE);

			$payload = [
			];
			if ($this->menu_property->edit_menu_property([
				'name' => $name,
				'is_serving' => $is_serving,
				'is_price' => $is_price,
				'is_info' => $is_info,
				'is_preference' => $is_preference,
				'status' => $status
            ], $id))
            {
				redirect('/admin/menu_propertys', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar la propiedad de menú.';
				$this->render('Admin/Menu_propertyEdit', $this->_data);
			}
		}
	}
}