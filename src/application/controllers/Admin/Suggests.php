<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Suggests extends Admin_Controller
{
    protected $model_file = 'suggest';
    public $_page_name = 'suggests';


	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->suggest->get_suggests();
		$this->render('Admin/Suggest', $this->_data);
	}

	public function delete($id)
	{
		$suggest = $this->suggest->get_suggest($id);
		$this->_data['model'] = $suggest;
		if (!$suggest)
		{
			$this->error('sugerencia no existe');
			redirect('/admin/suggests');
			exit;
		}
		$this->suggest->delete($id);
		$this->success('remoto');
		redirect('/admin/suggests');


	}
}