<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Profile extends Admin_Controller 
{
	protected $model_file = 'users';
	public $_page_name = 'profile';

	public function index() 
	{
		$user = $this->users->get_profile($_SESSION['user_id']);
		$this->_data['model'] = $user;
		
		if (!$user) 
		{
			$this->error('Su perfil no existe');
			redirect('/admin/dashboard');
		}
		
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email'); 
		$this->form_validation->set_rules('password', 'Contraseña', '');
		
		if ($this->form_validation->run() === false) 
		{
			return $this->render('Admin/Profile', $this->_data);
		} 
		else 
		{
			$email = $this->input->post('email');
			$password = (strlen($this->input->post('password')) > 0) ? $this->input->post('password') : '';;
			$first_name = (strlen($this->input->post('first_name')) > 0) ? $this->input->post('first_name') : '';
			$last_name = (strlen($this->input->post('last_name')) > 0) ? $this->input->post('last_name') : '';

			if ($this->users->edit_profile($_SESSION['user_id'], [
				'email' => $email,
				'password' => $password,
				'first_name' => $first_name,
				'last_name' => $last_name
			])) 
			{
				redirect('/admin/dashboard', 'refresh');
			} 
			else 
			{
				$this->_data['error'] = 'No se pudo editar el perfil.';
				return $this->render('Admin/Profile', $this->_data);
			}
			
		}
	}

}