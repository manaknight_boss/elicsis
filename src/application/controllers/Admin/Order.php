<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Order extends Admin_Controller
{
    protected $model_file = 'orders';
    public $_page_name = 'order';


	public function index($page)
	{
		$this->load->model('users');
		$this->load->model('restaurants');
		$this->load->library('pagination');

		$this->_data['restaurant_id'] = ($this->input->post('restaurant_id', TRUE)) ? $this->input->post('restaurant_id', TRUE) : '';
		$this->_data['order_id'] = ($this->input->post('order_id', TRUE)) ? $this->input->post('order_id', TRUE) : '';
		$this->_data['event_date_at'] = ($this->input->post('event_date_at', TRUE)) ? $this->input->post('event_date_at', TRUE) : '';
		$this->_data['status'] = ($this->input->post('status', TRUE)) ? $this->input->post('status', TRUE) : '';
		$this->_data['phone'] = ($this->input->post('phone', TRUE)) ? $this->input->post('phone', TRUE) : '';

		$this->_data['base_url'] = '/admin/orders/';
        $this->_data['total_rows'] = $this->orders->num_orders();
        $this->_data['mapping'] = $this->orders->mapping();
        $this->_data['delivery_mapping'] = $this->orders->delivery_type_mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;

		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$this->_data['list'] = $this->orders->get_paginated_orders($page, $this->_data['per_page'], [
			'id' => $this->_data['order_id'],
			'event_date_at' => $this->_data['event_date_at'],
			'phone' => $this->_data['phone'],
			'status' => $this->_data['status'],
			'restaurant_id' => $this->_data['restaurant_id']
		]);
		$this->_data['links'] = $this->pagination->create_links();

		$this->render('Admin/Order', $this->_data);
	}

	public function process ($order_id)
	{
		$this->load->model('metric');
		$this->load->library('sms_service');
		$this->sms_service->set_adapter('sms');
		$order = $this->orders->get_order($order_id);
		$this->_data['model'] = $order;

		if (!$order)
		{
			$this->error('Error');
			return redirect('/admin/orders/0');

		}

		if (!$this->orders->is_processable($order))
		{
			$this->error('Error');
			return redirect('/admin/orders/0');
		}

		if ($this->orders->edit_order([
			'status' => 3
		], $this->_data['model']->id))
		{
			$this->success('Actualizado');
			$this->metric->update_metric('orders');
			$this->metric->update_metric('sales');
			$this->metric->update_metric('pending');
			redirect('/admin/orders/0', 'refresh');
		}
		else
		{
			$this->error('Error');
			redirect('/admin/orders/0', 'refresh');
		}
	}

	public function accept ($order_id)
	{
		$this->load->model('restaurants');
		$this->load->model('users');
		$this->load->library('azul_service');
		$this->azul_service->set_keys($this->config->item('payment_url'), $this->config->item('payment_key'));
		$order = $this->orders->get_order($order_id);

		$this->_data['model'] = $order;

		if (!$order)
		{
			$this->error('Error');
			return redirect('/admin/orders/0');

		}
		$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
		$user = $this->users->get_user($order->user_id);

		if (!$this->orders->is_acceptable($order))
		{
			$this->error('Error');
			return redirect('/admin/orders/0');
		}

		$payment_detail = json_decode($this->_data['model']->payment_data, TRUE);
		$payment_payload = array(
			'CustomOrderId' => $this->_data['model']->id,
			'Amount' => $this->_data['model']->total * 100,
			'Itbis' => $this->_data['model']->tax * 100,
			'DataVaultToken' => $payment_detail['token'],
			'CVC' => '',
			'Expiration' => '',
			'CardNumber' => '',
			'SaveToDataVault' => '0'
		);

		$sale_result = $this->azul_service->sale($payment_payload);

		if (!$sale_result)
		{
			$this->error('Hubo un error procesando el pago. Contactar Servicio al cliente para este pedido.');
			return redirect('/admin/orders/0');
		}

		if (isset($sale_result['error']))
		{
			$this->orders->edit_order([
				'status' => 8
			], $this->_data['model']->id);

			$this->_send_email_notification('cancel',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $user->email);

			$this->_send_sms_notification('cancel',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $order->phone);
			error_log($sale_result['error']['message']);
			$this->error("Ocurrio un problema con el metodo de pago del cliente, esta orden será cancelada y el cliente será notificado");
			return redirect('/admin/orders/0');
		}

		$payment_detail['response'] = $sale_result;

		if ($this->orders->edit_order([
			'status' => 2,
			'on_time_rating' => $this->_calculate_on_time_rating($order->start_on_time_at, date('Y-m-d H:i:s')),
			'end_on_time_at' => date('Y-m-d H:i:s'),
			'payment_data' => json_encode($payment_detail)
		], $this->_data['model']->id))
		{
			$this->_send_email_notification('accept',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $user->email);

			$this->_send_sms_notification('accept',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $order->phone);

			$on_time_rating = $this->orders->calculate_on_time_rating_restaurant_last_30_days($order->restaurant_id);

			$this->restaurants->edit_restaurant([
				'on_time_rating' => $on_time_rating
			], $order->restaurant_id);
			$this->success('Actualizado');
			redirect('/admin/orders/0', 'refresh');
		}
		else
		{
			$this->error('Error');
			redirect('/admin/orders/0', 'refresh');
		}
	}

    private function _send_email_notification($slug, $payload, $email)
    {
		$this->load->model('emails');
		$this->load->library('mail_service');
        $this->mail_service->set_adapter('smtp');
        $email_template = $this->emails->get_template($slug, $payload);

        if ($email_template)
        {
            $from = $this->config->item('from_email');
            $this->mail_service->send($from, $email, $email_template->subject, $email_template->html);
        }
    }

	private function _send_sms_notification($slug, $payload, $to)
    {
		$this->load->model('sms');
		$this->load->library('sms_service');
        $this->sms_service->set_adapter('sms');
        $sms_template = $this->sms->get_template($slug, $payload);

        if ($sms_template)
        {
            $this->sms_service->send($to, $sms_template->content);
        }
    }

	public function delivery ($order_id)
	{
		$this->load->model('metric');
		$this->load->model('restaurants');
		$this->load->model('users');

		$order = $this->orders->get_order($order_id);
		$this->_data['model'] = $order;

		if (!$order)
		{
			$this->error('Error');
			return redirect('/admin/orders/0');

		}

		if (!$this->orders->is_deliverable($order))
		{
			$this->error('Error');
			return redirect('/admin/orders/0');
		}

		if ($this->orders->edit_order([
			'status' => 4,
			'start_on_delivery_at' => date('Y-m-d H:i:s')
		], $this->_data['model']->id))
		{
			$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
			$user = $this->users->get_user($order->user_id);

			$this->_send_email_notification('delivering',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $user->email);

			$this->_send_sms_notification('delivering',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $order->phone);

			$this->success('Actualizado');
			$this->metric->update_metric('delivered');
			redirect('/admin/orders/0', 'refresh');
		}
		else
		{
			$this->error('Error');
			redirect('/admin/orders/0', 'refresh');
		}
	}

	public function delivered ($order_id)
	{
		$this->load->model('restaurants');
		$this->load->model('users');

		$order = $this->orders->get_order($order_id);
		$this->_data['model'] = $order;

		if (!$order)
		{
			$this->error('Error');
			return redirect('/admin/orders/0');

		}

		if (!$this->orders->is_delivered($order))
		{
			$this->error('Error');
			return redirect('/admin/orders/0');
		}

		if ($this->orders->edit_order([
			'status' => 5,
			'on_delivery_rating' => $this->_calculate_on_delivery_rating($order->start_on_delivery_at, date('Y-m-d H:i:s')),
			'end_on_delivery_at' => date('Y-m-d H:i:s')
		], $this->_data['model']->id))
		{
			$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
			$user = $this->users->get_user($order->user_id);

			$this->_send_email_notification('delivered',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $user->email);

			$this->_send_sms_notification('delivered',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $order->phone);

			$on_delivery_rating = $this->orders->calculate_on_delivery_rating_restaurant_last_30_days($order->restaurant_id);
			$this->restaurants->edit_restaurant([
				'on_delivery_rating' => $on_delivery_rating
			], $order->restaurant_id);

			$this->success('Actualizado');
			redirect('/admin/orders/0', 'refresh');
		}
		else
		{
			$this->error('Error');
			redirect('/admin/orders/0', 'refresh');
		}
	}

	public function cancel ($order_id)
	{
		$this->load->model('metric');
		$this->load->model('restaurants');
		$this->load->model('users');
		$this->load->library('azul_service');
		$this->azul_service->set_keys($this->config->item('payment_url'), $this->config->item('payment_key'));
		$order = $this->orders->get_order($order_id);
		$this->_data['model'] = $order;

		if (!$order)
		{
			$this->error('Extraviado');
			return redirect('/admin/orders/0');

		}

		if (!$this->orders->is_cancelable($order))
		{
			$this->error('Extraviado');
			return redirect('/admin/orders/0');
		}

		$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
		$user = $this->users->get_user($order->user_id);

		if ($this->orders->refund_required($order))
		{
			//
			$payment_detail = json_decode($this->_data['model']->payment_data, TRUE);
			$payment_payload = array(
				'AzulOrderId' => $payment_detail['response']['AzulOrderId'],
				'Amount' => $this->_data['model']->total * 100,
				'Itbis' => $this->_data['model']->tax * 100,
				'OriginalDate' => substr($payment_detail['response']['DateTime'], 0, 8)
			);

			$refund_result = $this->azul_service->refund($payment_payload);

			if (!$refund_result)
			{
				$this->error('El pedido no se pudo cancelar.');
				return redirect('/admin/orders/0');
			}

			if (isset($refund_result['error']))
			{
				$this->error($refund_result['error']['message']);
				return redirect('/admin/orders/0');
			}

			$payment_detail['refund_detail'] = $refund_result;

			if ($this->orders->edit_order([
				'status' => 8,
				'payment_data' => json_encode($payment_detail)
			], $this->_data['model']->id))
			{
				$this->_send_email_notification('refund',  [
					'id' => $order->id,
					'title' => $restaurant->title,
					'event_date_at' => $order->event_date_at,
					'event_time' => $this->get_time($order->event_hour, $order->event_minute),
					'event_location' => $order->address,
				], $user->email);

				$this->_send_email_notification('refund',  [
					'id' => $order->id,
					'title' => $restaurant->title,
					'event_date_at' => $order->event_date_at,
					'event_time' => $this->get_time($order->event_hour, $order->event_minute),
					'event_location' => $order->address,
				], $restaurant->email);

				$this->_send_sms_notification('refund',  [
					'id' => $order->id,
					'title' => $restaurant->title,
					'event_date_at' => $order->event_date_at,
					'event_time' => $this->get_time($order->event_hour, $order->event_minute),
					'event_location' => $order->address,
				], $order->phone);

				$this->success('Pedido ha sido cancelado exitosamente!');
				$this->metric->update_metric('cancel');
				redirect('/admin/orders/0', 'refresh');
			}
			else
			{
				$this->error('El pedido no se pudo cancelar.');
				return redirect('/admin/orders/0');
			}
			//
		}
		else
		{
			if ($this->orders->edit_order([
				'status' => 8
			], $this->_data['model']->id))
			{
				$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
				$user = $this->users->get_user($order->user_id);

				$this->_send_email_notification('cancel',  [
					'id' => $order->id,
					'title' => $restaurant->title,
					'event_date_at' => $order->event_date_at,
					'event_time' => $this->get_time($order->event_hour, $order->event_minute),
					'event_location' => $order->address,
				], $user->email);

				$this->_send_sms_notification('cancel',  [
					'id' => $order->id,
					'title' => $restaurant->title,
					'event_date_at' => $order->event_date_at,
					'event_time' => $this->get_time($order->event_hour, $order->event_minute),
					'event_location' => $order->address,
				], $order->phone);

				$this->success('Cancelado Exitosamente');
				$this->metric->update_metric('cancel');
				redirect('/admin/orders/0', 'refresh');
			}
			else
			{
				$this->error('Error');
				redirect('/admin/orders/0', 'refresh');
			}
		}
	}

	public function refund ($order_id)
	{
		$this->load->model('metric');
		$this->load->model('restaurants');
		$this->load->model('users');
		$this->load->library('azul_service');
		$this->azul_service->set_keys($this->config->item('payment_url'), $this->config->item('payment_key'));

		$order = $this->orders->get_order($order_id);
		$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
		$user = $this->users->get_user($order->user_id);
		$this->_data['model'] = $order;

		if (!$order)
		{
			$this->error('Extraviado');
			return redirect('/admin/orders/0');

		}

		if (!$this->orders->is_refundable($order))
		{
			$this->error('Extraviado');
			return redirect('/admin/orders/0');
		}

		$payment_detail = json_decode($this->_data['model']->payment_data, TRUE);
		$payment_payload = array(
			'AzulOrderId' => $payment_detail['response']['AzulOrderId'],
			'Amount' => $this->_data['model']->total * 100,
			'Itbis' => $this->_data['model']->tax * 100,
			'OriginalDate' => substr($payment_detail['response']['DateTime'], 0, 8)
		);

		$refund_result = $this->azul_service->refund($payment_payload);
		error_log(print_r($payment_payload, TRUE));
		error_log(print_r($refund_result, TRUE));
		if (!$refund_result)
		{
			$this->error('El Reembolso no fue procesado. Contactar Azul para soporte con este pedido.');
			return redirect('/admin/orders/0');
		}

		if (isset($refund_result['error']))
		{
			$this->error($refund_result['error']['message']);
			return redirect('/admin/orders/0');
		}

		$payment_detail['refund_detail'] = $refund_result;

		if ($this->orders->edit_order([
			'status' => 9,
			'payment_data' => json_encode($payment_detail)
		], $this->_data['model']->id))
		{
			$this->_send_email_notification('refund',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $user->email);

			$this->_send_email_notification('refund',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $restaurant->email);

			$this->_send_sms_notification('refund',  [
				'id' => $order->id,
				'title' => $restaurant->title,
				'event_date_at' => $order->event_date_at,
				'event_time' => $this->get_time($order->event_hour, $order->event_minute),
				'event_location' => $order->address,
			], $order->phone);


			$this->load->model('issue');

			if (!$this->issue->is_issue_exist($order->id))
			{
				$this->issue->create_issue([
					'order_id' => $order->id,
					'notes' => 'Orden de problemas',
					'resolution' => 'usuario reembolsado',
					'status' => 1,
					'user_id' => $order->user_id,
					'restaurant_id' => $order->restaurant_id
				]);
			}

			$this->success('Reembolso exitoso');
			$this->metric->update_metric('refund');
			redirect('/admin/orders/0', 'refresh');
		}
		else
		{
			$this->error('Error');
			redirect('/admin/orders/0', 'refresh');
		}
	}

	private function isJson($string)
	{
    // decode the JSON data
    $result = json_decode($string);

    // switch and check possible JSON errors
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $error = ''; // JSON is valid // No error has occurred
            break;
        case JSON_ERROR_DEPTH:
            $error = 'The maximum stack depth has been exceeded.';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $error = 'Invalid or malformed JSON.';
            break;
        case JSON_ERROR_CTRL_CHAR:
            $error = 'Control character error, possibly incorrectly encoded.';
            break;
        case JSON_ERROR_SYNTAX:
            $error = 'Syntax error, malformed JSON.';
            break;
        // PHP >= 5.3.3
        case JSON_ERROR_UTF8:
            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_RECURSION:
            $error = 'One or more recursive references in the value to be encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_INF_OR_NAN:
            $error = 'One or more NAN or INF values in the value to be encoded.';
            break;
        case JSON_ERROR_UNSUPPORTED_TYPE:
            $error = 'A value of a type that cannot be encoded was given.';
            break;
        default:
            $error = 'Unknown JSON error occured.';
            break;
    }

		if ($error !== '')
		{
        // throw the Exception or exit // or whatever :)
				error_log($error);
				return FALSE;
    }

    // everything is OK
    return $result;
	}

	public function view($id)
	{
		$order = $this->orders->get_order($id);

		if (!$order)
		{
			$this->error('Error');
			return redirect('/admin/orders/0');
		}

		$this->_data['mapping'] = $this->orders->mapping();
		$this->_data['delivery_mapping'] = $this->orders->delivery_type_mapping();
		$this->_data['model'] = $this->process_data([$order])[0];
		$this->_data['cancelable'] = $this->orders->is_cancelable($order);
		$this->_data['data'] = json_decode($this->_data['model']->data, TRUE);
		$this->_data['rnc'] = isset($this->_data['data']['address']['rnc']) ? $this->_data['data']['address']['rnc'] : '';

		$this->load->view('Admin/OrderView', $this->_data);
	}

	public function edit ($order_id)
	{
		$order = $this->orders->get_order($order_id);

		if (!$order)
		{
			$this->error('El pedido no esta disponible!');
			return redirect('/admin/orders/0');
		}

		$this->_data['model'] = $order;

		$this->form_validation->set_rules('notes', 'Notas', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->render('Admin/OrderEdit', $this->_data);
        }
        else
        {
            $notes = $this->input->post('notes');

            $this->orders->edit_order([
				'notes' => $notes
            ], $order_id);

			$this->success('Actualizado');
            return redirect('/admin/orders/view/' . $order_id);
        }
	}

	private function process_data ($data)
	{
		$this->load->model('restaurants');
		foreach ($data as $key => $order)
		{
			$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
			$data[$key]->restaurant = $restaurant;
		}
		return $data;
	}

	private function _calculate_on_delivery_rating($start, $end)
	{
		$start_time_seconds = strtotime($start);
		$end_time_seconds = strtotime($end);
		$start_time_minutes = $start_time_seconds / 60;
		$end_time_minutes = $end_time_seconds / 60;
		$elapse_minutes = $end_time_minutes - $start_time_minutes;
		return $this->orders->grade_on_delivery($elapse_minutes);
	}

	private function _calculate_on_time_rating($start, $end)
	{
		$start_time_seconds = strtotime($start);
		$end_time_seconds = strtotime($end);
		$start_time_minutes = $start_time_seconds / 60;
		$end_time_minutes = $end_time_seconds / 60;
		$elapse_minutes = $end_time_minutes - $start_time_minutes;
		return $this->orders->grade_on_time($elapse_minutes);
	}

	private function get_time ($hour, $minute)
	{
		if ($minute == 0) {
			$minute = '00';
		}

		$minute_format = ":{$minute}";

		if ($hour == 24 || $hour == 0) {
			return '12' . $minute_format . ' AM';
		}
		if ($hour == 12) {
			return '12' . $minute_format . ' PM';
		}
		if ($hour < 12) {
			return $hour . $minute_format . ' AM';
		}

		if ($hour > 12) {
			return ($hour % 12) . $minute_format . ' PM';
		}
	}
}