<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';

class Email extends Admin_Controller
{
	protected $model_file = 'emails';
	public $_page_name = 'email';

	public function index()
	{
		$this->_data['list'] = $this->emails->get_emails();
		return $this->render('Admin/Email', $this->_data);
	}

	public function add()
	{
		// set validation rules
		$this->form_validation->set_rules('slug', 'Nombre', 'required|is_unique[emails.slug]');
		$this->form_validation->set_rules('subject', 'Tema', 'required');
		$this->form_validation->set_rules('tag', 'Etiquetas', 'required');
		$this->form_validation->set_rules('html', 'HTML', 'required');

		if ($this->form_validation->run() === false) {
			return $this->render('Admin/EmailAdd', $this->_data);
		}
		else
		{
			$slug = strtolower($this->input->post('slug'));
			$subject = $this->input->post('subject');
			$tag = $this->input->post('tag');
			$html = $this->input->post('html');

			if ($this->emails->create_email([
				'slug' => $slug,
				'tags' => $tag,
				'subject' => $subject,
				'html' => $html
			]))
			{
				redirect('/admin/emails', 'refresh');
			}
			else
			{
				$this->_data['error'] = 'No se Añadio el corréo.';
				return $this->render('Admin/EmailAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$email = $this->emails->get_email($id);
		$this->_data['model'] = $email;

		if (!$email)
		{
			$this->error('no se puede encontrar el correo electrónico');
			redirect('/admin/emails');
		}

		// set validation rules
		$this->form_validation->set_rules('slug', 'Nombre', 'required');
		$this->form_validation->set_rules('subject', 'Tema', 'required');
		$this->form_validation->set_rules('html', 'HTML', 'required');

		if ($this->form_validation->run() === false)
		{
			return $this->render('Admin/EmailEdit', $this->_data);
		}
		else
		{
			$slug = strtolower($this->input->post('slug'));
			$subject = $this->input->post('subject');
			$html = $this->input->post('html');

			if ($this->emails->edit_email([
				'slug' => $slug,
				'subject' => $subject,
				'html' => $html
			], $id))
			{
				redirect('/admin/emails', 'refresh');
			}
			else
			{
				$this->_data['error'] = 'No se pudo editar el corréo.';
				return $this->render('Admin/EmailEdit', $this->_data);
			}
		}
	}
}