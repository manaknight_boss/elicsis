<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Admin_Controller.php';
class Preference extends Admin_Controller
{
	protected $model_file = 'preferences';
	public $_page_name = 'preference';

	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->preferences->get_preferences();
		$this->_data['status'] = $this->preferences->mapping();
		$this->render('Admin/Preferences', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('icon', 'Icono', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
		{
			$this->render('Admin/PreferencesAdd', $this->_data);
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$icon = $this->input->post('icon', TRUE);
			$status = $this->input->post('status', TRUE);

			if ($this->preferences->create_preference([
				'name' => $name,
				'icon' => $icon,
				'status' => $status
			]))
			{
				redirect('/admin/preferences', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo añadir la preferencia.';
				$this->render('Admin/PreferencesAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$preferences = $this->preferences->get_preference($id);
		$this->_data['model'] = $preferences;
		if (!$preferences)
		{
			$this->error('Preferencias no existen');
			redirect('/admin/preferences');
		}
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('icon', 'Icono', 'required');
		$this->form_validation->set_rules('status', 'Estado', 'required|in_list[1,0]');

		if ($this->form_validation->run() === false)
        {
			$this->render('Admin/PreferencesEdit', $this->_data);
		}
        else
        {
    		$name = $this->input->post('name', TRUE);
			$icon = $this->input->post('icon', TRUE);
			$status = $this->input->post('status', TRUE);

			if ($this->preferences->edit_preference([
				'name' => $name,
				'icon' => $icon,
				'status' => $status,

            ], $id))
            {
				redirect('/admin/preferences', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar la preferencia.';
				$this->render('Admin/PreferencesEdit', $this->_data);
			}
		}
	}
}