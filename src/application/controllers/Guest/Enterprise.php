<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Guest_Controller.php';
class Enterprise extends Guest_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('new_catering');

        $this->form_validation->set_rules('first_name', 'Nombres', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');
		$this->form_validation->set_rules('business_name', 'Nombre del Negocio', 'required');
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');
		$this->form_validation->set_rules('comment', 'Commentario', '');

		if ($this->form_validation->run() === false)
		{
			$this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Guest/Enterprise', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
		}
		else
		{
    		$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
			$business_name = $this->input->post('business_name', TRUE);
			$email = $this->input->post('email', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$comment = '';

			if ($this->new_catering->create_new_catering([
				'first_name' => $first_name,
				'last_name' => $last_name,
				'business_name' => $business_name,
				'email' => $email,
				'phone' => $phone,
                'comment' => $comment,
                'type' => 1,
                'zip' => '',
                'url' => ''
			]))
			{
				$this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Guest/Thankyous', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo agregar el restaurante.';
                $this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Guest/Enterprise', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
			}
		}
    }

}
