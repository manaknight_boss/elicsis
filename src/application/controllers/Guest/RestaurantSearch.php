<?php

use function GuzzleHttp\json_encode;
include_once dirname(__FILE__) . '/../../core/Guest_Controller.php';
class RestaurantSearch extends Guest_Controller
{
    public $_page_name ='search';

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
    {
        $search = !empty($_SESSION['search']) ? $_SESSION['search'] : FALSE;
        if ($search == FALSE)
        {
            redirect('/');
        }
        else
        {
            $this->load->model('settings');
            $this->load->model('cuisines');
            $this->load->model('restaurants');
            $this->load->model('preferences');
            $this->load->library('search_service');
            $this->_data['preferences'] = $this->preferences->get_preferences(TRUE);
            $this->_data['cuisines'] = $this->cuisines->get_cuisines(TRUE);
            $this->_data['setting'] = $this->settings->get_config_settings();
            $this->_data['pricing'] = json_decode($this->_data['setting']['price'], TRUE);
            $this->_data['delivery_minimum'] = json_decode($this->_data['setting']['delivery_minimum'], TRUE);
            $this->_data['distance'] = json_decode($this->_data['setting']['distance'], TRUE);
            $this->_data['rating'] = json_decode($this->_data['setting']['rating'], TRUE);
            $this->_data['num_restaurant'] = 0;
            $this->_data['data'] = $this->search_service->transform_data($_SESSION['search']);
            $this->_data['enable_spa'] = TRUE;
            $this->_data['google_callback'] = 'initAutocompleteFilter';
            $this->dl('search', $this->_data['data']);
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Guest/Search');
            $this->load->view('Layout/GuestFooter', $this->_data);
        }
    }

    /**
     * 1. Get filter object, Save them into local variables
     * 2. Get paginated variables
     * 3. Call Query API to find restaurants
     * 4. Return Results
     */
    public function search ()
    {
        $this->load->library('search_service');
        $this->load->model('restaurants');
        //Step 1
        $cuisine_id = $this->input->post('cuisine_id', TRUE);
        $delivery = $this->input->post('delivery', TRUE);
        $distance = $this->input->post('distance', TRUE);
        $lat = $this->input->post('lat', TRUE);
        $lng = $this->input->post('lng', TRUE);
        $order_type = $this->input->post('order_type', TRUE);
        $preference = $this->input->post('preference', TRUE);
        $price = $this->input->post('price', TRUE);
        $day = $this->input->post('day', TRUE);
        $time = (float)$this->input->post('time', TRUE);
        $rating = $this->input->post('rating', TRUE);
        $search = $this->input->post('search', TRUE);
        $serving = $this->input->post('serving', TRUE);

        //Step 2
        $page = $this->input->post('page', TRUE) ? (int)$this->input->post('page', TRUE) : 0;

        //Step 3
        $this->search_service->init($this->restaurants);

        $results = $this->search_service->query($page, [
            'cuisine_id' => $cuisine_id,
            'delivery' => $delivery,
            'distance' => $distance,
            'lat' => $lat,
            'lng' => $lng,
            'order_type' => $order_type,
            'preference' => $preference,
            'price' => $price,
            'day' => $day,
            'time' => $time,
            'rating' => $rating,
            'search' => $search,
            'serving' => $serving
        ]);

        //Step 4
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($results));
    }

    public function restaurant ($id)
    {
        $this->load->model('restaurants');
        $this->load->model('catering_menu');
        $this->load->model('preferences');
        $this->load->model('menu');
        $this->load->model('settings');
        $this->load->model('orders');
        $this->load->model('customer_restaurant');
        $this->load->model('menu_addon');
        $this->load->model('menu_property');
        $this->load->library('search_service');
        $this->load->library('menu_service');
        $this->search_service->init($this->restaurants);

        $restaurant = $this->restaurants->get_restaurant($id);
        $search = !empty($_SESSION['search']) ? $_SESSION['search'] : FALSE;
        $this->_data['distance'] = 'Unknown';

        if (!$restaurant)
        {
            return redirect('/');
        }

        if (!$search)
        {
            return redirect('/');
        }

        $this->_data['data'] = $this->search_service->transform_data($search);
        $this->_data['restaurant'] = $restaurant;
        $this->_data['restaurant_config'] = [];

        if ($search && $search['lat'] && $search['lat'])
        {
            $this->_data['distance'] = round($this->search_service->calculate_distance($search['lat'],
             $search['lng'],
             $restaurant->lat,
             $restaurant->long,
             'km'), 2) . ' KM';
        }

        /**
         * Steps:
         * 1.Get Catering Menu List, Get all Menus, Join All tables you need
         * 2.Get Cart object
         * 3.Set flag if date, time set or not, Set time and date
         */
        $this->menu_service->init($this->catering_menu,
        $this->menu,
        $this->menu_addon,
        $this->menu_property,
        $this->preferences,
        $restaurant,
        $this->settings,
        $this->orders,
        $this->customer_restaurant);
        //1
        $this->_data['restaurant_config'] = $this->menu_service->build_config();
        $this->_data['search'] = $search;
        $this->_data['schedule'] = $this->restaurants->get_schedule($restaurant);
        $this->load->view('Layout/GuestHeader', $this->_data);
        $this->load->view('Guest/Restaurant', $this->_data);
        $this->load->view('Layout/GuestFooter', $this->_data);
    }

    public function save_search ()
    {
        $cuisine_id = (int)$this->input->post('cuisine_id', TRUE);
        $delivery = (int)$this->input->post('delivery', TRUE);
        $distance = (int)$this->input->post('distance', TRUE);
        $order_type = $this->input->post('order_type', TRUE);
        $price = $this->input->post('price', TRUE);
        $day = $this->input->post('day', TRUE);
        $date = $this->input->post('date', TRUE);
        $time = $this->input->post('time', TRUE);
        $rating = $this->input->post('rating', TRUE);
        $search = $this->input->post('search', TRUE);
        $serving = $this->input->post('serving', TRUE);

        if ($cuisine_id  !== NULL)
        {
            $_SESSION['search']['cuisine_id'] = $cuisine_id;
        }

        if ($distance !== NULL)
        {
            $_SESSION['search']['distance'] = $distance;
        }

        if ($order_type !== NULL)
        {
            $_SESSION['search']['order_type'] = $order_type;
        }

        if ($delivery !== NULL)
        {
            $_SESSION['search']['delivery'] = $delivery;
        }

        if ($price !== NULL)
        {
            $_SESSION['search']['price'] = $price;
        }

        if ($day !== NULL)
        {
            $_SESSION['search']['day'] = $day;
        }

        if ($date !== NULL)
        {
            $_SESSION['search']['date'] = $date;
        }

        if ($time !== NULL)
        {
            $_SESSION['search']['time'] = $time;
        }

        if ($rating !== NULL)
        {
            $_SESSION['search']['rating'] = $rating;
        }

        if ($search !== NULL)
        {
            $_SESSION['search']['search'] = $search;
        }

        if ($serving !== NULL)
        {
            $_SESSION['search']['serving'] = $serving;
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(['success' => TRUE]));
    }
}
