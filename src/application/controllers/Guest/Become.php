<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Guest_Controller.php';
class Become extends Guest_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('new_catering');

        $this->form_validation->set_rules('first_name', 'Nombres', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');
		$this->form_validation->set_rules('business_name', 'Nombre del Negocio', 'required');
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'telefono', 'required');
		$this->form_validation->set_rules('url', 'Pagina Web', '');
		$this->form_validation->set_rules('zip', 'Codigo Postal', 'required');
		$this->form_validation->set_rules('comment', 'Commentario', '');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Guest/Become', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
		}
		else
		{
    		$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
			$business_name = $this->input->post('business_name', TRUE);
			$email = $this->input->post('email', TRUE);
			$phone = $this->input->post('phone', TRUE);
			$comment = $this->input->post('comment', TRUE);
			$url = $this->input->post('url', TRUE);
			$zip = $this->input->post('zip', TRUE);

			if ($this->new_catering->create_new_catering([
				'first_name' => $first_name,
				'last_name' => $last_name,
				'business_name' => $business_name,
				'email' => $email,
				'phone' => $phone,
                'comment' => $comment,
                'type' => 2,
                'zip' => $zip,
                'url' => $url
			]))
			{
				$this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Guest/Thankyous', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo añadir afiliado.';
                $this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Guest/Become', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
			}
		}
    }

}
