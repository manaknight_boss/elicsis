<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('users');
        $data = [
            'google_key' => $this->config->item('google_key'),
            'page_name' => 'tos',
            'loggedin' => FALSE
        ];

        if (isset($_SESSION['user_id']) && isset($_SESSION['email']) &&
        $_SESSION['role'] == (string) $this->users->MEMBER)
        {
            $data['loggedin'] = TRUE;
        }

        $this->load->view('Layout/GuestHeader', $data);
        $this->load->view('Guest/Tos');
        $this->load->view('Layout/GuestFooter', $data);
    }

}
