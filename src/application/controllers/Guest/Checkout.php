<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Guest_Controller.php';
class Checkout extends Guest_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        /**
         * Steps:
         * 1.Get Cart from frontend
         * 2.Create cart
         * 3.Set the current step
         * 4.return the cart id
         * 5.Frontend redirect to checkout page
         * 6.Ask user to login
         * 7.If not user, register for them, redirect to next checkout page(Member Controller this time)
         * 8.Add Event Time, save into cart , redirect to next stage
         * 9.Add Address, save into cart , redirect to next stage
         * 10.Add Payment, save into cart , redirect to next stage
         * 11.Complete, save into cart link to member dashboard
         * 12.Implement back feature
         */
        $this->load->model('cart');
        $restaurant_id = (int)$this->input->post('restaurant_id', TRUE);
        $delivery_fee = (int)$this->input->post('delivery_fee', TRUE);
        $is_delivery = $this->input->post('is_delivery', TRUE);
        $items = $this->input->post('items', TRUE);
        $real_tips = (float)$this->input->post('real_tips', TRUE);
        $tips = $this->input->post('tips', TRUE);
        $tax = (float)$this->input->post('tax', TRUE);
        $total = (float)$this->input->post('total', TRUE);
        $date = $this->input->post('date', TRUE);
        $time = (integer)$this->input->post('time', TRUE);

        $this->form_validation->set_rules('restaurant_id', 'ID Restaurant', 'required');
		$this->form_validation->set_rules('delivery_fee', 'Delivery', 'required|integer');
		$this->form_validation->set_rules('is_delivery', 'Delivery', 'required|integer');
		$this->form_validation->set_rules('items', 'Artículos', '');
		$this->form_validation->set_rules('real_tips', 'Propinas', 'required|numeric');
		$this->form_validation->set_rules('tips', 'Propinas', 'required|numeric');
		$this->form_validation->set_rules('tax', 'ITBIS', 'required|numeric');
		$this->form_validation->set_rules('total', 'Total', 'required|numeric');
		$this->form_validation->set_rules('date', 'Fesha', 'required');
		$this->form_validation->set_rules('time', 'Hora', 'required');

		if ($this->form_validation->run() === FALSE)
		{
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'errors' => $this->form_validation->error_array(),
                'success' => FALSE
            ]));
        }

        $cart_id = $this->cart->create_cart([
            'is_login' => 0,
            'is_day_time' => 0,
            'is_address' => 0,
            'is_payment' => 0,
            'is_complete' => 0,
            'user_id' => 0,
            'restaurant_id' => $restaurant_id,
            'payment_data' => '{}',
            'data' => json_encode([
                'delivery_fee' => $delivery_fee,
                'is_delivery' => $is_delivery,
                'items' => $items,
                'real_tips' => $real_tips,
                'tips' => $tips,
                'total' => $total,
                'tax' => $tax,
                'date' => $date,
                'time' => $time,
                'minute' => (is_numeric( $time ) && floor( $time ) != $time) ? 30 : 0
            ])
        ]);

        if ($cart_id)
        {
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode([
                'success' => FALSE,
                'id' => $cart_id
            ]));
        }
        else
        {
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode(['success' => FALSE]));
        }

    }

    public function step_1 ($cart_id)
    {
        $this->load->library('google_service');
        $this->google_service->init();
        $this->_data['google_auth_url'] = $this->google_service->make_auth_url();
        $this->load->model('cart');
        $cart = $this->cart->get_cart($cart_id);

        if (!$cart)
        {
            return redirect('/');
        }

        if ($this->_data['loggedin'])
        {
            if ($this->cart->edit_cart([
                'is_login' => 1,
                'user_id' => $_SESSION['user_id']
            ], $cart_id))
            {

                return redirect('/checkout/2/' . $cart_id);
            }
        }

        $create_account_type = $this->input->post('create_account_type', TRUE);

        $_SESSION['checkout_flow_google_login'] = $cart_id;

        if (!$create_account_type || $create_account_type == '')
        {
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Guest/Step_1', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
        }

        if ($create_account_type && $create_account_type == '1')
        {
            $this->process_existing_customer($cart_id);
        }

        if ($create_account_type && $create_account_type == '2')
        {
            $this->process_new_customer($cart_id);
        }


    }

    private function process_existing_customer ($cart_id)
    {
        $this->form_validation->set_rules('email', 'Corréo', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Guest/Step_1', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
        }
        else
        {
            // set variables from the form
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            if ($this->users->authenticate($email, $password, FALSE))
            {
                $user_id = $this->users->get_user_id_from_email($email);
                $user = $this->users->get_user($user_id);

                $_SESSION['user_id'] = (int) $user->id;
                $_SESSION['email'] = (string) $user->email;
                $_SESSION['role'] = $user->role_id;
                $this->cart->edit_cart([
                    'is_login' => 1,
                    'user_id' => $user->id
                ], $cart_id);
                return redirect('/checkout/2/' . $cart_id);
            }
            else
            {
                $this->_data['error'] = 'Correo o contraseña invalido.';
                $this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Guest/Step_1', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
            }
        }

    }

    private function process_new_customer ($cart_id)
    {
        $this->load->model('metric');
        $this->load->model('users');
        $this->load->model('emails');
        $this->load->library('mail_service');

        $this->form_validation->set_rules('email', 'Corréo', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');
        $this->form_validation->set_rules('first_name', 'Nombre', 'required');
        $this->form_validation->set_rules('last_name', 'Apellidos', 'required');
        $this->form_validation->set_rules('phone', 'Teléfono', 'required');
        if ($this->form_validation->run() == FALSE)
		{
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Guest/Step_1', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
		}
		else
		{
			// set variables from the form
			$email = $this->input->post('email');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$phone = $this->input->post('phone');
            $password = $this->input->post('password');
            if (substr($phone, 0, 1) != '1')
            {
                $phone = '1' . $phone;
            }
            $user = $this->users->create_user($email, $password, $this->users->MEMBER, $email, 'n', $first_name, $last_name, '', $phone);

			if ($user)
			{
				$_SESSION['user_id']      = (int)$user;
				$_SESSION['email']        = (string)$email;
                $_SESSION['role']         = $this->users->MEMBER;
                $this->cart->edit_cart([
                    'is_login' => 1,
                    'user_id' => $user
                ], $cart_id);
                $this->mail_service->set_adapter('smtp');
                $email_template = $this->emails->get_template('guest-register', [
                    'email' => $email,
                    'password' => $password
                ]);

                if ($email_template)
                {
                    $from = $this->config->item('from_email');
                    $this->mail_service->send($from, $email, $email_template->subject, $email_template->html);
                }
                $this->metric->update_metric('users');
				return redirect('/checkout/2/' . $cart_id);
			}
			else
			{
				$this->_data['error'] = 'Corréo o contraseña invalidos.';
                $this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Guest/Step_1', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
			}
		}
    }
}
