<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Guest_Controller.php';
class Faq extends Guest_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('Layout/GuestHeader', $this->_data);
        $this->load->view('Guest/Faq');
        $this->load->view('Layout/GuestFooter', $this->_data);
    }

}
