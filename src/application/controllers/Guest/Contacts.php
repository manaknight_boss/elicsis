<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('contact');

		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('email', 'Correo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('comment', 'Commentario', 'required');

		if ($this->form_validation->run() === FALSE)
		{
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'errors' => $this->form_validation->error_array(),
                'success' => FALSE
            ]));
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$email = $this->input->post('email', TRUE);
			$comment = $this->input->post('comment', TRUE);

			if ($this->contact->create_contact([
				'name' => $name,
				'email' => $email,
				'comment' => $comment
			]))
			{
                return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode([
					'success' => TRUE
				]));
			}
			else
			{
				return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode([
					'success' => FALSE
				]));
			}
		}
    }

}
