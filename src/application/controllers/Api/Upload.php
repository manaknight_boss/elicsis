<?php
class Upload extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function ajax_multi_upload()
	{
		if(isset($_FILES['attachments']['name']))
		{
            $this->load->library('upload');
            $files = $_FILES;
            $number_of_files_uploaded = count($_FILES['attachments']['name']);
            $attachments = [];
            for ($i = 0; $i < $number_of_files_uploaded; $i++)
            {
                $_FILES['attachments' . $i]['name']     = $files['attachments']['name'][$i];
                $_FILES['attachments' . $i]['type']     = $files['attachments']['type'][$i];
                $_FILES['attachments' . $i]['tmp_name'] = $files['attachments']['tmp_name'][$i];
                $_FILES['attachments' . $i]['error']    = $files['attachments']['error'][$i];
                $_FILES['attachments' . $i]['size']     = $files['attachments']['size'][$i];

                $config = [
                    'upload_path' => $_SERVER['DOCUMENT_ROOT'] . '/uploads/',
                    'file_name' => uniqid('mkd_'),
                    'allowed_types' => 'jpg|jpeg|png|gif|pdf',
                    // 'max_size' => 3000
                ];
                $this->upload->initialize($config);

                if(!$this->upload->do_upload('attachments' . $i))
                {
                    $attachments[] = [
                        'error' => $this->upload->display_errors()
                    ];
                }
                else
                {
                    $data = [
                        'upload_data' => $this->upload->data()
                    ];
                    $attachments[] = [
                        'name' => base_url() . 'uploads/' . $data['upload_data']['file_name'],
                        'original' => $data['upload_data']['client_name']
                    ];
                }
            }

            return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode($attachments));
		}
    }

	public function ajax_single_upload()
	{
		if(isset($_FILES['attachments']['name']))
		{
			$config = [
				'upload_path' => $_SERVER['DOCUMENT_ROOT'] . '/uploads/',
				'file_name' => uniqid('mkd_'),
				'allowed_types' => 'jpg|jpeg|png|gif|pdf',
				// 'max_size' => 3000
			];
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('attachments'))
			{
				$error = array('error' => $this->upload->display_errors());
				return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode($error));
			}
			else
			{
                $data = [
                    'upload_data' => $this->upload->data()
                ];

                $data['url'] = base_url() . 'uploads/' . $data['upload_data']['file_name'];

				return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode($data));
			}
		}
	}

	public function ajax_single_upload_blob()
	{
		if(isset($_FILES['attachments']['name']))
		{
			$config = [
				'upload_path' => $_SERVER['DOCUMENT_ROOT'] . '/uploads/',
				'file_name' => uniqid('mkd_'),
				'allowed_types' => 'jpg|jpeg|png|gif|pdf|image\/png|image/png',
				// 'max_size' => 3000
			];
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('attachments'))
			{
				$error = array('error' => $this->upload->display_errors());
				return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode($error));
			}
			else
			{
                $data = [
                    'upload_data' => $this->upload->data()
				];

                $data['url'] = '/uploads/' . $data['upload_data']['file_name'];

				return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode($data));
			}
		}
	}

	public function image ($type)
	{
		$this->load->model('restaurants');
		$id = $this->input->post('id', TRUE);

		$restaurants = $this->restaurants->get_restaurant($id);
		$this->form_validation->set_rules($type, ucfirst($type), 'required');

		if ($this->form_validation->run() === FALSE)
        {
			$error = array('error' => $this->form_validation->error_array());
			return $this->output->set_content_type('application/json')
			->set_status_header(200)
			->set_output(json_encode($error));
		}
        else
        {
			$payload = [];
			$payload[$type] = $this->input->post($type, TRUE);
			$result = $this->restaurants->edit_restaurant($payload, $id);
			if ($result)
			{
				return $this->output->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode([
					'error' => FALSE
				]));
			}
			else
			{
				return $this->output->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode([
					'error' => TRUE
				]));
			}
		}
	}

	public function menu ()
	{
		$this->load->model('menu');
		$id = $this->input->post('id', TRUE);
		$this->form_validation->set_rules('image', 'Image', 'required');

		if ($this->form_validation->run() === FALSE)
        {
			$error = array('error' => $this->form_validation->error_array());
			return $this->output->set_content_type('application/json')
			->set_status_header(200)
			->set_output(json_encode($error));
		}
        else
        {
			$payload = [];
			$payload['image'] = $this->input->post('image', TRUE);
			$result = $this->menu->edit_menu($payload, $id);
			if ($result)
			{
				return $this->output->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode([
					'error' => FALSE
				]));
			}
			else
			{
				return $this->output->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode([
					'error' => TRUE
				]));
			}
		}
	}
}
