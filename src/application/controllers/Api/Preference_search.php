<?php
class Preference_search extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('preferences');
	}

	public function index()
    {
        if (strlen($_SESSION['email']) > 0)
        {
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array_values($this->preferences->get_preferences(TRUE))));
        }
    }
}
