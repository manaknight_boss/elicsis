<?php
class Payment_cron extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

    /**
     * Steps
     * 1.Load Settings
     * 2.Get payout day
     * 3.Loop through catering
     * 4.See if payment row exist. If exist, next
     * 5.See if payment row exist. If not exist, find all orders between those days
     * count them up
     * 6.Save payment record
     */
	public function index()
    {
        $this->load->model('settings');
        $this->load->model('orders');
        $this->load->model('restaurants');
        $this->load->model('payments');
        $this->load->library('payout_service');
        $this->payout_service->init($this->settings, $this->restaurants, $this->payments, $this->orders);
        $settings = $this->settings->get_config_settings();

        $restaurant_id = $this->input->get('restaurant_id', TRUE);
        $today = date('w', time());
        if ($today < 1 || $today > 5)
        {
            echo json_encode([
                'success' => FALSE,
                'message' => 'only runs on week days'
            ]);
            exit;
        }
        if ($restaurant_id)
        {
            $data = $this->payout_service->single_execute($restaurant_id, $settings['payout_day_of_week'], $settings['earning_cut']);
        }
        else
        {
            $data = $this->payout_service->execute($settings['payout_day_of_week'], $settings['earning_cut']);
        }
        echo json_encode($data);
        exit;
    }
}
