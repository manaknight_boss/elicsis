<?php 
class User extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
	}
	
	public function index($email)
    {
        if (strlen($_SESSION['email']) > 0) 
        {
            $this->load->model('users');
            if (strlen($email) < 2) 
            {
                return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode(array()));
            } 
            else 
            {
                $emails = $this->users->autocomplete_email($email, $this->users->MEMBER);
                return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($emails));
            }           
        }
	}
}
