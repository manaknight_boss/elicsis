<?php
class Reviews extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($restaurant_id, $page)
    {
        $this->load->model('review');
        $this->load->model('restaurants');
        $restaurant = $this->restaurants->get_restaurant($restaurant_id);

        if (!$restaurant)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode(array(
                    'has_more' => FALSE,
                    'items' => []
                )));
        }

        $limit = 5;
        $num_reviews = $this->review->num_reviews($restaurant_id);
        $reviews = $this->review->get_paginated_reviews($restaurant_id, $page, $limit);
        $num_pages = round($num_reviews / $limit);

        if (count($reviews) < 1)
        {
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                'has_more' => FALSE,
                'items' => []
            )));
        }

        return $this->output
        ->set_content_type('application/json')
        ->set_status_header(200)
        ->set_output(json_encode(array(
            'has_more' => ($num_pages >= $page) ? TRUE : FALSE,
            'items' => $reviews
        )));
	}
}
