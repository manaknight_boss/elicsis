<?php
class Search extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
    {
        $this->form_validation->set_rules('street_number', 'Street Number', '');
        $this->form_validation->set_rules('route', 'Address', 'required');
        $this->form_validation->set_rules('locality', 'City', '');
        $this->form_validation->set_rules('administrative_area_level_1', 'State', '');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('postal_code', 'ZIP Code', '');
        $this->form_validation->set_rules('lat', 'Latitude', 'required|numeric');
        $this->form_validation->set_rules('lng', 'Longitude', 'required|numeric');

        if ($this->form_validation->run() === FALSE)
		{
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'errors' => $this->form_validation->error_array(),
                'success' => FALSE
            ]));
		}
		else
		{
    		$street_number = $this->input->post('street_number', TRUE);
    		$address = $this->input->post('route', TRUE);
    		$city = $this->input->post('locality', TRUE);
    		$state = $this->input->post('administrative_area_level_1', TRUE);
    		$full_address = $this->input->post('full_address', TRUE);
    		$country = $this->input->post('country', TRUE);
    		$postal_code = $this->input->post('postal_code', TRUE);
    		$lat = $this->input->post('lat', TRUE);
    		$lng = $this->input->post('lng', TRUE);

            $_SESSION['search'] = [
                'street_number' => $street_number,
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'country' => $country,
                'postal_code' => $postal_code,
                'lat' => $lat,
                'lng' => $lng,
                'order_type' => '1',
                'serving' => 1,
                'address_full' => $full_address,
                'cuisine_id' => 0,
                'price' => '',
                'delivery' => 0,
                'distance' => 0,
                'date' => date('Y-m-d', time()),
                'search' => '',
                'time' => (int)date('H', time()),
                'rating' => 0
            ];

            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode([
                'success' => TRUE
            ]));
		}
	}
}
