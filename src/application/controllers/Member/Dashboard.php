<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Dashboard extends Member_Controller
{
	public $_page_name ='dashboard';

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $this->_data['google_key'] = $this->config->item('google_key');
		$this->render('Member/Dashboard', $this->_data);
	}

}