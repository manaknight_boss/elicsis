<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Refer extends Member_Controller
{
    protected $model_file = 'refer_log';
    public $_page_name = 'refer';


	public function index()
	{
		$this->load->model('users');
		$this->load->library('pagination');
		$this->_data['base_url'] = '/member/refer/';
        $this->_data['total_rows'] = $this->refer_log->num_refer_logs();
        $this->_data['mapping'] = $this->refer_log->mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->process_users($this->refer_log->get_paginated_refer_log($page, $this->_data['per_page']));
		$this->_data['links'] = $this->pagination->create_links();
		$user = $this->users->get_user($_SESSION['user_id']);
		$this->_data['code'] = $user->refer;
		$this->render('Member/Refer', $this->_data);
	}

	private function process_users ($data)
	{
		foreach ($data as $key => $refer)
		{
			$refer_user_id = $refer->refer_id;
			$data[$key]->refer_user = 'N\A';
			$refer_user = $this->users->get_user($refer_user_id);

			if ($refer_user)
			{
				$data[$key]->refer_user = $refer_user->email;
			}
		}

		return $data;
	}
}