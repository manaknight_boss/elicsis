<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Reciepts extends Member_Controller
{
    protected $model_file = 'orders';
    public $_page_name = 'reciept';


	public function index($page = 0)
	{
		$this->load->library('pagination');
		$this->_data['base_url'] = '/member/reciepts/';
		$this->_data['total_rows'] = $this->orders->num_user_orders($_SESSION['user_id']);
		$this->_data['mapping'] = $this->orders->mapping();
		$this->_data['delivery_mapping'] = $this->orders->delivery_type_mapping();
		$this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->process_data($this->orders->get_paginated_user_orders($_SESSION['user_id'], $page, $this->_data['per_page']));
		$this->_data['links'] = $this->pagination->create_links();
		$this->render('Member/Reciepts', $this->_data);
	}

	public function view($id)
	{
		$order = $this->orders->get_order($id);

		if (!$order)
		{
			return redirect('/member/reciepts/0');
		}

		if ($order->user_id != $_SESSION['user_id'])
		{
			return redirect('/member/reciepts/0');
		}

		$this->_data['mapping'] = $this->orders->mapping();
		$this->_data['delivery_mapping'] = $this->orders->delivery_type_mapping();
		$this->_data['model'] = $this->process_data([$order])[0];
		$this->_data['data'] = json_decode($this->_data['model']->data, TRUE);
		$this->_data['rnc'] = isset($this->_data['data']['address']['rnc']) ? $this->_data['data']['address']['rnc'] : '';
		$this->load->view('Member/RecieptView', $this->_data);
	}

	private function process_data ($data)
	{
		$this->load->model('restaurants');
		foreach ($data as $key => $order)
		{
			$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
			$data[$key]->restaurant = $restaurant;
		}
		return $data;
	}
}