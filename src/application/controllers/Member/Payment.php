<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';

class Payment extends Member_Controller
{
	protected $model_file = 'users';
	public $_page_name ='payment';

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		$this->load->model('cards');
		$this->_data['list'] = $this->cards->get_cards_by_user($_SESSION['user_id']);
		return $this->render('Member/Payment', $this->_data);
	}

	public function remove ($card_id)
	{
		$this->load->model('cards');
		$this->load->model('orders');
		$this->load->library('azul_service');
		$this->azul_service->set_keys($this->config->item('payment_url'), $this->config->item('payment_key'));
		$card = $this->cards->get_card($card_id);

		if (!$card)
		{
			$this->error('La tarjeta no existe');
			return redirect('/member/payment');
		}

		if ($card->user_id != $_SESSION['user_id'])
		{
			$this->error('La tarjeta no existe');
			return redirect('/member/payment');
		}
		$orders = $this->orders->get_order_by_card($card->token, $card->user_id);
		$no_linked_cards = TRUE;

		foreach ($orders as $key => $value)
		{
			if (!$this->orders->is_order_complete($value))
			{
				$no_linked_cards = FALSE;
			}
		}

		if ($no_linked_cards)
		{
			$delete_result = $this->azul_service->deleteToken([
				'DataVaultToken' => $card->token
			]);

			if (!$delete_result)
			{
				$this->error('No se puede procesar la solicitud en este momento, contáctenos directamente.');
				redirect('/member/payment');
			}

			if ($delete_result['error'])
			{
				$this->error('No se puede procesar la solicitud en este momento, contáctenos directamente.');
				redirect('/member/payment');
			}
			error_log(print_r($delete_result, TRUE));
			$this->cards->delete($card_id);
			$this->success('La tarjeta fue removida exitosamente');
			redirect('/member/payment');
		}
		else
		{
			$this->error('Esta tarjeta tiene un pedido en curso, cancele el pedido o vuelva a intentarlo más tarde.');
			redirect('/member/payment');
		}
	}
}