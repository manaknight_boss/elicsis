<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Login extends Manaknight_Controller
{
    protected $_redirect = '/member/dashboard';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users');
    }

    public function index()
    {
        $this->load->library('google_service');
        $this->google_service->init();
        $this->_data['google_auth_url'] = $this->google_service->make_auth_url();

        $_SESSION['checkout_flow_google_login'] = FALSE;

        $this->form_validation->set_rules('email', 'Corréo', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');

        if ($this->form_validation->run() == false) {
            $this->_render_strategy('Member/Login', $this->_data);
        } else {
            // set variables from the form
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            if ($this->users->authenticate($email, $password, false)) {
                $user_id = $this->users->get_user_id_from_email($email);
                $user = $this->users->get_user($user_id);

                $_SESSION['user_id'] = (int) $user->id;
                $_SESSION['email'] = (string) $user->email;
                $_SESSION['role'] = $user->role_id;
                redirect($this->_redirect);
            } else {
                $this->_data['error'] = 'Corréo o contraseña invalido.';
                $this->_render_strategy('Member/Login', $this->_data);
            }
        }
    }

    public function forgot()
    {
        $this->load->library('mail_service');
        $this->load->model('emails');
        $this->mail_service->set_adapter('smtp');
        $_SESSION['checkout_flow_google_login'] = FALSE;
        $this->_data = [];

        $this->form_validation->set_rules('email', 'Corréo', 'trim|required|valid_email');

        if ($this->form_validation->run() == false) {
            $this->_render_strategy('Member/Forgot', $this->_data);
        } else {
            // set variables from the form
            $email = $this->input->post('email');
            $user = $this->users->get_user_from_email($email);

            if ($user && $this->is_valid_user($user)) {
                $token = $this->users->reset_password($user->id);
                $from = $this->config->item('from_email');
                $to = $email;
                $template = $this->emails->get_template('reset-password', [
                    'email' => $email,
                    'reset_token' => $token,
                    'link' => base_url().'member/reset',
                ]);

                $this->mail_service->send($from, $to, $template->subject, $template->html);
                $this->_data['success'] = 'Hemos enviado un enlace a su corréo para acutalizar su contraseña';
                $this->_render_strategy('Member/Forgot', $this->_data);
            } else {
                $this->_data['error'] = 'El corréo no existe';
                $this->_render_strategy('Member/Forgot', $this->_data);
            }
        }
    }

    public function reset($token)
    {
        $this->_data = [];
        $_SESSION['checkout_flow_google_login'] = FALSE;
        $user = $this->users->get_user_from_reset_token($token);

        if (!$user) {
            $this->error('Usuario invalido');
            redirect('/member/forgot');
            stop_execution();
        }

        $this->form_validation->set_rules('password', 'Contraseña', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirmar Contraseña', 'required|matches[password]');

        if ($this->form_validation->run() == false) {
            $this->_render_strategy('Member/Reset', $this->_data);
        } else {
            $password = $this->input->post('password');

            if ($this->users->edit_profile($user->id, ['password' => $password])) {
                $this->success('Contraseña acutalizada exitosamente.');
                redirect('/member/login');
            } else {
                $this->_data['error'] = 'No se pudo actualizar la Contraseña';
                $this->_render_strategy('Member/Reset', $this->_data);
            }
        }
    }

    public function google()
    {
        $this->load->model('users');
        $this->load->helper('cookie');
        $this->load->library('mail_service');
        $this->load->library('google_service');
        $this->google_service->init();

        $checkout_flow_google_login_cart_id = isset($_SESSION['checkout_flow_google_login']) ? $_SESSION['checkout_flow_google_login'] : FALSE;
        $code = $this->input->get('code');
        $response = $this->google_service->get_me($code);
        $code = $response->getStatusCode(); // 200
        $body = $response->getBody();

        if ($code == 200) {
            $email = $this->google_service->get_email($body);

            if (strlen($email) > 0) {
                //create account ============
                $user_found = $this->users->get_user_from_email($email);

                if ($user_found) {
                    if ($this->users->is_google_user($user_found)) {
                        // set session user datas
                        $_SESSION['user_id'] = (int) $user_found->id;
                        $_SESSION['email'] = (string) $user_found->email;
                        $_SESSION['role'] = $user_found->role_id;
                        if ($checkout_flow_google_login_cart_id !== FALSE)
                        {
                            return redirect('/checkout/1/' . $checkout_flow_google_login_cart_id);
                        }
                        else
                        {
                            redirect($this->_redirect);
                        }
                    } else {
                        $this->error('Lo sentimos, este corréo no está registrado con Google en nuestros sistemas');
                        redirect('/member/login');
                    }
                } else {
                    $this->_register_email_only($email, $email, $this->users->GOGGLE_LOGIN);
                }
                //===============
            } else {
                $this->error('Lo sentimos, este corréo no está registrado con Google.');
                redirect('/member/login');
            }
        } else {
            $this->error('Lo sentimos, este corréo no está registrado con Google.');
            redirect('/member/login');
        }
    }

    private function _register_email_only($email, $username, $type)
    {
        $this->load->model('users');
        $this->load->model('emails');
        $checkout_flow_google_login_cart_id = isset($_SESSION['checkout_flow_google_login']) ? $_SESSION['checkout_flow_google_login'] : FALSE;

        if ($this->users->create_user($email, ' ', 2, $username, $type)) {
            $user_id = $this->users->get_user_id_from_email($email);
            $user = $this->users->get_user($user_id);

            $_SESSION['user_id'] = (int) $user->id;
            $_SESSION['email'] = (string) $user->email;
            $_SESSION['role'] = $user->role_id;
            error_log($checkout_flow_google_login_cart_id);
            if ($checkout_flow_google_login_cart_id !== FALSE)
            {
                return redirect('/checkout/1/' . $checkout_flow_google_login_cart_id);
            }
            else
            {
                redirect($this->_redirect);
            }
        } else {
            // user creation failed, this should never happen
            $data['error'] = 'Hubo un inconveniente al crear su cuenta, intentelo de nuevo.';
            $this->load->view('Member/Register', $data);
        }
    }

    protected function _render_strategy($view_name, $data)
    {
        $this->load->view($view_name, $data);
    }

    protected function _redirect_strategy()
    {
        $redirect = $this->_redirect;
        $redirect_raw = get_cookie('redirect', true);
        if (strlen($redirect_raw) > 0) {
            delete_cookie('redirect');
            $redirect = $redirect_raw;
        }
        redirect($redirect);
    }

    private function is_valid_user($user)
    {
        return $user && $user->role_id == $this->users->MEMBER && $user->status == 1;
    }
}
