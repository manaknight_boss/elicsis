<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Logout extends Manaknight_Controller 
{	
	public function index()
	{
		$_SESSION = [];
		unset($_SESSION);
		redirect('/member/login', 'refresh');
	}
}