<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Suggests extends Member_Controller
{
	public $_page_name = 'suggest';
	protected $model_file = 'suggest';


	public function index()
	{
		$this->form_validation->set_rules('name', 'Nombre de Empresa', 'required');
		$this->form_validation->set_rules('location', 'Ubicación', 'required');
		$this->form_validation->set_rules('note', 'Notas', '');
		$this->form_validation->set_rules('phone', 'Teléfono', 'required');

		if ($this->form_validation->run() === false)
		{
			$this->render('Member/Suggest', $this->_data);
		}
		else
		{
    		$name = $this->input->post('name', TRUE);
			$location = $this->input->post('location', TRUE);
			$note = $this->input->post('note', TRUE);
			$phone = $this->input->post('phone', TRUE);

			if ($this->suggest->create_suggest([
				'name' => $name,
				'location' => $location,
				'note' => $note,
				'phone' => $phone
			]))
			{
				$this->_data['success'] = 'Gracias por la sugerencia, haremos todo lo posible para tenerlo disponilbe con nosotros.';
				$this->render('Member/Suggest', $this->_data);
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo crear';
				$this->render('Member/Suggest', $this->_data);
			}
		}
		$this->render('Member/Suggest', $this->_data);
	}
}