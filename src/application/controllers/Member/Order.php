<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Order extends Member_Controller
{
    protected $model_file = 'orders';
    public $_page_name = 'order';


	public function index($page)
	{
		$this->load->model('users');
		$this->load->model('restaurants');
		$this->load->model('cart');
		$this->load->library('pagination');
		$this->_data['base_url'] = '/member/orders/';
        $this->_data['total_rows'] = $this->orders->num_user_orders($_SESSION['user_id']);
		$this->_data['mapping'] = $this->orders->mapping();
		$this->_data['delivery_mapping'] = $this->orders->delivery_type_mapping();
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->process_orders($this->orders->get_paginated_user_orders($_SESSION['user_id'], $page, $this->_data['per_page']));
		$this->_data['links'] = $this->pagination->create_links();
		$this->_data['cart'] = $this->process_cart($this->cart->get_carts_by_user($_SESSION['user_id']));
		$this->render('Member/Order', $this->_data);
	}

	public function cancel ($order_id)
	{
		$this->load->model('metric');
		$this->load->model('restaurants');
		$this->load->model('users');
		$this->load->library('azul_service');
		$order = $this->orders->get_order($order_id);
		$this->_data['model'] = $order;

		if (!$order)
		{
			$this->error('El pedido no existe');
			return redirect('/member/orders/0');

		}

		if (!$this->orders->is_cancelable($order))
		{
			$this->error('El pedido no se pudo cancelar');
			return redirect('/member/orders/0');
		}

		if ($this->orders->refund_required($order))
		{
			//
			$payment_detail = json_decode($this->_data['model']->payment_data, TRUE);
			$payment_payload = array(
				'AzulOrderId' => $payment_detail['response']['AzulOrderId'],
				'Amount' => $this->_data['model']->total * 100,
				'Itbis' => $this->_data['model']->tax * 100,
				'OriginalDate' => substr($payment_detail['response']['DateTime'], 0, 8)
			);

			$refund_result = $this->azul_service->refund($payment_payload);

			if (!$refund_result)
			{
				$this->error('El pedido no se pudo cancelar.');
				return redirect('/member/orders/0');
			}

			if ($refund_result['error'])
			{
				$this->error($refund_result['ErrorDescription']);
				return redirect('/member/orders/0');
			}

			$payment_detail['refund_detail'] = $refund_result;

			if ($this->orders->edit_order([
				'status' => 8,
				'payment_data' => json_encode($payment_detail)
			], $this->_data['model']->id))
			{
				$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);
				$user = $this->users->get_user($order->user_id);

				$this->_send_email_notification('refund',  [
					'id' => $order->id,
					'title' => $restaurant->title,
					'event_date_at' => $order->event_date_at,
					'event_time' => $this->get_time($order->event_hour, $order->event_minute),
					'event_location' => $order->address,
				], $user->email);

				$this->_send_email_notification('refund',  [
					'id' => $order->id,
					'title' => $restaurant->title,
					'event_date_at' => $order->event_date_at,
					'event_time' => $this->get_time($order->event_hour, $order->event_minute),
					'event_location' => $order->address,
				], $restaurant->email);

				$this->_send_sms_notification('refund',  [
					'id' => $order->id,
					'title' => $restaurant->title,
					'event_date_at' => $order->event_date_at,
					'event_time' => $this->get_time($order->event_hour, $order->event_minute),
					'event_location' => $order->address,
				], $order->phone);

				$this->success('Pedido ha sido cancelado exitosamente!');
				$this->metric->update_metric('cancel');
				redirect('/member/orders/0', 'refresh');
			}
			else
			{
				$this->error('El pedido no se pudo cancelar.');
				return redirect('/member/orders/0');
			}
			//
		}
		else
		{
			if ($this->orders->edit_order([
				'status' => 8
			], $this->_data['model']->id))
			{
				$this->success('Pedido ha sido cancelado exitosamente!');
				$this->metric->update_metric('cancel');
				redirect('/member/orders/0', 'refresh');
			}
			else
			{
				$this->error('Hubo un error!');
				redirect('/member/orders/0', 'refresh');
			}
		}
	}

	public function issue ($order_id)
	{
		$this->load->model('metric');
		$this->load->model('issue');

		$order = $this->orders->get_order($order_id);

		if (!$order)
		{
			$this->error('El pedido no existe');
			return redirect('/member/orders/0');
		}

		if (!$this->orders->is_reportable($order))
		{
			$this->error('Pedido no esta disponible para Reclamaciones');
			return redirect('/member/orders/0');
		}

		if ($this->issue->is_issue_exist($order_id))
		{
			$this->error('Ya hemos recibido una reclamación para este pedido');
			return redirect('/member/orders/0');
		}

		$this->_data['model'] = $order;

		$this->form_validation->set_rules('problem', 'Problema', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->render('Member/ReportIssue', $this->_data);
        }
        else
        {
            // set variables from the form
            $problem = $this->input->post('problem');

            $this->issue->create_issue([
								'order_id' => $order_id,
                'notes' => $problem,
                'resolution' => '',
                'status' => 0,
                'user_id' => $_SESSION['user_id'],
                'restaurant_id' => $order->restaurant_id
			]);

			$this->orders->edit_order([
				'status' => 6
			], $this->_data['model']->id);

			$this->success('Reclamación agendada Exitosamente');
			$this->metric->update_metric('issue');
            return redirect('/member/orders/0');
        }
	}

	public function reorder ($order_id)
	{
		$this->load->model('orders');
		$this->load->model('cart');
		$this->load->model('settings');

		$order = $this->orders->get_order($order_id);

		if (!$order)
		{
			$this->error('Error');
			return redirect('/member/orders/0');
		}

		$this->_data['model'] = $order;
		$setting = $this->settings->get_config_settings();
		$cart = $this->cart->process_reorder_cart(json_decode($this->_data['model']->data, TRUE), $setting);

		$cart_id = $this->cart->create_cart([
			'is_login' => 1,
			'is_day_time' => 0,
			'is_address' => 1,
			'is_payment' => 0,
			'is_complete' => 0,
			'user_id' => $_SESSION['user_id'],
			'restaurant_id' => $order->restaurant_id,
			'data' => json_encode($cart)
		]);

		if ($cart_id)
		{
			return redirect('/checkout/2/' . $cart_id);
		}
		else
		{
			$this->error('No se pudo reordenar el pedido');
			return redirect('/member/orders/0');
		}
	}

	public function review ($order_id)
	{
		$this->load->model('review');
		$this->load->model('users');
		$this->load->model('restaurants');

		$order = $this->orders->get_order($order_id);

		if (!$order)
		{
			$this->error('Lo senstimos, el  pedido no está disponible para Calicaciones');
			return redirect('/member/orders/0');
		}

		if (!$this->orders->is_reviewable($order))
		{
			$this->error('Error');
			return redirect('/member/orders/0');
		}

		$this->_data['model'] = $order;
		$user = $this->users->get_user($_SESSION['user_id']);
		$this->_data['name'] = (strlen($user->first_name) > 0) ? ($user->first_name  . ' ' . substr($user->last_name, 0, 1)) : '';

		$this->form_validation->set_rules('comment', 'Comentario', 'required');
		$this->form_validation->set_rules('rating', 'Calificación', 'required|numeric|greater_than[0]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->render('Member/LeaveReview', $this->_data);
        }
        else
        {
            // set variables from the form
            $comment = $this->input->post('comment');
            $rating = $this->input->post('rating');

            $this->review->create_review([
				'name' => $this->_data['name'],
				'order_id' => $order_id,
                'rating' => $rating,
                'message' => $comment,
                'status' => 1,
                'user_id' => $_SESSION['user_id'],
                'restaurant_id' => $order->restaurant_id
			]);
			$num = $this->review->num_reviews($order->restaurant_id);
			$avg = $this->review->get_avg_reviews($order->restaurant_id);
			$this->restaurants->edit_restaurant([
				'num_review' => $num,
				'rating' => $avg
			], $order->restaurant_id);
			$this->success('Calificación agendada, Gracias por su ayuda!');
            return redirect('/member/orders/0');
        }
	}

	public function resolution ($order_id)
	{
		$this->load->model('issue');
		$this->load->model('restaurants');

		$order = $this->orders->get_order($order_id);

		if (!$order)
		{
			$this->error('El pedido no esta disponible!');
			return redirect('/member/orders/0');
		}

		if (!$order->status == 5)
		{
			$this->error('El pedido no esta disponible');
			return redirect('/member/orders/0');
		}

		$this->_data['model'] = $order;
		$issue = $this->issue->get_issue_by_order($order->id);

		if (!$issue)
		{
			$this->error('Error, no existe reclamación para el pedido indicado');
			return redirect('/member/orders/0');
		}

		$restaurant = $this->restaurants->get_restaurant($order->restaurant_id);

		$this->_data['issue'] = $issue;
		$this->_data['restaurant'] = $restaurant;
		$this->render('Member/Resolution', $this->_data);
	}

	public function edit ($order_id)
	{
		$this->load->model('restaurants');

		$order = $this->orders->get_order($order_id);

		if (!$order)
		{
			$this->error('El pedido no esta disponible!');
			return redirect('/member/orders/0');
		}

		if (!$this->orders->is_editable($order))
		{
			$this->error('Orden no puede cambiar');
			return redirect('/member/orders/0');
		}

		$this->_data['model'] = $order;
		$this->_data['data'] = json_decode($this->_data['model']->data, TRUE);

		$this->form_validation->set_rules('name', 'Nombre del Local', 'required');
        $this->form_validation->set_rules('address', 'Dirección', 'required');
        $this->form_validation->set_rules('suite', 'Apartamento/Piso', '');
        $this->form_validation->set_rules('city', 'Ciudad', 'required');
        // $this->form_validation->set_rules('zip', 'Codigo Postal', 'required');
        $this->form_validation->set_rules('person_name', 'Conductor', '');
        $this->form_validation->set_rules('instructions', 'Instrucciones para el delivery', '');

        if ($this->form_validation->run() == FALSE)
        {
            $this->render('Member/EditOrder', $this->_data);
        }
        else
        {
            $name = $this->input->post('name');
            $address = $this->input->post('address');
            $suite = $this->input->post('suite');
            $city = $this->input->post('city');
            // $zip = $this->input->post('zip');
            $person_name = $this->input->post('person_name');
            $instructions = $this->input->post('instructions');

            if (!$zip)
            {
                $zip = '';
            }

            $data = json_decode($this->_data['model']->data, TRUE);

            $data['address']['name'] = $name;
            $data['address']['address'] = $address;
            $data['address']['suite'] = $suite;
            $data['address']['city'] = $city;
            // $data['address']['zip'] = $zip;
            $data['address']['person_name'] = $person_name;
            $data['address']['instructions'] = $instructions;


            $this->orders->edit_order([
				'address' => $address,
				'city' => $city,
				// 'zip' => $zip,
                'data' => json_encode($data)
            ], $order_id);

			$this->success('Actualizado');
            return redirect('/member/orders/0');
        }
	}

	private function process_orders ($data)
	{
		foreach ($data as $key => $order)
		{
			$restaurant_id = $order->restaurant_id;
			$data[$key]->restaurant_title = '';
			$data[$key]->is_reorder = TRUE;
			$data[$key]->is_cancel = FALSE;
			$data[$key]->is_report = FALSE;
			$data[$key]->is_review = FALSE;
			$data[$key]->is_editable = FALSE;
			$restaurant = $this->restaurants->get_restaurant($restaurant_id);

			if ($restaurant)
			{
				$data[$key]->restaurant = $restaurant;
				$data[$key]->restaurant_title = $restaurant->title;
			}

			//cancel
			if ($this->orders->is_cancelable($data[$key]))
			{
				$data[$key]->is_cancel = TRUE;
			}
			//report issue
			if ($this->orders->is_reportable($data[$key]))
			{
				$data[$key]->is_report = TRUE;
			}
			//leave review
			if ($this->orders->is_reviewable($data[$key]))
			{
				$data[$key]->is_review = TRUE;
			}
			if ($this->orders->is_editable($data[$key]))
			{
				$data[$key]->is_editable = TRUE;
			}
		}

		return $data;
	}

	private function process_cart ($data)
	{
		foreach ($data as $key => $cart)
		{
			$restaurant_id = $cart->restaurant_id;
			$restaurant = $this->restaurants->get_restaurant($restaurant_id);

			if ($restaurant)
			{
				$data[$key]->restaurant = $restaurant;
			}
		}

		return $data;
	}

	private function _send_email_notification($slug, $payload, $email)
	{
	$this->load->model('emails');
	$this->load->library('mail_service');
			$this->mail_service->set_adapter('smtp');
			$email_template = $this->emails->get_template($slug, $payload);

			if ($email_template)
			{
					$from = $this->config->item('from_email');
					$this->mail_service->send($from, $email, $email_template->subject, $email_template->html);
			}
	}

	private function _send_sms_notification($slug, $payload, $to)
	{
			$this->load->model('sms');
			$this->load->library('sms_service');
			$this->sms_service->set_adapter('sms');
			$sms_template = $this->sms->get_template($slug, $payload);

			if ($sms_template)
			{
					$this->sms_service->send($to, $sms_template->content);
			}
	}

	private function get_time ($hour, $minute)
	{
		if ($minute == 0) {
			$minute = '00';
		}

		$minute_format = ":{$minute}";

		if ($hour == 24 || $hour == 0) {
			return '12' . $minute_format . ' AM';
		}
		if ($hour == 12) {
			return '12' . $minute_format . ' PM';
		}
		if ($hour < 12) {
			return $hour . $minute_format . ' AM';
		}

		if ($hour > 12) {
			return ($hour % 12) . $minute_format . ' PM';
		}
	}
}