<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';

class Profile extends Member_Controller
{
	protected $model_file = 'users';
	public $_page_name ='profile';

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		$user = $this->users->get_profile($_SESSION['user_id']);

		$this->_data['model'] = $user;

		if (!$user)
		{
			$this->error('El perfil no existe', 'error');
			redirect('/member/dashboard');
		}

		if ($user->verify == 1)
		{
			$this->_edit_profile($user);
		}
		else
		{
			$this->_verify($user);
		}
	}

	private function _verify($user)
	{
		$this->load->model('verify_number');

		$this->form_validation->set_rules('phone', 'Teléfono', 'required|numeric');

		if ($this->form_validation->run() === false)
		{
			return $this->render('Member/Verify', $this->_data);
		}
		else
		{
			$code = mt_rand(100000, 999999);
			$this->verify_number->create_verify_number([
				'code' => $code,
				'user_id' => $user->id
			]);

			$phone = $this->input->post('phone');
			$sent_sms = $this->_send_sms_notification('verify',['num' => $code], $phone);

			if ($sent_sms)
			{
				$this->users->edit_profile($_SESSION['user_id'], [
					'phone' => $phone
				]);
				return $this->redirect('/member/verify/code', 'refresh');
			}
			else
			{
				$this->_data['error'] = 'No se pudo enviar el MMS';
				return $this->render('Member/Verify', $this->_data);
			}

		}
	}

	public function verify_code()
	{
		$this->load->model('verify_number');
		$this->form_validation->set_rules('code', 'Código', 'required|numeric');

		if ($this->form_validation->run() === false)
		{
			return $this->render('Member/VerifyCode', $this->_data);
		}
		else
		{
			$code = $this->input->post('code', TRUE);
			$verify_number = $this->verify_number->get_verify_number($code, $_SESSION['user_id']);
			if ($verify_number)
			{
				$this->users->edit_profile($_SESSION['user_id'], [
					'verify' => 1
				]);
				$this->verify_number->delete($verify_number);
				$this->success('verificado');
				return $this->redirect('/member/profile', 'refresh');
			}
			else
			{
				$this->error('No se pudo verificar el número');
				return $this->redirect('/member/profile', 'refresh');
			}
		}
	}

	private function _edit_profile($user)
	{
		$email_validation_rules = 'required|valid_email';

		if ($this->input->post('email') != $_SESSION['email'])
		{
			$email_validation_rules .= '|is_unique[users.email]';
		}

		$this->form_validation->set_rules('email', 'Corréo', $email_validation_rules);
		$this->form_validation->set_rules('password', 'Contraseña', '');
		$this->form_validation->set_rules('first_name', 'Nombre', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');
		$this->form_validation->set_rules('business_name', 'Nombre de la empresa', 'required');

		if ($this->form_validation->run() === false)
		{
			return $this->render('Member/Profile', $this->_data);
		}
		else
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$business_name = $this->input->post('business_name');
			$rnc = $this->input->post('rnc');
			if ($this->users->edit_profile($_SESSION['user_id'], [
				'email' => $email,
				'password' => $password,
				'first_name' => $first_name,
				'business_name' => $business_name,
				'rnc' => $rnc,
				'last_name' => $last_name
			]))
			{
				$_SESSION['email'] = $email;
				redirect('/member/profile', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo actualizar el perfil.';
				return $this->render('Member/Profile', $this->_data);
			}

		}
	}

	private function _send_email_notification($slug, $payload, $email)
    {
		$this->load->model('emails');
		$this->load->library('mail_service');
        $this->mail_service->set_adapter('smtp');
        $email_template = $this->emails->get_template($slug, $payload);

        if ($email_template)
        {
            $from = $this->config->item('from_email');
            $this->mail_service->send($from, $email, $email_template->subject, $email_template->html);
        }
    }

	private function _send_sms_notification($slug, $payload, $to)
    {
		$this->load->model('sms');
		$this->load->library('sms_service');
        $this->sms_service->set_adapter('sms');
        $sms_template = $this->sms->get_template($slug, $payload);

        if ($sms_template)
        {
           return $this->sms_service->send($to, $sms_template->content);
		}

		return FALSE;
    }
}