<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Checkout extends Member_Controller
{
	public $_page_name ='dashboard';

	public function __construct()
    {
        parent::__construct();
        $this->load->model('cart');
        $this->_data['google_key'] = $this->config->item('google_key');
        $this->_data['google_callback'] = 'initAutocomplete';
        $this->_data['payment_url'] = $this->config->item('payment_url');
        $this->_data['payment_key'] = $this->config->item('payment_key');
    }

    /**
     * Check cart legit
     *
     * @param integer $cart_id
     * @return void|mixed
     */
    private function _check_cart ($cart_id)
    {
        $cart = $this->cart->get_cart($cart_id);

        if (!$cart)
        {
            return redirect('/');
        }

        $cart_not_belong_user = $cart && ($cart->user_id == 0 || $cart->user_id != $_SESSION['user_id']);

        if ($cart_not_belong_user)
        {
            return redirect('/');
        }

        return $cart;
    }

    public function step_2 ($cart_id)
    {
        $cart = $this->_check_cart($cart_id);

        $this->_data['date'] = '';
        $this->_data['time'] = 0;

        if (strlen($cart->data) > 0)
        {
            $data = json_decode($cart->data, TRUE);
            if (isset($data) && isset($data['date']))
            {
                $this->_data['date'] = $data['date'];
            }
            if (isset($data) && isset($data['time']))
            {
                $this->_data['time'] = $data['time'];
            }
        }

        $this->form_validation->set_rules('date', 'Fecha', 'required');
        $this->form_validation->set_rules('time', 'Hora', 'required|numeric');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Member/Step_2', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
        }
        else
        {
            // set variables from the form
            $date = $this->input->post('date');
            $time = $this->input->post('time');
            $data = json_decode($cart->data, TRUE);
            $data['date'] = $date;
            $data['time'] = (int)floor($time);
            $data['minute'] = (is_numeric( $time ) && floor( $time ) != $time) ? 30 : 0;
            $this->load->model('restaurants');
            $this->load->library('search_service');
            if ($this->search_service->valid_event_date($this->restaurants, $cart->restaurant_id, $data['date'], $data['time'], $data['minute']))
            {
                $this->cart->edit_cart([
                    'is_day_time' => 1,
                    'data' => json_encode($data)
                ], $cart_id);
                return redirect('/checkout/3/' . $cart_id);
            }
            else
            {
                $this->_data['error'] = 'lo siento restaurante no abierto durante ese tiempo';
                $this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Member/Step_2', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
            }

        }
    }

    public function step_3 ($cart_id)
    {
        $this->load->model('users');
        $cart = $this->_check_cart($cart_id);
        $this->_add_prefilled_fields($cart);
        $user = $this->users->get_user($_SESSION['user_id']);
        $this->_data['phone'] = $user->phone;
        $this->_data['rnc'] = $user->rnc;

        $this->form_validation->set_rules('name', 'Nombre del Local', 'required');
        $this->form_validation->set_rules('address', 'Dirección', 'required');
        $this->form_validation->set_rules('suite', 'Apartamento/Piso', '');
        $this->form_validation->set_rules('city', 'Ciudad', 'required');
        $this->form_validation->set_rules('zip', 'Codigo Postal', '');
        $this->form_validation->set_rules('person_name', 'Conductor', '');
        $this->form_validation->set_rules('rnc', 'RNC', '');
        $this->form_validation->set_rules('phone', 'Número de teléfono', 'required');
        $this->form_validation->set_rules('instructions', 'Instrucciones para el delivery', '');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Member/Step_3', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
        }
        else
        {
            // set variables from the form
            $name = $this->input->post('name');
            $address = $this->input->post('address');
            $suite = $this->input->post('suite');
            $city = $this->input->post('city');
            $zip = $this->input->post('zip');
            $rnc = $this->input->post('rnc');
            $person_name = $this->input->post('person_name');
            $phone = $this->input->post('phone');
            $instructions = $this->input->post('instructions');

            if (!$zip)
            {
                $zip = '';
            }

            if (substr($phone, 0, 1) != '1')
            {
                $phone = '1' . $phone;
            }

            $data = json_decode($cart->data, TRUE);

            $data['address'] = [
                'name' => $name,
                'address' => $address,
                'suite' => $suite,
                'city' => $city,
                'zip' => $zip,
                'person_name' => $person_name,
                'phone' => $phone,
                'rnc' => $rnc,
                'instructions' => $instructions
            ];

            $this->users->edit_profile($_SESSION['user_id'], [
				'rnc' => $rnc
            ]);

            $this->cart->edit_cart([
                'is_address' => 1,
                'data' => json_encode($data)
            ], $cart_id);

            return redirect('/checkout/4/' . $cart_id);
        }
    }

    public function step_4 ($cart_id)
    {
        $this->load->model('orders');
        $this->load->model('cards');
        $this->load->model('metric');
        $this->load->model('cards');
        $this->load->model('emails');
        $this->load->model('refer_log');
        $this->load->model('customer_restaurant');
        $this->load->model('restaurants');
        $this->load->library('mail_service');
        $cart = $this->_check_cart($cart_id);
        $this->_data['data'] = json_decode($cart->data, TRUE);

        if ($this->_data['data']['is_delivery'] == '0')
        {
            $this->_data['data']['delivery_fee'] = 0;
        }

        $this->_data['cards'] = $this->cards->get_cards_by_user($_SESSION['user_id']);

        $this->form_validation->set_message('valid_token', 'La tarjeta de crédito no es válida.');
        $this->form_validation->set_rules('card', 'Card', 'integer');
        $this->form_validation->set_rules('token', 'Tarjeta de crédito', ['required', [
            'valid_token',
            function($value) {
                if (!strpos($value, '.'))
                {
                    // error_log('.');
                    return FALSE;
                }

                $parts = explode('.', $value);

                if (count($parts) !== 2)
                {
                    // error_log('2');
                    return FALSE;
                }

                $hash = md5($this->hexToStrWithQuotes($parts[0]));

                if ($hash !== $parts[1]) {
                    // error_log('hash');
                    return FALSE;
                }

                $json = @json_decode($this->hexToStr($parts[0]), TRUE);

                if (!$json) {
                    // error_log('json');
                    return FALSE;
                }
                if (!isset($json['Brand'])){
                    // error_log('Brand');
                    return FALSE;
                }
                if (!isset($json['CardNumber'])){
                    // error_log('CardNumber');
                    return FALSE;
                }
                if (!isset($json['DataVaultToken'])){
                    // error_log('DataVaultToken');
                    return FALSE;
                }
                if (!isset($json['Expiration'])){
                    // error_log('Expiration');
                    return FALSE;
                }
                return TRUE;
            }
            ]
        ]);
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Member/Step_4', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
        }
        else
        {
            $raw_token = $this->input->post('token');
            $parts = explode('.', $raw_token);
            $token_payload = @json_decode($this->hexToStr($parts[0]), TRUE);
            $discount = 0;
            $payment_method = 'credit';
            $payload = [
                'discount' => $discount,
                'payment_method' => $payment_method,
                'brand' => $token_payload['Brand'],
                'card_number' => $token_payload['CardNumber'],
                'token' => $token_payload['DataVaultToken'],
                'expire_date' => $token_payload['Expiration'],

            ];

            if ($token_payload['Brand'] == 'Dummy' ||
                $token_payload['CardNumber'] == 'Dummy' ||
                $token_payload['DataVaultToken'] == 'Dummy' ||
                $token_payload['Expiration'] == 'Dummy'
            )
            {
                $card_id = $this->input->post('card');
                $card = $this->cards->get_card($card_id);

                if (!$card || $card->user_id != $_SESSION['user_id'])
                {
                    $this->_data['error'] = 'Tarjeta de crédito inválida';
                    $this->load->view('Layout/GuestHeader', $this->_data);
                    $this->load->view('Member/Step_2', $this->_data);
                    $this->load->view('Layout/GuestFooter', $this->_data);
                    return;
                }
                else
                {
                    $payload = [
                        'discount' => $discount,
                        'payment_method' => $payment_method,
                        'brand' => $card->brand,
                        'card_number' => $card->card_number,
                        'token' => $card->token,
                        'expire_date' => $card->expire_date
                    ];
                }
            }

            $this->cart->edit_cart([
                'is_payment' => 1,
                'payment_data' => json_encode($payload),
            ], $cart_id);

            if ($token_payload['Brand'] != 'Dummy')
            {
                $this->cards->create_card([
                    'brand' => $token_payload['Brand'],
                    'card_number' => $token_payload['CardNumber'],
                    'token' => $token_payload['DataVaultToken'],
                    'expire_date' => $token_payload['Expiration'],
                    'user_id' => $cart->user_id
                ]);
            }

            return redirect('/checkout/5/' . $cart_id);
        }
    }

    public function step_5 ($cart_id)
    {
        $this->load->model('orders');
        $this->load->model('metric');
        $this->load->model('emails');
        $this->load->model('refer_log');
        $this->load->model('customer_restaurant');
        $this->load->model('restaurants');
        $this->load->library('mail_service');
        $cart = $this->_check_cart($cart_id);
        $this->_data['cart'] = $cart;
        $this->_data['data'] = json_decode($cart->data, TRUE);
        $this->_data['payment_data'] = json_decode($cart->payment_data, TRUE);
        if ($this->_data['data']['is_delivery'] == '0')
        {
            $this->_data['data']['delivery_fee'] = 0;
        }

        $this->form_validation->set_rules('submit', 'Complete Order', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Layout/GuestHeader', $this->_data);
            $this->load->view('Member/Step_5', $this->_data);
            $this->load->view('Layout/GuestFooter', $this->_data);
        }
        else
        {
            $amount = $this->_data['data']['total'];
            $tips = $this->_data['data']['real_tips'];
            $currency = 'DOP';
            $event_date_at = $this->_data['data']['date'];
            $event_hour = $this->_data['data']['time'];
            $event_minute = $this->_data['data']['minute'];
            $address = $this->_data['data']['address']['address'];
            $phone = $this->_data['data']['address']['phone'];
            $lat = 0;
            $long = 0;
            $tax = $this->_data['data']['tax'];
            $total = $this->_data['data']['real_tips'] + $this->_data['data']['total'] +
            $this->_data['data']['delivery_fee'] + $this->_data['data']['tax'];
            $discount = $this->_data['payment_data']['discount'];
            $payment_method = $this->_data['payment_data']['payment_method'];
            $zip = $this->_data['data']['address']['zip'];
            $city = $this->_data['data']['address']['city'];
            $state = 'Santo Domingo';
            $data = json_encode($this->_data['data']);
            $user_id = $_SESSION['user_id'];
            $payload = [
                'amount' => $amount,
                'tips' => $tips,
                'restaurant_id' => $cart->restaurant_id,
                'currency' => $currency,
                'event_date_at' => $event_date_at,
                'event_hour' => $event_hour,
                'event_minute' => $event_minute,
                'address' => $address,
                'phone' => $phone,
                'lat' => $lat,
                'long' => $long,
                'tax' => $tax,
                'total' => $total,
                'is_delivery' => $this->_data['data']['is_delivery'],
                'delivery_fee' => $this->_data['data']['delivery_fee'],
                'discount' => $discount,
                'payment_method' => $payment_method,
                'zip' => $zip,
                'city' => $city,
                'state' => $state,
                'data' => $data,
                'payment_data' => $cart->payment_data,
                'notes' => '',
                'user_id' => $user_id,
                'status' => $this->orders->PAID,
                'on_time_rating' => 0,
                'on_delivery_rating' => 0,
                'start_on_time_at' => date('Y-m-d H:i:s'),
                'end_on_time_at' => null,
                'start_on_delivery_at' => null,
                'end_on_delivery_at' => null
            ];

            $order_id = $this->orders->create_order($payload);

            $this->cart->edit_cart([
                'is_complete' => 1
            ], $cart_id);

            if ($order_id)
            {
                $this->mail_service->set_adapter('smtp');
                $email_template = $this->emails->get_template('reciept', $this->_make_email_payload($payload, $order_id, $this->_data['data']));

                if ($email_template)
                {
                    $from = $this->config->item('from_email');
                    $this->mail_service->send($from, $_SESSION['email'], $email_template->subject, $email_template->html);
                }

                $refer_code = (isset($_SESSION['refer']) && strlen($_SESSION['refer']) > 0) ? $_SESSION['refer'] : '';
                $this->refer_log->create_referral($user_id, $refer_code);

                $this->customer_restaurant->create_relationship($cart->restaurant_id, $user_id);
                $this->metric->update_metric('orders');


                $restaurant = $this->restaurants->get_restaurant($cart->restaurant_id);
                if ($restaurant) {
                    $phone = $restaurant->phone;
                    if (strlen($phone) > 0) {
                        $country_code = substr($phone, 0, 1);
                        if ($country_code != '1') {
                            $phone = '1' . $phone;
                        }
                        $this->_send_sms_notification('new_order',['id' => $order_id, 'title' => $restaurant->title], $phone);
                    }
                }

                return redirect('member/reciepts/0');
            }
            else
            {
                $this->_data['error'] = 'No se pudo procesar el pedido. Por favor contactar servicio al cliente';
                $this->load->view('Layout/GuestHeader', $this->_data);
                $this->load->view('Member/Step_5', $this->_data);
                $this->load->view('Layout/GuestFooter', $this->_data);
            }
        }
    }

    private function _make_email_payload ($payload, $order_id, $cart)
    {
        $this->load->model('restaurants');
        $restaurant = $this->restaurants->get_restaurant($payload['restaurant_id']);
        return [
            'title' => $restaurant->title,
            'rnc' => $restaurant->rnc,
            'phone' => $restaurant->phone,
            'address' => $restaurant->address,
            'state' => $restaurant->state,
            'city' => $restaurant->city,
            'zip' => $restaurant->zip,
            'user_phone' => $payload['phone'],
            'id' => $order_id,
            'event_date_at' => $payload['event_date_at'],
            'event_hour' => $this->get_time($payload['event_hour'], $payload['event_minute']),
            'items' => $this->render_items($payload['data']),
            'sub_total' => number_format($payload['total'], 2),
            'tips' => number_format($payload['tips'], 2),
            'delivery' => ($payload['is_delivery'] == '1') ? number_format($payload['delivery_fee'], 2) : 0,
            'total' => number_format($payload['tips'] + $payload['total'] + $payload['delivery_fee'], 2),
            'final_total' => number_format($payload['tips'] + $payload['total'] + $payload['delivery_fee'] + $payload['tax'], 2),
            'tax' => number_format($payload['tax'], 2)
        ];
    }

    private function render_items($data)
    {
        $result = '';
        $data_json = json_decode($data, TRUE);
        foreach($data_json['items'] as $item)
        {
            $result .= '<div>' . $item['quantity'] . 'x ' . $item['title'] . '</div>';
            $result .= ($item['specialInstruction'] != '') ? ('<small>Instrucciones especiales: ' . $item['specialInstruction'] . '</small>') : '';
            if (!empty($item['addons']))
            {
                $result .= '<div>' . 'Addons:' . '</div>';
                foreach($item['addons'] as $addon)
                {
                    $result .= '<div> - ' . $addon['type'] . ' : ' . $addon['name'] . '</div>';
                }
            }
        }
        return $result;
    }

    private function delivery_text($data)
    {
        return ($data['is_delivery'] == '1') ? ('<tr><th scope="row" colspan="2" >Costo de Delivery</th><td>DOP' . number_format($data['delivery_fee'], 2) . '</td></tr>') : '';
    }

    private function get_time ($hour, $minute)
    {
        if ($minute == 0) {
            $minute = '00';
        }

        $minute_format = ":{$minute}";

        if ($hour == 24 || $hour == 0) {
            return '12' . $minute_format . ' AM';
        }
        if ($hour == 12) {
            return '12' . $minute_format . ' PM';
        }
        if ($hour < 12) {
            return $hour . $minute_format . ' AM';
        }

        if ($hour > 12) {
            return ($hour % 12) . $minute_format . ' PM';
        }
    }

    private function _add_prefilled_fields($cart)
    {
        $this->_data['name'] = '';
        $this->_data['address'] = '';
        $this->_data['suite'] = '';
        $this->_data['city'] = '';
        $this->_data['zip'] = '';
        $this->_data['phone'] = '';
        $this->_data['person_name'] = '';
        $this->_data['instructions'] = '';

        if (!$cart)
        {
            return;
        }

        $data = json_decode($cart->data, TRUE);

        if (!$data)
        {
            return;
        }

        if (isset($data['address']))
        {
            $this->_data['name'] = $data['address']['name'];
            $this->_data['address'] = $data['address']['address'];
            $this->_data['suite'] = $data['address']['suite'];
            $this->_data['city'] = $data['address']['city'];
            $this->_data['zip'] = $data['address']['zip'];
            $this->_data['phone'] = $data['address']['phone'];
            $this->_data['person_name'] = $data['address']['person_name'];
            $this->_data['instructions'] = $data['address']['instructions'];
        }
    }

    private function _send_sms_notification($slug, $payload, $to)
    {
		$this->load->model('sms');
		$this->load->library('sms_service');
        $this->sms_service->set_adapter('sms');
        $sms_template = $this->sms->get_template($slug, $payload);

        if ($sms_template)
        {
           return $this->sms_service->send($to, $sms_template->content);
		}

		return FALSE;
    }

    private function hexToStr($hex)
    {
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return str_replace(array('\"', '"{', '}"'), array('"', '{', '}'), $string);
    }

    private function hexToStrWithQuotes($hex)
    {
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }
}