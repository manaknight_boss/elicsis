<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Reviews extends Member_Controller
{
    protected $model_file = 'review';
    public $_page_name = 'review';


	public function index()
	{
		$this->load->model('users');
		$this->load->library('pagination');
		$this->_data['base_url'] = '/member/reviews/';
        $this->_data['total_rows'] = $this->review->num_user_review($_SESSION['user_id']);
        $this->_data['per_page'] = 10;
		$this->_data['uri_segment'] = 3;
		$this->_data['num_links'] = round($this->_data['total_rows'] / $this->_data['per_page']);
		$this->pagination->initialize($this->_data);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->_data['list'] = $this->_process_reviews($this->review->get_paginated_user_review($_SESSION['user_id'], $page, $this->_data['per_page']));
		$this->_data['links'] = $this->pagination->create_links();
		$this->render('Member/Reviews', $this->_data);
	}

	private function _process_reviews ($data)
	{
		$this->load->model('restaurants');
		$this->load->model('orders');
		foreach ($data as $key => $review)
		{
			$restaurant_id = $review->restaurant_id;
			$restaurant = $this->restaurants->get_restaurant($restaurant_id);

			if ($restaurant)
			{
				$data[$key]->restaurant = $restaurant;
			}

			$order = $this->orders->get_order($review->order_id);

			if ($order)
			{
				$data[$key]->order = $order;
			}
		}
		return $data;
	}
}