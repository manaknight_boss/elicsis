<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Expense extends Member_Controller
{
    protected $model_file = 'orders';
	public $_page_name ='expense';

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $this->load->library('report_service');
		$this->form_validation->set_rules('start_date', 'fecha de inicio', 'required|date');
		$this->form_validation->set_rules('end_date', 'Fecha final', 'required|date');

		if ($this->form_validation->run() === false)
		{
			$this->render('Member/Expense', $this->_data);
		}
		else
		{
			$start_date = $this->input->post('start_date', TRUE);
			$end_date = $this->input->post('end_date', TRUE);
			$orders = $this->orders->generate_expense_report($start_date, $end_date, $_SESSION['user_id']);

			$this->report_service->generate_csv([
                'Pedido #',
                'Fecha',
                'Monto',
                '# Personas',
                'Total',
                'Total/Persona'
			], $orders, 'informe_' . $start_date . '_' . $end_date . '.csv');
		}
	}

}