<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once dirname(__FILE__) . '/../../core/Member_Controller.php';
class Users extends Member_Controller
{
    protected $model_file = 'users';
    public $_page_name = 'users';

    
	public function index()
	{
        $this->load->helper('date');
		$this->_data['list'] = $this->users->get_users();
		$this->render('Member/Users', $this->_data);
	}

	public function add()
	{
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('first_name', 'Nombres', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');

		if ($this->form_validation->run() === false)
		{
			$this->render('Member/UsersAdd', $this->_data);
		}
		else
		{
    		$email = $this->input->post('email', TRUE);
			$password = $this->input->post('password', TRUE);
			$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
	
			if ($this->users->create_user([
				'email' => $email,
				'password' => $password,
				'first_name' => $first_name,
				'last_name' => $last_name,
				
			]))
			{
				redirect('/member/users', 'refresh');
			}
			else
			{
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo crear el usuario.';
				$this->render('Member/UsersAdd', $this->_data);
			}
		}
	}

	public function edit($id)
	{
		$users = $this->users->get_user($id);
		$this->_data['model'] = $users;
		if (!$users)
		{
			$this->error('El usuario no existe!');
			redirect('/member/users');
		}
		$this->form_validation->set_rules('email', 'Corréo', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('first_name', 'Nombres', 'required');
		$this->form_validation->set_rules('last_name', 'Apellidos', 'required');

		if ($this->form_validation->run() === false)
        {
			$this->render('Member/UsersEdit', $this->_data);
		}
        else
        {
    		$email = $this->input->post('email', TRUE);
			$password = $this->input->post('password', TRUE);
			$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
	
			if ($this->users->edit_user([
				'email' => $email,
				'password' => $password,
				'first_name' => $first_name,
				'last_name' => $last_name,
				
            ], $id))
            {
				redirect('/member/users', 'refresh');
			}
            else
            {
				// user creation failed, this should never happen
				$this->_data['error'] = 'No se pudo editar el usuario.';
				$this->render('Member/UsersEdit', $this->_data);
			}
		}
	}
}