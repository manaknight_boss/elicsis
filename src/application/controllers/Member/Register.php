<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends Manaknight_Controller
{

	protected $_redirect = '/member/dashboard';

	public function __construct()
    {
        parent::__construct();
    }

	public function index ()
	{
		$this->_data = [];
		$this->load->model('users');
		$this->load->model('refer_log');
		$this->load->helper('cookie');

		$this->form_validation->set_rules('email', 'Corréo', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('first_name', 'Nombre', 'required');
        $this->form_validation->set_rules('last_name', 'Apellidos', 'required');
        $this->form_validation->set_rules('phone', 'Teléfono', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->_render_strategy('Member/Register', $this->_data);
		}
		else
		{
			// set variables from the form
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$phone = $this->input->post('phone');
			$user = $this->users->create_user($email, $password, $this->users->MEMBER, $email, 'n', $first_name, $last_name, '', $phone);

			if ($user)
			{
				$refer_code = (isset($_SESSION['refer']) && strlen($_SESSION['refer']) > 0) ? $_SESSION['refer'] : '';
				$this->refer_log->create_referral($user, $refer_code);
				$_SESSION['user_id']      = (int)$user;
				$_SESSION['email']     = (string)$email;
				$_SESSION['role']     = $this->users->MEMBER;
				redirect($this->_redirect);
			}
			else
			{
				$this->_data['error'] = 'Corréo o Contraseña invalidos.';
				$this->_render_strategy('Member/Register', $this->_data);
			}
		}
	}

	protected function _render_strategy ($view_name, $data)
	{
		$this->load->view($view_name, $data);
	}

	protected function _redirect_strategy()
	{
		$redirect = $this->_redirect;
		$redirect_raw = get_cookie('redirect', TRUE);
		if (strlen($redirect_raw) > 0)
		{
			delete_cookie('redirect');
			$redirect = $redirect_raw;
		}
		redirect($redirect);
	}
}