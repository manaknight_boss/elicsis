<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//Admin
$route['admin/coupons'] = 'Admin/Coupon';
$route['admin/coupons/add'] = 'Admin/Coupon/add';
$route['admin/coupons/edit/(:num)'] = 'Admin/Coupon/edit/$1';

$route['admin/dashboard'] = 'Admin/Dashboard/index/0';
$route['admin/dashboard/(:num)'] = 'Admin/Dashboard/index/$1';
$route['admin/issue/(:num)'] = 'Admin/Issues/edit/$1';

$route['admin/emails'] = 'Admin/Email';
$route['admin/emails/add'] = 'Admin/Email/add';
$route['admin/emails/edit/(:num)'] = 'Admin/Email/edit/$1';

$route['admin/sms'] = 'Admin/Smss';
$route['admin/sms/add'] = 'Admin/Smss/add';
$route['admin/sms/edit/(:num)'] = 'Admin/Smss/edit/$1';

$route['admin/login'] = 'Admin/Login';
$route['admin/logout'] = 'Admin/Logout';

$route['admin/orders'] = 'Admin/Order/index/0';
$route['admin/orders/(:num)'] = 'Admin/Order/index/$1';
$route['admin/orders/view/(:num)'] = 'Admin/Order/view/$1';
$route['admin/orders/edit/(:num)'] = 'Admin/Order/edit/$1';
$route['admin/orders/refund/(:num)'] = 'Admin/Order/refund/$1';
$route['admin/orders/cancel/(:num)'] = 'Admin/Order/cancel/$1';
$route['admin/orders/accept/(:num)'] = 'Admin/Order/accept/$1';
$route['admin/orders/process/(:num)'] = 'Admin/Order/process/$1';
$route['admin/orders/delivery/(:num)'] = 'Admin/Order/delivery/$1';
$route['admin/orders/delivered/(:num)'] = 'Admin/Order/delivered/$1';


$route['admin/catering/new/(:num)'] = 'Admin/New_caterings/index/$1';
$route['admin/catering/add'] = 'Admin/New_caterings/add';
$route['admin/catering/edit/(:num)'] = 'Admin/New_caterings/edit/$1';
$route['admin/catering/approve/(:num)'] = 'Admin/New_caterings/approve/$1';

$route['admin/restaurants'] = 'Admin/Restaurant/index/0';
$route['admin/restaurants/(:num)'] = 'Admin/Restaurant/index/$1';
$route['admin/restaurants/add'] = 'Admin/Restaurant/add';
$route['admin/restaurants/edit/(:num)'] = 'Admin/Restaurant/edit/$1';
$route['admin/restaurants/view/(:num)'] = 'Admin/Restaurant/view/$1';

$route['admin/cuisines'] = 'Admin/Cuisine';
$route['admin/cuisines/add'] = 'Admin/Cuisine/add';
$route['admin/cuisines/edit/(:num)'] = 'Admin/Cuisine/edit/$1';
$route['admin/cuisines/view/(:num)'] = 'Admin/Cuisine/view/$1';

$route['admin/preferences'] = 'Admin/Preference';
$route['admin/preferences/add'] = 'Admin/Preference/add';
$route['admin/preferences/edit/(:num)'] = 'Admin/Preference/edit/$1';
$route['admin/preferences/view/(:num)'] = 'Admin/Preference/view/$1';

$route['admin/contacts'] = 'Admin/Contacts/index/0';
$route['admin/contacts/(:num)'] = 'Admin/Contacts/index/$1';
$route['admin/contacts/add'] = 'Admin/Contacts/add';
$route['admin/contacts/edit/(:num)'] = 'Admin/Contacts/edit/$1';
$route['admin/contacts/view/(:num)'] = 'Admin/Contacts/view/$1';

$route['admin/suggests'] = 'Admin/Suggests/index';

$route['admin/catering_menu'] = 'Admin/Catering_menus';
$route['admin/catering_menu/add'] = 'Admin/Catering_menus/add';
$route['admin/catering_menu/edit/(:num)'] = 'Admin/Catering_menus/edit/$1';
$route['admin/catering_menu/view/(:num)'] = 'Admin/Catering_menus/view/$1';

$route['admin/menu_propertys'] = 'Admin/Menu_properties';
$route['admin/menu_propertys/add'] = 'Admin/Menu_properties/add';
$route['admin/menu_propertys/edit/(:num)'] = 'Admin/Menu_properties/edit/$1';
$route['admin/menu_propertys/view/(:num)'] = 'Admin/Menu_properties/view/$1';

$route['admin/settings'] = 'Admin/Setting';
$route['admin/settings/add'] = 'Admin/Setting/add';
$route['admin/settings/edit/(:num)'] = 'Admin/Setting/edit/$1';

$route['admin/refer'] = 'Admin/Refer_logs/index/0';
$route['admin/refer/(:num)'] = 'Admin/Refer_logs/index/$1';
$route['admin/refer/approve/(:num)'] = 'Admin/Refer_logs/approve/$1';
$route['admin/refer/decline/(:num)'] = 'Admin/Refer_logs/decline/$1';

$route['admin/users'] = 'Admin/User/index/0';
$route['admin/users/(:num)'] = 'Admin/User/index/$1';
$route['admin/users/edit/(:num)'] = 'Admin/User/edit/$1';
$route['admin/users/add'] = 'Admin/User/add';

$route['admin/invoices'] = 'Admin/Invoice/index/0';
$route['admin/invoices/(:num)'] = 'Admin/Invoice/index/$1';
$route['admin/invoices'] = 'Admin/Invoice';
$route['admin/invoices/view/(:num)'] = 'Admin/Invoice/view/$1';

$route['admin/payments'] = 'Admin/Payment/index/0';
$route['admin/payments/(:num)'] = 'Admin/Payment/index/$1';
$route['admin/payments/view/(:num)'] = 'Admin/Payment/view/$1';
$route['admin/payments/paid/(:num)'] = 'Admin/Payment/paid/$1';
$route['admin/reports/order'] = 'Admin/Reports/order_report';
$route['admin/reports/item'] = 'Admin/Reports/item_order_report';
$route['admin/reports/earning'] = 'Admin/Reports/earning_order_report';
$route['admin/reports/metric'] = 'Admin/Reports/metric_order_report';

$route['admin/menu'] = 'Admin/Menus/index/0';
$route['admin/menu/(:num)'] = 'Admin/Menus/index/$1';
$route['admin/menu/add'] = 'Admin/Menus/add';
$route['admin/menu/edit/(:num)'] = 'Admin/Menus/edit/$1';
$route['admin/menu/remove/(:num)'] = 'Admin/Menus/remove/$1';
$route['admin/menu/image/(:num)'] = 'Admin/Menus/add_image';
$route['admin/menu/image/remove/(:num)'] = 'Admin/Menus/remove_image';
$route['admin/menu/addon/(:num)'] = 'Admin/Menu_addons/index/$1';
$route['admin/menu/addon/add/(:num)'] = 'Admin/Menu_addons/add/$1';
$route['admin/menu/addon/edit/(:num)'] = 'Admin/Menu_addons/edit/$1';
$route['admin/menu/addon/remove/(:num)'] = 'Admin/Menu_addons/remove/$1';

$route['admin/profile'] = 'Admin/Profile';
//Member
$route['member/login'] = 'Member/Login';
$route['member/register'] = 'Member/Register';
$route['member/logout'] = 'Member/Logout';
$route['member/forgot'] = 'Member/Login/forgot';
$route['member/reset/(:any)'] = 'Member/Login/reset/$1';
$route['member/profile'] = 'Member/Profile';
$route['member/dashboard'] = 'Member/Dashboard';
$route['member/payment'] = 'Member/Payment';
$route['member/card/remove/(:num)'] = 'Member/Payment/remove/$1';

$route['member/expense'] = 'Member/Expense/index';
$route['member/orders'] = 'Member/Order/index/0';
$route['member/orders/(:num)'] = 'Member/Order/index/$1';
$route['member/orders/view'] = 'Member/Order/view';
$route['member/orders/cancel/(:num)'] = 'Member/Order/cancel/$1';
$route['member/orders/edit/(:num)'] = 'Member/Order/edit/$1';
$route['member/orders/reorder/(:num)'] = 'Member/Order/reorder/$1';
$route['member/orders/issue/(:num)'] = 'Member/Order/issue/$1';
$route['member/orders/resolution/(:num)'] = 'Member/Order/resolution/$1';
$route['member/orders/review/(:num)'] = 'Member/Order/review/$1';
$route['member/reciepts/(:num)'] = 'Member/Reciepts/index/$1';
$route['member/reciepts/view/(:num)'] = 'Member/Reciepts/view/$1';
$route['member/reviews/(:num)'] = 'Member/Reviews/index/$1';
$route['member/suggests'] = 'Member/Suggests';
$route['member/refers/(:num)'] = 'Member/Refer/index/$1';
$route['member/verify/code'] = 'Member/Profile/verify_code';

//catering
$route['catering/login'] = 'Catering/Login';
$route['catering/logout'] = 'Catering/Login/logout';
$route['catering/forgot'] = 'Catering/Login/forgot';
$route['catering/reset/(:any)'] = 'Catering/Login/reset/$1';
$route['catering/profile'] = 'Catering/Profile';

$route['catering/dashboard'] = 'Catering/Dashboard';
$route['catering/rankings'] = 'Catering/Ranking';
$route['catering/payments'] = 'Catering/Payment/index/0';
$route['catering/payments/(:num)'] = 'Catering/Payment/index/$1';
$route['catering/payments/view/(:num)'] = 'Catering/Payment/view/$1';
$route['catering/customers'] = 'Catering/Customer_restaurants/index/0';
$route['catering/customers/(:num)'] = 'Catering/Customer_restaurants/index/$1';
$route['catering/reports/order'] = 'Catering/Reports/order_report';
$route['catering/reports/item'] = 'Catering/Reports/item_order_report';
$route['catering/reports/add'] = 'Catering/Reports/add';
$route['catering/reviews'] = 'Catering/Reviews/index/0';
$route['catering/reviews/(:num)'] = 'Catering/Reviews/index/$1';
$route['catering/restaurant'] = 'Catering/Restaurant';

$route['catering/menu'] = 'Catering/Menus';
$route['catering/menu/add'] = 'Catering/Menus/add';
$route['catering/menu/edit/(:num)'] = 'Catering/Menus/edit/$1';
$route['catering/menu/remove/(:num)'] = 'Catering/Menus/remove/$1';
$route['catering/menu/image/(:num)'] = 'Catering/Menus/add_image';
$route['catering/menu/image/remove/(:num)'] = 'Catering/Menus/remove_image';
$route['catering/menu/addon/(:num)'] = 'Catering/Menu_addons/index/$1';
$route['catering/menu/addon/add/(:num)'] = 'Catering/Menu_addons/add/$1';
$route['catering/menu/addon/edit/(:num)'] = 'Catering/Menu_addons/edit/$1';
$route['catering/menu/addon/remove/(:num)'] = 'Catering/Menu_addons/remove/$1';

$route['catering/orders'] = 'Catering/Order/index/0';
$route['catering/orders/(:num)'] = 'Catering/Order/index/$1';
$route['catering/orders/edit/(:num)'] = 'Catering/Order/edit/$1';
$route['catering/orders/cancel/(:num)'] = 'Catering/Order/cancel/$1';
$route['catering/orders/accept/(:num)'] = 'Catering/Order/accept/$1';
$route['catering/orders/process/(:num)'] = 'Catering/Order/process/$1';
$route['catering/orders/delivery/(:num)'] = 'Catering/Order/delivery/$1';
$route['catering/orders/delivered/(:num)'] = 'Catering/Order/delivered/$1';
$route['catering/orders/view/(:num)'] = 'Catering/Order/view/$1';
$route['catering/orders/status/(:num)'] = 'Catering/Order/status/$1';

//guest
$route['google'] = 'Member/Login/google';
$route['about'] = 'Guest/About';
$route['enterprise'] = 'Guest/Enterprise';
$route['become'] = 'Guest/Become';
$route['policy'] = 'Guest/Policy';
$route['v1/api/contact'] = 'Guest/Contacts';
$route['search'] = 'Guest/RestaurantSearch';
$route['faq'] = 'Guest/Faq';
$route['tos'] = 'Guest/Tos';
$route['v1/api/filter'] = 'Guest/RestaurantSearch/search';
$route['v1/api/search/save'] = 'Guest/RestaurantSearch/save_search';
$route['v1/api/checkout'] = 'Guest/Checkout';
$route['v1/api/payments'] = 'Api/Payment_cron';
$route['restaurant/(:num)'] = 'Guest/RestaurantSearch/restaurant/$1';
$route['checkout/1/(:num)'] = 'Guest/Checkout/step_1/$1';
$route['checkout/2/(:num)'] = 'Member/Checkout/step_2/$1';
$route['checkout/3/(:num)'] = 'Member/Checkout/step_3/$1';
$route['checkout/4/(:num)'] = 'Member/Checkout/step_4/$1';
$route['checkout/5/(:num)'] = 'Member/Checkout/step_5/$1';

//api
$route['v1/api/attachment/upload/blob'] = 'Api/Upload/ajax_single_upload_blob';
$route['v1/api/attachment/upload/single'] = 'Api/Upload/ajax_single_upload';
$route['v1/api/attachment/upload/multi'] = 'Api/Upload/ajax_multi_upload';
$route['v1/api/catering/(:any)'] = 'Api/Catering/index/$1';
$route['v1/api/user/(:any)'] = 'Api/User/index/$1';
$route['v1/api/menu/(:any)'] = 'Api/Menu/index/$1';
$route['v1/api/category/(:any)'] = 'Api/Category/index/$1';
$route['v1/api/cuisine/(:any)'] = 'Api/Cuisine/index/$1';
$route['v1/api/preferences'] = 'Api/Preference_search';
$route['v1/api/reviews/(:num)/page/(:num)'] = 'Api/Reviews/index/$1/$2';
$route['v1/api/address/(:any)'] = 'Api/Address/index/$1';
$route['v1/api/menu_property/(:any)'] = 'Api/Menu_property/index/$1';
$route['v1/api/search'] = 'Api/Search/index';
$route['v1/api/restaurant/upload/(:any)'] = 'Api/Upload/image/$1';
$route['v1/api/menus/upload'] = 'Api/Upload/menu';

$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
$route['default_controller'] = 'Welcome/index';
