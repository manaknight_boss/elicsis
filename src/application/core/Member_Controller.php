<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Member_Controller extends Manaknight_Controller
{

    public $_page_id = 0;
    public $_page_name ='dashboard';

    public function __construct()
    {
        parent::__construct();

        if (empty($_SESSION) || ! isset($_SESSION['user_id']) || ! isset($_SESSION['email']))
        {
            $_SESSION = [];
            unset($_SESSION);
            $this->load->helper('cookie');
            $cookie = [
                'name'   => 'redirect',
                'value'  => '/' . uri_string(),
                'expire' => '60',
                'secure' => FALSE
            ];
            set_cookie($cookie);
            return redirect('/member/login', 'refresh');
        }

        $user_id = $_SESSION['user_id'];
        $email = $_SESSION['email'];
        $role = $_SESSION['role'];

        $condition = $this->is_member($role) && ($user_id > 0) && (strlen($email) > 0);

        if (!$condition)
        {
            unset($_SESSION);
            redirect('/member/login', 'refresh');
        }

        $this->_data['loggedin'] = TRUE;
        $this->_data['is_enterprise_user'] = FALSE;
        $this->_data['layout_role'] = $role;
        $this->_data['page_name'] = $this->_page_name;
        $this->_data['enable_google_api'] = FALSE;
        $this->_data['google_key'] = '';
        $this->_data['google_api_callback'] = '';
        // $this->_data['google_callback'] = 'initAutocomplete';

        $refer_code = $this->input->get('affilate', TRUE);

        if ($refer_code && strlen($refer_code) > 0)
        {
            $_SESSION['refer'] = $refer_code;
        }

        $this->load->model('users');
        $user = $this->users->get_user($user_id);
        if ($user && $user->profile_type == 'Empresa')
        {
            $this->_data['is_enterprise_user'] = TRUE;
        }

        if (MAINTENANCE)
        {
            header( '503 Service Unavailable', true, 503 );
            include dirname(__FILE__) . '/../../maintenance.html';
            exit;
        }
    }

    protected function is_member($role)
    {
        $this->load->model('users');

        $valid_roles = [
            $this->users->MEMBER
        ];

        return ($role != NULL) && in_array((int)$role, $valid_roles);
    }

    protected function is_paying_member($role)
    {
        $this->load->model('users');
        $valid_roles = [
            $this->users->MEMBER,
            $this->users->ENTERPRISE,
            $this->users->SYSTEM,
            $this->users->ADMIN
        ];
        return in_array($role, $valid_roles);
    }

    public function render($template, $data) {
        $this->load->view('Layout/MemberHeader', $data);
        $this->load->view($template, $data);
        $this->load->view('Layout/MemberFooter', $data);
    }
}