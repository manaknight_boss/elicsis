<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Abstract Controller
 *
 * @author manaknight
 *
 */
class Guest_Controller extends CI_Controller
{

    /**
     * View Model
     * @var array
     */
    public $_data = [
        'error' => '',
        'success' => ''
    ];

    public $_loggedin = FALSE;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users');

        $this->_data['google_key'] = $this->config->item('google_key');
        $this->_data['loggedin'] = FALSE;
        $this->_data['enable_spa'] = FALSE;
        $this->_data['google_callback'] = 'initAutocomplete';

        if (isset($_SESSION['user_id']) && isset($_SESSION['email']) &&
        $_SESSION['role'] == (string) $this->users->MEMBER)
        {
            $this->_data['loggedin'] = TRUE;
        }

        $refer_code = $this->input->get('affilate', TRUE);

        if ($refer_code && strlen($refer_code) > 0)
        {
            $_SESSION['refer'] = $refer_code;
        }
    }

    /**
     * Debug Controller to error_log and turn off in production
     *
     * @param mixed $data
     * @return void
     */
    public function dl($key, $data)
    {
        if (ENVIRONMENT == 'development')
        {
            log_message('debug', $key . ' CONTROLLER : <pre>' . print_r($data, TRUE) . '</pre>');
        }
    }

    /**
     * Debug json Controller to error_log and turn off in production
     *
     * @param mixed $data
     * @return void
     */
    public function dj($key, $data)
    {
        if (ENVIRONMENT == 'development')
        {
            log_message('debug', $key . ' CONTROLLER : ' . json_encode($data));
        }
    }
}