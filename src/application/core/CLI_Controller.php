<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Abstract Controller
 * 
 * @author manaknight
 *
 */
class CLI_Controller extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        if(!$this->input->is_cli_request())
        {
            echo 'This script can only be accessed via the command line' . PHP_EOL;
            exit;
        }
    }

    public function notify_error($message)
    {
        $this->load->library('mail_service');
        $this->mail_service->set_adapter('mailgun');
        $this->mail_service->setDomain('manaknightdigital.com');
        $this->mail_service->send('ryan@manaknightdigital.com', 'ryan@manaknightdigital.com', 'Critical Error ' . date('Y-m-j H:i:s'), $message);
    }
}