<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Catering_Controller extends Manaknight_Controller
{

    public $_page_name ='dashboard';

    public function __construct()
    {
        parent::__construct();
        $session = $_SESSION;

        if (empty($_SESSION) || ! isset($_SESSION['user_id']) || ! isset($_SESSION['email']))
        {
            $_SESSION = [];
            unset($_SESSION);
            return redirect('/catering/login', 'refresh');
        }

        $user_id = $_SESSION['user_id'];
        $email = $_SESSION['email'];
        $role = $_SESSION['role'];

        $condition = $this->is_catering($role) && ($user_id > 0) && (strlen($email) > 0);

        $this->_data['page_name'] = $this->_page_name;
        $this->_data['enable_google_api'] = FALSE;
        $this->_data['google_key'] = '';
        $this->_data['google_api_callback'] = '';
        $this->_data['restaurant_menu_name'] = isset($_SESSION['restaurant_name']) ? $_SESSION['restaurant_name'] : '';

        if (!$condition)
        {
            unset($_SESSION);
            redirect('/catering/login', 'refresh');
        }
    }

    public function render($template, $data) {
            $this->load->view('Layout/CateringHeader', $data);
            $this->load->view($template, $data);
            $this->load->view('Layout/CateringFooter', $data);
    }

    protected function is_catering($role)
    {
        $this->load->model('restaurants');
        $valid_roles = [
            1
        ];
        return ($role != NULL) && in_array((int)$role, $valid_roles);
    }
}