<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Abstract Controller
 *
 * @author manaknight
 *
 */
class Manaknight_Controller extends CI_Controller
{

    // If set, this language file will automatically be loaded.
    protected $language_file = NULL;

    // If set, this model file will automatically be loaded.
    protected $model_file = NULL;

    //testMode flag
    protected $test_mode = false;

    protected $_format = 'json';

    /**
     * View Model
     * @var array
     */
    public $_data = [
        'error' => '',
        'success' => ''
    ];

    /**
     * Flash Data
     * @var array
     */
    protected $flash_error = [
        'error' => '',
        'success' => ''
    ];

    public function __construct()
    {
        parent::__construct();

        //--------------------------------------------------------------------
        // Language & Model Files
        //--------------------------------------------------------------------

        if (!is_null($this->language_file))
        {
            $this->lang->load($this->language_file);
        }

        if (!is_null($this->model_file))
        {
            $this->load->model($this->model_file);
        }

        //Flashdata setup
        if ($this->session->flashdata('error')) {
            $this->flash_error['error'] = $this->session->flashdata('error');
            $this->session->set_flashdata('error', '');
            $this->_data['error'] = $this->flash_error['error'];
        }

        if ($this->session->flashdata('success')) {
            $this->flash_error['success'] = $this->session->flashdata('success');
            $this->session->set_flashdata('success', '');
            $this->_data['success'] = $this->flash_error['success'];
        }
    }

    /**
     * Render view
     *
     * @param string $template
     * @param array $data
     */
    public function render($template, $data)
    {
            $this->load->view('Layout/Header', $data);
            $this->load->view($template, $data);
            $this->load->view('Layout/Footer');
    }

    /**
     * Render view
     *
     * @param string $template
     * @param array $data
     */
    public function redirect($url, $option)
    {
            if ($option)
            {
                redirect($url, $option);
            }
            else
            {
                redirect($url);
            }
    }

    /**
     * Set the Flashdata
     *
     * @param string $message
     */
    public function success($message)
	{
      $this->session->set_flashdata('success', $message);
    }

    /**
     * Set the Flashdata
     *
     * @param string $message
     */
    public function error($message)
	{
      $this->session->set_flashdata('error', $message);
    }

    /**
     * Used for API graphql to translate text.
     *
     * @param string $lang
     * @param string $text
     * @return string
     */
    public function translate_text($lang='en', $text)
    {
        return $text;
    }

    public function notify_error($message)
    {
        $this->load->library('mail_service');
        $this->mail_service->set_adapter('mailgun');
        $this->mail_service->setDomain('manaknightdigital.com');
        $this->mail_service->send('admin@manaknightdigital.com', 'ryan@manaknightdigital.com', 'Critical Error ' . date('Y-m-j H:i:s'), $message);
    }

    protected function check_license_key ()
    {
        if (strlen($this->license_key) < 1)
        {
            exit();
        }

    }

    /**
     * Debug Controller to error_log and turn off in production
     *
     * @param mixed $data
     * @return void
     */
    public function dl($key, $data)
    {
        if (ENVIRONMENT == 'development')
        {
            log_message('debug', $key . ' CONTROLLER : <pre>' . print_r($data, TRUE) . '</pre>');
        }
    }

    /**
     * Debug json Controller to error_log and turn off in production
     *
     * @param mixed $data
     * @return void
     */
    public function dj($key, $data)
    {
        if (ENVIRONMENT == 'development')
        {
            log_message('debug', $key . ' CONTROLLER : ' . json_encode($data));
        }
    }
}