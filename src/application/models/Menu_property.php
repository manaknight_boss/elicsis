<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Menu_property Model
 * @author manaknight inc.
 *
 */
class Menu_property extends CI_Model
{
    public $_status_mapping = [
			0 => 'Inactivo',
			1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Menu_property
	 *
	 * @param integer $id
	 * @return menu_property
	 */
	public function get_menu_property($id)
    {
		$this->db->from('menu_property');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Menu_property
	 *
	 * @return array menu_property
	 */
	public function get_menu_properties($key_value=FALSE)
    {
		$this->db->from('menu_property');
		$results = $this->db->get()->result();
		if ($key_value)
		{
			$data = [];
			foreach ($results as $key => $value) {
				$data[$value->id] = $value->name;
			}
			return $data;
		}
		else
		{
			return $results;
		}
	}

	/**
	 * Create Menu_property
	 *
	 * @param array $data
	 * @return Menu_property
	 */
	public function create_menu_property($data)
	{
		return $this->db->insert('menu_property', $data, TRUE);
	}

	/**
	 * Edit Menu_property
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_menu_property($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('menu_property', $data);
	}

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}