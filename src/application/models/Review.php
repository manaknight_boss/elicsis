<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Review Model
 * @author manaknight inc.
 *
 */
class Review extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Review
	 *
	 * @param integer $id
	 * @return review
	 */
	public function get_review($id)
    {
		$this->db->from('review');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Review
	 *
	 * @return array review
	 */
	public function get_reviews()
    {
		$this->db->from('review');
        return $this->db->get()->result();
	}

    /**
	 * Number of Review
	 *
	 * @access public
	 * @param $restaurant_id
	 * @return integer $result
	 */
	public function num_reviews($restaurant_id)
	{
		$this->db->where('restaurant_id', $restaurant_id);
		$this->db->where('status', 1);
		$this->db->from('review');
		return $this->db->count_all_results();
    }

	/**
	 * Average Review
	 *
	 * @access public
	 * @param $restaurant_id
	 * @return integer $result
	 */
	public function get_avg_reviews($restaurant_id)
	{
		$this->db->where('restaurant_id', $restaurant_id);
		$this->db->where('status', 1);
		$this->db->select_avg('rating');
		$result = $this->db->get('review')->row();
		$rating = 0;
		if (!$result || !$result->rating)
		{
			$rating = 0;
		}
		else
		{
			$rating = (float)$result->rating;
		}
		return $rating;
    }

    /**
	 * Number of Review
	 *
	 * @access public
	 * @param $restaurant_id
	 * @return integer $result
	 */
	public function all_num_reviews($restaurant_id)
	{
		$this->db->where('restaurant_id', $restaurant_id);
		$this->db->from('review');
		return $this->db->count_all_results();
    }

	/**
	 * Get paginated reviews
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $reviews
	 */
	public function get_paginated_reviews($restaurant_id, $page = 1, $limit=10)
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('created_at', 'DESC');
		$this->db->where('restaurant_id', $restaurant_id);
		$query = $this->db->get('review');
		$result = [];

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }
		}

        return $result;
    }

	/**
	 * Create Review
	 *
	 * @param array $data
	 * @return Review
	 */
	public function create_review($data)
	{
        $data['created_at'] = date('Y-m-j H:i:s');
		return $this->db->insert('review', $data, TRUE);
	}

	/**
	 * Edit Review
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_review($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('review', $data);
	}

    /**
     * num_review function.
     *
     * @access public
     * @return integer
     */
    public function num_user_review($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->from('review');
        return $this->db->count_all_results();
    }

    /**
     * get_paginated_review function.
     *
     * @access public
     * @param mixed $page
     * @param mixed $limit
     * @return resuly array on success, blank array on failure
     */
    public function get_paginated_user_review($user_id, $page = 1, $limit = 10)
    {
        $this->db->where('user_id', $user_id);
        $this->db->limit($limit, $page);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('review');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }
}