<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Cart Model
 * @author manaknight inc.
 *
 */
class Cart extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get cart
	 *
	 * @param integer $id
	 * @return cart
	 */
	public function get_cart($id)
  {
		$this->db->from('cart');
		$this->db->where('id', $id, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * Get all cart by Restaurant
	 *
	 * @return array cart
	 */
	public function get_carts_by_restaurant($restaurant_id)
	{
		$this->db->from('cart');
		$this->db->where('restaurant_id', $restaurant_id, TRUE);
		return $this->db->get()->result();
	}

	/**
	 * Get all cart by User
	 *
	 * @return array cart
	 */
	public function get_carts_by_user($user_id)
	{
		$this->db->from('cart');
		$this->db->where('user_id', $user_id, TRUE);
		$this->db->where('is_complete', 0, TRUE);
		$this->db->limit(10, 0);
		return $this->db->get()->result();
	}


	/**
	 * Create cart
	 *
	 * @param array $data
	 * @return cart
	 */
	public function create_cart($data)
	{
		$data['created_at'] = date('Y-m-j');
		if ($this->db->insert('cart', $data, TRUE))
        {
            return $this->db->insert_id();
        }
        else
        {
            return FALSE;
        }
	}

	/**
	 * Edit cart
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_cart($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('cart', $data);
	}

	public function process_reorder_cart($data, $setting)
	{
		unset($data['date']);
		unset($data['time']);
		$total = 0;

		$tax_rate = (float)$setting['tax'] / 100;
		$tips = json_decode($setting['tips'], TRUE);
		$delivery_fee = ($data['is_delivery'] == '1') ? $data['delivery_fee'] : 0;

		foreach ($data['items'] as $key => $item)
		{
			$id = $item['id'];
			$price = $this->_get_menu_price($id);
			$quantity = (int)$item['quantity'];
			if ($price != 0)
			{
				$data['items'][$key]['price'] = $price;
			}
			else
			{
				$price = $item['price'];
			}

			$total += ($price * $quantity);

			foreach ($item['addons'] as $index => $addon)
			{
				$addon_id = $addon['id'];
				$addon_price = $this->_get_addon_price($addon_id);

				if ($addon_price != 0)
				{
					$data['items'][$key]['addons'][$index]['price'] = $addon_price;
				}
				else
				{
					$addon_price = $addon['price'];
				}

				$total += ($addon_price * $quantity);
			}
		}

		$data['total'] = $total;

		if (in_array($data['tips'], $tips))
		{
			$data['real_tips'] = ($data['tips'] / 100) *
			($data['total'] + $delivery_fee + $data['tax']);
		}

		$data['tax'] = $tax_rate * $data['total'];
		return $data;
	}

	private function _get_menu_price ($id)
	{
		$this->db->from('menu');
		$this->db->where('id', $id, TRUE);
		$menu = $this->db->get()->row();

		if ($menu)
		{
			return $menu->price;
		}

		return 0;
	}

	private function _get_addon_price ($id)
	{
		$this->db->from('menu_addon');
		$this->db->where('id', $id, TRUE);
		$menu_addon = $this->db->get()->row();

		if ($menu_addon)
		{
			return $menu_addon->price;
		}

		return 0;
	}
}