<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Verify_number Model
 * @author manaknight inc.
 *
 */
class Verify_number extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Verify_number
	 *
	 * @param integer $id
	 * @return verify_number
	 */
	public function get_verify_number($code, $user_id)
	{
		$this->db->from('verify_number');
		$this->db->where('code', $code, TRUE);
		$this->db->where('user_id', $user_id, TRUE);
        $row =  $this->db->get()->row();

        if (!$row)
        {
            return NULL;
        }

        return $row->id;
	}

	/**
	 * Get all Verify_number
	 *
	 * @return array verify_number
	 */
	public function get_verify_numbers()
	{
		$this->db->from('verify_number');
		return $this->db->get()->result();
	}


	/**
	 * Create Verify_number
	 *
	 * @param array $data
	 * @return Verify_number
	 */
	public function create_verify_number($data)
	{
		$data['created_at'] = date('Y-m-j');
		$data['expire_at'] = date('Y-m-j H:i:s', strtotime('+ 5 minutes'));
		return $this->db->insert('verify_number', $data, TRUE);
    }

    /**
	 * Delete suggest
	 *
	 * @param integer $id
	 * @return suggest
	 */
	public function delete($id)
    {
		$this->db->where('id', $id);
		return $this->db->delete('verify_number');
	}
}