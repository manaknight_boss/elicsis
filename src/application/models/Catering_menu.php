<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Catering_menu Model
 * @author manaknight inc.
 *
 */
class Catering_menu extends CI_Model
{
    public $_status_mapping = [
			0 => 'Inactivo',
			1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Catering_menu
	 *
	 * @param integer $id
	 * @return catering_menu
	 */
	public function get_catering_menu($id)
    {
		$this->db->from('catering_menu');
		$this->db->where('id', $id, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * Get all Catering_menu
	 *
	 * @return array catering_menu
	 */
	public function get_catering_menus($key_value=FALSE)
    {
		$this->db->from('catering_menu');
		$results = $this->db->get()->result();
		if ($key_value)
		{
			$data = [];
			foreach ($results as $key => $value) {
				$data[$value->id] = $value->name;
			}
			return $data;
		}
		else
		{
			return $results;
		}
	}

	/**
	 * Create Catering_menu
	 *
	 * @param array $data
	 * @return Catering_menu
	 */
	public function create_catering_menu($data)
	{

		return $this->db->insert('catering_menu', $data, TRUE);
	}

	/**
	 * Edit Catering_menu
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_catering_menu($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('catering_menu', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}