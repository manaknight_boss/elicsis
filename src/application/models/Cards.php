<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Cards Model
 * @author manaknight inc.
 *
 */
class Cards extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Card
	 *
	 * @param integer $id
	 * @return Cards
	 */
	public function get_card($id)
    {
		$this->db->from('cards');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Cards
	 *
	 * @return array Cards
	 */
	public function get_cards()
	{
		$this->db->from('cards');
		return $this->db->get()->result();
	}

	/**
	 * Get all Cards by user
	 *
	 * @return array Cards
	 */
	public function get_cards_by_user($user_id)
	{
		$this->db->from('cards');
		$this->db->where('user_id', $user_id, TRUE);
		return $this->db->get()->result();
	}

	/**
	 * Create Card
	 *
	 * @param array $data
	 * @return Card
	 */
	public function create_card($data)
	{
		$data['create_at'] = date('Y-m-j');
		$data['update_at'] = date('Y-m-j');
		return $this->db->insert('cards', $data, TRUE);
	}

  /**
	 * Delete cards
	 *
	 * @param integer $id
	 * @return cards
	 */
	public function delete($id)
    {
		$this->db->where('id', $id);
		return $this->db->delete('cards');
	}
}