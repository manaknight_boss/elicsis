<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Coupons Model
 * @author manaknight inc.
 *
 */
class Coupons extends CI_Model
{

	public $PERCENT = 'percent';
	public $FIXED = 'fixed';

    public $_status_mapping = [
			0 => 'Inactivo',
			1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Coupons
	 *
	 * @param integer $id
	 * @return coupons
	 */
	public function get_coupon($id)
    {
		$this->db->from('coupons');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Coupons
	 *
	 * @return array coupons
	 */
	public function get_coupons()
    {
		$this->db->from('coupons');
        return $this->db->get()->result();
	}

	/**
	 * Create Coupons
	 *
	 * @param array $data
	 * @return Coupons
	 */
	public function create_coupon($data)
	{
        $data['created_at'] = date('Y-m-j');
		return $this->db->insert('coupons', $data, TRUE);
	}

	/**
	 * Edit Coupons
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_coupon($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('coupons', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}