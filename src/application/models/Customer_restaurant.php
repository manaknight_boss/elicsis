<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Customer_restaurant Model
 * @author manaknight inc.
 *
 */
class Customer_restaurant extends CI_Model
{
    public $_status_mapping = [
		0 => 'Inactivo',
		1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Number of customer_restaurant
	 *
	 * @access public
	 * @return integer $result
	 */
	public function num_customer_restaurants($restaurant_id)
	{
		$this->db->where('restaurant_id',$restaurant_id);
		$this->db->from('customer_restaurant');
		return $this->db->count_all_results();
	}

	/**
	 * Number of customer_restaurant
	 *
	 * @access public
	 * @return integer $result
	 */
	public function num_repeat_customer($restaurant_id)
	{
		$this->db->where('restaurant_id',$restaurant_id);
		$this->db->where('num_order>1');
		$this->db->from('customer_restaurant');
		return $this->db->count_all_results();
	}

	/**
	 * Get paginated customer_restaurants
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $customer_restaurants
	 */
	public function get_paginated_customer_restaurants($page = 1, $limit=10, $where)
    {
		$this->db->limit($limit, $page);
		if (!empty($where))
        {
			foreach ($where as $key => $value)
			{
				$this->db->where($key, $value);
            }
        }
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('customer_restaurant');
		$result = [];

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }
		}

        return $result;
	}

	/**
	 * Get Customer_restaurant
	 *
	 * @param integer $id
	 * @return customer_restaurant
	 */
	public function get_customer_restaurant($id)
    {
		$this->db->from('customer_restaurant');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Customer_restaurant
	 *
	 * @return array customer_restaurant
	 */
	public function get_customer_restaurants()
    {
		$this->db->from('customer_restaurant');
        return $this->db->get()->result();
	}

	/**
	 * Create Customer_restaurant
	 *
	 * @param array $data
	 * @return Customer_restaurant
	 */
	public function create_customer_restaurant($data)
	{
		$data['first_order_date'] = date('Y-m-j');
		return $this->db->insert('customer_restaurant', $data, TRUE);
	}

	/**
	 * Edit Customer_restaurant
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_customer_restaurant($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('customer_restaurant', $data);
	}

	public function create_relationship($restaurant_id, $user_id)
	{
		$this->db->from('customer_restaurant');
        $this->db->where('restaurant_id', $restaurant_id, TRUE);
        $this->db->where('user_id', $user_id, TRUE);
		$relationship = $this->db->get()->row();
		if ($relationship)
		{
			$relationship->num_order++;
			return $this->edit_customer_restaurant([
				'num_order' => $relationship->num_order
			], $relationship->id);
		}
		else
		{
			return $this->create_customer_restaurant([
				'restaurant_id'	 => $restaurant_id,
				'user_id'	 => $user_id,
				'num_order' => 1
			]);
		}
	}

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}