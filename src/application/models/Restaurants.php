<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Restaurants Model
 * @author manaknight inc.
 *
 */
class Restaurants extends CI_Model
{
	public $_status_mapping = [
		0 => 'Inactive',
		1 => 'Activo'
	];

	public $_delivery_type_mapping = [
		0 => 'Ambos',
		1 => 'Delivery',
		2 => 'Recoger'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

/**
	 * resolve_restaurant_login function.
	 *
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function authenticate($email, $password)
	{
		$this->db->select('password');
		$this->db->from('restaurants');
		$this->db->where('email', $email, TRUE);
		$this->db->where('status', 1, TRUE);

		$hash = $this->db->get()->row('password');

		return $this->verify_password_hash($password, $hash);
	}

	/**
	 * get_restaurant_id_from_restaurantname function.
	 *
	 * @access public
	 * @param mixed $email
	 * @return int the restaurant id
	 */
	public function get_restaurant_id_from_email($email)
	{
		$this->db->from('restaurants');
		$this->db->where('email', $email, TRUE);
		return $this->db->get()->row('id');
	}

	/**
	 * get_restaurant_from_email function.
	 *
	 * @access public
	 * @param mixed $email
	 * @return object restaurant
	 */
	public function get_restaurant_from_email($email)
	{
		$this->db->from('restaurants');
		$this->db->where('email', $email, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * get_restaurant_from_email function.
	 *
	 * @access public
	 * @param mixed $email
	 * @return object restaurant
	 */
	public function reset_password($restaurant_id)
	{
		$token = md5(uniqid() . time());
		$this->db->set('reset_token', $token, TRUE);
		$this->db->where('id', $restaurant_id, TRUE);
		$this->db->update('restaurants');
		return $token;
	}

	/**
	 * Get Restaurants
	 *
	 * @param integer $id
	 * @return restaurants
	 */
	public function get_restaurant($id)
	{
		$this->db->from('restaurants');
		$this->db->where('id', $id, TRUE);
		return $this->db->get()->row();
	}

		/**
	 * Get Restaurants
	 *
	 * @param integer $id
	 * @return restaurants
	 */
	public function get_restaurants_by_ids($ids)
	{
		if (count($ids) < 1)
		{
			return [];
		}

		$this->db->from('restaurants');
		$this->db->where_in('id', $ids);
		return $this->db->get()->result();
	}


	/**
	 * Get all Restaurants
	 *
	 * @return array restaurants
	 */
	public function get_restaurants()
	{
		$this->db->from('restaurants');
		return $this->db->get()->result();
	}

	/**
	 * Number of restaurant
	 *
	 * @access public
	 * @return integer $result
	 */
	public function num_restaurants()
	{
		$this->db->from('restaurants');
		return $this->db->count_all_results();
	}

	/**
	 * Get paginated restaurants
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $restaurants
	 */
	public function get_paginated_restaurants($page = 1, $limit=10)
	{
		$this->db->limit($limit, $page);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('restaurants');
		$result = [];

		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$result[] = $row;
			}
		}
		return $result;
	}

	/**
	 * Create Restaurants
	 *
	 * @param array $data
	 * @return Restaurants
	 */
	public function create_restaurant($data)
	{
		$data['created_at'] = date('Y-m-j');
		$data['password'] = $this->hash_password($data['password']);
		foreach ($data as $key => $value) {
			if ($value === NULL)
			{
				$data[$key] = 0;
			}
		}
		$data['u_open_total'] = ((integer)$data['u_open_min']) + (((integer)$data['u_open']) * 60);
		$data['u_close_total'] = ((integer)$data['u_close_min']) + (((integer)$data['u_close']) * 60);
		$data['m_open_total'] = ((integer)$data['m_open_min']) + (((integer)$data['m_open']) * 60);
		$data['m_close_total'] = ((integer)$data['m_close_min']) + (((integer)$data['m_close']) * 60);
		$data['t_open_total'] = ((integer)$data['t_open_min']) + (((integer)$data['t_open']) * 60);
		$data['t_close_total'] = ((integer)$data['t_close_min']) + (((integer)$data['t_close']) * 60);
		$data['w_open_total'] = ((integer)$data['w_open_min']) + (((integer)$data['w_open']) * 60);
		$data['w_close_total'] = ((integer)$data['w_close_min']) + (((integer)$data['w_close']) * 60);
		$data['r_open_total'] = ((integer)$data['r_open_min']) + (((integer)$data['r_open']) * 60);
		$data['r_close_total'] = ((integer)$data['r_close_min']) + (((integer)$data['r_close']) * 60);
		$data['f_open_total'] = ((integer)$data['f_open_min']) + (((integer)$data['f_open']) * 60);
		$data['f_close_total'] = ((integer)$data['f_close_min']) + (((integer)$data['f_close']) * 60);
		$data['s_open_total'] = ((integer)$data['s_open_min']) + (((integer)$data['s_open']) * 60);
		$data['s_close_total'] = ((integer)$data['s_close_min']) + (((integer)$data['s_close']) * 60);

		return $this->db->insert('restaurants', $data, TRUE);
	}

	/**
	 * get_restaurant_from_reset_token function
	 *
	 * @access public
	 * @param string $token
	 * @return object the restaurant object
	 */
	public function get_restaurant_from_reset_token($token)
	{

		$this->db->from('restaurants');
		$this->db->where('reset_token', $token, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * Edit Restaurants
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_restaurant($data, $id)
	{
		$this->db->where('id', $id, TRUE);

		if (isset($data['password']) && strlen($data['password']) > 0)
		{
			$data['password'] = $this->hash_password($data['password']);
			$data['reset_token'] = '';
		}
		else
		{
			if (isset($data['password']))
			{
				unset($data['password']);
			}
		}

		if (isset($data['u_open']))
		{
			$data['u_open_total'] = ((integer)$data['u_open_min']) + (((integer)$data['u_open']) * 60);
			$data['u_close_total'] = ((integer)$data['u_close_min']) + (((integer)$data['u_close']) * 60);
			$data['m_open_total'] = ((integer)$data['m_open_min']) + (((integer)$data['m_open']) * 60);
			$data['m_close_total'] = ((integer)$data['m_close_min']) + (((integer)$data['m_close']) * 60);
			$data['t_open_total'] = ((integer)$data['t_open_min']) + (((integer)$data['t_open']) * 60);
			$data['t_close_total'] = ((integer)$data['t_close_min']) + (((integer)$data['t_close']) * 60);
			$data['w_open_total'] = ((integer)$data['w_open_min']) + (((integer)$data['w_open']) * 60);
			$data['w_close_total'] = ((integer)$data['w_close_min']) + (((integer)$data['w_close']) * 60);
			$data['r_open_total'] = ((integer)$data['r_open_min']) + (((integer)$data['r_open']) * 60);
			$data['r_close_total'] = ((integer)$data['r_close_min']) + (((integer)$data['r_close']) * 60);
			$data['f_open_total'] = ((integer)$data['f_open_min']) + (((integer)$data['f_open']) * 60);
			$data['f_close_total'] = ((integer)$data['f_close_min']) + (((integer)$data['f_close']) * 60);
			$data['s_open_total'] = ((integer)$data['s_open_min']) + (((integer)$data['s_open']) * 60);
			$data['s_close_total'] = ((integer)$data['s_close_min']) + (((integer)$data['s_close']) * 60);
		}

		return $this->db->update('restaurants', $data);
	}

	/**
	 * Return Status Mapping
	 * @return array
	 */
	public function mapping ()
	{
		return $this->_status_mapping;
	}

	/**
	 * Return Status Mapping
	 * @return array
	 */
	public function delivery_type_mapping ()
	{
		return $this->_delivery_type_mapping;
	}

	/**
	 * hash_password function.
	 *
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password)
	{
		return password_hash($password, PASSWORD_BCRYPT);
	}

	/**
	 * verify_password_hash function.
	 *
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash)
	{
		return password_verify($password, $hash);
	}


	/**
	 * Get Profile
	 *
	 * @access public
	 * @param mixed $id
	 * @return mixed $users
	 */
	public function get_profile($id)
	{
		$this->db->from('restaurants');
		$this->db->where('id', $id, TRUE);
		$result =  $this->db->get()->row();

		$profile_obj = new stdClass;

		if ($result)
		{
			$profile_obj->email = $result->email;
		}

		return $profile_obj;
	}

	/**
	 * edit_profile
	 *
	 * @access public
	 * @param mixed $user_id
	 * @param mixed $data
	 * @return bool
	 */
	public function edit_profile($user_id, $data)
	{
		if (!$data)
		{
			return false;
		}

		if (isset($data['password']) && strlen($data['password']) > 0)
		{
			$this->db->set('password', $this->hash_password($data['password']), TRUE);
		}

		if (isset($data['email']) && strlen($data['email']) > 0)
		{
			$this->db->set('email', $data['email'], TRUE);
		}

		$this->db->where('id', $user_id, TRUE);

		return $this->db->update('restaurants');
	}

	/**
	 * Raw Mysql query
	 *
	 * @param string $sql
	 * @return mixed
	 */
	public function raw_query($sql)
	{
		return $this->db->query($sql);
	}

	/**
	 * Build Search parameter
	 *
	 * @param string $search
	 * @return string
	 */
    public function build_search_parameter ($search)
    {
		$search = $this->db->escape_like_str($search);
        return "( `title` LIKE '%{$search}%' OR `description` LIKE '%{$search}%')";
	}

	public function update_serving($restaurant_id, $serving)
	{
		$restaurant = $this->get_restaurant($restaurant_id);
		if ($restaurant)
		{
			$serving_old = $restaurant->serving;
			if ($serving_old < $serving)
			{
				$serving_old = $serving;
			}

			return $this->edit_restaurant(['serving' => $serving_old], $restaurant_id);
		}

		return FALSE;
	}

	public function generate_metric_report()
	{

        //TODO paginate this report

		$this->db->where("status", 1);
		$query = $this->db->get('restaurants');
		$data = [];
		if ($query->num_rows() > 0)
		{
            foreach ($query->result() as $row)
            {
                $payload = [
                    'id' => $row->id,
                    'on_time_rating' => $row->on_time_rating,
                    'on_delivery_rating' => $row->on_delivery_rating,
                    'avg_review' => $row->rating,
                    'num_review' => $row->num_review
                ];
                $data[] = array_values($payload);
            }
		}
		return $data;
	}

	public function get_schedule ($restaurant)
	{
		return [
			0 => ['open' => $restaurant->u_open, 'close' => $restaurant->u_close, 'active' => $restaurant->u],
			1 => ['open' => $restaurant->m_open, 'close' => $restaurant->m_close, 'active' => $restaurant->m],
			2 => ['open' => $restaurant->t_open, 'close' => $restaurant->t_close, 'active' => $restaurant->t],
			3 => ['open' => $restaurant->w_open, 'close' => $restaurant->w_close, 'active' => $restaurant->w],
			4 => ['open' => $restaurant->r_open, 'close' => $restaurant->r_close, 'active' => $restaurant->r],
			5 => ['open' => $restaurant->f_open, 'close' => $restaurant->f_close, 'active' => $restaurant->f],
			6 => ['open' => $restaurant->s_open, 'close' => $restaurant->s_close, 'active' => $restaurant->s]
		];
	}
}
