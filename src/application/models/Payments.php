<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * payments Model
 * @author manaknight inc.
 *
 */
class Payments extends CI_Model
{

	public $_status_mapping = [
		0 => 'Sin Pago',
		1 => 'Pagado',
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    /**
     * num_payments function.
     *
     * @access public
     * @return integer
     */
    public function num_restaurant_payments($restaurant_id)
    {
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->from('payments');
        return $this->db->count_all_results();
    }

	/**
     * num_payments function.
     *
     * @access public
     * @return integer
     */
    public function num_payments()
    {
        $this->db->from('payments');
        return $this->db->count_all_results();
    }

    /**
     * get_paginated_orders function.
     *
     * @access public
     * @param mixed $page
     * @param mixed $limit
     * @return resuly array on success, blank array on failure
     */
    public function get_paginated_payments($page = 1, $limit = 10, $where)
    {
        $this->db->limit($limit, $page);
        if (!empty($where))
        {
            foreach ($where as $key => $value)
            {
				if ($value == null)
				{
					continue;
				}

                if ($value)
                {
                    $this->db->where($key, $value);
				}

				if (strlen($key) > 0 && strlen($value) < 1)
				{
					$this->db->where($key);
				}
            }
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('payments');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }

	/**
	 * Get payments
	 *
	 * @param integer $id
	 * @return payments
	 */
	public function get_payment($id)
	{
		$this->db->from('payments');
		$this->db->where('id', $id, TRUE);
		$row =  $this->db->get()->row();

		if (!$row)
		{
			return NULL;
		}

		return $row;
	}

	/**
	 * Get payments
	 *
	 * @param integer $id
	 * @return payments
	 */
	public function get_payment_by_restaurant($id)
	{
		$this->db->from('payments');
		$this->db->where('restaurant_id', $id, TRUE);
		$row =  $this->db->get()->row();

		if (!$row)
		{
			return NULL;
		}

		return $row;
	}

	/**
	 * Get last payments
	 *
	 * @param integer $id
	 * @return payments
	 */
	public function get_last_payment_by_restaurant($id)
	{
		$this->db->from('payments');
		$this->db->where('restaurant_id', $id, TRUE);
		$this->db->order_by('payment_date', 'DESC');
		$row =  $this->db->get()->row();

		if (!$row)
		{
			return NULL;
		}

		return $row;
	}

	/**
	 * Get all payments
	 *
	 * @return array payments
	 */
	public function get_payments()
	{
		$this->db->from('payments');
		return $this->db->get()->result();
	}


	/**
	 * Create payments
	 *
	 * @param array $data
	 * @return payments
	 */
	public function create_payment($data)
	{
		$data['created_at'] = date('Y-m-j');
		return $this->db->insert('payments', $data, TRUE);
    }

	/**
	 * Edit Payment
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_payment($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('payments', $data);
	}

    /**
	 * Delete suggest
	 *
	 * @param integer $id
	 * @return suggest
	 */
	public function delete($id)
    {
		$this->db->where('id', $id);
		return $this->db->delete('payments');
	}

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}