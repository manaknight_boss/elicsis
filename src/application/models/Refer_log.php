<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Refer_log Model
 * @author manaknight inc.
 *
 */
class Refer_log extends CI_Model
{
    public $_status_mapping = [
		0 => 'Pendiente',
		1 => 'Aprobado',
		2 => 'Rechazado'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Refer_log
	 *
	 * @param integer $id
	 * @return refer_log
	 */
	public function get_refer_log($id)
    {
		$this->db->from('refer_log');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get Refer_log
	 *
	 * @param integer $id
	 * @return refer_log
	 */
	public function get_refer_log_by_code($code)
    {
			$this->db->from('users');
			$this->db->where('refer', $code, TRUE);
			return $this->db->get()->row();
	}

	/**
	 * Get all Refer_log
	 *
	 * @return array refer_log
	 */
	public function get_refer_logs()
    {
		$this->db->from('refer_log');
        return $this->db->get()->result();
	}


	/**
	 * Create Refer_log
	 *
	 * @param array $data
	 * @return Refer_log
	 */
	public function create_referral($user_id, $code)
	{
        $refer = $this->get_refer_log_by_code($code);

		if ($refer)
		{
			$payload = [
				'user_id' => $user_id,
				'refer_id' => $refer->id,
				'status' => 0,
				'created_at' => date('Y-m-j H:i:s')
			];
			return $this->db->insert('refer_log', $payload, TRUE);
		}

		return FALSE;
	}

	/**
	 * Number of refer_log
	 *
	 * @access public
	 * @return integer $result
	 */
	public function num_refer_logs()
	{
        $this->db->where('status', 0);
        $this->db->from('refer_log');
		return $this->db->count_all_results();
    }

	/**
	 * Get paginated refer_log
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $users
	 */
	public function get_paginated_refer_log($page = 1, $limit=10)
    {
		$this->db->limit($limit, $page);
        $this->db->order_by('id', 'ASC');
        $this->db->where('status', 0);
		$query = $this->db->get('refer_log');
		$result = [];

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }
		}

        return $result;
	}

	/**
	 * Edit Refer_log
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_refer_log($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('refer_log', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}