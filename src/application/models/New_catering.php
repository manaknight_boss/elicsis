<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * New_catering Model
 * @author manaknight inc.
 *
 */
class New_catering extends CI_Model
{
    public $_status_mapping = [
		0 => 'Inactivo',
		1 => 'Activo'
	];

	public $_type_mapping = [
		1 => 'Enterprise',
		2 => 'New Catering'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function num_new_catering()
	{
		return $this->db->count_all('new_catering');
	}

	/**
	 * Get paginated users
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $users
	 */
	public function get_paginated_new_caterings($page = 1, $limit=10)
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('new_catering');
		$result = [];

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }
		}
        return $result;
	}

	/**
	 * Get New_catering
	 *
	 * @param integer $id
	 * @return new_catering
	 */
	public function get_new_catering($id)
    {
		$this->db->from('new_catering');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all New_catering
	 *
	 * @return array new_catering
	 */
	public function get_new_caterings()
    {
		$this->db->from('new_catering');
        return $this->db->get()->result();
	}

	/**
	 * Create New_catering
	 *
	 * @param array $data
	 * @return New_catering
	 */
	public function create_new_catering($data)
	{
		$data['created_at'] = date('Y-m-j');
		return $this->db->insert('new_catering', $data, TRUE);
	}

	/**
	 * Edit New_catering
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_new_catering($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('new_catering', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}

	/**
     * Return Type Mapping
     * @return array
     */
    public function type_mapping ()
	{
		return $this->_type_mapping;
	}
}