<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Menu Model
 * @author manaknight inc.
 *
 */
class Menu extends CI_Model
{
    public $_status_mapping = [
		0 => 'Inactivo',
		1 => 'Activo'
	];

	public $_sold_by_mapping = [
		0 => 'Unidad(es)',
		1 => 'Servicio(s)',
		2 => 'Plato(s)',
		3 => 'Pedazo(s)',
		4 => 'Copa(s)',
		5 => 'Caja(s)',
		6 => 'Rebanada(s)',
		7 => 'Sarten(es)'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Menu
	 *
	 * @param integer $id
	 * @return menu
	 */
	public function get_menu($id)
    {
		$this->db->from('menu');
		$this->db->where('id', $id, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * Get all Menu
	 *
	 * @return array menu
	 */
	public function get_menus()
    {
		$this->db->from('menu');
		return $this->db->get()->result();
	}

	/**
	 * Number of menu
	 *
	 * @access public
	 * @return integer $result
	 */
	public function num_menus($restaurant_id)
	{
		if ($restaurant_id !== 0)
		{
			$this->db->where('restaurant_id', $restaurant_id);
		}

		$this->db->from('menu');
		return $this->db->count_all_results();
	}

	/**
	 * Get paginated menus
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $menus
	 */
	public function get_paginated_menus($page = 1, $limit=10, $restaurant_id)
	{
		if ($restaurant_id !== 0)
		{
			$this->db->where('restaurant_id', $restaurant_id);
		}

		$this->db->limit($limit, $page);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('menu');
		$result = [];

		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$result[] = $row;
			}
		}
		return $result;
	}

	/**
	 * Get all Menu
	 *
	 * @return array menu
	 */
	public function get_menus_by_restaurant($restaurant_id)
    {
		$this->db->from('menu');
		$this->db->where('restaurant_id', $restaurant_id, TRUE);
		$this->db->order_by('catering_menu_id', 'ASC');
		$this->db->order_by('order', 'ASC');
		return $this->db->get()->result();
	}

	/**
	 * Get all Menu
	 *
	 * @return array menu
	 */
	public function get_active_menus_by_restaurant($restaurant_id)
    {
		$this->db->from('menu');
		$this->db->where('restaurant_id', $restaurant_id, TRUE);
		$this->db->where('status', 1, TRUE);
		$this->db->order_by('catering_menu_id', 'ASC');
		$this->db->order_by('order', 'ASC');
		return $this->db->get()->result();
	}

	/**
	 * Create Menu
	 *
	 * @param array $data
	 * @return Menu
	 */
	public function create_menu($data)
	{
		if (isset($data['preference']) && !$data['preference'])
		{
			$data['preference'] = '[]';
		}

		return $this->db->insert('menu', $data, TRUE);
	}

	/**
	 * Edit Menu
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_menu($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('menu', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
		{
			return $this->_status_mapping;
		}

		/**
     * Return Status Mapping
     * @return array
     */
    public function sold_by_mapping ()
		{
			return $this->_sold_by_mapping;
		}
}