<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Coupon Model
 * @author manaknight
 *
 */
class Coupon extends CI_Model 
{
	public $PRODUCT = 1;
	public $SUBSCRIPTION = 2;

	public $AMOUNT = '$';
	public $PERCENT = '%';
	public $ACTIVE = 1;
	public $INACTIVE = 0;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get 1 coupon
	 * @param integer $id
	 * @return coupon
	 */
	public function get_coupon_by_id($id) 
    {
		$this->db->from('coupon');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get 1 coupon
	 * @param string $code
	 * @return coupon
	 */
	public function get_coupon($code) 
    {
		$this->db->from('coupon');
        $this->db->where('code', $code, TRUE);
        return $this->db->get()->row();
	}	

	/**
	 * Get all coupons
	 * 
	 * @return array coupon
	 */
	public function get_coupons() 
    {
		$this->db->from('coupon');
        return $this->db->get()->result();
	}

	/**
	 * Create coupon
	 * 
	 * @param array $data
	 * @return coupon
	 */
	public function create_coupon($data)
	{
		$data['created_at'] = date('Y-m-j H:i:s');
		return $this->db->insert('coupon', $data, TRUE);
	}
	
	
	/**
	 * Edit coupon
	 * 
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_coupon($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('coupon', $data);
	}

	/**
	 * Filter out fields not to be revealed to public
	 * 
	 * @param array $data
	 * @return array
	 */
	public function filter ($data)
	{
		unset($data->id);
		unset($data->created_at);
		unset($data->status);
		unset($data->end_date);
		if ($data->product_type == $this->PRODUCT)
		{
			$data->product_type = 'product';
		}
		else
		{
			$data->product_type = 'subscription';
		}
		return $data;
	}

	public function mapping()
	{
		return [
			'status' => [
				0 => 'inactive',
				1 => 'active'
			],
			'type' => [
				1 => 'product',
				2 => 'subscription'
			],
			'symbol' => [
				'$' => 'Amount',
				'%' => 'Percent'
			]
		];
	}
}