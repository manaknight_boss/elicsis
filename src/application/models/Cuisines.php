<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Cuisines Model
 * @author manaknight inc.
 *
 */
class Cuisines extends CI_Model
{
    public $_status_mapping = [
			0 => 'Inactivo',
			1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Cuisines
	 *
	 * @param integer $id
	 * @return cuisines
	 */
	public function get_cuisine($id)
    {
		$this->db->from('cuisines');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Cuisines
	 *
	 * @return array cuisines
	 */
	public function get_cuisines($is_active = FALSE)
	{
		$this->db->from('cuisines');

		if ($is_active)
		{
			$this->db->where('status', 1, TRUE);
		}

		return $this->db->get()->result();
	}

	/**
	 * Get all Cuisines by key value
	 *
	 * @return array cuisines
	 */
	public function get_cuisines_key_value()
	{
		$this->db->from('cuisines');
		$this->db->where('status', 1, TRUE);

		$data = [];
		$results = $this->db->get()->result();

		foreach ($results as $key => $value)
		{
			$data[$value->id] = $value->name;
		}

		return $data;
	}

	/**
	 * Create Cuisines
	 *
	 * @param array $data
	 * @return Cuisines
	 */
	public function create_cuisine($data)
	{

		return $this->db->insert('cuisines', $data, TRUE);
	}

	/**
	 * Edit Cuisines
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_cuisine($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('cuisines', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}