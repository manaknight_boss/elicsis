<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Menu_addon Model
 * @author manaknight inc.
 *
 */
class Menu_addon extends CI_Model
{
    public $_status_mapping = [
			0 => 'Inactivo',
			1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get menu_addon
	 *
	 * @param integer $id
	 * @return menu_addon
	 */
	public function get_menu_addon($id)
    {
		$this->db->from('menu_addon');
		$this->db->where('id', $id, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * Get all menu_addon
	 *
	 * @return array menu_addon
	 */
	public function get_menu_addons($menu_id, $is_active=FALSE)
  {
		$this->db->from('menu_addon');
		$this->db->where('menu_id', $menu_id, TRUE);

		if ($is_active)
		{
			$this->db->where('status', 1, TRUE);
		}

		return $this->db->get()->result();
	}

	/**
	 * Create menu_addon
	 *
	 * @param array $data
	 * @return menu_addon
	 */
	public function create_menu_addon($data)
	{
		return $this->db->insert('menu_addon', $data, TRUE);
	}

	/**
	 * Edit menu_addon
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_menu_addon($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('menu_addon', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}