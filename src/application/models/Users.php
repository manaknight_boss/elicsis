<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User Management Model
 * @author manaknight
 *
 */
class Users extends CI_Model
{
	public $MEMBER = 2;
	public $ADMIN = 3;
	public $ENTERPRISE = 4;

	public $NORMAL_LOGIN = 'n';
	public $GOGGLE_LOGIN = 'g';
	public $FACEBOOK_LOGIN = 'f';
	public $TWITTER_LOGIN = 't';
	public $GITHUB_LOGIN = 'h';

	public $_role_mapping = [
		2 => 'Member',
		3 => 'Admin',
		// 4 => 'Enterprise'
	];

	public $_login_mapping = [
		'n' => 'Normal Login',
		'g' => 'Google',
		'f' => 'Facebook',
		't' => 'Twitter',
		'h' => 'Github'
	];

	public $_status_mapping = [
		0 => 'Inactive',
		1 => 'Activo',
		2 => 'Suspend',
		3 => 'Pending'
	];

	public $_profile_type_mapping = [
		'Regular' => 'Regular',
		'Empresa' => 'Empresa'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * create_user function.
	 *
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @param mixed $plan
	 * @return bool true on success, false on failure
	 */
	public function create_user($email, $password, $role, $username, $type='n', $first_name='', $last_name='', $stripe_customer_id='', $phone='')
	{
		$data = [
			'email'      => $email,
			'password'   => $this->hash_password($password),
			'role_id'		 => $role,
			'username'   => $username,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'profile_type' => 'Regular',
			'business_name' => '',
			'phone' => $phone,
			'rnc' => '',
			'image'   => 'https://i.imgur.com/AzJ7DRw.png',
			'created_at' => date('Y-m-j H:i:s'),
			'updated_at' => date('Y-m-j H:i:s'),
			'refer' => uniqid(),
			'status'	 => 1,
			'verify'	 => 0,
			'type'		 => $type,
			'stripe_id' => $stripe_customer_id
		];

		if ($this->db->insert('users', $data, TRUE))
        {
            return $this->db->insert_id();
        }
        else
        {
            return FALSE;
        }

	}

	/**
	 * create_user_by_admin function.
	 *
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @param mixed $plan
	 * @return bool true on success, false on failure
	 */
	public function create_user_by_admin($data)
	{
		$data['password'] = $this->hash_password($data['password']);
		$data['created_at'] = date('Y-m-j H:i:s');
		$data['updated_at'] = date('Y-m-j H:i:s');
		$data['verify'] = 1;

		if ($this->db->insert('users', $data, TRUE))
        {
            return $this->db->insert_id();
        }
        else
        {
            return FALSE;
        }

	}

	/**
	 * resolve_user_login function.
	 *
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @param bool $isAdmin false
	 * @return bool true on success, false on failure
	 */
	public function authenticate($email, $password, $isAdmin = false)
	{

		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('email', $email, TRUE);
		$this->db->where('type', 'n', TRUE);
		$this->db->where('status', 1, TRUE);

		if ($isAdmin)
		{
			$this->db->where('role_id', $this->ADMIN, TRUE);
		}

		$hash = $this->db->get()->row('password');

		return $this->verify_password_hash($password, $hash);
	}

	/**
	 * Get Profile
	 *
	 * @access public
	 * @param mixed $id
	 * @return mixed $users
	 */
	public function get_profile($id)
	{
		$this->db->from('users');
		$this->db->where('id', $id, TRUE);
		$result =  $this->db->get()->row();

		if ($result)
		{
			$result->role_name = $this->_role_mapping[$result->role_id];
			$result->status_name = $this->_status_mapping[$result->status];
			$result->type = $this->_login_mapping[$result->type];
			unset($result->password);
			unset($result->type);
			unset($result->customer_id);
			unset($result->role_id);
			unset($result->status);
		}

		return $result;

	}

	/**
	 * get_user_id_from_username function.
	 *
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function get_user_id_from_email($email)
	{
		$this->db->from('users');
		$this->db->where('email', $email, TRUE);
		return $this->db->get()->row('id');
	}

	/**
	 * get_user_from_email function.
	 *
	 * @access public
	 * @param mixed $email
	 * @return object user
	 */
	public function get_user_from_email($email)
	{
		$this->db->from('users');
		$this->db->where('email', $email, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * get_user_from_email function.
	 *
	 * @access public
	 * @param mixed $email
	 * @return object user
	 */
	public function autocomplete_email($email, $role)
	{
		$this->db->like('email', $email);
		$this->db->where('role_id', $this->MEMBER);
		$query = $this->db->get('users');
		$result = [];
		foreach ($query->result() as $row)
		{
			$result[] = [
				'id' => $row->id,
				'email' => $row->email
			];
		}
		return $result;
	}

	/**
	 * existing_google_user_from_email function that find if user is google user.
	 *
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function is_google_user($user)
	{
		return ($user->type == $this->GOGGLE_LOGIN) && ($user->status == 1);
	}

	/**
	 * existing_github_user_from_email function that find if user is github user.
	 *
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function is_github_user($user)
	{
		return ($user->type == $this->GITHUB_LOGIN) && ($user->status == 1);
	}

	/**
	 * existing_facebook_user_from_email function that find if user is facebook user.
	 *
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function is_facebook_user($user)
	{
		return ($user->type == $this->FACEBOOK_LOGIN) && ($user->status == 1);
	}

	/**
	 * get_user function
	 *
	 * @access public
	 * @param mixed $user_id
	 * @return object the user object
	 */
	public function get_user($user_id)
	{

		$this->db->from('users');
		$this->db->where('id', $user_id, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * get_user_from_reset_token function
	 *
	 * @access public
	 * @param string $token
	 * @return object the user object
	 */
	public function get_user_from_reset_token($token)
	{

		$this->db->from('users');
		$this->db->where('reset_token', $token, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * edit_profile
	 *
	 * @access public
	 * @param mixed $user_id
	 * @param mixed $data
	 * @return bool
	 */
	public function edit_profile($user_id, $data)
	{
		if (!$data)
		{
			return FALSE;
		}

		if (isset($data['password']) && strlen($data['password']) > 0)
		{
			$this->db->set('password', $this->hash_password($data['password']), TRUE);
			$this->db->set('reset_token', '', TRUE);
		}

		if (isset($data['username']) && strlen($data['username']) > 0)
		{
			$this->db->set('username', $data['username'], TRUE);
		}

		if (isset($data['stripe_id']) && strlen($data['stripe_id']) > 0)
		{
			$this->db->set('stripe_id', $data['stripe_id'], TRUE);
		}

		if (isset($data['email']) && strlen($data['email']) > 0)
		{
			$this->db->set('email', $data['email'], TRUE);
		}

		if (isset($data['first_name']) && strlen($data['first_name']) > 0)
		{
			$this->db->set('first_name', $data['first_name'], TRUE);
		}

		if (isset($data['last_name']) && strlen($data['last_name']) > 0)
		{
			$this->db->set('last_name', $data['last_name'], TRUE);
		}

		if (isset($data['rnc']) && strlen($data['rnc']) > 0)
		{
			$this->db->set('rnc', $data['rnc'], TRUE);
		}

		if (isset($data['verify']) && strlen($data['verify']) > 0)
		{
			$this->db->set('verify', $data['verify'], TRUE);
		}

		if (isset($data['business_name']) && strlen($data['business_name']) > 0)
		{
			$this->db->set('business_name', $data['business_name'], TRUE);
		}

		if (isset($data['role_id']) && strlen($data['role_id']) > 0)
		{
			$this->db->set('role_id', $data['role_id'], TRUE);
		}

		if (isset($data['status']))
		{
			$this->db->set('status', $data['status'], TRUE);
		}

		if (isset($data['profile_type']))
		{
			$this->db->set('profile_type', $data['profile_type'], TRUE);
		}

		if (isset($data['phone']) && strlen($data['phone']) > 0)
		{
			$this->db->set('phone', $data['phone'], TRUE);
		}

		$this->db->set('updated_at', date('Y-m-j H:i:s'), TRUE);
		$this->db->where('id', $user_id, TRUE);
		return $this->db->update('users');
	}

	/**
	 * edit_user function.
	 *
	 * @access public
	 * @param integer $user_id
	 * @param string $email
	 * @param string $password
	 * @param integer $role
	 * @param string $stripe_id
	 * @param integer $status
	 * @return bool
	 */
	public function edit_user($user_id, $email, $password, $role, $stripe_id, $status)
	{
		if ($password && strlen($password) > 0)
		{
			$this->db->set('password', $this->hash_password($password), TRUE);
		}

		if (strlen($stripe_id) > 1)
		{
			$this->db->set('stripe_id', $stripe_id, TRUE);
		}

		$this->db->set('email', $email, TRUE);
		$this->db->set('role_id', $role, TRUE);
		$this->db->set('stripe_id', $stripe_id, TRUE);
		$this->db->set('status', $status, TRUE);
		$this->db->set('updated_at', date('Y-m-j H:i:s'), TRUE);
		$this->db->where('id', $user_id, TRUE);
		return $this->db->update('users');
	}

	public function reset_password($user_id)
	{
		$token = md5(uniqid() . time());
		$this->db->set('reset_token', $token, TRUE);
		$this->db->where('id', $user_id, TRUE);
		$this->db->update('users');
		return $token;
	}

	/**
	 * Number of users
	 *
	 * @access public
	 * @return integer $result
	 */
	public function num_users()
	{
        return $this->db->count_all('users');
    }

	/**
	 * Get paginated users
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $users
	 */
	public function get_paginated_users($page = 1, $limit=10)
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('users');
		$result = [];

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }
		}

        return $result;
	}

	/**
	 * Map the role to its name
	 *
	 * @access public
	 * @param integer $role
	 * @return integer
	 */
	public function map_role ($role)
	{
		return $this->_role_mapping[$role] || 0;
	}

	/**
	 * hash_password function.
	 *
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password)
	{
		return password_hash($password, PASSWORD_BCRYPT);
	}

	/**
	 * verify_password_hash function.
	 *
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash)
	{
		return password_verify($password, $hash);
	}

	public function mapping ()
	{
		return [
			'role' => $this->_role_mapping,
			'status' => $this->_status_mapping,
			'login' => $this->_login_mapping,
			'profile_type' => $this->_profile_type_mapping
		];
	}

	/**
	 * Search by filter and count users
	 *
	 * @param array $filter
	 * @return integer
	 */
	public function search_and_count($filter)
	{
		$fields = array_keys($filter);
		$num_fields = count(array_keys($filter));
		$query = $this->db->where($fields[0], $filter[$fields[0]], TRUE);
		if ($num_fields > 1)
		{
			for ($i=1; $i < $num_fields ; $i++)
			{
				$query = $query->where($fields[$i], $filter[$fields[$i]], TRUE);
			}
		}

		$query = $query->get('users');
		return $query->num_rows();
	}
}