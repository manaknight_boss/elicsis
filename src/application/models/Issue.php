<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Issue Model
 * @author manaknight inc.
 *
 */
class Issue extends CI_Model
{
    public $_status_mapping = [
			0 => 'Open',
			1 => 'Resolved'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Issue
	 *
	 * @param integer $id
	 * @return issue
	 */
	public function get_issue($id)
    {
		$this->db->from('issue');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get Issue
	 *
	 * @param integer $id
	 * @return issue
	 */
	public function is_issue_exist($order_id)
    {
			$this->db->from('issue');
			$this->db->where('order_id', $order_id, TRUE);
			$result = $this->db->get()->result();
			return count($result) > 0;
	}

	/**
	 * Get all Issue
	 *
	 * @return array issue
	 */
	public function get_issues()
    {
		$this->db->from('issue');
        return $this->db->get()->result();
	}

	/**
	 * Get all Issue by order id
	 *
	 * @return array issue
	 */
	public function get_issue_by_order($order_id)
    {
			$this->db->from('issue');
			$this->db->where('order_id', $order_id);
			$results = $this->db->get()->result();
			if (empty($results))
			{
				return FALSE;
			}

			if (count($results) > 1)
			{
				$last = count($results);
				return $results[$last];
			}

			return $results[0];
	}


	/**
	 * Create Issue
	 *
	 * @param array $data
	 * @return Issue
	 */
	public function create_issue($data)
	{
        $data['created_at'] = date('Y-m-j');
		return $this->db->insert('issue', $data, TRUE);
	}

	/**
	 * Edit Issue
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_issue($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('issue', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}

/**
	 * Number of customer_restaurant
	 *
	 * @access public
	 * @return integer $result
	 */
	public function num_active_issues()
	{
		$this->db->where('status', 0);
		$this->db->from('issue');
		return $this->db->count_all_results();
	}

	/**
	 * Get paginated issues
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $issues
	 */
	public function get_paginated_active_issues($page = 1, $limit=10)
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('id', 'ASC');
		$this->db->where('status', 0);
		$query = $this->db->get('issue');
		$result = [];

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }
		}

        return $result;
	}
}