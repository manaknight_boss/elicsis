<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Preferences Model
 * @author manaknight inc.
 *
 */
class Preferences extends CI_Model
{
    public $_status_mapping = [
			0 => 'Inactivo',
			1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Preferences
	 *
	 * @param integer $id
	 * @return preferences
	 */
	public function get_preference($id)
    {
		$this->db->from('preferences');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Preferences
	 *
	 * @return array preferences
	 */
	public function get_preferences($key_value=FALSE)
    {
		$this->db->from('preferences');
		$results = $this->db->get()->result();
		if ($key_value)
		{
			$data = [];
			foreach ($results as $key => $value) {
				$data[$value->id] = $value->name;
			}
			return $data;
		}
		else
		{
			return $results;
		}
	}

	/**
	 * Create Preferences
	 *
	 * @param array $data
	 * @return Preferences
	 */
	public function create_preference($data)
	{

		return $this->db->insert('preferences', $data, TRUE);
	}

	/**
	 * Edit Preferences
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_preference($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('preferences', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
		{
			return $this->_status_mapping;
		}
}