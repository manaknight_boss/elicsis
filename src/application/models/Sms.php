<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Sms Model
 * @author manaknight
 *
 */
class Sms extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get 1 sms
	 * @param integer $id
	 * @return sms
	 */
	public function get_sms($id)
    {
		$this->db->from('sms');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get 1 sms
	 * @param integer $id
	 * @return sms
	 */
	public function get_template($slug, $data)
    {
		$this->db->from('sms');
        $this->db->where('slug', $slug, TRUE);
		$template = $this->db->get()->row();

		if (!$template)
		{
			return FALSE;
		}
		$tags_raw = $template->tags;
		$tags = explode(',', $tags_raw);
		$template->content = $this->inject_substitute($template->content, $tags, $data);
		return $template;
	}

	/**
	 * Get all smss
	 *
	 * @return array sms
	 */
	public function get_all_sms()
    {
		$this->db->from('sms');
        return $this->db->get()->result();
	}

	/**
	 * Create sms
	 *
	 * @param array $data
	 * @return sms
	 */
	public function create_sms($data)
	{
		return $this->db->insert('sms', $data);
	}


	/**
	 * Edit sms
	 *
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_sms($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('sms', $data);
	}

	public function inject_substitute($raw, $tags, $data)
	{
		foreach ($data as $key => $value)
        {
			if (in_array($key, $tags))
			{
				$raw = str_replace('{{{' . $key . '}}}', $value, $raw);
			}
        }

        return $raw;
	}
}