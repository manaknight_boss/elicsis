<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Plans Model
 * @author manaknight inc.
 *
 */
class Plans extends CI_Model
{

	public $SINGLE = 'single';
	public $RECURRING = 'recurring';

    public $_status_mapping = [
			0 => 'Inactivo',
			1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Plans
	 *
	 * @param integer $id
	 * @return plans
	 */
	public function get_plan($id)
    {
		$this->db->from('plans');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Plans
	 *
	 * @return array plans
	 */
	public function get_plans()
    {
		$this->db->from('plans');
        return $this->db->get()->result();
	}

	/**
	 * Create Plans
	 *
	 * @param array $data
	 * @return Plans
	 */
	public function create_plan($data)
	{
        $data['created_at'] = date('Y-m-j');
		return $this->db->insert('plans', $data, TRUE);
	}

	/**
	 * Edit Plans
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_plan($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('plans', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}

	/**
	 * Plans List
	 *
	 * @return array plans
	 */
	public function get_plans_list()
	{
		$this->db->select('id, name, display_name');
		$this->db->from('plans');
		return $this->db->get()->result();
    }
}