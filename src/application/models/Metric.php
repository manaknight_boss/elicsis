<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * metrics Model
 * @author manaknight inc.
 *
 */
class Metric extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get Metric
	 *
	 * @return metrics
	 */
	public function get_metric()
	{
		$this->db->from('metric');
		$this->db->where('id', 1, TRUE);
		return $this->db->get()->row();
	}

	/**
	 * Get all metrics
	 *
	 * @return array metrics
	 */
	public function get_metrics()
	{
		$this->db->from('metric');
		return $this->db->get()->result();
	}

	/**
	 * Edit metrics
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_metric($data)
	{
		$this->db->where('id', 1, TRUE);
		return $this->db->update('metric', $data);
	}

	public function update_metric($type)
	{
		switch ($type) {
			case 'users':
				$this->db->where('status', 1);
				$this->db->where('role_id', 2);
				$this->db->from('users');
				$result = $this->db->count_all_results();
				return $this->edit_metric(['num_user' => $result + 1], 1);
			break;
			case 'orders':
				$this->db->from('orders');
				$result = $this->db->count_all_results();
				return $this->edit_metric(['num_order' => $result + 1], 1);
			break;
			case 'sales':
				$this->db->select_sum('total');
				$result = $this->db->get('orders')->row();
				$sale = 0;
				if ($result && $result->total)
				{
					$sale = $result->total;
				}
				return $this->edit_metric(['total_sale' => $sale], 1);
			break;
			case 'restaurants':
				$this->db->from('restaurants');
				$result = $this->db->count_all_results();
				return $this->edit_metric(['num_catering' => $result + 1], 1);
			break;
			case 'pending':
				$this->db->select_sum('total');
				$this->db->where('(status=1 OR status=2 OR status=3)');
				$result = $this->db->get('orders')->row();
				$pending = 0;
				if ($result && $result->total)
				{
					$pending = $result->total;
				}
				return $this->edit_metric(['total_pending' => $pending], 1);
			break;
			case 'delivered':
				$this->db->select_sum('total');
				$this->db->where('(status=4 OR status=5)');
				$result = $this->db->get('orders')->row();
				$delivered = 0;
				if ($result && $result->total)
				{
					$delivered = $result->total;
				}
				return $this->edit_metric(['total_delivered' => $delivered], 1);
			break;
			case 'cancel':
				$this->db->select_sum('total');
				$this->db->where('(status=8)');
				$result = $this->db->get('orders')->row();
				$cancel = 0;
				if ($result && $result->total)
				{
					$cancel = $result->total;
				}
				return $this->edit_metric(['total_cancel' => $cancel], 1);
			break;
			case 'refund':
				$this->db->select_sum('total');
				$this->db->where('(status=9)');
				$result = $this->db->get('orders')->row();
				$refund = 0;
				if ($result && $result->total)
				{
					$refund = $result->total;
				}
				return $this->edit_metric(['total_refund' => $refund], 1);
			break;
			case 'issue':
				$this->db->select_sum('total');
				$this->db->where('(status=6 OR status=7)');
				$result = $this->db->get('orders')->row();
				$issue = 0;
				if ($result && $result->total)
				{
					$issue = $result->total;
				}
				return $this->edit_metric(['total_issue' => $issue], 1);
			break;
			default:
				return 0;
				break;
		}
	}
}