<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Orders Management Model
 * @author manaknight
 *
 */
class Orders extends CI_Model
{
    public $PAID = 1;
    public $UNPAID = 0;

    public $_type_mapping = [
        0 => 'Recoger',
        1 => 'Delivery'
    ];

    public $_status_mapping = [
		0 => 'No Pagado',
		1 => 'Revisando',
		2 => 'Confirmado',
		3 => 'Procesando',
		4 => 'En Camino ',
		5 => 'Entregado',
		6 => 'Problema con la orden',
		7 => 'Resolucion/quejas',
		8 => 'Cancelado',
		9 => 'Reembolsado'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Create Orders
     *
     * @param array $data
     * @return Orders
     */
    public function create_order($data)
    {
        $data['created_at'] = date('Y-m-j');

        if ($this->db->insert('orders', $data, TRUE))
        {
            return $this->db->insert_id();
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * num_orders function.
     *
     * @access public
     * @return integer
     */
    public function num_orders()
    {
        return $this->db->count_all('orders');
    }

    /**
     * num_orders function.
     *
     * @access public
     * @return integer
     */
    public function num_user_orders($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->from('orders');
        return $this->db->count_all_results();
    }

    /**
     * num_orders function.
     *
     * @access public
     * @return integer
     */
    public function num_restaurant_orders($restaurant_id)
    {
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->from('orders');
        return $this->db->count_all_results();
    }

    /**
     * num_orders function.
     *
     * @access public
     * @return integer
     */
    public function num_restaurant_orders_delivered($restaurant_id)
    {
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('(status=4 OR status=5)');
        $this->db->from('orders');
        $result = $this->db->count_all_results();
        if ($result == 0)
        {
            return 1;
        }
        return $result;
    }

    /**
     * num_orders function.
     *
     * @access public
     * @return integer
     */
    public function total_restaurant_orders_delivered($restaurant_id)
    {
        $this->db->select_sum('total');
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('(status=4 OR status=5)');
        $result = $this->db->get('orders')->row();

        if ($result && $result->total)
        {
            return number_format($result->total, 2);
        }

        return 0;
    }

    /**
     * get_total_sales_by_restaurant function.
     *
     * @access public
     * @return integer
     */
    public function get_total_sales_by_restaurant($restaurant_id)
    {
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->from('orders');
        return $this->db->count_all_results();
    }

    /**
     * get_paginated_orders function.
     *
     * @access public
     * @param mixed $page
     * @param mixed $limit
     * @return resuly array on success, blank array on failure
     */
    public function get_paginated_orders($page = 1, $limit = 10, $where)
    {
        $this->db->limit($limit, $page);
        if (!empty($where))
        {
            foreach ($where as $key => $value)
            {
                if ($value)
                {
                    $this->db->where($key, $value);
                }
            }
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }

    /**
     * get_paginated_orders function.
     *
     * @access public
     * @param mixed $page
     * @param mixed $limit
     * @return resuly array on success, blank array on failure
     */
    public function get_paginated_user_orders($user_id, $page = 1, $limit = 10)
    {
        $this->db->where('user_id', $user_id);
        $this->db->limit($limit, $page);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }

    /**
     * get_paginated_orders function.
     *
     * @access public
     * @param mixed $page
     * @param mixed $limit
     * @return resuly array on success, blank array on failure
     */
    public function get_paginated_restaurant_orders($restaurant_id, $page = 1, $limit = 10, $where)
    {
        $this->db->where('restaurant_id', $restaurant_id);
        if (!empty($where))
        {
            foreach ($where as $key => $value) {
                if ($value)
                {
                    $this->db->where($key, $value);
                }
            }
        }
        $this->db->limit($limit, $page);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }

    /**
     * get_new_event_restaurant_orders function.
     *
     * @access public
     * @param mixed $restaurant_id
     * @return resuly array on success, blank array on failure
     */
    public function get_new_event_restaurant_orders($restaurant_id)
    {
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('(status=1 OR event_date_at="' . date('Y-m-d') . '")');
        $this->db->limit(100, 0);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }

    /**
     * get_order_by_card function.
     *
     * @access public
     * @param mixed $card_id
     * @return result array on success, blank array on failure
     */
    public function get_order_by_card($card_token, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('`payment_data` LIKE "%' . $card_token . '%"');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }

    /**
	 * Edit Order
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_order($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('orders', $data);
    }

	/**
	 * Get order
	 *
	 * @return array report
	 */
	public function get_order($id)
    {
		$this->db->from('orders');
		$this->db->where('id', $id, TRUE);
		return $this->db->get()->row();
	}


    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function delivery_type_mapping ()
	{
		return $this->_type_mapping;
    }

    public function is_order_complete ($order)
    {
        if ($order->status == 9 || $order->status == 8 || $order->status == 7 ||
        $order->status == 6 || $order->status == 5)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_cancelable ($order)
    {
        if ($order->status < 2)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function refund_required ($order)
    {
        if ($order->status >= 2)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_reportable ($order)
    {
        if ($order->status > 3 && ($order->status != 6 || $order->status != 7))
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_processable ($order)
    {
        if ($order->status == 2)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_acceptable ($order)
    {
        if ($order->status == 1)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_deliverable ($order)
    {
        if ($order->status == 3)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_delivered ($order)
    {
        if ($order->status == 4)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_refundable ($order)
    {
        if ($order->status > 1 && $order->status != 9)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_editable ($order)
    {
        if ($order->status < 4)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function is_reviewable ($order)
    {
        if ($order->status == 4 || $order->status == 5)
        {
            $this->db->from('review');
            $this->db->where('order_id', $order->id, TRUE);
            $review = $this->db->get()->row();

            if (!$review)
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function grade_on_time($time)
    {
        $grade = 0;

        if ($time <= 5)
        {
            $grade = 100;
        }

        if ($time <= 10 && $time > 5)
        {
            $grade = 75;
        }

        if ($time <= 20 && $time > 10)
        {
            $grade = 30;
        }

        return $grade;
    }

    public function grade_on_delivery($time)
    {
        $grade = 0;

        if ($time <= 5)
        {
            $grade = 100;
        }

        if ($time <= 10 && $time > 5)
        {
            $grade = 75;
        }

        if ($time <= 15 && $time > 10)
        {
            $grade = 30;
        }

        return $grade;
    }

    public function calculate_on_time_rating_restaurant_last_30_days_by_score($restaurant_id, $score)
    {
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('on_time_rating', $score);
        $this->db->where('(status=2 OR status=3)');
        $this->db->where('event_date_at>=\'' . date('Y-m-j', strtotime('-30day')) . '\'');
        $this->db->from('orders');
        return $this->db->count_all_results();
    }

    public function calculate_on_time_rating_restaurant_last_30_days($restaurant_id)
    {
        $this->db->select_avg('on_time_rating');
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('(status=2 OR status=3)');
        $this->db->where('event_date_at>=\'' . date('Y-m-j', strtotime('-30day')) . '\'');
        $result = $this->db->get('orders')->row();
        if ($result && $result->on_time_rating)
        {
            return number_format($result->on_time_rating, 2);
        }

        return 0;
    }

    public function calculate_on_delivery_rating_restaurant_last_30_days($restaurant_id)
    {
        $this->db->select_avg('on_delivery_rating');
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('(status=4 OR status=5)');
        $this->db->where('event_date_at>=\'' . date('Y-m-j', strtotime('-30day')) . '\'');
        $result = $this->db->get('orders')->row();
        if ($result && $result->on_delivery_rating)
        {
            return number_format($result->on_delivery_rating, 2);
        }

        return 0;
    }

    public function calculate_on_delivery_rating_restaurant_last_30_days_by_score($restaurant_id, $score)
    {
        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('on_delivery_rating', $score);
        $this->db->where('(status=4 OR status=5)');
        $this->db->where('event_date_at>=\'' . date('Y-m-j', strtotime('-30day')) . '\'');
        $this->db->from('orders');
        return $this->db->count_all_results();
    }

    public function generate_order_report($start_date, $end_date, $restaurant_id, $status)
    {
        $start_date_time = strtotime($start_date);
        $end_date_time = strtotime($end_date);
        if ($end_date_time < $start_date_time)
        {
            $tmp = $start_date_time;
            $start_date_time = $end_date_time;
            $end_date_time = $tmp;
        }

        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('status', $status);
        $this->db->where("(event_date_at>='$start_date' AND event_date_at<='$end_date')");
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $payload = [
                    'id' => $row->id,
                    'event_date_at' => $row->event_date_at,
                    'event_hour' => $row->event_hour,
                    'event_minute' => $row->event_minute,
                    'payment_method' => $row->payment_method,
                    'amount' => $row->amount,
                    'delivery_fee' => $row->delivery_fee,
                    'discount' => $row->discount,
                    'tips' => $row->tips,
                    'tax' => $row->tax,
                    'total' => $row->total,
                    'status' => $this->_status_mapping[$row->status],
                ];
                $data[] = array_values($payload);
            }
        }
        return $data;
    }

    public function get_payout_orders_by_date_range($payout_day, $previous_payout_day, $restaurant_id, $earning_percentage)
    {

        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where("(event_date_at>='$previous_payout_day' AND event_date_at<='$payout_day')");
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = [
                    'id' => $row->id,
                    'event_date_at' => $row->event_date_at,
                    'amount' => $row->amount,
                    'delivery_fee' => $row->delivery_fee,
                    'discount' => $row->discount,
                    'tips' => $row->tips,
                    'tax' => $row->tax,
                    'total' => $row->total,
                    'commission' => $row->total * $earning_percentage,
                    'status' => $row->status
                ];
            }
        }
        return $data;
    }

    public function get_orders_by_date_range($payout_day, $previous_payout_day, $restaurant_id)
    {

        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where("(event_date_at>='$previous_payout_day' AND event_date_at<='$payout_day')");
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function generate_expense_report($start_date, $end_date, $user_id)
    {
        $start_date_time = strtotime($start_date);
        $end_date_time = strtotime($end_date);
        if ($end_date_time < $start_date_time)
        {
            $tmp = $start_date_time;
            $start_date_time = $end_date_time;
            $end_date_time = $tmp;
        }

        $this->db->where('user_id', $user_id);
        $this->db->where('status', 5);
        $this->db->where("(event_date_at>='$start_date' AND event_date_at<='$end_date')");
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('orders');
        $data = [];
        $sum = 0;
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $row_data = json_decode($row->data, TRUE);
                $quantity = 1;
                if ($row_data && $row_data['items'] && count($row_data['items']) > 0)
                {
                    foreach ($row_data['items'] as $item) {
                        if (($item['quantity'] * $item['serving']) > $quantity)
                        {
                            $quantity = $item['quantity'] * $item['serving'];
                        }
                    }
                }
                $sum += ((float)$row->total / (int)$quantity);
                $payload = [
                    'id' => $row->id,
                    'date' => $row->event_date_at,
                    'amount' => $row->amount,
                    'quantity' => $quantity,
                    'total' => $row->total,
                    'total_per_person' => number_format((float)$row->total / (int)$quantity, 2),
                ];
                $data[] = array_values($payload);
            }
            $data[] = ['avg', '', '', '', '', number_format($sum/$query->num_rows(), 2)];
        }
        return $data;
    }

    public function generate_earning_report($start_date, $end_date, $status, $earning_percentage)
    {
        $start_date_time = strtotime($start_date);
        $end_date_time = strtotime($end_date);
        if ($end_date_time < $start_date_time)
        {
            $tmp = $start_date_time;
            $start_date_time = $end_date_time;
            $end_date_time = $tmp;
        }

        if ($status == 0)
        {
            $this->db->where('(status=4 OR status=5)');
        }
        else if ($status == 1)
        {
            $this->db->where('(status=1 OR status=2 OR status=3)');
        }
        else if ($status == 2)
        {
            $this->db->where('(status=0 OR status=6 OR status=7 OR status=8 OR status=9)');
        }

        $this->db->where("(event_date_at>='$start_date' AND event_date_at<='$end_date')");
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $payload = [
                    'id' => $row->id,
                    'restaurant_id' => $row->restaurant_id,
                    'event_date_at' => $row->event_date_at,
                    'payment_method' => $row->payment_method,
                    'amount' => $row->amount,
                    'delivery_fee' => $row->delivery_fee,
                    'discount' => $row->discount,
                    'tips' => $row->tips,
                    'tax' => $row->tax,
                    'total' => $row->total,
                    'status' => $this->_status_mapping[$row->status],
                    'earning' => $row->total * $earning_percentage
                ];
                $data[] = array_values($payload);
            }
        }
        return $data;
    }

    public function generate_item_order_report($start_date, $end_date, $restaurant_id, $status)
    {
        $start_date_time = strtotime($start_date);
        $end_date_time = strtotime($end_date);
        if ($end_date_time < $start_date_time)
        {
            $tmp = $start_date_time;
            $start_date_time = $end_date_time;
            $end_date_time = $tmp;
        }

        $this->db->where('restaurant_id', $restaurant_id);
        $this->db->where('status', $status);
        $this->db->where("(event_date_at>='$start_date' AND event_date_at<='$end_date')");
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('orders');
        $data = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $json_data = json_decode($row->data, TRUE);
                $items = $json_data['items'];
                $payload = [];
                foreach ($items as $item) {
                    $payload = [
                        'id' => $row->id,
                        'event_date_at' => $row->event_date_at,
                        'title' => $item['title'],
                        'addons' => $this->_generate_addon_list($item['addons']),
                        'quantity' => $item['quantity'],
                        'price' => $item['price'],
                        'status' => $this->_status_mapping[$row->status],
                    ];
                    $data[] = array_values($payload);
                }
            }
        }
        return $data;
    }

    private function _generate_addon_list ($addons)
    {
        $list = [];
        foreach ($addons as $key => $addon)
        {
            $list[] = $addon['name'];
        }
        return implode(';', $list);
    }

}
