<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Suggest Model
 * @author manaknight inc.
 *
 */
class Suggest extends CI_Model
{
    public $_status_mapping = [
		0 => 'Inactivo',
		1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function num_suggest()
	{
		return $this->db->count_all('suggest');
	}

	/**
	 * Get paginated users
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $users
	 */
	public function get_paginated_suggests($page = 1, $limit=10)
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('suggest');
		$result = [];

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }
		}
        return $result;
	}

	/**
	 * Get suggest
	 *
	 * @param integer $id
	 * @return suggest
	 */
	public function get_suggest($id)
    {
		$this->db->from('suggest');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Delete suggest
	 *
	 * @param integer $id
	 * @return suggest
	 */
	public function delete($id)
    {
		$this->db->where('id', $id);
		return $this->db->delete('suggest');
	}

	/**
	 * Get all suggest
	 *
	 * @return array suggest
	 */
	public function get_suggests()
    {
		$this->db->from('suggest');
        return $this->db->get()->result();
	}

	/**
	 * Create suggest
	 *
	 * @param array $data
	 * @return suggest
	 */
	public function create_suggest($data)
	{
		$data['created_at'] = date('Y-m-j H:i:s');
		return $this->db->insert('suggest', $data, TRUE);
	}

	/**
	 * Edit suggest
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_suggest($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('suggest', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}

	/**
     * Return Type Mapping
     * @return array
     */
    public function type_mapping ()
	{
		return $this->_type_mapping;
	}
}