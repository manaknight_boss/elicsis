<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Contact Model
 * @author manaknight inc.
 *
 */
class Contact extends CI_Model
{
    public $_status_mapping = [
		0 => 'Inactivo',
		1 => 'Activo'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function num_contacts()
	{
		return $this->db->count_all('contact');
	}

	/**
	 * Get paginated users
	 *
	 * @access public
	 * @param integer $page default 1
	 * @param integer $limit default 1
	 * @return array $users
	 */
	public function get_paginated_contacts($page = 1, $limit=10)
    {
		$this->db->limit($limit, $page);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('contact');
		$result = [];

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }
		}
        return $result;
	}

	/**
	 * Get Contact
	 *
	 * @param integer $id
	 * @return contact
	 */
	public function get_contact($id)
    {
		$this->db->from('contact');
        $this->db->where('id', $id, TRUE);
        return $this->db->get()->row();
	}

	/**
	 * Get all Contact
	 *
	 * @return array contact
	 */
	public function get_contacts()
    {
		$this->db->from('contact');
        return $this->db->get()->result();
	}

	/**
	 * Create Contact
	 *
	 * @param array $data
	 * @return Contact
	 */
	public function create_contact($data)
	{
        $data['created_at'] = date('Y-m-j');
		return $this->db->insert('contact', $data, TRUE);
	}

	/**
	 * Edit Contact
	 * @param array $data
	 * @param integer $id
	 * @return bool
	 */
	public function edit_contact($data, $id)
	{
		$this->db->where('id', $id, TRUE);
		return $this->db->update('contact', $data);
    }

    /**
     * Return Status Mapping
     * @return array
     */
    public function mapping ()
	{
		return $this->_status_mapping;
	}
}