<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Search_service
{
    /**
     * Restaurant Model
     *
     * @var CI_Model
     */
    private $_model;

    private $_day_mapping = [
        0 => 'm',
        1 => 't',
        2 => 'w',
        3 => 'r',
        4 => 'f',
        5 => 's',
        6 => 'u'
    ];

    /**
     * Load up Database for query
     *
     * @param CI_Model $restaurant
     * @return void
     */
    public function init ($restaurant)
    {
        $this->_model = $restaurant;
    }

    /**
     * Query Restaurant Table for given parameters
     *
     * @param integer $page
     * @param mixed $data
     * @return mixed
     */
    public function query ($page, $data)
    {
        /**
         * Steps:
         * 1. If cuisine_id, do filter
         * 2. If delivery, do filter
         * 3. If distance, do filter
         * 6. If order_type, do filter
         * 8. If price, do filter
         * 9. If rating, do filter
         * 10. If search, do filter
         * 11. If serving, do filter
         * 12. Paginate payload
         * 13. Query
         * 14. Build results
         * 15. Return results
         */
        $payload = [
            'status' => "=1"
        ];
        $items = [];
        $limit = 10;
        //1
        if ($data['cuisine_id'] && $data['cuisine_id'] > 0)
        {
            $payload['cuisines_id'] = "= {$data['cuisine_id']}";
        }

        //2
        if ($data['delivery'] && $data['delivery'] > 0)
        {
            $payload['food_minimum'] = "<= {$data['delivery']}";
        }

        //6
        if ($data['order_type']  && $data['order_type'] > 0)
        {
            $payload['   '] = "(type={$data['order_type']} OR type=0)";
        }
        else
        {
            $payload['type'] = "=0 ";
        }

        //8
        if ($data['price']  && strlen($data['price']) > 0)
        {
            $payload['price_range'] = "='{$data['price']}'";
        }

        //9
        if ($data['rating']  && $data['rating'] > 0)
        {
            $payload['rating'] = ">={$data['rating']}";
        }

        //10
        if ($data['search']  && strlen($data['search']) > 0)
        {
            $payload[' '] = $this->_model->build_search_parameter($data['search']);
        }

        //11
        if ($data['serving']  && $data['serving'] > 0)
        {
            $payload['serving'] = ">={$data['serving']}";
        }

        if ($data['day'])
        {
            $day_mapping = $this->day_mapping($data['day']);
            $payload[$day_mapping] = "=1 ";
        }

        if ($data['time'])
        {
            $time_mapping = $this->build_time_query($data['day'], $data['time']);
            $payload[$time_mapping['field']] = $time_mapping['query'];
        }

        //12 paginate
        //13 query
        $num_rows = $this->get_num_rows($payload, $data);

        if ($num_rows > 0)
        {
            $items = $this->get_query_results($payload, $data, $page, $limit);
        }

        //14 build result
        return [
            'page' => $page,
            'num_items' => $num_rows,
            'num_pages' => ceil($num_rows / $limit),
            'items' => $items
        ];
    }

    /**
     * Calculate Distance
     *
     * @param [type] $lat
     * @param [type] $lng
     * @param [type] $type
     * @return string
     */
    public function calculate_distance ($lat, $lng, $lat2, $lng2, $type)
    {
        $num = $this->distance_number($type);
        return ($num * acos(
            cos( deg2rad($lat) )
          * cos( deg2rad( $lat2 ) )
          * cos( deg2rad( $lng2 ) - deg2rad($lng) )
          + sin( deg2rad($lat) )
          * sin( deg2rad( $lat2 ) )) );
    }
    /**
     * Calculate distance Mysql text
     *
     * @param mixed $data
     * @return mixed
     */
    private function calculate_distance_sql ($data)
    {
        $result = [
            'select' => '',
            'having' => ''
        ];

        $result['select'] = $this->distance_query_precompute($data['lat'], $data['lng'], 'miles');
        $result['having'] = "HAVING distance <= {$data['distance']}";

        return $result;
    }
    /**
     * Get Number of Rows for given query
     *
     * @param array $payload
     * @param string $distance_select
     * @return integer
     */
    private function get_num_rows ($payload, $distance_data)
    {
        $result = $this->_model->raw_query($this->generate_query_num($payload, $distance_data));
        $num = 0;

        //D::ep($this->generate_query_num($payload, $distance_data));

        if ($result->num_rows() > 0)
		{
			foreach ($result->result() as $row)
            {
                    $num = (int)$row->num;
            }
        }

        return $num;
    }

    /**
     * Get Number of Rows for given query
     *
     * @param array $payload
     * @param string $distance_select
     * @return integer
     */
    private function get_query_results ($payload, $distance_data, $page, $limit)
    {
        // D::ep($this->generate_query($payload, $distance_data, $page, $limit));
        $result = $this->_model->raw_query($this->generate_query($payload, $distance_data, $page, $limit));
        // D::ep($result);
        $items = [];
        if ($result->num_rows() > 0)
		{
			foreach ($result->result() as $row)
            {
                $items[] = $this->transform_data(json_decode(json_encode($row), TRUE));
                // D::ep($row);
            }
        }

        return $items;
    }

    /**
     * Generate mysql query
     *
     * @param mixed $payload
     * @param string $distance_select
     * @return string
     */
    private function generate_query($payload, $distance_data, $page, $limit)
    {
        $select = '`id`, `title`, `description`, `serving`, `about`, `delivery_fee`, `rating`, `num_review`, `food_minimum`, `cuisines_id`, `price_range`, `type`, `status`, `address`, `city`, `state`, `zip`, `lat`, `long`, `logo`, `image`, `banner_image`, `started_at`, on_time_rating, on_delivery_rating, num_review ';
        $where = [];
        $where_str = '';
        $having_str = '';
        $distance_query = $this->calculate_distance_sql($distance_data);
        $distance_select = $distance_query['select'];
        $having = $distance_query['having'];
        $offset = $page * $limit;
        $offset_str = " LIMIT {$offset}, {$limit}";
        // D::ep($payload);
        $select = $select . ', ' . $distance_select;

        if ($distance_data['distance'] > 0)
        {
            $having_str = $having;
        }

        foreach ($payload as $key => $value)
        {
            $where[] = "$key$value";
        }

        $where_str = implode(" AND ", $where);


        return "SELECT $select FROM restaurants WHERE $where_str $having_str ORDER BY `distance` ASC $offset_str";
    }

    /**
     * Generate mysql query count
     *
     * @param mixed $payload
     * @param string $distance_select
     * @return string
     */
    private function generate_query_num($payload, $distance_data)
    {
        $select = 'SELECT count(*) num FROM (';
        $where = [];
        $where_str = '';
        $having_str = '';

        $distance_query = $this->calculate_distance_sql($distance_data);
        $distance_select = $distance_query['select'];
        $having = $distance_query['having'];

        //3
        if ($distance_data['distance'] > 0)
        {
            $having = "HAVING distance <= {$distance_data['distance']}";
        } else
        {
            $having = '';
        }

        if (strlen($having) > 0)
        {
            $having_str = $having;
        }

        foreach ($payload as $key => $value)
        {
            $where[] = "{$key}{$value}";
        }

        $where_str = implode(" AND ", $where);

        if (strlen($distance_select) > 0)
        {
            return "$select SELECT  $distance_select FROM restaurants WHERE $where_str $having_str ) as x";
        }
        else
        {
            return "SELECT count(*) num FROM restaurants WHERE $where_str";
        }
    }

    /**
     * Distance number on earth
     *
     * @param string $type
     * @return integer
     */
    private function distance_number ($type)
    {
        $distance_type = 3961;

        if ($type == 'miles')
        {
            $distance_type = 3961;
        }
        if ($type == 'km')
        {
            $distance_type = 6373;
        }
        if ($type == 'm')
        {
            $distance_type = 6373000;
        }
        if ($type == 'feet')
        {
            $distance_type = 20914080;
        }
        return $distance_type;
    }

    /**
     * Distance Query
     *
     * @param [type] $lat
     * @param [type] $lng
     * @param [type] $type
     * @return string
     */
    private function distance_query ($lat, $lng, $type)
    {
        $num = $this->distance_number('km');
        return "($num * acos(
            cos( radians($lat) )
          * cos( radians( `lat` ) )
          * cos( radians( `long` ) - radians($lng) )
          + sin( radians($lat) )
          * sin( radians( `lat` ) )
            ) ) as distance";
    }

    /**
     * Distance Query
     *
     * @param [type] $lat
     * @param [type] $lng
     * @param [type] $type
     * @return string
     */
    private function distance_query_precompute ($lat, $lng, $type)
    {
        $num = $this->distance_number('km');
        $precompute_lat_cos = cos(deg2rad($lat));
        $precompute_lng = deg2rad($lng);
        $precompute_lat_sin = sin(deg2rad($lat));

        return "($num * acos(
            $precompute_lat_cos
          * cos( radians( `lat` ) )
          * cos( radians( `long` ) - ($precompute_lng) )
          + $precompute_lat_sin
          * sin( radians( `lat` ) )
            ) ) as distance";
    }

    /**
     * Build Price Parameter
     *
     * @param [type] $price
     * @return void
     */
    private function build_price_parameter ($price)
    {
        $num_symbols = strlen($price);
        $result = [];
        for ($i=0; $i < $num_symbols; $i++)
        {
            $result[] = "price_range='" . str_repeat('$', $i + 1) . "'";
        }
        // D::ep('(' . implode(" OR ", $result) . ')');
        return '(' . implode(" OR ", $result) . ')';
    }

    /**
     * Maps day to day symbol
     *
     * @param mixed $day
     * @return string
     */
    private function day_mapping($day)
    {
        return $this->_day_mapping[$day];
    }

    /**
     * Build Time MYsql Query and sets field to empty
     *
     * @param integer $day
     * @param integer $time
     * @return array
     */
    private function build_time_query($day, $time)
    {
        $day_symbol = $this->day_mapping($day);
        if ($this->is_decimal($time))
        {
            return [
                'field' => '  ',
                'query' => " {$day_symbol}_open_total <=" . (($time * 60) + 30) . " AND {$day_symbol}_close_total >=" . (($time * 60) + 30)
            ];
        }

        return [
            'field' => '  ',
            'query' => " {$day_symbol}_open_total <= " . ($time * 60) . " AND {$day_symbol}_close_total >=" .  ($time * 60)
        ];
    }

    private function is_decimal( $val )
	{
		return is_numeric( $val ) && floor( $val ) != $val;
    }

    /**
     * Fix up the types of fields in data
     *
     * @param mixed $data
     * @return mixed
     */
    public function transform_data($data)
    {
        if (isset($data['serving']))
        {
            $data['serving'] = (int)$data['serving'];
        }

        if (isset($data['delivery_fee']))
        {
            $data['delivery_fee'] = (int)$data['delivery_fee'];
        }

        if (isset($data['cuisines_id']))
        {
            $data['cuisines_id'] = (int)$data['cuisines_id'];
        }

        if (isset($data['food_minimum']))
        {
            $data['food_minimum'] = (int)$data['food_minimum'];
        }

        if (isset($data['distance']))
        {
            $data['distance'] = (float)$data['distance'];
        }

        if (isset($data['lat']))
        {
            $data['lat'] = (float)$data['lat'];
        }

        if (isset($data['long']))
        {
            $data['long'] = (float)$data['long'];
        }

        if (isset($data['lng']))
        {
            $data['lng'] = (float)$data['lng'];
        }

        if (isset($data['num_review']))
        {
            $data['num_review'] = (int)$data['num_review'];
        }

        if (isset($data['rating']))
        {
            $data['rating'] = (float)$data['rating'];
        }

        if (isset($data['day']))
        {
            $data['day'] = (int)$data['day'];
        }

        if (isset($data['time']))
        {
            $data['time'] = (int)$data['time'];
        }
        return $data;
    }

    public function valid_event_date($restaurant_model, $restaurant_id, $date, $time, $minute)
    {
        $valid = FALSE;
        $restaurant = $restaurant_model->get_restaurant($restaurant_id);
        if ($restaurant)
        {
            $time_total = ($time * 60) + $minute;
            $day_of_week = date('w', strtotime($date));
            $day_code = $this->_day_mapping[$day_of_week];
            if ($restaurant->{$day_code} == 1)
            {
                $open_total = $day_code . '_open_total';
                $close_total = $day_code . '_close_total';
                if ($restaurant->{$open_total} <= $time_total && $restaurant->{$close_total} >= $time_total)
                {
                    $valid = TRUE;
                }
            }
        }
        return $valid;
    }
}