<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_service
{
    /**
     * Catering Menu Key Value Pairs
     *
     * @var mixed
     */
    private $_catering_menu;

    /**
     * Catering preferences key value pairs
     *
     * @var mixed
     */
    private $_preferences;

    /**
     * menu property key value pairs
     *
     * @var mixed
     */
    private $_menu_property;

    /**
     * Menu
     *
     * @var mixed
     */
    private $_menu;

    /**
     * Menu Addons
     *
     * @var mixed
     */
    private $_menu_addon;

    /**
     * Restaurant
     *
     * @var mixed
     */
    private $_restaurant;

    /**
     * Orders
     *
     * @var mixed
     */
    private $_orders;

    /**
     * customer restaurant
     *
     * @var mixed
     */
    private $_customer_restaurant;

    /**
     * setting
     *
     * @var mixed
     */
    private $_setting;

    /**
     * sold_by_mapping
     *
     * @var mixed
     */
    private $_sold_by_mapping;

    /**
     * Load up Database for query
     *
     * @param CI_Model $restaurant
     * @return void
     */
    public function init ($catering_menu, $menu, $menu_addon,
                          $menu_property, $preferences, $restaurant,
                          $setting, $orders, $customer_restaurant)
    {
        $this->_sold_by_mapping = $menu->sold_by_mapping();
        $this->_menu = $menu->get_active_menus_by_restaurant($restaurant->id);
        $this->_menu_addon = $menu_addon;
        $this->_menu_property = $menu_property->get_menu_properties();
        $this->_catering_menu = $catering_menu->get_catering_menus(TRUE);
        $this->_preferences = $preferences->get_preferences();
        $this->_restaurant = $restaurant;
        $this->_orders = $orders;
        $this->_customer_restaurant = $customer_restaurant;
        $this->_setting = $setting->get_config_settings();
    }

    public function build_config()
    {
        $payload = [
            'id' => $this->_restaurant->id,
            'on_time_rating' => $this->_restaurant->on_time_rating,
            'delivery_type' => $this->_restaurant->type,
            'on_delivery_rating' => $this->_restaurant->on_delivery_rating,
            'repeat_customer' => $this->_customer_restaurant->num_repeat_customer($this->_restaurant->id),
            'total_customer' => $this->_customer_restaurant->num_customer_restaurants($this->_restaurant->id),
            'num_orders' => $this->_orders->num_restaurant_orders($this->_restaurant->id),
            'catering_menu' => $this->get_used_catering_menu(),
            'preferences' => $this->_preferences,
            'menu_property' => $this->_menu_property,
            'food_minimum' => $this->_restaurant->food_minimum,
            'delivery_fee' => $this->_restaurant->delivery_fee,
            'tax' => $this->_setting['tax'],
            'tips' => json_decode($this->_setting['tips'], TRUE)
        ];

        $menu_list_by_catering_menu = $this->aggregate_menu_addon_to_menu($payload['catering_menu']);

        $payload['menu'] = $menu_list_by_catering_menu;
        return $payload;
    }

    /**
     * Get Only Catering Menus that are in menu
     *
     * @return void
     */
    private function get_used_catering_menu ()
    {
        $catering_menus = [];
        $catering_menu_keys = array_keys($this->_catering_menu);

        foreach ($this->_menu as $key => $value)
        {
            $catering_menu_id = $value->catering_menu_id;
            if (in_array($catering_menu_id, $catering_menu_keys))
            {
                $catering_menus[$catering_menu_id] = $this->_catering_menu[$catering_menu_id];
            }
        }

        return $catering_menus;
    }

    /**
     * Add Menu Addon to Menu
     *
     * @return void
     */
    private function aggregate_menu_addon_to_menu($catering_menu)
    {
        $menu = [];

        foreach($catering_menu as $id => $menu_type)
        {
            $menu[$id] = [];
            foreach ($this->_menu as $key => $value)
            {
                $menu_id = $value->id;
                $addon = $this->_menu_addon->get_menu_addons($menu_id, TRUE);
                $this->_menu[$key]->addon = (count($addon) > 0) ? $this->process_addon($addon) : [];
                $this->_menu[$key]->preference = $this->manage_preference($value->preference);

                if (is_numeric($value->sold_by))
                {
                    $this->_menu[$key]->sold_by = $this->_sold_by_mapping[$value->sold_by];
                }

                if ($value->catering_menu_id == $id)
                {
                    $menu[$id][] = $this->_menu[$key];
                }
            }
        }
        return $menu;
    }

    /**
     * Create KEY Value Pair between menu property and addons
     *
     * @param mixed $addons
     * @return array
     */
    private function process_addon ($addons)
    {
        $menu_properties = $this->_menu_property;
        $addon_list = [];
        foreach ($menu_properties as $key => $value)
        {
            foreach ($addons as $addon_key => $addon)
            {
                if ($addon->menu_property_id == $value->id)
                {
                    $addon_list[$value->name][] = $this->process_addon_by_property($addon, $value, $value->name);
                }
            }
        }
        return $addon_list;
    }

    /**
     * Remove fields that should not be used
     *
     * @param [type] $addon
     * @param [type] $property
     * @return void
     */
    private function process_addon_by_property($addon, $property, $addon_type)
    {
        if ($property->is_serving == "0")
        {
            unset($addon->serving);
        }

        if ($property->is_price == "0")
        {
            unset($addon->price);
        }

        if ($property->is_info == "0")
        {
            unset($addon->info);
        }
        if ($property->is_preference == "0")
        {
            unset($addon->preference);
        }
        else
        {
            $addon->preference = $this->manage_preference($addon->preference);
        }

        unset($addon->status);
        $addon->type = $addon_type;
        return $addon;
    }

    /**
     * Translate preferences from ids to strings
     *
     * @param [type] $preferences
     * @return void
     */
    private function manage_preference($preferences)
    {
        if($this->is_json($preferences))
        {
            $list = json_decode($preferences, TRUE);
            $preference_list = [];
            foreach ($list as $key => $value)
            {
                $preference_id = (int) $value;
                foreach ($this->_preferences as $key => $preference)
                {
                    if ($preference_id == $preference->id)
                    {
                        $preference_list[] = $preference->icon;
                    }
                }
            }

            return implode('&nbsp;',$preference_list);
        }

        return $preferences;
    }

    private function is_json($string) {
        return !empty($string) && is_string($string) && is_array(json_decode($string, true)) && json_last_error() == 0;
    }
}