<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Payout_service
{
    protected $_settings;
    protected $_restaurants;
    protected $_payments;
    protected $_orders;

    public function init($settings, $restaurants, $payments, $orders)
    {
        $this->_settings = $settings;
        $this->_restaurants = $restaurants;
        $this->_payments = $payments;
        $this->_orders = $orders;
    }

    /**
     * Execute cron job to make all payouts
     *
     * @param integer $payout_day_num
     * @param integer $earning_rate
     * @return void
     */
    public function execute($payout_day_num, $earning_rate)
    {
        $data = [];

        $data['payout_day'] = $this->get_payout_day($payout_day_num);

        $data['previous_payout_day'] = $this->get_previous_payout_day($data['payout_day']);

        $data['total_rows'] = $this->_restaurants->num_restaurants();
        $data['per_page'] = 100;
		$data['num_pages'] = ceil($data['total_rows'] / $data['per_page']);

        for ($i=0; $i < $data['num_pages']; $i++)
        {
            $list = $this->_restaurants->get_paginated_restaurants($i, $data['per_page']);
            $list_count = count($list);
            for ($j=0; $j < $list_count; $j++)
            {
                $restaurant_id = $list[$j]->id;
                if ($list[$j]->status == 1)
                {
                    //4
                    $payment_exist = $this->_payments->get_payment_by_restaurant($restaurant_id);
                    if (!$payment_exist)
                    {
                        //5
                        $order_result = $this->_orders->get_payout_orders_by_date_range($data['payout_day']->format('Y-m-d'),
                        $data['previous_payout_day']->format('Y-m-d'),
                        $restaurant_id,
                        $earning_rate / 100);

                        if (count($order_result) > 0)
                        {
                            $this->_payments->create_payment($this->process_orders($restaurant_id,
                            $data['payout_day']->format('Y-m-d'),
                            $order_result));
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * get previous payout day
     *
     * @param date $payout_day
     * @return date
     */
    protected function get_previous_payout_day($payout_day)
    {
        $previous_payout_day = clone $payout_day;
        return $previous_payout_day->modify('- 7 days');
    }

    /**
     * Execute for 1 restaurant
     *
     * @param integer $restaurant_id
     * @param integer $payout_day_num
     * @param integer $earning_rate
     * @return mixed
     */
    public function single_execute($restaurant_id, $payout_day_num, $earning_rate)
    {
        $data = [];

        $data['payout_day'] = $this->get_payout_day($payout_day_num);

        $data['previous_payout_day'] = $this->get_previous_payout_day($data['payout_day']);

        $payment_exist = $this->_payments->get_payment_by_restaurant($restaurant_id);

        if (!$payment_exist)
        {
            //5
            $order_result = $this->_orders->get_payout_orders_by_date_range($data['payout_day']->format('Y-m-d'),
            $data['previous_payout_day']->format('Y-m-d'),
            $restaurant_id,
            $earning_rate / 100);

            if (count($order_result) > 0)
            {
                $this->_payments->create_payment($this->process_orders($restaurant_id,
                $data['payout_day']->format('Y-m-d'),
                $order_result));
            }
        }

        return $data;
    }

    /**
     * get payout date
     *
     * @param integer $payout_day_num
     * @return date
     */
    protected function get_payout_day($payout_day_num)
    {
        $payout_day = new DateTime();
        $today_day = date('w', time());

        if ($today_day > $payout_day_num)
        {
            $payout_day = $payout_day->modify('- ' . $today_day . ' days');
        }

        if ($today_day < $payout_day_num)
        {
            $payout_day = $payout_day->modify('+ ' . $today_day . ' days');
        }

        return $payout_day;
    }

    /**
     * Process order
     *
     * Steps:
     * 1.Add up all tips, delivery
     * 2.If order is 3,4,5 add up amount, delivery, tips,total, commission, final_total
     * 3.If orders in 0,1,2,8 add up total and put in not paid
     * 4.If orders in 6, 7, 9 add up total and put in problem orders
     */
    protected function process_orders($restaurant_id, $payment_date, $orders)
    {
        $adjustment = 0;
        $not_paid = 0;
        $problem_not_paid = 0;
        $commission = 0;
        $tips = 0;
        $tax = 0;
        $total = 0;
        $delivery = 0;
        $amount = 0;

        foreach ($orders as $single_order)
        {
            if ($this->is_success_order($single_order['status']))
            {
                $tips = $tips + $single_order['tips'];
                $amount = $amount + $single_order['amount'];
                $tax = $tax + $single_order['tax'];
                $total = $total + $single_order['total'];
                $commission = $commission + $single_order['commission'];
                $delivery = $delivery + $single_order['delivery_fee'];
            }

            if ($this->is_not_paid_order($single_order['status']))
            {
                $not_paid = $not_paid + $single_order['total'];
            }

            if ($this->is_problem_order($single_order['status']))
            {
                $problem_not_paid = $problem_not_paid + $single_order['total'];
            }
        }

        return [
            'payment_date' => $payment_date,
            'notes' => '',
            'restaurant_id' => $restaurant_id,
            'adjustment' => number_format($adjustment, 2),
            'not_paid' => number_format($not_paid, 2),
            'problem_order' => number_format($problem_not_paid, 2),
            'commission' => number_format($commission, 2),
            'total' => number_format($total, 2),
            'num_orders' => count($orders),
            'status' => 0,
            'tax' => number_format($tax, 2),
            'tips' => number_format($tips, 2),
            'delivery' => number_format($delivery, 2),
            'amount' => number_format($amount, 2)
        ];
    }

    /**
     * is problem order
     *
     * @param string $status
     * @return boolean
     */
    protected function is_problem_order($status)
    {
        $problem_orders = [6, 7, 9];
        return in_array($status, $problem_orders);
    }

    /**
     * is not paid order
     *
     * @param string $status
     * @return boolean
     */
    protected function is_not_paid_order($status)
    {
        $not_success_orders = [0, 1, 2, 8];
        return in_array($status, $not_success_orders);
    }

    /**
     * is success order
     *
     * @param string $status
     * @return boolean
     */
    protected function is_success_order($status)
    {
        $success_orders = [3, 4, 5];
        return in_array($status, $success_orders);
    }
}