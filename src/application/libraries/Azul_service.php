<?php

class Azul_service
{
  private $_url = '';
  private $_key = '';

  public function set_keys($url, $key)
  {
    $this->_url = $url;
    $this->_key = $key;
  }

  public function sale($payload)
  {
    return $this->curl("sale", json_encode($payload));
  }

  public function refund($payload)
  {
    return $this->curl('refund', json_encode($payload));
  }

  public function createToken($payload) {
    return $this->curl('create-token', json_encode($payload));
  }

  public function deleteToken($payload) {
    return $this->curl('delete-token', json_encode($payload));
  }

  private function encrypt(string $original) : string {
    $publicKey = openssl_get_publickey($this->publicKey());

    $iv = openssl_random_pseudo_bytes(16);

    $success = openssl_seal(
      $original ,
      $sealed ,
      $ekeys ,
      [$publicKey] ,
      "AES-256-CBC" ,
      $iv
    );

    openssl_free_key($publicKey);

    if(!$success) throw new Exception('Could not encrypt data', 500);

    $sealed64 = base64_encode($sealed);
    $key64 = base64_encode($ekeys[0]);
    $iv64 = base64_encode($iv);

    $output = $this->envelope($sealed64, $key64, $iv64);

    return $output;

  }

  private function decrypt(string $json) : string
  {
    $envelope = json_decode($json);

    $sealed = base64_decode($envelope->a);
    $key = base64_decode($envelope->b);
    $iv = base64_decode($envelope->c);

    $privateKey = openssl_get_privatekey($this->privateKey(), null);

    $success = openssl_open(
      $sealed,
      $original,
      $key,
      $privateKey,
      'AES-256-CBC',
      $iv
    );

    openssl_free_key($privateKey);

    if(!$success) throw new Exception('Could not decrypt data', 500);

    return $original;

  }


  private function publicKey() {
    $path = dirname(__FILE__) . '/../config/payment_elicsis_public_key_4096.pem';
    // $path = dirname(__FILE__) . '/elicsisblue_client_public_4096.pem';
    return file_get_contents($path);
  }

  private function privateKey() {
    $path = dirname(__FILE__) . '/../config/elicsisblue_server_private_4096.pem';
    // $path = dirname(__FILE__) . '/elicsisblue_server_private_4096.pem';
    return file_get_contents($path);
  }

  private function envelope($sealed64, $key64, $iv64)
  {
    return <<<JSON
{
  "a": "$sealed64",
  "b": "$key64",
  "c": "$iv64"
}
JSON;
  }

  private function curl($path, $payload) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "{$this->_url}/$path");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->encrypt($payload));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json"
    ));

    $output = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    // echo $output;
    if ($http_code == 200)
    {
      $plain_text = $this->decrypt($output);
      $json = @json_decode($plain_text, TRUE);
      if (!$json)
      {
        error_log('Not json');
        error_log($plain_text);
        return FALSE;
      }
      if (isset($json['ResponseMessage']) && $json['ResponseMessage'] != 'APROBADA')
      {
        error_log('Not approved');
        error_log($plain_text);
        return $json;
      }

      return $json;
    }
    else
    {
      error_log('different code');
      error_log($http_code);
      error_log($output);
      return FALSE;
    }
  }
}
