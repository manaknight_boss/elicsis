<?php
use Twilio\Rest\Client;
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Sms_service
{
    /**
     * Mail Adapter
     *
     * @var mixed
     */
    public $_adapter = null;

    /**
     * Adapter selected
     *
     * @var string
     */
    public $_type = '';

    /**
     * CI
     *
     * @var mixed
     */
    public $_ci = null;

    /**
     * Set mail service to correct way to send emails
     *
     * @param string $type
     * @throws Exception
     */
    public function set_adapter ($type)
    {

        $this->_type = $type;
        $this->_ci = &get_instance();

        switch ($type)
        {
            case 'sms':
                $this->_adapter = new Client($this->_ci->config->item('twilio_sid'), $this->_ci->config->item('twilio_token'));
                break;

            case 'whatsapp':
                // Your Account Sid and Auth Token from twilio.com/user/account
                $this->_adapter = new Client($this->_ci->config->item('twilio_sid'), $this->_ci->config->item('twilio_token'));
                break;
            default:
                break;
        }
    }

    /**
     * Send email
     *
     * @param string $to
     * @param string $message
     */
    public function send ($to, $message)
    {
        switch ($this->_type)
        {
            case 'sms':
                try {
                    $result = $this->_adapter->messages->create(
                        "+{$to}",
                        [
                            'from' => "+17178999425",
                            'body' => $message,
                        ]
                    );
                    // error_log('TO: ' . $to);
                    // error_log('Message: ' . $message);
                    // error_log('Result: ' . $result);
                    return TRUE;
                } catch (Exception $e) {
                    log_message('error', 'TO: ' . $to);
                    log_message('error', 'Message: ' . $message);
                    log_message('error', 'SMS Error: ' . $e->getMessage());
                    return FALSE;
                }
                break;

            case 'whatsapp':
                try {
                    $result = $this->_adapter->messages->create(
                        "whatsapp:+{$to}",
                        [
                            'from' => "whatsapp:+14155238886",
                            'body' => $message,
                        ]
                    );
                    // error_log('TO: ' . $to);
                    // error_log('Message: ' . $message);
                    // error_log('Result: ' . $result);
                    return TRUE;
                } catch (Exception $e) {
                    log_message('error', 'TO: ' . $to);
                    log_message('error', 'Message: ' . $message);
                    log_message('error', 'SMS Error: ' . $e->getMessage());
                    return FALSE;
                }
                break;
            default:
                break;
        }

    }
}