<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Report_service
{
    public function generate_csv($header, $data, $filename)
    {
        $csv = $this->generate_csv_raw($header, $data);
        $this->force_download($filename, $csv);
    }

    public function generate_csv_raw($header, $data)
    {
        $csv = '';
        $csv .= join(',', $header) . "\n";

        foreach ($data as $row) {
            $csv .= join(',', $row) . "\n";
        }
        return $csv;
    }

    public function force_download($file, $text)
    {
        $mime = 'application/force-download';
        header('Pragma: public'); // required
        header('Expires: 0'); // no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: ' . $mime);
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: close');
        echo $text;
        exit();
    }
}