<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <title>Elicsis</title>

    <!--  CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.min.css"/>
    <link href="/assets/css/main.css" rel="stylesheet" >
  </head>
  <body class="page">

  <nav id="navbarMain" class="global-nav navbar navbar-expand-lg navbar-dark fixed-top">

<a class="navbar-brand" href="/">
    <img src="/assets/image/Aplicaciones logo elicsis-07.png" alt="" width="100">
</a>
<button class="navbar-toggler my-2" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
  <span>&#9776;</span>
</button>
<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
  <ul class="navbar-nav text-right">
    <li class="nav-item ">
      <a class="nav-link " href="/about" id="navbarDropdown">
      Nosotros
      </a>
    </li>

    <li class="dropdown-divider"></li>
    <li class="nav-item">
      <a class="nav-link " href="/enterprise">Empresas</a>
    </li>
    <li class="dropdown-divider"></li>
    <li class="nav-item">
      <a class="nav-link " href="/become">Vender con Elicsis</a>
    </li>
    <li class="dropdown-divider"></li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownProfile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Mi Cuenta
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownProfile">
        <a class="dropdown-item" href="/member/dashboard">Hacer un Pedido</a>
        <a class="dropdown-item" href="/member/orders/0">Pedidos</a>
        <a class="dropdown-item" href="/member/reciepts/0">Comprobantes</a>
        <a class="dropdown-item" href="/member/reviews/0">Calificaciones</a>
        <a class="dropdown-item" href="/member/suggests">Sugerir</a>
        <a class="dropdown-item" href="/member/refers/0">Referir</a>
        <a class="dropdown-item" href="/member/profile">Perfil</a>
        <a class="dropdown-item" href="/member/payment">Pago</a>
        <?php
        if ($is_enterprise_user) {
          echo '<a class="dropdown-item" href="/member/expense">Reporte de consumo</a>';
        }
        ?>
        <a class="dropdown-item" href="/member/logout">Cerrar Sesión</a>
        </div>
    </li>
    <li class="dropdown-divider"></li>
    <li class="nav-item">
      <a class="nav-link " href="#" data-toggle="modal" data-target="#contactModal">Contáctanos</a>
    </li>
  </ul>
</div>
</nav>
<br/>
<br/>
<br/>
<div class="container">
  <ul class="nav nav-pills nav-fill">
    <li class="nav-item">
      <a class="nav-link <?php echo ($page_name == 'dashboard') ? 'active': '';?>" href="/member/dashboard">Hacer un Pedido</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo ($page_name == 'order') ? 'active': '';?>" href="/member/orders/0">Pedidos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo ($page_name == 'reciept') ? 'active': '';?>" href="/member/reciepts/0">Comprobantes</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo ($page_name == 'review') ? 'active': '';?>" href="/member/reviews/0">Calificaciones</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo ($page_name == 'suggest') ? 'active': '';?>" href="/member/suggests">Sugerir</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo ($page_name == 'refer') ? 'active': '';?>" href="/member/refers/0">Referir</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo ($page_name == 'profile') ? 'active': '';?>" href="/member/profile">Perfil</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo ($page_name == 'payment') ? 'active': '';?>" href="/member/payment">Pago</a>
    </li>
    <?php
      if ($is_enterprise_user) {
        echo '<li class="nav-item">';
          echo '<a class="nav-link ' . (($page_name == 'expense') ? 'active': '') .'" href="/member/expense">Reporte de consumo</a>';
        echo '</li>';
      }
    ?>
  </ul>
</div>