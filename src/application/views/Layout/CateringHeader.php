<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="/assets/css/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/vendor.css">
    <link rel="stylesheet" href="/assets/css/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="/assets/css/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/assets/css/fonts/flag-icon-css/flag-icon.min.css">
    <link rel="stylesheet" href="/assets/css/portal.css">
    <title>Restaurante</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="/catering/dashboard"><img src="/assets/image/Aplicaciones logo elicsis-07.png" alt="" srcset="" style="width:100px;"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <div style="position: relative;padding: 25px 15px 0px 15px;font-size: 18px;">
                    <?php echo $restaurant_menu_name;?>
                </div>
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="/catering/dashboard">Panel de control</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-item ">
                                <a class="nav-link <?php echo ($page_name == 'dashboard') ? 'active': '';?>" href="/catering/dashboard">
                                    <i class="fa fa-fw fa-home"></i>
                                    PANEL DE CONTROL
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($page_name == 'order') ? 'active': '';?>" href="/catering/orders/0">
                                    <i class="fa far fa-address-book"></i>
                                    Pedidos
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($page_name == 'payment') ? 'active': '';?>" href="/catering/payments/0">
                                    <i class="fa far fa-money-bill-alt"></i>
                                    Pagos
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($page_name == 'customer') ? 'active': '';?>" href="/catering/customers/0">
                                    <i class="fa fas fa-user"></i>
                                    Clientes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($page_name == 'review') ? 'active': '';?>" href="/catering/reviews/0">
                                    <i class="fa fas fa-bookmark"></i>
                                    Calificaciones
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fab fa-wpforms"></i>Reportes</a>
                                <div id="submenu-3" class="collapse submenu <?php echo ($page_name == 'report') ? ' show': '';?>" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                        <a class="nav-link " href="/catering/reports/order">
                                            <i class="fa fas fa-glass-martini"></i>
                                            Ventas/Pedidos
                                        </a>
                                        <a class="nav-link " href="/catering/reports/item">
                                            <i class="fab fa-wpforms"></i>
                                            Ventas/Productos
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($page_name == 'menu') ? 'active': '';?>" href="/catering/menu">
                                <i class="fas fa-file-alt"></i>
                                Menú
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($page_name == 'restaurant') ? 'active': '';?>" href="/catering/restaurant">
                                    <i class="fa fas fa-glass-martini"></i>
                                    Restaurante
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($page_name == 'profile') ? 'active': '';?>" href="/catering/profile">
                                    <i class="fa fas fa-user"></i>
                                    Perfil
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/catering/logout">
                                    <i class="fa fas fa-arrow-circle-left"></i>
                                    Cerrar Sesión
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">