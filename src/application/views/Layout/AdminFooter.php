<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        Copyright © 2019 Elicsis SRL. Todos Derechos Reservados.
                        </div>

                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <script src="/assets/js/vendor.js"></script>
    <script src="/assets/js/main.js<?php echo "?v=" . md5(time());?>"></script>
    <?php if ($enable_google_api) {
        echo '<script src="https://maps.googleapis.com/maps/api/js?key=' . $google_key . '&libraries=places&region=do&city=sdq&radius=50000&callback=' . $google_api_callback . '" async defer></script>';
    }
    ?>

</body>

</html>
<!-- This Platform is Powered by Manaknight Inc. https://manaknightdigital.com/ -->