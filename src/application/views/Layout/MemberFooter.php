<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    function initFreshChat() {
    window.fcWidget.init({
      token: "d1aa7963-77e5-4b2f-968a-7e9edded07ed",
      host: "https://wchat.freshchat.com"
    });
  }
    function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);

</script>
<footer class="py-5 px-3 bg-dark text-white">

  <div class="row">
    <div class="col-12 col-md">
      <h4>
        <i class="fas fa-utensils"></i> Elicsis, SRL.
        <br>
      </h4>
      <div class="">
      <a href="https://www.facebook.com/elicsisdominicana/" target="_blank">
        <i class="fab fa-facebook mr-2"></i>
        </a>
        <a href="https://www.instagram.com/elicsisdominicana/" target="_blank">
        <i class="fab fa-instagram"></i>
        </a>

      </div>
      <small class="d-block mt-2 mb-3 text-muted">Elicis todos derechos Reservados &copy; 2019</small>
      <small class="d-block mt-2 mb-3 text-muted">Nicolás Penson 73</br>Gazcue</br> Santon Domingo, Rep. Dom</small>
    </div>

    <div class="col-6 col-md">
      <h5>Compañia</h5>
      <ul class="list-unstyled text-small">
        <li><a class="text-muted" href="/about">Nosotros</a></li>
        <li><a class="text-muted" href="/tos">Terminos y Privacidad</a></li>
        <li><a class="text-muted" href="/policy">Politicas</a></li>
      </ul>
    </div>
    <div class="col-6 col-md">
      <h5>Comencemos</h5>
      <ul class="list-unstyled text-small">
        <li><a class="text-muted" href="#">Haz un pedido</a></li>
        <li><a class="text-muted" href="/become">Vender en Elicsis</a></li>
        <li><a class="text-muted" href="/enterprise">Para empresas</a></li>
      </ul>
    </div>
    <div class="col-6 col-md">
      <h5>Apoyo</h5>
      <ul class="list-unstyled text-small">
        <li><a class="text-muted" href="/faq">Preguntas Frecuentes</a></li>
        <li><a class="text-muted" href="#" href="#" data-toggle="modal" data-target="#contactModal">Contáctanos</a></li>
        <li><a class="text-muted" href="/catering/login">Acceder a restaurante</a></li>
      </ul>
    </div>

</footer>
<!-- modal -->
<div class="contact-us-modal modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="desktop-modal-container modal-dialog" role="document">
    <div class="modal-content">
      <div class="contact-us-modal__body modal-body flex-column-reverse">
            <a class="close" data-dismiss="modal" aria-label="Close" role="button" href="#">
                <i aria-hidden="true" class="fas fa-times"></i>
            </a>
            <div class="row flex-column-reverse flex-md-row">
            <div class="col-12 col-md-7">
                    <div class="contact-us-modal__form ">
                        <div class="alert alert-danger d-none contact-form-error">Por favor completa todos los campos</div>
                        <div class="alert alert-success d-none contact-form-success">Solicitud recibida, Gracias por contactarnos.</div>
                        <div class="simple_form new_contact_us_form" id="new_contact_us_form">
                            <p class="contact-us-modal__form__description">Completa este formulario y estaremos en contacto.</p>

                            <div class="contact-us-modal__form__user">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input string required contact_us_form_name">
                                          <label class="string required" for="contact_us_form_name">Nombre</label>
                                          <input class="form-control" placeholder="" maxlength="50" size="50" type="text" value="" name="name" id="contact_us_form_name">
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="input email required contact_us_form_email">
                                          <label class="email required" for="contact_us_form_email">Correo</label>
                                          <input class="form-control" placeholder="ejemplo@gmail.com" type="email" name="email" id="contact_us_form_email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="input text required contact_us_form_comments">
                              <label class="text required" for="contact_us_form_comments">Mensaje</label>
                              <textarea rows="8" class="form-control" placeholder="Tu mensaje aqui" name="comment" id="contact_us_form_comments"></textarea></div>

                            <div class="contact-us-modal__submission">
                                <span class="reviewed"></span>
                                <input type="submit" value="Enviar Mensaje" class="button btn btn-primary contact-us-modal__submit">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-12 col-md-5">
                    <div class="contact-us-modal__information">
                        <div class="contact-us-modal__header">
                            <h1 class="contact-us-modal__title">
                                Contáctanos
                            </h1>
                        </div>
                        <div class="contact-us-modal__contact-methods">
                            <div class="contact-us-modal__method">
                                <!-- <div class="contact-us-modal__icon">
                                    <i class="fas fa-phone fa-sm"></i>
                                </div> -->

                                <!-- <div class="contact-us-modal__details">
                                    <p id="contact-us-modal-phone-label">Llámanos</p>
                                    <a data-contact-method="call" aria-labelledby="contact-us-modal-phone-label" href="tel:8297615013">(829) 761-5013</a>
                                </div> -->
                            </div>
                            <div class="contact-us-modal__method">
                                <!-- <div class="contact-us-modal__icon">
                                    <i class="fas fa-sm fa-mobile"></i>
                                </div>
                                <div class="contact-us-modal__details">
                                    <p id="contact-us-modal-sms-label">WhatsApp</p>
                                    <a data-contact-method="text" aria-labelledby="contact-us-modal-sms-label" href="whatsapp://send?text=Elicsis&phone=+18297615013">(829) 761-5013</a>
                                </div> -->
                            </div>

                            <div class="contact-us-modal__method">
                                <div class="contact-us-modal__icon">
                                    <i class="fas fa-sm fa-envelope"></i>
                                </div>
                                <div class="contact-us-modal__details">
                                    <p id="contact-us-modal-email-label">Correo</p>
                                    <a data-contact-method="email" aria-labelledby="contact-us-modal-email-label" href="mailto:soporte@elicsis.com">soporte@elicsis.com</a>
                                </div>
                            </div>
                        </div>
                        <div class="contact-us-modal__hours">
                            <div class="contact-us-modal__icon">
                                <i class="far fa-sm fa-clock"></i>
                            </div>
                            <span>Estamos Disponible via Chat de lunes a viernes 9AM a 10PM.</span>
                        </div>
                    </div>

                </div>
            </div>
      </div>
    </div>
  </div>
</div>
<!-- modal end -->
<script src="/assets/js/vendor.js"></script>
<script src="/assets/js/guest.js<?php echo "?v=" . md5(time());?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_key;?>&libraries=places&region=do&city=sdq&radius=50000&callback=initAutocomplete" async defer></script>
</body>
</html>