<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="/assets/css/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/vendor.css">
    <link rel="stylesheet" href="/assets/css/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="/assets/css/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/assets/css/fonts/flag-icon-css/flag-icon.min.css">
    <link rel="stylesheet" href="/assets/css/portal.css">
    <title>Portal Administrativo</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="/admin/dashboard"><img src="/assets/image/Aplicaciones logo elicsis-07.png" alt="" srcset="" style="width:100px;"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="/admin/dashboard">PANEL DE CONTROL</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menú
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link <?php echo ($page_name == 'dashboard') ? 'active': '';?>" href="/admin/dashboard">
                                    <i class="fa fa-fw fa-home"></i>
                                    Panel de Control
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-restaurant" aria-controls="submenu-restaurant"><i class="fa fas fa-money-bill-alt"></i>Restaurantes</a>
                                <div id="submenu-restaurant" class="collapse submenu <?php echo ($page_name == 'restaurant' || $page_name == 'menu') ? ' show': '';?>" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                        <a class="nav-link <?php echo ($page_name == 'restaurant') ? 'active': '';?>" href="/admin/restaurants/0">
                                            <i class="fa far fa-address-book"></i>
                                            Restaurantes
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'menu') ? 'active': '';?>" href="/admin/menu/0">
                                            <i class="fas fa-file-alt"></i>
                                            Menú
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5"><i class="fa fas fa-money-bill-alt"></i>Ventas</a>
                                <div id="submenu-5" class="collapse submenu <?php echo ($page_name == 'order' || $page_name == 'payment') ? ' show': '';?>" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                        <a class="nav-link <?php echo ($page_name == 'order') ? 'active': '';?>" href="/admin/orders/0">
                                            <i class="fa far fa-address-book"></i>
                                            Pedidos
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'payment') ? 'active': '';?>" href="/admin/payments/0">
                                            <i class="fa fas fa-money-bill-alt"></i>
                                            Pagos
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="fa fas fa-glass-martini"></i>Ajustes/Restaurantes</a>
                                <div id="submenu-2" class="collapse submenu <?php echo ($page_name == 'catering_menu' || $page_name == 'cuisine' || $page_name == 'preference' || $page_name == 'menu_propertys') ? ' show': '';?>" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                        <a class="nav-link <?php echo ($page_name == 'catering_menu') ? 'active': '';?>" href="/admin/catering_menu">
                                            <i class="fa fas fa-trophy"></i>
                                            Menú de Catering
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'cuisine') ? 'active': '';?>" href="/admin/cuisines">
                                            <i class="fa fas fa-clipboard-list"></i>
                                            Gastronomía
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'preference') ? 'active': '';?>" href="/admin/preferences">
                                            <i class="fa fas fa-clipboard-list"></i>
                                            Preferencias
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'menu_propertys') ? 'active': '';?>" href="/admin/menu_propertys">
                                            <i class="fa fas fa-clipboard-list"></i>
                                            Propiedades de Menú
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fab fa-wpforms"></i>Formularios</a>
                                <div id="submenu-3" class="collapse submenu <?php echo ($page_name == 'catering' || $page_name == 'contacts' || $page_name == 'suggests') ? ' show': '';?>" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                        <a class="nav-link <?php echo ($page_name == 'catering') ? 'active': '';?>" href="/admin/catering/new/0">
                                            <i class="fa fas fa-glass-martini"></i>
                                            Solicitud/Afiliación
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'contacts') ? 'active': '';?>" href="/admin/contacts/0">
                                            <i class="fab fa-wpforms"></i>
                                            Formularios de Contacto
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'suggests') ? 'active': '';?>" href="/admin/suggests">
                                            <i class="fab fa-wpforms"></i>
                                            Sugerencias
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4"><i class="fa fas fa-cogs"></i>Configuración</a>
                                <div id="submenu-4" class="collapse submenu <?php echo ($page_name == 'setting' || $page_name == 'email') ? ' show': '';?>" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                        <a class="nav-link <?php echo ($page_name == 'setting') ? 'active': '';?>" href="/admin/settings">
                                            <i class="fa fas fa-cogs"></i>
                                        Ajustes
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'email') ? 'active': '';?>" href="/admin/emails">
                                            <i class="fa fas fa-comment-alt"></i>
                                        Plantillas/Correo
                                        </a>
                                        <a class="nav-link <?php echo ($page_name == 'sms') ? 'active': '';?>" href="/admin/sms">
                                            <i class="fa fas fa-comment-alt"></i>
                                        SMS
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-6" aria-controls="submenu-6"><i class="fab fa-wpforms"></i>Reportes</a>
                                <div id="submenu-6" class="collapse submenu <?php echo ($page_name == 'report') ? ' show': '';?>" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                        <a class="nav-link " href="/admin/reports/order">
                                            <i class="fa fas fa-glass-martini"></i>
                                            Ventas por Pedidos
                                        </a>
                                        <a class="nav-link " href="/admin/reports/item">
                                            <i class="fab fa-wpforms"></i>
                                            Ventas por Productos
                                        </a>
                                        <a class="nav-link " href="/admin/reports/earning">
                                            <i class="fab fa-wpforms"></i>
                                            Informe de Ganancias
                                        </a>
                                        <a class="nav-link " href="/admin/reports/metric">
                                            <i class="fab fa-wpforms"></i>
                                            Informe de Métricas
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">

                                <a class="nav-link <?php echo ($page_name == 'users') ? 'active': '';?>" href="/admin/users/0">
                                    <i class="fa fas fa-user"></i>
                                Usuarios
                                </a>

                                <a class="nav-link <?php echo ($page_name == 'refer_log') ? 'active': '';?>" href="/admin/refer/0">
                                    <i class="fa far fas fa-chart-line"></i>
                                Referencias
                                </a>
                                <a class="nav-link <?php echo ($page_name == 'profile') ? 'active': '';?>" href="/admin/profile">
                                    <i class="fa fas fa-user"></i>
                                Perfil
                                </a>
                                <a class="nav-link" href="/admin/logout">
                                    <i class="fa fas fa-arrow-circle-left"></i>
                                Cerrar Sesión
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">