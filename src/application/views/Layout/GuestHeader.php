<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en" >
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <title>Elicsis</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
    <link rel="manifest" href="/assets/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/ms-icon-144x144.png">

    <script src="https://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
    <script src="https://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
    <!--  CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.min.css"/>
    <link href="/assets/css/main.css<?php echo "?v=" . md5(time());?>" rel="stylesheet" >
    <script src="/assets/js/azhul.js<?php echo "?v=" . md5(time());?>"></script>
  </head>
  <body class="page">

  <nav id="navbarMain" class="global-nav navbar navbar-expand-lg navbar-dark fixed-top">

<a class="navbar-brand" href="/">
    <img src="/assets/image/Aplicaciones logo elicsis-07.png" alt="" width="100">
</a>
<button class="navbar-toggler my-2" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
  <span>&#9776;</span>
</button>
<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
  <ul class="navbar-nav text-right">
    <li class="nav-item ">
      <a class="nav-link " href="/about" id="navbarDropdown">
      Nosotros
      </a>
    </li>

    <li class="dropdown-divider"></li>
    <li class="nav-item">
      <a class="nav-link " href="/enterprise">Empresas</a>
    </li>
    <li class="dropdown-divider"></li>
    <li class="nav-item">
      <a class="nav-link " href="/become">Vender en Elicsis</a>
    </li>
    <li class="dropdown-divider"></li>
    <li class="nav-item">
    <?php
      if ($loggedin) {
        echo '<a class="nav-link" href="/member/dashboard">Mi Cuenta</a>';
      } else {
        echo '<a class="nav-link" href="/member/login">Iniciar Sesión</a>';
      }
    ?>
    </li>
    <li class="dropdown-divider"></li>
    <li class="nav-item">
      <a class="nav-link " href="#" data-toggle="modal" data-target="#contactModal">Contáctanos</a>
    </li>
  </ul>
</div>
</nav>