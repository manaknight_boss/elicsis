<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Solicitudes de Afiliación</h2>
<div>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Solicitudes</h5>
            <div class="card-body">
    <div class="table-responsive">
  <table class="table  table-hover table-condensed">
    <thead>
    	<th class="no-bt">Nombres</th>
		<th class="no-bt">Apellidos</th>
		<th class="no-bt">Negocio/Empresa</th>
		<th class="no-bt">Correo</th>
		<th class="no-bt">Típo</th>
		<th class="no-bt">Fecha</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->first_name . '</td>';
		echo '<td>' . $data->last_name . '</td>';
		echo '<td>' . $data->business_name . '<br/><br/>URL:<br/>' . $data->url . '<br/><br/>Comentario:<br/>' . $data->comment . '</td>';
		echo '<td>' . $data->email . '<br/><br/>Teléfono:<br/>' . $data->phone . '<br/><br/>Codigo Postal:<br/>' . $data->zip . '</td>';
		echo '<td>' . $type_mapping[$data->type] . '</td>';
		echo '<td>' . date('Y-m-d', strtotime($data->created_at)) . '</td>';
        echo '</tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
    </div>
  </div>
  </div>
  </div>
</div>