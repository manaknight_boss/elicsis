<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Ventas por Productos</h2>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Productos Vendidos </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Panel de control</a></li>
						<li class="breadcrumb-item active">Reporte/Productos</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<br>
<div class="clear"></div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Productos Pedidos</h5>
            <div class="card-body">
                <?= form_open() ?>
                    <div class="form-group">
                        <label for='name'>Fecha de Inicio</label>
                        <input type='date' class='form-control' id='start_date' name='start_date' value=''/>
                    </div>
                    <div class="form-group">
                        <label for='name'>Fecha Final</label>
                        <input type='date' class='form-control' id='end_date' name='end_date' value=''/>
                    </div>
                    <div class="form-group">
                        <label for='name'>ID Restaurante</label>
                        <input type='number' class='form-control' id='restaurant_id' name='restaurant_id' value=''/>
                    </div>
                    <div class="form-group">
							<label for='status'>Estado </label>
							<select name='status' class='form-control'>
							<?php
							foreach ($mapping as $key => $value) {
								echo "<option value='{$key}'> {$value} </option>";
							}
							?>
							</select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Crear reporte">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>