<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Cupones</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/coupon/add">Crear Cupón</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="table-responsive">
    <table class="table table-striped table-hover table-condensed">
        <thead>
        <th>Codigo de Cupón</th>
        <th>Descuento</th>
        <th>Plan</th>
        <th>Correo del cliente</th>
        <th>límite</th>
        <th>Usado</th>
        <th>Estado</th>
        <th>Fecha Expira</th>
        <th>Acción</th>
        </thead>
        <tbody>
            <?php foreach ($list as $data) { ?>
                <?php
                $discount_type = ($data->discount_type === 'percent') ? '%' : '';
                echo '<tr>';
                echo '<td>' . $data->coupon_code . '</td>';
                echo '<td>Flat ' . $data->coupon_amount . $discount_type . ' Off</td>';
                echo (isset($data->plan) && $data->plan != "0") ? '<td>' . $plans_list[$data->plan] . '</td>' : '<td>All</td>';
                echo '<td>' . $data->customer_email . '</td>';
                echo ($data->usage_limit == "0" OR $data->usage_limit == "") ? '<td>&#8734</td>' : '<td>' . $data->usage_limit . '</td>';
                echo '<td>' . $data->usage_total . '</td>';
                echo ($data->status === "1") ? '<td><span class="label label-success">Active</span></td>' : '<td><span class="label label-danger">Inactive</span></td>';
                echo '<td>' . $data->expiry_date . '</td>';
                echo '<td><a class="btn btn-default btn-primary" target="__blank" href="/admin/coupon/edit/' . $data->id . '">Editar</a>';
                echo '</td></tr>';
                ?>
            <?php } ?>
        </tbody>
    </table>
</div>