<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Contactos</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Contactos</h5>
            <div class="card-body">
    <div class="table-responsive">
  <table class="table  table-hover table-condensed">
    <thead>
    	<th class="no-bt">Nombre</th>
		<th class="no-bt">Correo</th>
		<th class="no-bt">Comentarios</th>
		<th class="no-bt">Fecha</th>
		<!-- <th class="no-bt">Action</th> -->
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->name . '</td>';
		echo '<td>' . $data->email . '</td>';
		echo '<td>' . $data->comment . '</td>';
		echo '<td>' . date('Y-m-d', strtotime($data->created_at)) . '</td>';

        // echo '<td>';
        // echo ' <a class="btn btn-primary" target="__blank" href="/admin/contact/view/' . $data->id . '">View</a>';
        // echo '</td>';
        echo '</tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
    </div>
  </div>
  </div>
  </div>
</div>