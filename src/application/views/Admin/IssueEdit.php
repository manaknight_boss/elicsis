<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Reclamación </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Panel de control</a></li>
						<li class="breadcrumb-item active" aria-current="page">Reclamación</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
        </div>
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Reclamación</h5>
                <div class="card-body">
				<?= form_open() ?>
				<div class="form-group">
					<label class='col-form-label' for='created_at'>ID Usuario </label>
						<input type='number' class='form-control' id='user_id' readonly name='user_id' value='<?php echo set_value('user_id', $model->user_id); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='created_at'>ID Restaurante </label>
						<input type='number' class='form-control' id='restaurant_id' readonly name='restaurant_id' value='<?php echo set_value('restaurant_id', $model->restaurant_id); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='created_at'>ID Pedido </label>
						<input type='number' class='form-control' id='order_id' readonly name='order_id' value='<?php echo set_value('order_id', $model->order_id); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='notes'>Notas </label>
						<textarea id='notes' name='notes' class='form-control' readonly rows='10'><?php echo set_value('notes', $model->notes); ?></textarea>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='resolution'>Resolución </label>
						<textarea id='resolution' name='resolution' class='form-control' rows='10'><?php echo set_value('resolution', $model->resolution); ?></textarea>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='status'>Estado </label>
						<select name='status' class='form-control'>
						<option value='0' <?php echo ($model->status == 0) ? 'selected' : '';?>> Abierta </option>
						<option value='1' <?php echo ($model->status == 1) ? 'selected' : '';?>> Resuelta </option>
						</select>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='created_at'>Ingresada el: </label>
						<input type='text' class='form-control' readonly id='created_at' name='created_at' value='<?php echo set_value('created_at', $model->created_at); ?>'/>
				</div>


				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
                </form>
            </div>
        </div>
        </div>