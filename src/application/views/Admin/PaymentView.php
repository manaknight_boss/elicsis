<?php defined('BASEPATH') OR exit('No direct script access allowed');
function get_time ($hour, $minute)
{
    if ($minute == 0) {
        $minute = '00';
    }

    $minute_format = ":{$minute}";

    if ($hour == 24 || $hour == 0) {
        return '12' . $minute_format . ' AM';
    }
    if ($hour == 12) {
        return '12' . $minute_format . ' PM';
    }
    if ($hour < 12) {
        return $hour . $minute_format . ' AM';
    }

    if ($hour > 12) {
        return ($hour % 12) . $minute_format . ' PM';
    }
}
?>
<h2>Resumen de Pago</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
		<h5 class="card-header">Detalles del Pago
			<?php
			if ($model->status == 0) {
				echo '<a class="btn btn-success" href="/admin/payment/paid/' . $model->id . '"> Pagar</a>';
			}
			?>
		</h5>
            <div class="card-body">
				<h6>Pago #: &nbsp; <?php echo $model->id;?></h6>
				<h6>Fecha/Corte: &nbsp; <?php echo $model->payment_date;?></h6>
				<h6>Numero de Pedidos: &nbsp; <?php echo $model->num_orders;?></h6>
				<h6>Estado: &nbsp; <?php echo $mapping[$model->status];?></h6>
				<h6>Total Comida: &nbsp; <?php echo number_format($model->amount, 2) . ' DOP';?></h6>
				<h6>Total Delivery: &nbsp; <?php echo number_format($model->delivery, 2) . ' DOP';?></h6>
				<h6>Total Propinas: &nbsp; <?php echo number_format($model->tips, 2) . ' DOP';?></h6>
				<h6>Total Impuestos: &nbsp; <?php echo number_format($model->tax, 2) . ' DOP';?></h6>
				<h6>Total Bruto: &nbsp; <?php echo number_format($model->total, 2) . ' DOP';?></h6>
				<h6>Comisíon: &nbsp; <?php echo number_format($model->commission, 2) . ' DOP';?></h6>
				<h6>Ajustes: &nbsp; <?php echo number_format($model->adjustment, 2) . ' DOP';?></h6>
				<h6>Total Pedidos No pagados: &nbsp; <?php echo number_format($model->not_paid, 2) . ' DOP';?></h6>
				<h6>Total Pedidos/Reclamaciones: &nbsp; <?php echo number_format($model->problem_order, 2) . ' DOP';?></h6>
				<h6>Pago Final: &nbsp; <?php echo number_format(($model->total - $model->commission + $model->adjustment), 2) . ' DOP';?></h6>
			</div>
		</div>
    </div>

	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
		<h5 class="card-header"> Editar Detalles</h5>
            <div class="card-body">
			<div class="row">
				<?php if (validation_errors()) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?= form_open() ?>
			<div class="form-group">
				<label class='col-form-label' for='adjustment'>Ajustes </label>
				<input type='text' class='form-control' id='adjustment'  data-inputmask="'alias': 'decimal', 'radixpoint': '.'" name='adjustment' value='<?php echo $model->adjustment;?>'/>
			</div>
			<div class="form-group">
				<label for='Notes'>Notas </label>
				<textarea id='notes' name='notes' class='form-control' rows='10'><?php echo $model->notes;?></textarea>
			</div>
			<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
			</form>
			</div>
		</div>
    </div>
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
		<h5 class="card-header">Períodos Anteriores</h5>
            <div class="card-body">
			<p>Aquí verás agendado los ultimos 5 períodos pagados.</p>
	<div class="table-responsive">
		<table class="table table-hover table-condensed">
			<thead>
				<th class="no-bt">Pago #</th>
				<th class="no-bt">Período</th>
				<th class="no-bt">Comida RD$</th>
				<th class="no-bt">Delivery</th>
				<th class="no-bt">Propinas</th>
				<th class="no-bt">Total Bruto</th>
				<th class="no-bt">Total Pedidos No pagados</th>
				<th class="no-bt">Total Pedidos/Reclamaciones</th>
				<th class="no-bt">Comisión</th>
				<th class="no-bt">Ajustes</th>
				<th class="no-bt">Pago Final</th>
				<th class="no-bt">Accíon</th>
			</thead>
			<tbody>
			<?php foreach ($payment_list as $data) { ?>
				<?php
				echo '<tr>';
				echo '<td>' . $data->id  . '</td>';
				echo '<td>' . $data->payment_date  . '</td>';
				echo '<td>' . $data->amount  . '</td>';
				echo '<td>' . $data->delivery  . '</td>';
				echo '<td>' . $data->tips  . '</td>';
				echo '<td>' . $data->total  . '</td>';
				echo '<td>' . $data->not_paid  . '</td>';
				echo '<td>' . $data->problem_order  . '</td>';
				echo '<td>' . $data->commission  . '</td>';
				echo '<td>' . $data->adjustment  . '</td>';
				echo '<td>' . number_format($data->total - $data->commission + $data->adjustment, 2)  . '</td>';
				echo '<td>';
				echo '<a class="btn btn-sm btn-primary my-1" href="/admin/payments/view/' . $data->id . '">Ver</a><br/>';
				echo '</td></tr>';
				?>
			<?php } ?>
			</tbody>
		</table>
	</div>
	</div>
	</div>
	</div>

  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
		<h5 class="card-header">Pedidos Realizados entre <?php echo $previous_payout_day . ' Y ' . $payout_day;?></h5>
            <div class="card-body">
			<div class="table-responsive">
            <table class="table  table-hover table-condensed">
					<thead>
						<th class="no-bt"># Pedido</th>
						<th class="no-bt">Estado</th>
						<th class="no-bt">Tipo</th>
						<th class="no-bt">Fecha/Hora</th>
						<th class="no-bt">Ubicación</th>
						<th class="no-bt">Total</th>
						<th class="no-bt">Acción</th>
					</thead>
					<tbody>
					<?php foreach ($list as $data) { ?>
						<?php
						echo '<tr>';
						echo '<td>' . $data->id  . '</td>';
						echo '<td>' . $mapping[$data->status] . '</td>';
						echo '<td>' . $delivery_mapping[$data->is_delivery] . '</td>';
						echo '<td>' . $data->event_date_at . '<br/>' . get_time($data->event_hour, $data->event_minute) . '</td>';
						echo '<td>' . $data->address . '<br/>' . $data->city . ', ' . $data->state . '<br/>' . $data->zip . '<br/>' . $data->phone . '</td>';
						echo '<td>' . $data->total  . ' DOP</td>';
                        echo '<td>';
                        echo '<a class="btn btn-sm btn-primary my-1" href="/admin/orders/view/' . $data->id . '">Ver</a><br/>';
						echo '</td></tr>';
						?>
					<?php } ?>
					</tbody>
				</table>
			</div>
  </div>
  </div>
  </div>
</div>