<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Panel de control</h2>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
            <h4>Número de Pedidos<br/> <?php echo $num_order;?> </h4>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                <h4>Número de Usuarios<br/> <?php echo $num_user;?> </h4>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                <h4>Número de Restaurantes<br/> <?php echo $num_catering;?> </h4>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                <h4>Ventas a la fecha<br/> <?php echo $total_sale;?> </h4>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                <h4>Entregas a la fecha<br/> <?php echo $total_delivered;?> </h4>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                <h4>Ventas Pendientes<br/> <?php echo $total_pending;?> </h4>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                <h4>Cancelaciones<br/> <?php echo $total_cancel;?> </h4>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                <h4>Reembolsos a la fecha<br/> <?php echo $total_refund;?> </h4>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                <h4>Reclamaciones a la fecha<br/> <?php echo $total_issue;?> </h4>
        </div>
</div>
<br/>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Reclamaciones</h5>
            <div class="card-body">
            <div class="table-responsive">
  <table class="table  table-hover table-condensed">
    <thead>
    	<th class="no-bt">Notas</th>
		<th class="no-bt">Resolución</th>
		<th class="no-bt">Estado</th>
		<th class="no-bt">Fecha creada</th>
		<th class="no-bt">Acción</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->notes . '</td>';
		echo '<td>' . $data->resolution . '</td>';
		echo '<td>' . $mapping[$data->status] . '</td>';
		echo '<td>' . $data->created_at . '</td>';

        echo '<td>';
        echo '<a class="btn btn-primary" target="__blank" href="/admin/issue/' . $data->id . '">Editar</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
            </div>
  </div>
  </div>
  </div>
</div>