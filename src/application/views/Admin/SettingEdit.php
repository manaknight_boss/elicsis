<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>


		<div class="col-md-12">
			<div class="page-header">
				<h1>Editar Ajuste</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for="text">Clave </label>
					<input type="text" readonly="true" class="form-control" id="name" name="name" value="<?php echo set_value('name', $model->key); ?>"/>
				</div>
				<div class="form-group">
					<label for="text">Valor</label>
					<textarea id="value" name="value" class="form-control" rows="15"><?php echo set_value('value', $model->value); ?></textarea>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default btn-primary" value="Enviar">
				</div>
			</form>
		</div>
	</div><!-- .row -->