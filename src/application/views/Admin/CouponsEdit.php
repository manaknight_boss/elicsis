<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <?php echo $error; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success" role="success">
                    <?php echo $success; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-md-12">
        <div class="page-header">
            <h1>Edit Coupon</h1>
        </div>
        <?= form_open() ?>
        <div class="form-group">
            <label for='coupon_code'>Código de Cupón </label>
            <input type='text' class='form-control' id='coupon_code' name='coupon_code' value='<?php echo set_value('coupon_code', $model->coupon_code); ?>'/>
        </div>
        <div class="form-group">
            <label for='discount_type'>Tipo de descuento </label>
            <select name='discount_type' class='form-control'>
                <option value='percent' <?php echo ($model->discount_type == 'percent') ? 'selected' : ''; ?>> Porcentaje </option>
                <option value='fixed' <?php echo ($model->discount_type == 'fixed') ? 'selected' : ''; ?>> Fíjo </option>
            </select>
        </div>
        <div class="form-group">
            <label for='coupon_amount'>Monto de Cupón </label>
            <input type='number' class='form-control' id='coupon_amount' name='coupon_amount' value='<?php echo set_value('coupon_amount', $model->coupon_amount); ?>'/>
        </div>
        <div class="form-group">
            <label for='plan'>Plan </label>
            <select name='plan' class='form-control'>
                <option value=''> Elije el Plan </option>
                <?php
                foreach ($plans_list as $value => $label) {
                    $select = ($model->plan == $value) ? 'selected' : '';
                    echo "<option $select value='$value'> $label </option>";
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for='customer_email'>Correo del cliente </label>
            <input type='email' class='form-control' id='customer_email' name='customer_email' value='<?php echo set_value('customer_email', $model->customer_email); ?>'/>
        </div>
        <div class="form-group">
            <label for='usage_limit'>Límite de uso/Mes</label>
            <input type='number' class='form-control' id='usage_limit' name='usage_limit' value='<?php echo set_value('usage_limit', $model->usage_limit); ?>'/>
        </div>
        <div class="form-group">
            <label for='status'>Estado </label>
            <select name='status' class='form-control'>
                <option value='1' <?php echo ($model->status == '1') ? 'selected' : ''; ?>> Actívo </option>
                <option value='0' <?php echo ($model->status == '0') ? 'selected' : ''; ?>> Inactívo </option>
            </select>
        </div>
        <div class="form-group">
            <label for='expiry_date'>Fecha Expira </label>
            <input type='date' class='form-control' id='expiry_date' name='expiry_date' value='<?php echo set_value('expiry_date', $model->expiry_date); ?>'/>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-default btn-primary" value="Enviar">
            <a href="/admin/coupon/" class="btn btn-default btn-primary">Cancelar</a>
        </div>
        </form>
    </div>
</div>