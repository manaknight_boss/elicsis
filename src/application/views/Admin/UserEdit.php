<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Usuarios </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Panel de control</a></li>
						<li class="breadcrumb-item"><a href="/admin/users/0" class="breadcrumb-link">Usuarios</a></li>
						<li class="breadcrumb-item active" aria-current="page">Editar Usuarios</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
</div>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	<div class="card">
		<h5 class="card-header">Editar Userio</h5>
            <div class="card-body">
			<?= form_open() ?>
				<div class="form-group">
					<label for="email">Correo </label>
					<input type="email" class="form-control" id="email" name="email" placeholder="a@gmail.com" value="<?php echo set_value('email', $model->email); ?>"/>
				</div>
				<div class="form-group">
					<label for="password">Contraseña </label>
					<input type="password" class="form-control" id="password" name="password" placeholder="" value=""/>
				</div>
				<div class="form-group">
					<label for="first_name">Nombres </label>
					<input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo set_value('first_name', $model->first_name); ?>"/>
				</div>
				<div class="form-group">
					<label for="last_name">Apellidos </label>
					<input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo set_value('last_name', $model->last_name); ?>"/>
				</div>
				<div class="form-group">
					<label for="rnc">RNC # </label>
					<input type="text" class="form-control" id="rnc" name="last_name" value="<?php echo set_value('last_name', $model->last_name); ?>"/>
				</div>
				<div class="form-group">
					<label for="business_name">Nombre del Negocio </label>
					<input type="text" class="form-control" id="business_name" name="last_name" value="<?php echo set_value('last_name', $model->last_name); ?>"/>
				</div>
				<div class="form-group">
					<label for="phone">Teléfono </label>
					<input type="number" class="form-control" id="phone" name="phone" placeholder="" value="<?php echo set_value('phone', $model->phone); ?>"/>
				</div>
				<div class="form-group">
					<label for="Status">Estado </label>
					<select name="status" class="form-control">
						<option value="0" <?php echo ($model->status == 0) ? 'selected' : '';?>> Inactívo </option>
						<option value="1" <?php echo ($model->status == 1) ? 'selected' : '';?>> Actívo </option>
						<option value="2" <?php echo ($model->status == 2) ? 'selected' : '';?>> Suspendida </option>
					</select>
				</div>
				<div class="form-group">
					<label for="Role">Papel </label>
					<select name="role" class="form-control">
						<?php foreach ($mapping['role'] as $key => $value) {
							echo '<option value="' . $key . '"' . ( ($model->role_id == $key) ? 'selected' : '' ). '> ' . $value . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="profile_type">Tipe de Perfil </label>
					<select name="profile_type" class="form-control">
						<?php foreach ($mapping['profile_type'] as $key => $value) {
							echo '<option value="' . $key . '"' . ( ($model->profile_type == $key) ? 'selected' : '' ). '> ' . $value . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
			</form>
		</div>
	</div><!-- .row -->
</div>
</div>
</div>