<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Restaurantes </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Panel de control</a></li>
						<li class="breadcrumb-item"><a href="/admin/restaurants/0" class="breadcrumb-link">Restaurantes</a></li>
						<li class="breadcrumb-item active" aria-current="page"> Añadir Restaurante</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
        </div>
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Añadir Restaurante</h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label class='col-form-label' for='title'>Nombre </label>
					<input type='text' class='form-control' id='title' name='title' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='description'>Descripción </label>
					<textarea id='description' name='description' class='form-control' rows='5'></textarea>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='about'>Detalles </label>
						<textarea id='about' name='about' class='form-control' rows='5'></textarea>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='delivery_hour'>Horario de Entregas </label><br/>
					<label class="custom-control custom-checkbox">
							<input type="checkbox"  value="1" name="u" class="custom-control-input"/>
							<span class="custom-control-label">Domingo</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="u_open" name='u_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="u_close" name='u_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox"  value="1" name="m" class="custom-control-input"/>
							<span class="custom-control-label">Lunes</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="m_open" name='m_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="m_close" name='m_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox"  value="1" name="t" class="custom-control-input"/>
							<span class="custom-control-label">Martes</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="t_open" name='t_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="t_close" name='t_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox"  value="1" name="w" class="custom-control-input"/>
							<span class="custom-control-label">Miercoles</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="w_open" name='w_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="w_close" name='w_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox"  value="1" name="r" class="custom-control-input"/>
							<span class="custom-control-label">Jueves</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="r_open" name='r_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="r_close" name='r_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox"  value="1" name="f" class="custom-control-input"/>
							<span class="custom-control-label">Viernes</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="f_open" name='f_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="f_close" name='f_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox"  value="1" name="s" class="custom-control-input"/>
							<span class="custom-control-label">Sabado</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="s_open" name='s_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="s_close" name='s_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}

								echo "<option value='{$i}'> {$v} </option>";
								if ($i != 24) {

									echo "<option value='" . ($i + 0.5) . "' > {$v2} </option>";
								}
							}?>
							</select>
					</label>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='delivery_fee'>Costo/Delivery </label>
						<input type='text' class='form-control' id='delivery_fee'  data-inputmask="'alias': 'decimal', 'radixpoint': '.'" name='delivery_fee' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='food_minimum'>Consumo Mínimo/Entregas </label>
						<input type='number' class='form-control' id='food_minimum' name='food_minimum' value='' onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='type'>Tipo de Restaurante </label>
						<select name='type' class='form-control'>
						<?php foreach ($type as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}
						?>
						</select>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='cuisines_id'>Gastronomias </label>
						<select name='cuisines_id' class='form-control'>
						<?php foreach ($cuisines as $key => $cuisine) {
							echo "<option value='{$cuisine->id}'> {$cuisine->name} </option>";
						}
						?>
						</select>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='price_range'>Rango de Precios </label>
						<select name='price_range' class='form-control'>
						<?php $price = $setting['price'];
						$price_list = json_decode($price, TRUE);
						foreach ($price_list as $key => $price) {
							echo "<option value='{$price}'> {$price} </option>";
						}
						?>
						</select>
				</div>

				<div class="form-group">
					<label class='col-form-label' for='address'>Dirección </label>
						<input type='text' class='form-control' id='google_address' name='google_address' value='' placeholder=""/>
						<input type='hidden' class='form-control' id="address" name='address' value=''/>
						<small>Actualiza la dirección y el sistema autocompletara la Provincia.</small>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='city'>Ciudad </label>
						<input type='text' class='form-control' id='google_city' name='google_city' value=''/>
						<input type='hidden' class='form-control' id='city' name='city' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='state'>Provincia </label>
						<input type='text' class='form-control' id='google_state' name='google_state' value=''/>
						<input type='hidden' class='form-control' id='state' name='state' value=''/>
				</div>

				<div class="form-group">
					<label class='col-form-label' for='zip'>Código Postal </label>
						<input type='text' class='form-control' id='google_zip' name='google_zip' value=''/>
						<input type='hidden' class='form-control' id='zip' name='zip' value=''/>
						<input type='hidden' class='form-control' id='lat' name='lat' value=''/>
						<input type='hidden' class='form-control' id='long' name='long' value=''/>
				</div>

				<div class="form-group">
					<label class='col-form-label' for='email'>Correo </label>
					<input type='email' class='form-control' id='email' name='email' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='password'>Contraseña </label>
					<input type='password' class='form-control' id='password' name='password' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='started_at'>Abierto desde </label>
						<input type='date' class='form-control' id='started_at' name='started_at' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='serving'>Teléfono </label>
						<input type='number' class='form-control' id='phone' name='phone' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='serving'>No. Personas Max/Orden </label>
						<input type='number' class='form-control' id='serving' name='serving' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='serving'>RNC # </label>
						<input type='text' class='form-control' id='rnc' name='rnc' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='serving'>Notas </label>
					<textarea id='notes' name='notes' class='form-control' rows='5'></textarea>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
                </form>
            </div>
        </div>
		</div>