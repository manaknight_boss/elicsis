<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Pagos por Períodos</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Filtrar Pagos</h5>
            <div class="card-body">
				<?= form_open() ?>
				<div class="row">
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='id'>Pago # </label>
							<input type='number' class='form-control' id='id' name='id' value='<?php echo $id;?>'/>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='restaurant_id'>ID Restaurante </label>
							<input type='number' class='form-control' id='restaurant_id' name='restaurant_id' value='<?php echo $restaurant_id;?>'/>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='status'>Estado </label>
							<select name='status' class='form-control'>
								<option value=''>Todos</option>
							<?php
							foreach ($mapping as $key => $value) {
								echo "<option value='{$key}' " . (($status == $key && $status != '') ? 'selected' : '') . "> {$value} </option>";
							}
							?>
							</select>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12"></div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Enviar">
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Resumen de Pagos</h5>
            <div class="card-body">
                <?php if (strlen($error) > 0) : ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (strlen($success) > 0) : ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success" role="success">
                                <?php echo $success; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <p>Aquí estan agendados los pagos por período por pedidos entregados. Períodos consisten de 7 dias, pagos son ejecutados el dia siguiente a la fecha de corte. *Pago Proyectado es el monto a pagar al restaurente despues de comisión y ajustes. </p>
                <div class="clear"></div>
                <div class="table-responsive">
                <table class="table  table-hover table-condensed">
					<thead>
						<th class="no-bt">Pago #</th>
						<th class="no-bt">Fecha/Corte</th>
						<th class="no-bt">Restaurante</th>
						<th class="no-bt"># de Pedidos</th>
						<th class="no-bt">Retenidos</th>
						<th class="no-bt">Ajustes</th>
						<th class="no-bt">Pago Final</th>
						<th class="no-bt">Estado</th>
						<th class="no-bt">Accíon</th>
					</thead>
					<tbody>
					<?php foreach ($list as $data) { ?>
						<?php
						echo '<tr>';
						echo '<td>' . $data->id  . '</td>';
						echo '<td>' . $data->payment_date  . '</td>';
						echo '<td>' . $data->restaurant_title  . '</td>';
						echo '<td>' . $data->num_orders  . '</td>';
						echo '<td>' . $data->not_paid  . '</td>';
						echo '<td>' . $data->adjustment  . '</td>';
						echo '<td>' . number_format($data->total - $data->commission + $data->adjustment, 2)  . '</td>';
						echo '<td>' . $mapping[$data->status]  . '</td>';
                        echo '<td>';
                        echo '<a class="btn btn-sm btn-primary my-1" href="/admin/payments/view/' . $data->id . '">Ver</a><br/>';
                        if ($data->status == 0) {
                            echo '<a class="btn btn-sm btn-success" href="/admin/payments/paid/' . $data->id . '"> Pagar</a>';
                        }
						echo '</td></tr>';
						?>
					<?php } ?>
					</tbody>
                </table>
                <p class="pagination_custom"><?php echo $links; ?></p>
                </div>
  </div>
  </div>
  </div>
</div>
<script>
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {};
    xhr.open('GET', '/v1/api/payments?restaurant_id=<?php echo $restaurant_id?>');
    xhr.send();
</script>