<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Solicitud de Afiliación </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Panel de control</a></li>
						<li class="breadcrumb-item active" aria-current="page">Solicitud/Afiliación</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
        </div>
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Solicitud</h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label class='col-form-label' for='first_name'>Nombres </label>
						<input type='text' class='form-control' id='first_name' name='first_name' value='<?php echo set_value('first_name', $model->first_name); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='last_name'>Apellidos</label>
						<input type='text' class='form-control' id='last_name' name='last_name' value='<?php echo set_value('last_name', $model->last_name); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='business_name'>Negocio/Empresa </label>
						<input type='text' class='form-control' id='business_name' name='business_name' value='<?php echo set_value('business_name', $model->business_name); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='email'>Correo </label>
						<input type='text' class='form-control' id='email' name='email' value='<?php echo set_value('email', $model->email); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='phone'>Teléfono </label>
						<input type='text' class='form-control' id='phone' name='phone' value='<?php echo set_value('phone', $model->phone); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='comment'>Comentario </label>
						<textarea id='comment' name='comment' class='form-control' rows='10'><?php echo set_value('comment', $model->comment); ?></textarea>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='created_at'>Ingresada el: </label>
						<input type='date' class='form-control' id='created_at' name='created_at' value='<?php echo set_value('created_at', $model->created_at); ?>'/>
				</div>


				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
                </form>
            </div>
        </div>
        </div>