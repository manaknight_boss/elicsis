<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Complementos de Menu</h2>
<div>
    <a class="btn btn-primary" href="/admin/menu/addon/add/<?php echo $model->id;?>">Añadir</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Complementos</h5>
            <div class="card-body">
            <div class="table-responsive">
  <table class="table  table-hover table-condensed">
    <thead>
    	<th class="no-bt">Nombre</th>
		<th class="no-bt">Tipo de Menu</th>
		<th class="no-bt">Precio</th>
		<th class="no-bt">Estado</th>
		<th class="no-bt">Acción</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->name . '</td>';
        echo '<td>' . $menu_property_list[$data->menu_property_id] . '</td>';
		echo '<td>' . $data->price . '</td>';
		echo '<td>' . $status[$data->status] . '</td>';
        echo '<td>';
        echo '<a class="btn btn-primary" target="__blank" href="/admin/menu/addon/edit/' . $data->id . '">Editar</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
            </div>
  </div>
  </div>
  </div>
</div>