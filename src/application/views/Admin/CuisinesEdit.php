<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Gastronomias </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Panel de control</a></li>
						<li class="breadcrumb-item active" aria-current="page">Gastronomias</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
        </div>
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Gastronomias</h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label class='col-form-label' for='name'>Nombre </label>
						<input type='text' class='form-control' id='name' name='name' value='<?php echo set_value('name', $model->name); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='status'>Estado </label>
						<select name='status' class='form-control'>
						<option value='1' <?php echo ($model->status == 1) ? 'selected' : '';?>> Actívo </option>
						<option value='0' <?php echo ($model->status == 0) ? 'selected' : '';?>> Inactívo </option>
						</select>
		</div>


				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
                </form>
            </div>
        </div>
        </div>