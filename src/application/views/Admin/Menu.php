<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Menú</h2>
<div>
    <a class="btn btn-primary" href="/admin/menu/add">Crear Menú</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Filtrar Menú</h5>
            <div class="card-body">
				<?= form_open('', array('method' => 'GET')) ?>
				<div class="row">
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='restaurant_id'>ID Restaurante </label>
							<input type='number' class='form-control' id='restaurant_id' name='restaurant_id' value='<?php echo $restaurant_id;?>'/>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12"></div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Enviar">
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Menú</h5>
            <div class="card-body">
            <div class="table-responsive">
  <table class="table  table-hover table-condensed">
    <thead  class="text-center">
    	<th class="no-bt">Nombre</th>
    	<th class="no-bt">ID Restaurante</th>
		<th class="no-bt">Tipo </th>
		<th class="no-bt">Más Pedido</th>
		<th class="no-bt">Popular</th>
		<th class="no-bt">Precio</th>
		<th class="no-bt">Orden</th>
		<th class="no-bt">Estado</th>
		<th class="no-bt">Acción</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->name . '</td>';
        echo '<td>' . $data->restaurant_title . '(' . $data->restaurant_id . ')' . '</td>';
		echo '<td  class="text-center">' . (($data->catering_menu_id != 0) ? $catering_menu[$data->catering_menu_id] : $data->catering_menu_id) . '</td>';
		echo '<td class="text-center">' . ($data->most_ordered == 1 ? '<i class="fas fa-check"></i>' : '') . '</td>';
		echo '<td class="text-center">' . ($data->popular == 1 ? '<i class="fas fa-check"></i>' : '') . '</td>';
		echo '<td class="text-center">$' . $data->price . '</td>';
		echo '<td class="text-center">' . $data->order . '</td>';
		echo '<td class="text-center">' . $status[$data->status] . '</td>';

        echo '<td>';
        echo '<a class="btn btn-primary btn-sm" target="__blank" href="/admin/menu/edit/' . $data->id . '">Editar</a>';
        if ($data->status == 1) {
            echo ' <a class="btn btn-primary btn-sm" target="__blank" href="/admin/menu/remove/' . $data->id . '">Remover</a>';
        }
        echo ' <a class="btn btn-warning btn-sm" target="__blank" href="/admin/menu/addon/' . $data->id . '">Complementar</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
    </div>
  </div>
  </div>
  </div>
</div>