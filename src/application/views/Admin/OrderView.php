<?php
function get_time ($hour, $minute)
{
    if ($minute == 0) {
        $minute = '00';
    }

    $minute_format = ":{$minute}";

    if ($hour == 24 || $hour == 0) {
        return '12' . $minute_format . ' AM';
    }
    if ($hour == 12) {
        return '12' . $minute_format . ' PM';
    }
    if ($hour < 12) {
        return $hour . $minute_format . ' AM';
    }

    if ($hour > 12) {
        return ($hour % 12) . $minute_format . ' PM';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pedido</title>
    <link href="/assets/css/main.css" rel="stylesheet">
    <style>
        .table thead th {
            vertical-align: middle;
            /* border: none; */
        }

        body {
            margin: 0;
            padding: 0;
        }
        .collapse-mobile {
            display: table-cell;
        }
        .show-mobile {
            display: none;
        }
        @media screen and (max-width: 600px) {
            .collapse-mobile {
                display: none;
            }
            .show-mobile {
                display: block;
                border: none !important;
            }
        }
    </style>
</head>

<body>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <tr>
                        <th>
                            <img style="width:100px;" src="/assets/image/Aplicaciones%20logo%20elicsis-07.png">
                            <br>

                        </th>
                        <th colspan="2" scope="row">
                            Orden# <?php echo $model->id;?>
                            <div>Restaurante: <?php echo $model->restaurant->title;?></div>
                            <div>Tipo: <?php echo $delivery_mapping[$model->is_delivery];?></div>
                            <div>Estado: <?php echo $mapping[$model->status];?></div>
                            <div>Pago: Crédito</div>
                            <!-- <div>Pago: <?php echo $model->payment_method;?></div> -->
                        </th>


                    </tr>

                    <th class="collapse-mobile text-left" scope="col" colspan="2">
                        Ubicacion de Entrega:
                        <div><?php echo $data['address']['name'];?>, <?php echo $model->city;?> <?php echo $model->zip;?></div>
                        <div><?php echo 'Apt/No. ' . $data['address']['suite'];?></div>
                        <div><?php echo $model->address;?></div>
                        <small>Tel: <?php echo $model->phone;?></small><br/>
                        <?php echo (strlen($rnc) > 0) ? "<small>Comprobante fiscal al RNC: {$rnc} </small><br>" : '';?>
                    </th>
                    <th class="collapse-mobile text-right">
                        <div>Fecha: <?php echo $model->event_date_at;?></div>
                        <div>Hora: <?php echo get_time($model->event_hour, $model->event_minute);?></div>
                        <div>Preguntar por: <?php echo $data['address']['person_name'];?></div>
                        <div>Instrucciónes: <small><?php echo $data['address']['instructions'];?></small> </div>
                    </th>

                    <th class="show-mobile text-left" scope="col" colspan="2">
                        Ubicacion de Entrega:
                        <div><?php echo $data['address']['name'];?></div>
                        <div><?php echo 'Apt/No. ' . $data['address']['suite'];?></div>
                        <div><?php echo $model->address;?></div>
                        <small>Tel: <?php echo $model->phone;?></small>
                        <br>
                        <small><?php echo $model->address;?>, <?php echo $model->city;?> <?php echo $model->zip;?></small>
                        <br/>
                        <br/>
                        <div>Fecha: <?php echo $model->event_date_at;?></div>
                        <div>Hora: <?php echo get_time($model->event_hour, $model->event_minute);?></div>
                        <div>Preguntar por: <?php echo $data['address']['person_name'];?></div>
                        <div>Instrucciónes: <small><?php echo $data['address']['instructions'];?></small> </div>
                    </th>
                </tr>

                <tr>
                    <th colspan="2">Articulos:</th>

                    <th colspan="" class="text-right">Montos</th>
                </tr>
            </thead>



            <tbody>
                <tr>
                    <th colspan="2" scope="row">
                        <?php
                        foreach($data['items'] as $item)
                        {
                            echo '<div>' . $item['quantity'] . 'x ' . $item['title'] . '</div>';
                            echo ($item['specialInstruction'] != '') ? ('<small>Instrucciones especiales: ' . $item['specialInstruction'] . '</small>') : '';

                            if (!empty($item['addons']))
                            {
                                echo '<div>' . 'Addons:' . '</div>';
                                foreach($item['addons'] as $addon)
                                {
                                    echo '<div> - ' . $addon['type'] . ' : ' . $addon['name'] . '</div>';
                                }
                            }
                        }
                        ?>
                    </th>
                    <td class="text-right">DOP <?php echo number_format($data['total'], 2);?></td>
                </tr>
                <tr>
                    <th colspan="2" scope="row">Propinas</th>
                    <td class="text-right">DOP <?php echo number_format($data['real_tips'], 2);?></td>
                </tr>
                <tr>
                    <th colspan="2" scope="row" <?php echo ($data['is_delivery'] == '1' ? '' : 'style="display:none;"');?>>Delivery</th>
                    <td class="text-right">DOP <?php echo number_format($data['delivery_fee'], 2);?></td>
                </tr>
                <tr style="border-top:2px solid black;">
                    <th colspan="2" scope="row">Total Exc. Impuesto</th>
                    <td class="text-right" class="text-right">DOP <?php echo number_format($data['real_tips'] + $data['total'] + $data['delivery_fee'], 2);?></td>
                </tr>
                <tr>
                    <th colspan="2" scope="row">ITBIS</th>
                    <td class="text-right">DOP <?php echo number_format($data['tax'], 2);?></td>
                </tr>
                <tr style="border-top:2px solid black;">
                    <th style="font-size:1.5rem" colspan="2" scope="row">Total</th>
                    <td style="font-size:1.5rem" class="text-right font-weight-bold">DOP <?php echo number_format($data['real_tips'] + $data['total'] + $data['delivery_fee'] + $data['tax'], 2);?></td>
                </tr>
                <!--Edit is only availabe if order not Confirmed and Payment Method is Cash, only Delivery Notes and Delivery Address can be Edited-->
                <?php if (strlen($model->notes) > 0) { ?>
                    <tr style="border-top:2px">
                        <td style="font-size:1.5rem" colspan="4" class="text-left font-weight-bold">
                            <div class="form-group">
                                <b>Notas:</b><br/>
                                <p><?php echo $model->notes;?></p>
                            </div>
                        </td>
                    </tr>
                <?php }?>
                <tr style="border-top:2px">
                    <td style="font-size:1.5rem" colspan="2" class="text-left font-weight-bold">
                        <div class="form-group">
                            <a href="/admin/orders/edit/<?php echo $model->id;?>" class="btn btn-primary">Editar</a>
                        </div>
                    </td>
                    <td style="font-size:1.5rem" colspan="2" class="text-right font-weight-bold">
                        <div class="form-group">
                            <a href="/admin/orders/cancel/<?php echo $model->id;?>" class="btn btn-danger">Cancelar</a>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

</body>

</html>