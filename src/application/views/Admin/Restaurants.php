<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Restaurantes</h2>
<div>
    <a class="btn btn-primary" href="/admin/restaurants/add">Crear Restaurante</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Restaurantes</h5>
            <div class="card-body">
  <table class="table  table-hover table-condensed">
    <thead>
    	<th class="no-bt">Nombre</th>
		<th class="no-bt">Gastronomia</th>
		<th class="no-bt">Dirección</th>
		<th class="no-bt">Estado</th>
		<th class="no-bt">Acción</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->title . '</td>';
		echo '<td>' . $cuisines[$data->cuisines_id] . '</td>';
		echo '<td>' . $data->address . '<br/>' . $data->city . '<br/>' . $data->state . '<br/>' . $data->zip . '</td>';
		echo '<td>' . $status[$data->status] . '</td>';
        echo '<td>';
        echo '<a class="btn btn-primary btn-sm" target="__blank" href="/admin/restaurants/edit/' . $data->id . '">Editar</a><br/>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
  </div>
  </div>
  </div>
</div>