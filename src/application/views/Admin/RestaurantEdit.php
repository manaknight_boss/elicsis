<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Restaurantes </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/restaurants/0" class="breadcrumb-link">Restaurantes</a></li>
						<li class="breadcrumb-item active" aria-current="page">Editar Restaurante</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="card">
			<h5 class="card-header">Editar Imagenes de Restaurantes</h5>
			<div class="card-body">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pb-5 text-center">
				<div id="upload_logo_container_wrapper" class="d-none">
					<div id="upload_logo_container">
					</div>
					<button class="btn btn-primary btn-block" id="save_restaurant_logo">Almacenar Logo</button>
					<br/>
				</div>
				<?php
				echo '<div id="preview_logo_container_wrapper">';
				if (strlen($model->logo) > 1) {
					echo '<img src="' . $model->logo . '" class="img-fluid"/>';
				}
				echo '</div>';
				?>
				<div class="custom-file mb-3">
					<input type="file" class="custom-file-input" id="restaurant_logo">
					<label class="custom-file-label" for="customFile">Subir Logo</label>
				</div>
				</div>
				<!-- thumbnail -->
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pb-5 text-center">
				<div id="upload_thumbnail_container_wrapper" class="d-none">
					<div id="upload_thumbnail_container">
					</div>
					<button class="btn btn-primary btn-block" id="save_restaurant_thumbnail">Almacenar Miniatura</button>
					<br/>
				</div>
				<?php
				echo '<div id="preview_thumbnail_container_wrapper">';
				if (strlen($model->image) > 1) {
					echo '<img src="' . $model->image . '" class="img-fluid"/>';
				}
				echo '</div>';
				?>
				<div class="custom-file mb-3">
					<input type="file" class="custom-file-input" id="restaurant_thumbnail">
					<label class="custom-file-label" for="customFile">Subir Miniatura</label>
				</div>
				</div>
				<!-- thumbnail end -->
				<!-- banner -->
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pb-5 text-center">
				<div id="upload_banner_container_wrapper" class="d-none">
					<div id="upload_banner_container">
					</div>
					<button class="btn btn-primary btn-block" id="save_restaurant_banner">Almacenar Bandera</button>
					<br/>
				</div>
				<?php
				echo '<div id="preview_banner_container_wrapper">';
				if (strlen($model->banner_image) > 1) {
					echo '<img src="' . $model->banner_image . '" class="img-fluid"/>';
				}
				echo '</div>';
				?>
				<div class="custom-file mb-3">
					<input type="file" class="custom-file-input" id="restaurant_banner">
					<label class="custom-file-label" for="customFile">Subir Bandera</label>
				</div>
				</div>
				<!-- banner end -->
			</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
        </div>
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Restaurante</h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label class='col-form-label' for='title'>Nombre </label>
						<input type='text' class='form-control' id='title' name='title' value='<?php echo set_value('title', $model->title); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='description'>Descripción </label>
						<textarea id='description' name='description' class='form-control' rows='5'><?php echo set_value('description', $model->description); ?></textarea>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='about'>Detalles </label>
						<textarea id='about' name='about' class='form-control' rows='5'><?php echo set_value('about', $model->about); ?></textarea>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='delivery_hour'>Horario de Entregas </label><br/>
					<label class="custom-control custom-checkbox">
							<input type="checkbox" <?php echo ($model->u == 1) ? 'checked=true' : '';?> value="1" name="u" class="custom-control-input"/>
							<span class="custom-control-label">Domingo</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="u_open" name='u_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->u_open == $i && $model->u_open_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->u_open == $i && $model->u_open_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="u_close" name='u_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->u_close == $i && $model->u_close_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->u_close == $i && $model->u_close_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox" <?php echo ($model->m == 1) ? 'checked=true' : '';?> value="1" name="m" class="custom-control-input"/>
							<span class="custom-control-label">Lunes</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="m_open" name='m_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->m_open == $i && $model->m_open_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->m_open == $i && $model->m_open_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="m_close" name='m_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->m_close == $i && $model->m_close_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->m_close == $i && $model->m_close_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox" <?php echo ($model->t == 1) ? 'checked=true' : '';?> value="1" name="t" class="custom-control-input"/>
							<span class="custom-control-label">Martes</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="t_open" name='t_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->t_open == $i && $model->t_open_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->t_open == $i && $model->t_open_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="t_close" name='t_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->t_close == $i && $model->t_close_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->t_close == $i && $model->t_close_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox" <?php echo ($model->w == 1) ? 'checked=true' : '';?> value="1" name="w" class="custom-control-input"/>
							<span class="custom-control-label">Miercoles</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="w_open" name='w_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->w_open == $i && $model->w_open_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->w_open == $i && $model->w_open_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="w_close" name='w_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->w_close == $i && $model->w_close_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->w_close == $i && $model->w_close_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox" <?php echo ($model->r == 1) ? 'checked=true' : '';?> value="1" name="r" class="custom-control-input"/>
							<span class="custom-control-label">Jueves</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="r_open" name='r_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->r_open == $i && $model->r_open_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->r_open == $i && $model->r_open_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="r_close" name='r_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->r_close == $i && $model->r_close_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->r_close == $i && $model->r_close_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox" <?php echo ($model->f == 1) ? 'checked=true' : '';?> value="1" name="f" class="custom-control-input"/>
							<span class="custom-control-label">Viernes</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="f_open" name='f_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->f_open == $i && $model->f_open_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->f_open == $i && $model->f_open_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="f_close" name='f_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->f_close == $i && $model->f_close_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->f_close == $i && $model->f_close_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
					</label>
					<label class="custom-control custom-checkbox">
							<input type="checkbox" <?php echo ($model->s == 1) ? 'checked=true' : '';?> value="1" name="s" class="custom-control-input"/>
							<span class="custom-control-label">Sabado</span>
							<span class="custom-control-label">&nbsp;&nbsp;Abierto:</span>
							<select id="s_open" name='s_open' style="display:inline-block;">
							<?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->s_open == $i && $model->s_open_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->s_open == $i && $model->s_open_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
							<span class="custom-control-label">Cerrado:</span>
							<select id="s_close" name='s_close' style="display:inline-block;">
							<?php for ($i=2; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = ($model->s_close == $i && $model->s_close_min == 0) ? 'selected' : '';
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($model->s_close == $i && $model->s_close_min == 30) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
					</label>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='delivery_fee'>Costo/Delivery </label>
						<input type='text' class='form-control' id='delivery_fee'  data-inputmask="'alias': 'decimal', 'radixpoint': '.'" name='delivery_fee' value='<?php echo set_value('delivery_fee', $model->delivery_fee); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='food_minimum'>Consumo Mínimo/Entregas </label>
						<input type='number' class='form-control' id='food_minimum' name='food_minimum' value='<?php echo set_value('food_minimum', $model->food_minimum); ?>' onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='type'>Tipo de Restaurante </label>
						<select name='type' class='form-control'>
						<?php foreach ($type as $key => $value) {
							echo "<option value='{$key}' " . (($model->type == $key) ? 'selected' : '') . "> {$value} </option>";
						}
						?>
						</select>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='cuisines_id'>Gastronomias </label>
						<select name='cuisines_id' class='form-control'>
						<?php foreach ($cuisines as $key => $cuisine) {
							echo "<option value='{$cuisine->id}' " . (($model->cuisines_id == $cuisine->id) ? 'selected' : '') . "> {$cuisine->name} </option>";
						}
						?>
						</select>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='price_range'>Rango de Precios </label>
						<select name='price_range' class='form-control'>
						<?php $price = $setting['price'];
						$price_list = json_decode($price, TRUE);
						foreach ($price_list as $key => $price) {
							echo "<option value='{$price}' " . (($model->price_range == $price) ? 'selected' : '') . "> {$price} </option>";
						}
						?>
						</select>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='address'>Dirección </label>
						<input type='text' class='form-control' id='google_address' name='google_address' value='' placeholder="<?php echo set_value('address', $model->address); ?>"/>
						<input type='hidden' class='form-control' id="address" name='address' value='<?php echo set_value('address', $model->address); ?>'/>
						<small>Actualiza la dirección y el sistema autocompletara la Ciudad y Provincia..</small>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='city'>Ciudad </label>
						<input type='text'  class='form-control' id='google_city' name='google_city' value='<?php echo set_value('city', $model->city); ?>'/>
						<input type='hidden' class='form-control' id='city' name='city' value='<?php echo set_value('city', $model->city); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='state'>Provincia </label>
						<input type='text'  class='form-control' id='google_state' name='google_state' value='<?php echo set_value('state', $model->state); ?>'/>
						<input type='hidden' class='form-control' id='state' name='state' value='<?php echo set_value('state', $model->state); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='zip'>Codigo Postal </label>
						<input type='text'  class='form-control' id='google_zip' name='google_zip' value='<?php echo set_value('zip', $model->zip); ?>'/>
						<input type='hidden' class='form-control' id='zip' name='zip' value='<?php echo set_value('zip', $model->zip); ?>'/>
						<input type='hidden' class='form-control' id='lat' name='lat' value='<?php echo set_value('lat', $model->lat); ?>'/>
						<input type='hidden' class='form-control' id='long' name='long' value='<?php echo set_value('long', $model->long); ?>'/>
				</div>

				<div class="form-group">
					<label class='col-form-label' for='email'>Correo </label>
					<input type='email' class='form-control' id='email' name='email' value='<?php echo set_value('email', $model->email); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='password'>Contraseña </label>
					<input type='password' class='form-control' id='password' name='password' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='phone'>Teléfono  </label>
						<input type='text' class='form-control' id='phone' name='phone' value='<?php echo set_value('phone', $model->phone); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='serving'>No. Personas Max/Orden  </label>
						<input type='text' class='form-control' id='serving' name='serving' value='<?php echo set_value('serving', $model->serving); ?>'/>
				</div>
				<div class="form-group">
					<label for="Status">Estado </label>
					<select name="status" class="form-control">
						<option value="0" <?php echo ($model->status == 0) ? 'selected' : '';?>> Inactívo </option>
						<option value="1" <?php echo ($model->status == 1) ? 'selected' : '';?>> Actívo </option>
					</select>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='started_at'>Abierto desde </label>
						<input type='date' class='form-control' id='started_at' name='started_at' value='<?php echo set_value('started_at', $model->started_at); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='serving'>RNC # </label>
						<input type='text' class='form-control' id='rnc' name='rnc' value='<?php echo set_value('rnc', $model->rnc); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='serving'>Notas </label>
					<textarea id='notes' name='notes' class='form-control' rows='5'><?php echo set_value('notes', $model->notes); ?></textarea>
				</div>
				<div class="form-group">
					<input type='hidden' class='form-control' id='restaurant_id' name='restaurant_id' value='<?php echo set_value('id', $model->id); ?>'/>
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
                </form>
            </div>
        </div>
		</div>
