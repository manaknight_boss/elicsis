<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Referidos</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Invitaciones</h5>
            <div class="card-body">
  <table class="table  table-hover table-condensed">
    <thead>
    	<th class="no-bt">Usuario</th>
		<th class="no-bt">Invitado</th>
		<th class="no-bt">Estado</th>
		<th class="no-bt">Fecha</th>
		<th class="no-bt">Acción</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->user . '</td>';
		echo '<td>' . $data->refer_user . '</td>';
		echo '<td>' . $mapping[$data->status] . '</td>';
		echo '<td>' . $data->created_at . '</td>';

        echo '<td>';
        echo '<a class="btn btn-primary" href="/admin/refer/approve/' . $data->id . '">Aprobar</a>';
        echo ' <a class="btn btn-danger" href="/admin/refer/decline/' . $data->id . '">Declinar</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
  </div>
  </div>
  </div>
</div>