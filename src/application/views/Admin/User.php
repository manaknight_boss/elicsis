<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Usuarios</h2>
<div>
    <a class="btn btn-default btn-primary" href="/admin/users/add">Crear Usuario</a>
</div>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Usuarios</h5>
            <div class="card-body">
            <div class="table-responsive">
  <table class="table table-hover table-condensed">
    <thead>
        <th class="no-bt">Correo</th>
        <th class="no-bt">Papel</th>
        <th class="no-bt">Creado el</th>
        <th class="no-bt">Estado</th>
        <th class="no-bt">Acción</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->email . '<br/>' . $data->phone . '<br/>' . $data->first_name . ' ' . $data->last_name . '<br/>Tipo de Perfil: ' . $data->profile_type  . '</td>';
        echo '<td>' . $mapping['role'][$data->role_id] . '</td>';
        echo '<td>' . date('Y-m-d', strtotime($data->updated_at)) . '</td>';
        echo '<td>' . $mapping['status'][$data->status] . '</td>';
        echo '<td><a class="btn btn-default btn-primary" target="__blank" href="/admin/users/edit/' . $data->id . '">Editar</a>';
        // echo '<td><a class="btn btn-default btn-primary" target="__blank" href="/member/users/view/' . $data->id . '">Ver</a></div>';
        echo '</tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
            </div>
</div>
    </div>
    </div>
    </div>