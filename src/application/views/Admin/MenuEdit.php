<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Menú </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Panel de Control</a></li>
						<li class="breadcrumb-item"><a href="/admin/menu" class="breadcrumb-link">Menú</a></li>
						<li class="breadcrumb-item active" aria-current="page">Editar Menú</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="card">
			<h5 class="card-header">Editar imagenes de Menú</h5>
			<div class="card-body">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pb-5 text-center">
				<div id="upload_menu_container_wrapper" class="d-none">
					<div id="upload_menu_container">
					</div>
					<button class="btn btn-primary btn-block" id="save_menu_image">Almacenar Imagen</button>
					<br/>
				</div>
				<?php
				if (strlen($model->image) > 1) {
					echo '<div id="preview_menu_container_wrapper">';
					echo '<img src="' . $model->image . '" class="img-fluid"/>';
					echo '</div>';
				}
				?>
				<div class="custom-file mb-3">
					<input type="file" class="custom-file-input" id="menu_image">
					<label class="custom-file-label" for="customFile">Cargar Imagen</label>
				</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
        </div>
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Menú</h5>
                <div class="card-body">
                <?= form_open() ?>
				<input type='hidden' class='form-control' id='menu_id' name='menu_id' value='<?php echo set_value('id', $model->id); ?>'/>
				<div class="form-group">
					<label class='col-form-label' for='name'>Nombre </label>
						<input type='text' class='form-control' id='name' name='name' value='<?php echo set_value('name', $model->name); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='description'>Descripción </label>
						<textarea id='description' name='description' class='form-control' rows='10'><?php echo set_value('description', $model->description); ?></textarea>
				</div>
				<div class="form-group">
					<label for='catering_menu_id'>Tipo de Menú </label>
					<select id="catering_menu_id" name='catering_menu_id' class='form-control'>
					<?php
						foreach ($catering_menu as $key => $value) {
							echo "<option value='{$key}'" . ($model->catering_menu_id == $key ? "selected='true'" : '') . "> {$value} </option>";
						}
					?>
					</select>
				</div>
				<div class="form-group">
					<label class="custom-control custom-checkbox">
							<input type="checkbox" value="1" name="most_ordered" class="custom-control-input" <?php echo ($model->most_ordered == 1) ? 'checked="true"' : ''; ?>><span class="custom-control-label">Más Pedido?</span>
					</label>
				</div>
				<div class="form-group">
					<label class="custom-control custom-checkbox">
							<input type="checkbox" value="1" name="popular" class="custom-control-input" <?php echo ($model->popular == 1) ? 'checked="true"' : ''; ?>><span class="custom-control-label">Popular</span>
					</label>
				</div>
				<div class="form-group">
					<label for='Restaurante'>Restaurante ID </label>
					<input type='number' class='form-control' id='restaurant_id' name='restaurant_id' min="1" step="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57" readonly="true" value='<?php echo set_value('restaurant_id', $model->restaurant_id); ?>'/>
				</div>
				<div class="form-group">
					<label for='quantity_min'>Cantidad Mínima </label>
						<input type='number' class='form-control' id='quantity_min' name='quantity_min' min="1" value='<?php echo set_value('quantity_min', $model->quantity_min); ?>'/>
						</div>
				<div class="form-group">
					<label for='quantity_max'>Cantidad Máxima </label>
						<input type='number' class='form-control' id='quantity_max' name='quantity_max' min="1" value='<?php echo set_value('quantity_max', $model->quantity_max); ?>'/>
						</div>
				<div class="form-group">
					<label class='col-form-label' for='serving'>No. de Porciones </label>
						<input type='number' class='form-control' id='serving' name='serving' value='<?php echo set_value('serving', $model->serving); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='price'>Precio </label>
						<input type='number' class='form-control' id='price' name='price' value='<?php echo set_value('price', $model->price); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='order'>Orden de Aparición </label>
						<input type='number' class='form-control' id='order' name='order' value='<?php echo set_value('order', $model->order); ?>'/>
				</div>
				<div class="form-group">
						<label for='order'>Preferencia </label>
						<select id="preference" name='preference[]' multiple="multiple" class='form-control'>
						<?php foreach ($preferences as $key => $value) {
							$selected = '';
							foreach ($processed_preference as $preference_id) {
								if ($preference_id == $key) {
									$selected = ' selected ';
								}
							}
							echo "<option value='{$key}' $selected> {$value} </option>";
						}?>
						</select>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='status'>Estado </label>
						<select name='status' class='form-control'>
						<option value='1' <?php echo ($model->status == 1) ? 'selected' : '';?>> Actívo </option>
						<option value='0' <?php echo ($model->status == 0) ? 'selected' : '';?>> Inactívo </option>
						</select>
				</div>

				<div class="form-group">
						<label for='order'>Se Vende por </label>
						<select id="sold_by" name='sold_by' class='form-control'>
						<?php foreach ($sold_by_mapping as $key => $value) {
							echo "<option value='{$key}' " . (($model->sold_by == $key) ? 'selected' : '') . " > {$value} </option>";
						}?>
						</select>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
                </form>
            </div>
        </div>
        </div>