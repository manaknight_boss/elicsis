<br/>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-8">
                <h2>Iniciar sesión</h2>
                <p>¿Es tu primera vez, o ya tienes una cuenta?</p>
                <div class="custom-control custom-radio">
                <input type="radio" name="choose" class="custom-control-input" id="return-customer" value="1">
                <label class="custom-control-label" for="return-customer">Acceder a mi Cuenta </label>
                <br/>
                </div>
                <div class="custom-control custom-radio">
                <input type="radio" name="choose" class="custom-control-input" id="new-customer" value="2">
                <label class="custom-control-label" for="new-customer">Es mi primera Compra</label>
                </div>
                <div class="custom-control custom-radio">
                <input type="radio" name="choose" class="custom-control-input" id="google-customer" value="3">
                <input type="hidden" id="google_url" name="google_url" value="<?php echo $google_auth_url; ?>">
                <label class="custom-control-label" for="google-customer">Acceder con Google</label>
                </div>
                <br/>
                <?php if (validation_errors()) : ?>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors() ?>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <?php if (strlen($error) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (strlen($success) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success" role="success">
                                <?php echo $success; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <div class="login-step-1">
                    <?= form_open(); ?>
                        <div class="form-group">
                            <input class="form-control form-control-lg email" id="email" type="email" name="email" placeholder="Correo" >
                        </div>
                        <div class="form-group">
                            <input class="form-control form-control-lg" id="password" type="password" name="password" placeholder="Contraseña">
                        </div>
                        <input type="hidden" id="create_account_type" name="create_account_type" value="1">
                        <button type="submit" class="btn btn-primary btn-lg create_account_type_submit_button">Enviar</button>
                    </form>
                </div>
                <div class="register-step-1">
                    <?= form_open(); ?>
                        <div class="form-group">
                            <input class="form-control form-control-lg email" id="register-email" type="email" name="email" placeholder="Correo" >
                        </div>
                        <div class="form-group">
                            <input class="form-control form-control-lg" id="password" type="password" name="password" placeholder="Contraseña">
                        </div>
                        <div class="form-group">
                            <input type="text" id='first_name' name='first_name' class='form-control form-control-lg' placeholder="Nombres" value=""/>
                        </div>
                        <div class="form-group">
                            <input type="text" id='first_name' name='last_name' class='form-control form-control-lg' placeholder="Apellidos" value=""/>
                        </div>
                        <div class="form-group">
                            <input type="text" id='phone' name='phone' class='form-control form-control-lg' placeholder="Teléfono" value=""/>
                        </div>
                        <input type="hidden" id="create_account_type" name="create_account_type" value="2">
                        <button type="submit" class="btn btn-primary btn-lg create_account_type_submit_button">Enviar</button>
                    </form>
                </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
