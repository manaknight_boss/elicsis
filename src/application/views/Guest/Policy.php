<br/>
<br/>
<br/>
<div class="container">
<h1>Nombre comercial</h1>

<h2>Elicsis</h2>

<h3>Descripción completa y clara de los bienes y servicios</h3>

<p>Plataforma intermediaria que conecta restaurantes y suplidores de comida con usuarios que desean ordenar servicios de catering través de la  plataforma. <br/>
El restaurante/suplidor  recibe la orden y se encarga de prepararla y entregársela al usuario.</p>

<p>Políticas de devoluciones, reembolsos y cancelaciones</p>
<p>Devoluciones y Cancelaciones</p>
<p>Una vez que hayas confirmado tu compra, la solicitud será definitiva y no se aceptará devolución del dinero. No habrá devoluciones ni cancelaciones.</p>

<h6>Reembolsos</h6>
<p> Se harán reembolsos completos siempre y cuando el restaurante no pueda cumplir con la orden por cualquiera de las siguientes causas:</p>
<ol>
<li>Restaurante no pudo entregar la orden de manera satisfactoria.</li>
<li>Recibimiento de productos incorrectos o incompletos.</li>
<li>Restaurante no esta en horas de operaciones.</li>
<li>Restaurante no tiene capacidad de entrega a domicilio para la hora requerida por el usuario.</li>
<li>No fue posible comunicarse con el usuario para notificarle de cualquier inconveniente.</li>
<li>No fue posible comunicarse con el restaurante.</li>
<li>Algún producto no está disponible.</li>
</ol>

<h6>Política de Entrega</h6>
<p> El usuario tiene la opción de pasar a recoger el pedido o solicitar la entrega del mismo. <br/>
En caso de solicitar entrega a domicilio, se le da una confirmación al usuario de que la orden llegara en el tiempo solicitado.<br/>
El restaurante es estricta y absolutamente responsable de realizar la entrega de dicho pedido. Elicsis se encargará de monitorear dicho proceso y proveer soporte al cliente durante el proceso con el fin de garantizar la calidad del servicio.<br/>
 Dicho restaurante en caso de aceptar la hora, entregara el pedido a la fecha y hora establecida por el cliente.</p>



<h6>Políticas de seguridad  para la transmisión de datos de tarjetas.</h6>
<p>A través de nuestro portal no se guardan informaciones de tarjeta de créditos ni débitos de ninguno de los tarjetahabientes.<br/>
 Esta información, en ningún momento será retenida, divulgada o registrada en el portal elicsis.com , si no que utiliza procesadores terceros de pago.<br/>
Las únicas informaciones que pasan hacia la plataforma tercera son los datos del propietario, correo electrónico y monto total del importe a pagar.</p>


<h6>Métodos de Pago </h6>
<p>Visa, Mastercard, American Express, Paypal y Efectivo</p>

<p>Moneda de compra</p>
<p>Pesos Dominicanos</p>
</div>