<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="home">
    <header class="position-relative overflow-hidden pt-5  bg-light">

      <div class="hero-image"></div>

      <div class="col-md-9 mx-auto py-5 my-5  mb-5 position-relative text-white">
          <h1 class="display-4 text-center text-shadow mt-5 position-relative">CATERING CUANDO Y DONDE QUIERAS  </h1>
          <h2 class="text-center text-shadow mb-4 position-relative">Catering de gran variedad de restaurantes en todo Santo Domingo.</h2>

        <form class="col-md-9 mx-auto mb-5 pb-5" action="/search">


            <div class="order-box input-icon">
              <i class="icon fas fa-map-marker-alt text-primary"></i>
              <div class="input-group mb-3 shadow-sm">
                <input type="text" id="address_string" class="form-control" placeholder="Dirección de entrega" aria-label="Introduzca Su dirección de delivery" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-primary text-uppercase px-4 " >
                    <i class="fas fa-search d-md-none"></i>
                    <span class="d-none d-md-inline">Buscar</span>
                  </button>

                </div>
              </div>
            </div>

        </form>
      </div>
    </header>
    <section id="benefitsSection">
    <div class="row ">
      <!-- col 1 -->
      <div class="col-md-6  pt-3 px-3 pt-md-5 px-md-5  overflow-hidden">
        <div class="w-75 m-auto my-3 py-3">

          <h2 class="static-intro-header">Comida para cualquier Ocasión </h2>
          <p class="mb-5 lead"> Sin importar el objetivo de tu evento o cantidad de invitados, elegir la comida nunca fue tan sencillo. Elicsis, con su sistema de orden en línea, evaluaciones y servicio al cliente garantiza que encontrarás lo que
            necesites para tu evento.</p>
          <h2 class="static-intro-header">Ahorra tiempo y evita problemas</h2>
          <p class="mb-5 lead"> Olvídate de las largas búsquedas y extensa espera, y encuentra de manera inmediata restaurantes que entreguen catering a tu ubicación.Encuentra restaurantes por tipo de comida, precio y mas. Crea órdenes a tu gusto y
            requerimientos. </p>

          <h2 class="static-intro-header">Gasta menos</h2>
          <p class="mb-5 lead"> Con Elicsis puedes encontrar servicios que se ajusten a tu presupuesto. Ordenando catering para grupos ahorras más que comprando a precio individual. Puedes comparar entre las diferentes opciones disponibles y recibir
            ofertas y promociones diarias. </p>
        </div>
        <!-- <div class="bg-light shadow-sm mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div> -->
      </div>
      <!-- end col 1 -->

      <!-- col 2 -->
      <div class="col-md-6 d-md-flex align-items-center  overflow-hidden">
        <img src="/assets/image/home-page-1.jpg" alt="" class="w-100">

        <!-- <div class="bg-dark shadow-sm mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div> -->
      </div>
      <!-- end col 2 -->
  </section>

  <section id="mapSection" class="bg-light pt-5">
    <div class="col-md-7 mx-auto">

      <h3 class="h2 text-center mt-5">Entrega de alimentos o recogida en restaurantes locales.</h3>
      <p class="lead text-center mb-5">
        Elicsis te facilita encontrar los restaurantes con servicio de catering cerca de ti con servicio de delivery o para recoger en el lugar. Con opciones diferentes para cada gastronomia o gusto es fácil encontrar lo que deseas. Encuentra
        excelentes almuerzos para grupos con menús locales.
      </p>
    </div>

    <img src="/assets/image/home-page-2.jpg" alt="map of our locations" class="w-100">

  </section>
</div>