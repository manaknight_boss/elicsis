<br/>
<br/>
<br/>
<br/>
<script>
<?php
  echo 'function c () {';
  echo 'var config = ' . json_encode($data) . ';';
  echo 'var restaurant_id = ' . $restaurant->id . ';';
  echo 'var restaurant_config = ' . json_encode($restaurant_config) . ';';
  echo 'var search = ' . json_encode($search) . ';';
  echo 'var schedule = ' . json_encode($schedule) . ';';
  echo 'return [config, restaurant_id, restaurant_config, search, schedule];';
  echo '}';
?>
</script>
<script src="/assets/js/angular.min.js"></script>
<script src="/assets/js/spa.js<?php echo "?v=" . md5(time());?>"></script>
<div class="search page" ng-controller="RestaurantController" ng-cloak>
<div id="navbarOrder" class="navbar navbar-expand-lg navbar-light bg-light  border-bottom fixed-top-search">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-filter"></i>
    </button>

    <div class="collapse navbar-collapse align-items-center" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item mr-4 my-3 my-md-0 d-flex flex-column justify-content-start">
        <label class="filter-label text-uppercase font-weight-bold text-primary mb-0">Ordenar</label>
        <div class="btn-group btn-group-sm" role="group">
          <?php
          $delivery_button = '<button type="button" class="btn btn-outline-primary" ng-class="{active: filterObj.order_type == 1, \'d-none\': restaurant_config.delivery_type == 2}" ng-click="orderTypeChange(1)">Delivery</button>';
          $takeout_button = '<button type="button" class="btn btn-outline-primary" ng-class="{active: filterObj.order_type == 2, \'d-none\': restaurant_config.delivery_type == 1}" ng-click="orderTypeChange(2)">Recoger</button>';
            switch ($restaurant->type) {
              case 1:
                echo $delivery_button;
                break;
              case 2:
                echo $takeout_button;
                break;
              default:
                echo $delivery_button;
                echo $takeout_button;
                break;
            }
          ?>

        </div>
        </li>
        <li class="nav-item mr-3 mb-3 mb-md-0">
        <label class=" text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
        ¿Para Cuando?
        </label>
        <span class="d-block mt-1 pointer" data-toggle="modal" data-target="#filterModal"><span ng-bind-html="filterObj.date"><?php echo $data['date'];?></span> a las <span ng-bind-html="am_or_pm(filterObj.time)"><?php echo am_or_pm((int)$search['time'], (is_numeric( $search['time'] ) && floor( $search['time'] ) != $search['time']) ? 30 : 0);?></span> <i class="fas fa-sort-down"></i></span>
        </li>
        <li class="nav-item mr-3 mb-3 mb-md-0">
            <label class="text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
            ¿Para cuantos?
            </label>
            <span class="d-block mt-1 pointer" data-toggle="modal" data-target="#filterModal"><span ng-bind-html="filterObj.serving"><?php echo $search['serving'];?></span> persona(s) <i class="fas fa-sort-down"></i></span>
        </li>

        </ul>
        <div>
    </div>
    </div>
</div>
    <main class="py-5 container">
      <header>
         <a href="/search">< Atrás</a>
         <div class="d-flex flex-column flex-md-row justify-content-between align-items-center mb-3">
           <h1 class="font-weight-bold m-0"><?php echo $restaurant->title;?></h1>
         </div>

          <p class="" ><?php echo $restaurant->address;?>,&nbsp; <?php echo $restaurant->city;?>,&nbsp; <?php echo $restaurant->state;?> <?php echo $restaurant->zip;?>
              <span class="d-inline-block mx-2">|</span>
              <span style="color:#565a5c;">
                <?php
                if ($restaurant->num_review < 2)
                {
                  $restaurant->rating = 5;
                  $restaurant->num_review = 1;
                  $restaurant->on_time_rating = 100;
                  $restaurant->on_delivery_rating = 100;
                }
                $restaurant->rating = floor($restaurant->rating);
                for($i = 0; $i < $restaurant->rating; $i++) {
                  echo '<i class="fas fa-star fa-xs"></i>';
                }
                for($j = 5 - $i; $j > 0; $j--) {
                  echo '<i class="far fa-star fa-xs"></i>';
                }
                ?>

                <span class="text-info "><?php echo number_format($restaurant->rating, 2);?></span>
            </span>
            <?php
            if ($restaurant->num_review > 1) {
              echo '(<a href="#" class="">' . $restaurant->num_review . ' Calificaciones</a>)';
            }
            ?>
          </p>

            <div class="caterer-header ">

                <img class="caterer-header__cover img-fluid" src="<?php echo $restaurant->banner_image;?>"/>
                <div class="caterer-logo caterer-header-details d-flex justify-content-center align-items-top mt-1">
                    <figure class="mx-3">
                        <img src="<?php echo $restaurant->logo;?>"  alt="" class="img-thumbnail">
                    </figure>

                </div>
            </div>
      </header>

      <?php
      /**
       * 1.Save JSON data of menu and catering menu
       * 2.Loop through catering menu and make list
       * 3.Bind scroll down mechanic to menu
       * 4.Loop through all menu and show them
       * 5.If user click on a menu, pop open a modal with information
       * 6.Add To cart
       * 7.Cart list all items
       * 8.Cart Remove item
       * 9.Cart calculate tax, total, delivery fee
       * 10.Calculate tips
       * 11.Checkout button
       * 12.If time and date not set, can't checkout
       */
      ?>

      <section id="menuSection" class="mt-4">
        <header class="d-flex justify-content-between align-items-end border-bottom pb-2 mb-4 ">
            <h2 class="m-0 h4 font-weight-black">Menú</h2>
            <nav class="list-unstyled d-flex justify-content-center">
                <a class="d-inline-block mx-3" ng-click="scrollToMenu(catering_menu)" ng-repeat="catering_menu in catering_menus" ng-cloak>{{catering_menu}}</a>
            </nav>
        </header>

        <div class="row">
          <div class="col-md-8 order-2 order-md-1">
            <div class="menu-container" ng-repeat="(key,catering_menu) in catering_menus">
              <div class="menu-category cart__heading">
                <h3 class="section-subhead" id="{{catering_menu}}">{{catering_menu}} </h3>
              </div>
              <div class="menu-items" >
                <a ng-repeat="one_menu in menu[key]" ng-click="openMenu(one_menu)" class="menu-item" ng-class="{'menu-item--most-ordered':isMostOrdered(one_menu.most_ordered),'menu-item--popular':isMostPopular(one_menu.popular)}">
                  <div class="menu-item__content" >
                    <div class="menu-item__name-and-price" >
                      <div class="menu-item__name">{{one_menu.name}}</div>
                      <div class="menu-item__price" >DOP {{one_menu.price | number:2}}</div>
                    </div><div class="menu-item__meta" >
                      <span class="menu-item__most-ordered" ng-if="one_menu.most_ordered == 1">Más Pedido</span>
                      <span class="menu-item__popular" ng-if="one_menu.popular == 1">Popular</span>
                      <span class="menu-item__serving-size" >Sirve {{one_menu.serving}}</span>
                    </div><div class="menu-item__description" >
                        {{one_menu.description}}
                    </div>

                    <div class="menu-item__icons" ></div>
                  </div>
                </a>
              </div>
            </div>
              <div class="d-flex justify-content-center mt-3" ng-if="false">

                <a href="javascript:void(0)" tabindex="0" role="button" class="btn btn--show-more" >
                  <div class="svg-icon--baseline svg-icon" >
                    <svg class="i-circle-plus" aria-labelledby="circle-plus-title" viewBox="0 0 32 32" width="32" height="32" role="img" ><title id="circle-plus-title" >Circle Plus Icon</title><path fill="currentColor" d="M16 0c8.825 0 16 7.175 16 16s-7.175 16-16 16-16-7.175-16-16 7.175-16 16-16zM16 29.714c7.556 0 13.714-6.159 13.714-13.714s-6.159-13.714-13.714-13.714-13.714 6.159-13.714 13.714 6.159 13.714 13.714 13.714zM17.143 6.857v8h8v2.286h-8v8h-2.286v-8h-8v-2.286h8v-8h2.286z" > </path></svg>
                  </div>
                  Mostrar Más
                </a>
              </div>



            <div class="caterer-menu-feedback my-4" >
              <a href="mailto:soporte@elicsis.com?subject=Feedback For Menu (<?php echo $restaurant->title;?>)" >
                <div class="svg-icon--baseline svg-icon" >
                  <svg class="i-speech-bubble" aria-labelledby="speech-bubble-title" viewBox="0 0 32 32" width="32" height="32" role="img" ><title id="speech-bubble-title" >Speech Bubble Icon</title><path fill="currentColor" d="M8 32h-1.079q-0.635-0.317-0.635-0.857v-8h-5.206q-0.349 0-0.714-0.317t-0.365-0.762v-20.984q0-0.476 0.302-0.778t0.778-0.302h29.841q0.54 0 0.81 0.381t0.27 0.698v21.206q0 0.635-0.635 0.952-0.222 0.127-0.444 0.127h-14.286zM1.937 21.175h5.429q0.508 0 0.794 0.397t0.286 0.714v6.476l7.111-7.143q0.222-0.222 0.667-0.222h13.841v-19.46h-28.127v19.238z" > </path>
                </svg>
                </div> ¿Ves algún error en este menú? ¡Por favor reportalo!
              </a>
            </div>

            <div class="caterer-reviews-list">
              <h2 class="caterer-review-count">
              Calificaciones para
                <span> <?php echo $restaurant->title;?> </span>
              </h2>
              <div>
                  <div ng-if="reviews.length == 0" ng-cloak><br/>No hay Calificaciones</div>
                  <div class="caterer-review" ng-repeat="review in reviews" ng-cloak>
                    <div class="rating-and-time">
                      <span>
                        <div class="star-rating">
                          <span class="empty">★★★★★</span>
                          <div class="filled" style="width: 100%;">{{review.star}}</div>
                          <span class="sr-only">{{review.rating}}  estrellas
                          </span>
                        </div>
                      </span>
                      <div class="order-reviewed-at">{{review.date_format | timeago}}</div>
                    </div>
                      <div class="reviewer-name-and-location">
                        <span class="reviewer-name">
                          {{review.name}}
                        </span>
                        <div class="caterer-review-address">Santo Domingo</div>
                      </div>
                      <div class="caterer-review-text">
                        {{review.message}}
                      </div>
                  </div>
                  <div class="d-flex justify-content-center mt-3" ng-if="hasMoreReviews && reviews.length > 0" ng-cloak >
                    <a href="javascript:void(0)" ng-click="getReview()" tabindex="0" role="button" class="btn btn--show-more">
                      <div class="svg-icon--baseline svg-icon">
                        <svg class="i-circle-plus" aria-labelledby="circle-plus-title" viewBox="0 0 32 32" width="32" height="32" role="img">
                          <title id="circle-plus-title" >Circle Plus Icon</title>
                          <path fill="currentColor" d="M16 0c8.825 0 16 7.175 16 16s-7.175 16-16 16-16-7.175-16-16 7.175-16 16-16zM16 29.714c7.556 0 13.714-6.159 13.714-13.714s-6.159-13.714-13.714-13.714-13.714 6.159-13.714 13.714 6.159 13.714 13.714 13.714zM17.143 6.857v8h8v2.286h-8v8h-2.286v-8h-8v-2.286h8v-8h2.286z">
                          </path></svg>
                      </div>
                      Mostrar más
                    </a>
                  </div>
              </div>
            </div>

            <div class="caterer-about" >
              <h2 class="caterer-show-title" >
              Acerca de <span ><?php echo $restaurant->title;?></span>
              </h2>
              <p class="" >
              Abierto desde <?php
              setlocale(LC_ALL,"es_ES");
              $date = DateTime::createFromFormat("Y-m-d", $restaurant->started_at);
              echo strftime("%b %d %Y",$date->getTimestamp());
              ?>

              </p>

              <p class="caterer-description caterer-about-section" >
              <?php echo $restaurant->about;?>
              </p>
              <div class="caterer-about-section" >
                <h4 >Dirección</h4>
                <div class="caterer-about-detail" >
                  <span > <?php echo $restaurant->address;?> <br ></span> <?php echo $restaurant->state;?>, <?php echo $restaurant->city;?> <?php echo $restaurant->zip;?>
                </div>
              </div>
              <div class="caterer-about-section" >
                <h4 >
                Horas de Operación
                </h4>
                <div class="caterer-about-detail" >
                  <?php
                  function am_or_pm ($hour, $minute) {
                    if ($minute == 0) {
                      $minute = '00';
                    }

                    $minute_format = ":{$minute}";

                    if ($hour == 24 || $hour == 0) {
                        return '12' . $minute_format . ' AM';
                    }
                    if ($hour == 12) {
                        return '12' . $minute_format . ' PM';
                    }
                    if ($hour < 12) {
                        return $hour . $minute_format . ' AM';
                    }

                    if ($hour > 12) {
                        return ($hour % 12) . $minute_format . ' PM';
                    }
                  }

                  echo ($restaurant->u == 1) ? ('Domingo: ' . am_or_pm($restaurant->u_open, $restaurant->u_open_min) . ' - ' . am_or_pm($restaurant->u_close, $restaurant->u_close_min) . '<br/>') : 'Domingo: Cerrado<br/>';
                  echo ($restaurant->m == 1) ? ('Lunes: ' . am_or_pm($restaurant->m_open, $restaurant->m_open_min) . ' - ' . am_or_pm($restaurant->m_close, $restaurant->m_close_min) . '<br/>') : 'Lunes: Cerrado<br/>';
                  echo ($restaurant->t == 1) ? ('Martes: ' . am_or_pm($restaurant->t_open, $restaurant->t_open_min) . ' - ' . am_or_pm($restaurant->t_close, $restaurant->t_close_min) . '<br/>') : 'Martes: Cerrado<br/>';
                  echo ($restaurant->w == 1) ? ('Miércoles: ' . am_or_pm($restaurant->w_open, $restaurant->w_open_min) . ' - ' . am_or_pm($restaurant->w_close, $restaurant->w_close_min) . '<br/>') : 'Miércoles: Cerrado<br/>';
                  echo ($restaurant->r == 1) ? ('Jueves: ' . am_or_pm($restaurant->r_open, $restaurant->r_open_min) . ' - ' . am_or_pm($restaurant->r_close, $restaurant->r_close_min) . '<br/>') : 'Jueves: Cerrado<br/>';
                  echo ($restaurant->f == 1) ? ('Viernes: ' . am_or_pm($restaurant->f_open, $restaurant->f_open_min) . ' - ' . am_or_pm($restaurant->f_close, $restaurant->f_close_min) . '<br/>') : 'Viernes: Cerrado<br/>';
                  echo ($restaurant->s == 1) ? ('Sábado: ' . am_or_pm($restaurant->s_open, $restaurant->s_open_min) . ' - ' . am_or_pm($restaurant->s_close, $restaurant->s_close_min) . '<br/>') : 'Sábado: Cerrado<br/>';
                  ?>
                </div>
              </div>
              <div class="caterer-about-section">
                <h4>Distancia</h4>
                <div class="caterer-about-detail">
                  <?php echo $distance; ?>
                </div>
              </div>
              <div class="caterer-about-section">
                <h4>Delivery</h4>
                <div class="caterer-about-detail">RD$<?php echo $restaurant->delivery_fee;?> a esta ubicación</div>
              </div>
              <div class="caterer-about-section">
                <h4>Compra Mínima para Delivery </h4>
                <div class="caterer-about-detail">RD$<?php echo $restaurant->food_minimum;?>  o más</div>
              </div>
              <div class="caterer-about-section">
                <h4>Resumen de Desempeño</h4>
                <p><em>Basado en {{restaurant_config.num_orders}} pedidos.</em></p>
                <ul class="caterer-performance caterer-performance--any-threshold-met">
                  <li class="caterer-performance__item caterer-performance__item--threshold-met"  ng-show="restaurant_config.on_delivery_rating > 0">
                  Entrega Puntual — <strong> {{restaurant_config.on_delivery_rating}}% </strong>
                  </li>
                  <li class="caterer-performance__item caterer-performance__item--threshold-met"   ng-show="restaurant_config.total_customer > 0">
                  Clientes Habituales — <strong> {{restaurant_config.repeat_customer / restaurant_config.total_customer * 100}}% </strong>
                  </li>
                  <li class="caterer-performance__item caterer-performance__item--threshold-met" ng-show="restaurant_config.on_time_rating > 0">
                  Confirmación rápida — <strong> {{restaurant_config.on_time_rating}}% </strong>
                  </li>
                </ul>
              </div>
            </div>
        </div>


          <div class="col-md-4 order-1 order-md-2">
            <div class="menu-category cart__heading">
              <h3 class="section-subhead">Carrito De Compras
                <div class="svg-icon--baseline svg-icon">
                  <svg class="i-shopping-cart" aria-labelledby="shopping-cart-title" viewBox="0 0 36 32" width="36" height="32" role="img"><title id="shopping-cart-title">Shopping Cart Icon</title><path fill="currentColor" d="M27.873 23.714q1.714 0 2.921 1.222t1.206 2.936-1.206 2.921-2.921 1.206-2.936-1.206-1.222-2.921q0-1.111 0.73-2.286h-10.286q0.698 0.952 0.698 2.476t-1.206 2.73-2.921 1.206-2.937-1.206-1.222-2.921q0-1.079 0.492-2t1.381-1.429v-18.857h-7.175l-1.27-5.587h10.286v5.714h25.429l-4.413 15.873h-21.016v2.127h17.587zM10.286 7.429v12.444h19.556l3.429-12.444h-22.984zM2.73 3.873h5.714v-2.032h-6.159zM10.714 30.159q0.968 0 1.635-0.667t0.667-1.635-0.667-1.619-1.635-0.651-1.619 0.651-0.651 1.619 0.651 1.635 1.619 0.667zM27.857 30.159q0.968 0 1.635-0.667t0.667-1.635-0.667-1.619-1.635-0.651-1.619 0.651-0.651 1.619 0.651 1.635 1.619 0.667z"></path></svg>
                </div>
              </h3>
            </div>
            <div class="cart__content cart__content--empty">
              <div class="cart__content-inner">
                <!-- cart start -->
                <div class="cart__order-items" ng-if="cart.items.length > 0">
                <ul>
                  <li class="cart__order-item order-item" ng-repeat="item in cart.items">
                    <div class="order-item__container">
                      <div class="order-item__qty">{{item.quantity}}</div>
                      <div class="order-item__info">
                        <div class="order-item__name-and-cost">
                          <a class="order-item__name" href="javascript:void(0)" ng-click="editMenu($index)">{{item.title}}</a>
                          <div class="order-item__cost">DOP {{item.total | number:2}}</div>
                        </div>
                        <div class="order-item__serves">
                        Sirve {{item.serving}}
                        </div>
                        <ul class="order-item__notes">
                          <li ng-repeat="addon in item.addons">
                            - {{addon.type}}&nbsp;:&nbsp; {{addon.name}}
                          </li>
                          <li ng-if="item.specialInstruction.length > 0">
                            - Instrucciones:&nbsp;{{item.specialInstruction}}
                          </li>
                        </ul>
                      </div>
                      <a href="javascript:void(0)" class="order-item__remove" ng-click="removeCartItem($index, item)">
                        <div class="svg-icon--baseline svg-icon">
                          <svg class="i-close" aria-labelledby="close-title" viewBox="0 0 32 32" width="32" height="32" role="img">
                            <title id="close-title">Close Icon</title>
                            <path fill="currentColor" d="M16 13.825l8.627-8.627 2.175 2.175-8.627 8.627 8.627 8.627-2.175 2.175-8.627-8.627-8.627 8.627-2.175-2.175 8.627-8.627-8.627-8.627 2.175-2.175z"></path>
                          </svg>
                        </div>
                      </a>
                    </div>
                  </li>
                </ul>
                <div class="cart__extras">
                  <div class="cart__extras-item">
                    <div class="cart__extras-item-label">Comida y Bebida</div>
                    <div class="cart__extras-item-value">DOP {{cart.total | number:2}}</div>
                  </div>
                  <div class="cart__extras-item" ng-if="cart.is_delivery">
                    <div class="cart__extras-item-label">Delivery</div>
                    <div class="cart__extras-item-value">DOP {{cart.delivery_fee | number:2}}</div>
                  </div>
                  <div class="cart__extras-item">
                    <div class="cart__extras-item-label">{{restaurant_config.tax | number:2}}% ITBIS</div>
                    <div class="cart__extras-item-value">DOP {{cart.tax | number:2}}</div>
                  </div>
                  <div class="cart__tip">
                    <div class="cart__extras-item cart__extras-item--tip">
                      <div class="cart__extras-item-label" data-role="tip-description">Propinas para el conductor</div>
                      <div class="cart__extras-item-value">DOP {{cart.real_tips | number:2}}</div>
                    </div>
                    <div>
                      <span class="cart__tip-buttons">
                        <a href="javascript:void(0)" ng-repeat="tip in restaurant_config.tips" ng-click="updateTip(tip, 'tip-' + tip)" class="cart__tip-button" ng-class="{'active': cart.tip_option == 'tip-{{tip}}'}" aria-label="AGREGAR a tip">
                          {{tip}}%
                        </a>
                        <a class="cart__tip-button cart__tip-modal-button" href="javascript:void(0)" ng-click="updateTipCustom(tip, 'tip-custom')" ng-class="{'active': cart.tip_option == 'tip-custom'}">Otro monto</a>
                      </span>
                    </div>
                  </div>
                  <br/>
                  <div class="cart__extras-item">
                    <div class="cart__extras-item-label">TOTAL</div>
                    <div class="cart__extras-item-value">DOP {{cart.tax +  cart.total + cart.real_tips + cart.delivery_fee| number:2}}</div>
                  </div>
                </div>
              </div>
              <!-- cart done -->
                <div class="cart__empty-description" ng-if="cart.items.length < 1">
                  <div class="svg-icon">
                    <svg class="i-shopping-cart" aria-labelledby="shopping-cart-title" viewBox="0 0 36 32" width="36" height="32" role="img"><title id="shopping-cart-title">Carrito De Compras Icon</title><path fill="currentColor" d="M27.873 23.714q1.714 0 2.921 1.222t1.206 2.936-1.206 2.921-2.921 1.206-2.936-1.206-1.222-2.921q0-1.111 0.73-2.286h-10.286q0.698 0.952 0.698 2.476t-1.206 2.73-2.921 1.206-2.937-1.206-1.222-2.921q0-1.079 0.492-2t1.381-1.429v-18.857h-7.175l-1.27-5.587h10.286v5.714h25.429l-4.413 15.873h-21.016v2.127h17.587zM10.286 7.429v12.444h19.556l3.429-12.444h-22.984zM2.73 3.873h5.714v-2.032h-6.159zM10.714 30.159q0.968 0 1.635-0.667t0.667-1.635-0.667-1.619-1.635-0.651-1.619 0.651-0.651 1.619 0.651 1.635 1.619 0.667zM27.857 30.159q0.968 0 1.635-0.667t0.667-1.635-0.667-1.619-1.635-0.651-1.619 0.651-0.651 1.619 0.651 1.635 1.619 0.667z"></path></svg>
                  </div>
                  <p>Tu carrito esta vacío.</p>
                  <p>Elije un producto del menú para iniciar el pedido.</p>
                </div>
                <div class="cart__checkout-bottom">
                  <div class="cart__checkout">
                    <a ng-click="checkout()" class="btn btn-primary btn--full-width cart__checkout-button">
                      <span>
                      Checkout
                        <div class="svg-icon--baseline svg-icon">
                          <svg class="i-circle-arrow-right" aria-labelledby="circle-arrow-right-title" viewBox="0 0 32 32" width="32" height="32" role="img"><title id="circle-arrow-right-title">Circle Arrow Right Icon</title><path fill="currentColor" d="M16 0c8.825 0 16 7.175 16 16s-7.175 16-16 16-16-7.175-16-16 7.175-16 16-16zM15.905 27.333l11.333-11.333-11.333-11.333-3.238 3.238 5.81 5.81h-13.905v4.571h13.905l-5.81 5.81z"></path></svg>
                        </div>
                      </span>
                    </a>
                  </div>
                  <div class="cart__totals-minimum">$<?php echo $restaurant->food_minimum;?> Mínimo para Delivery</div>
                </div>
              </div>
            </div>
            <br/>
          </div>
        </div>

        <div>

        </div>
      </section>
    </main>
    <div class="modal fade" id="menuPopup" tabindex="-1" role="dialog" aria-labelledby="menu popup" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">{{menuPopup.title}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <div class="menu-item-price">
              <div class="menu-item-per-person">
                <span class="menu-item-per-person-suffix">DOP {{menuPopup.price / menuPopup.serving | number:2}} / Persona</span>
                <span class="menu-item-price-suffix">, sirve {{menuPopup.serving}} Personas</span>
              </div>
          </div>
            <img ng-show="menuPopup.image.length > 0" ng-src="{{menuPopup.image}}" class="img-fluid"/>
            <br/>
            <p>{{menuPopup.description}}</p>
            <span ng-bind-html="menuPopup.preference"></span>
            <div class="prompt section-subhead">SELECIONAR CANTIDAD:</div>
            <div class="quantity-wrapper">
              <div class="quantity-controls">
                <div class="input select">
                  <label for="quantity-select" class="sr-only">Cantidad</label>
                  <select id="quantity-select"
                          class="btn btn--secondary quantity-select-tap-target"
                          ng-model="menuPopup.quantity_select"
                          ng-change="menuPopupSelected()">
                    <option class="tight"
                    ng-repeat="one_quantity in menuPopup.quantity_range"
                    ng-selected="menuPopup.quantity_select == one_quantity"
                    value="{{one_quantity}}">{{one_quantity}} {{menuPopup.sold_by}}</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="menu-popup-addon-container">
                  <div class="menu-popup-addon" ng-repeat="addon_key in menuPopup.addon_keys">
                    <br/>
                    <div><b>{{addon_key | uppercase}}: </b></div>
                    <div ng-repeat="addon in menuPopup.addons[addon_key]" ng-click="addonSelected(addon)">
                      <input type="checkbox" ng-model="addon.checked"/> {{addon.name}}<span ng-if="addon.price > 0">&nbsp; (+ DOP {{addon.price | number:2}})</span>
                      <span ng-if="addon.info.length > 0">&nbsp; <i class="fas fa-info-circle tooltips" title="{{addon.info}}"></i>&nbsp;</span>
                      <span ng-if="addon.preference.length > 0" ng-bind-html="addon.preference"></span>
                    </div>
                  </div>
            </div>
            <div>
              <br/>
              <div ng-click="menuPopup.isSpecialInstruction = !menuPopup.isSpecialInstruction"><i class="fas " ng-class="{'fa-plus-circle': !menuPopup.isSpecialInstruction, 'fa-minus-circle': menuPopup.isSpecialInstruction}"></i><span>¿Algunas Instrucciones especiales?</span></div>
              <br/>
              <textarea  class="form-control" cols="30" rows="2" ng-model="menuPopup.specialInstruction" ng-if="menuPopup.isSpecialInstruction"></textarea>
            </div>
          </div>
          <div class="modal-footer" style="justify-content: left;">
            <button type="button" class="btn btn-primary" ng-click="addToCart(menuPopup)">Agregar al Carrito DOP {{menuPopup.total | number:2}}</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="menuPopupEdit" tabindex="-1" role="dialog" aria-labelledby="menu popup" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">{{menuPopup.title}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <div class="menu-item-price">
              <div class="menu-item-per-person">
                <span class="menu-item-per-person-suffix">DOP {{menuPopup.price / menuPopup.serving | number:2}} / Persona</span>
                <span class="menu-item-price-suffix">, Sirve {{menuPopup.serving}} Personas</span>
              </div>
          </div>
            <img ng-show="menuPopup.image.length > 0" ng-src="{{menuPopup.image}}" class="img-fluid"/>
            <br/>
            <p>{{menuPopup.description}}</p>
            <span ng-bind-html="menuPopup.preference"></span>
            <div class="prompt section-subhead">SELECIONAR CANTIDAD:</div>
            <div class="quantity-wrapper">
              <div class="quantity-controls">
                <div class="input select">
                  <label for="quantity-select" class="sr-only">Cantidad</label>
                  <select id="quantity-select"
                          class="btn btn--secondary quantity-select-tap-target"
                          ng-model="menuPopup.quantity_select"
                          ng-change="menuPopupSelected()">
                    <option class="tight"
                    ng-repeat="one_quantity in menuPopup.quantity_range"
                    ng-selected="menuPopup.quantity_select == one_quantity"
                    value="{{one_quantity}}">{{one_quantity}} {{menuPopup.sold_by}}</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="menu-popup-addon-container">
                  <div class="menu-popup-addon" ng-repeat="addon_key in menuPopup.addon_keys">
                    <br/>
                    <div><b>{{addon_key | uppercase}}: </b></div>
                    <div ng-repeat="addon in menuPopup.addons[addon_key]" ng-click="addonSelected(addon)">
                      <input type="checkbox" ng-model="addon.checked"/> {{addon.name}}<span ng-if="addon.price > 0">&nbsp; (+ DOP {{addon.price | number:2}})</span>
                      <span ng-if="addon.info.length > 0">&nbsp; <i class="fas fa-info-circle tooltips" title="{{addon.info}}"></i>&nbsp;</span>
                      <span ng-if="addon.preference.length > 0" ng-bind-html="addon.preference"></span>
                    </div>
                  </div>
            </div>
            <div>
              <br/>
              <div ng-click="menuPopup.isSpecialInstruction = !menuPopup.isSpecialInstruction"><i class="fas " ng-class="{'fa-plus-circle': !menuPopup.isSpecialInstruction, 'fa-minus-circle': menuPopup.isSpecialInstruction}"></i><span>¿Algunas Instrucciones especiales?</span></div>
              <br/>
              <textarea  class="form-control" cols="30" rows="2" ng-model="menuPopup.specialInstruction" ng-if="menuPopup.isSpecialInstruction"></textarea>
            </div>
          </div>
          <div class="modal-footer" style="justify-content: left;">
            <button type="button" class="btn btn-primary" ng-click="editToCart(menuPopup)">Editar Carrito DOP {{menuPopup.total | number:2}}</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="cartPopup" tabindex="-1" role="dialog" aria-labelledby="menu popup" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">¿Tienes suficiente para todos?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div>
              Necesitas suficiente para {{filterObj.serving}} personas
            </div>
            <div>
              Tu selección solo Sirve {{total_serving}} personas
            </div>
            <!-- cart start -->
            <div class="cart__content cart__content--empty">
              <div class="cart__content-inner">
                <div class="cart__order-items" ng-if="cart.items.length > 0">
                <ul>
                  <li class="cart__order-item order-item" ng-repeat="item in cart.items">
                    <div class="order-item__container">
                      <div class="order-item__qty">{{item.quantity}}</div>
                      <div class="order-item__info">
                        <div class="order-item__name-and-cost">
                          <a class="order-item__name" href="javascript:void(0)">{{item.title}}</a>
                          <div class="order-item__cost">DOP {{item.total | number:2}}</div>
                        </div>
                        <div class="order-item__serves">
                        Sirve {{item.serving}}
                        </div>
                        <ul class="order-item__notes">
                          <li ng-repeat="addon in item.addons">
                            - {{addon.type}}&nbsp;:&nbsp; {{addon.name}}
                          </li>
                          <li ng-if="item.specialInstruction.length > 0">
                            - Instrucciones:&nbsp;{{item.specialInstruction}}
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              </div>
            </div>
            <!-- cart end -->
          </div>
          <div class="modal-footer" style="justify-content: space-around;">
            <button type="button" class="btn btn-warning" ng-click="closeCartPopup()">Regresar al menu</button>
            <button type="button" class="btn btn-primary" ng-click="continueCheckout()">Es suficiente, Continuar</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detalles del Pedido</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item mb-3 ">
                            <div class="row">
                                <div class="col-6">
                                    <label class=" text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
                                        Fecha de Entrega
                                    </label>
                                    <input id="filter-date" class="form-control form-control-sm outline-0" type="text" placeholder="YYYY/MM/DD" value="" autocomplete="off"/>
                                </div>

                                <div class="col-6">
                                    <label class=" text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
                                    Hora
                                    </label>
                                    <select class="custom-select" ng-model="filterObj.time" ng-change="timeChange()">
                                    <option class="tight" ng-repeat="hour in hour_key"
                                    ng-selected="filterObj.time == hour"
                                    value="{{hour}}">{{hour_value[$index]}}</option>
                                    </select>
                                </div>

                                <div class="col-6 mt-3">
                                    <label class="text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
                                        ¿Para Cuantas Personas?
                                    </label>

                                    <input class="form-control form-control-sm  outline-0" ng-model="filterObj.serving" ng-change="servingChange()" type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                </div>


                            </div>
                        </li>

                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-block btn-primary" data-dismiss="modal">Almacenar</button>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>