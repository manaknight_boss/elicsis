<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="home">
<header class="masthead position-relative overflow-hidden d-flex align-items-center  bg-light">

        <div class="hero-image" style="background-image:url(/assets/image/empresa-header.jpg)"></div>

        <div class="col-md-9 mx-auto  position-relative text-center text-white">
            <h1 class="display-4 text-center mt-5 position-relative">Elicsis para Empresas </h1>
        </div>
      </header>

      <section id="" class="py-5">
        <div class="row ">

                  <!-- col 2 -->
                  <div class="col-md-6 d-md-flex align-items-start  overflow-hidden">
                    <img src="/assets/image/elicsis-para-empresas-1.jpg" alt="" class="mw-100">
                  </div>
                  <!-- end col 2 -->
      <!-- col 1 -->
      <div class="col-md-6  pt-3 px-3 pt-md-5 px-md-5  overflow-hidden">
        <div class=" text-section px-sm-5">
          <h2 class="static-intro-header">Incrementa tu productividad</h2>
          <p class="static-p-text">Ahórrale tiempo a tu equipo corporativo a la hora de pedir comida, tiempo que pueden dedicar a otras actividades dentro de la empresa.</p>
          <h2 class="static-intro-header">Mas Visibilidad y Control</h2>
          <p class="static-p-text">Elicsis te permite visualizar todos los gastos asociados a la comida de tus empleados. Puedes definir un presupuesto específico para cada miembro de tu equipo y generar reportes que te permiten tomar mejores decisiones a la hora de pedir. Además, crea parámetros donde establezcas políticas de propinas y presupuestos generales.</p>
          <h2 class="static-intro-header">Ahorra mas</h2>
          <p class="static-p-text">Con los ahorros de tiempo, visibilidad de gastos y las herramientas para establecer presupuestos generales, podras tomar decisiones con impactos financieros positivos para tu empresa.</p>
        </div>
      </div>
      <!-- end col 1 -->
  </section>

  <section id="mapSection" class="bg-light py-5">
    <div class="container py-5">
      <div class="row ">
        <div class="col-md-6 d-md-flex align-items-start  overflow-hidden">
            <img src="/assets/image/elicsis-pasra-empresas-2.jpg" alt="Corporate Catering Made Easy">
        </div>
        <div class="col-md-6  pt-3 px-3 pt-md-5 px-md-5  overflow-hidden">
          <div class=" text-section">
            <h2 class="static-intro-header">Mas tiempo, Mas control y Mas ahorros</h2>
            <p class="static-p-text"><b>Contactanos vía web 24/7. Estamos disponibles para servirte. Puedes acceder a tu cuenta en cualquier instante.</p>
            <p class="static-p-text"><b>Afiliados en todo Santo Domingo. Te ofrecemos Restaurantes con una gran variedad de opciones de Catering en toda la ciudad.</p>
            <p class="static-p-text"><b>Portal Online</b>. Ponle fin a la incertidumbre. Con nuestro portal, las empresas pueden manejar sus reportes financieros y rastrear sus gastos.</p>
          </div>
        </div>
      </div>

    </div>
  </section>

    <section class="py-5 " id="enterprise-form-section" style="background-color: #565a5c; ">
      <div class="container py-5">
          <div class="row ">
            <div class="col-sm-8 ">
                <div class=" text-section">
                <h2 class="static-bold-header" style="color: #fff; text-align: left; margin-bottom: 5px;">Abre tu cuenta empresarial hoy mismo</h2>
            <p class="static-p-text" style="color: #ffffff; margin-bottom: 20px; max-width: 600px;">
              Completa el siguiene formulario y nos pondremos en contacto contigo. ¿Tienes un restaurante o vendes comida?
                <a class="text-info" href="/become" ><b>Haz Click Aquí</b> para más información sobre cómo afiliarte a Elicsis</a>.</p>
                </div>
            </div>
          </div>

          <div id="enterprise-signup-form" class="row ">
            <div class="col-sm-8 pr-5">
            <div class="row">
                <?php if (validation_errors()) : ?>
                  <div class="col-sm-8">
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors() ?>
                    </div>
                  </div>
                <?php endif; ?>
                <?php if (strlen($error) > 0) : ?>
                        <div class="col-sm-8">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                <?php endif; ?>
                <?php if (strlen($success) > 0) : ?>
                        <div class="col-sm-8">
                            <div class="alert alert-success" role="success">
                                <?php echo $success; ?>
                            </div>
                        </div>
                    <?php endif; ?></div>
            <?= form_open('', 'class=""') ?>
            <div class="row mb-3">
              <div class="col col-xs-12">
                <input type="text" class="form-control" name="business_name" placeholder="Nombre de la Empresa">
              </div>
            </div>
            <div class="row mb-3">
              <div class="col mobile-col">
                <input type="text" class="form-control" name="first_name" placeholder="Su Nombre">
              </div>
              <div class="col mobile-col">
                <input type="text" class="form-control" name="last_name" placeholder="Apellidos">
              </div>

            </div>
            <div class="row mb-3">
              <div class="col mobile-col">
                <input type="email" class="form-control" name="email" placeholder="Correo Electrónico">
              </div>
              <div class="col mobile-col">
                <input type="phone" class="form-control" name="phone" placeholder="Teléfono">
              </div>

            </div>
            <div class="row">
              <div class="col-sm-5 mobile-col">
                <input type="submit" value="Registrar Empresa" class="hs-button primary large" data-reactid=".hbspt-forms-0.5.1.0">
              </div>

            </div>
          </form>
            </div>
            <div class="col-sm-4 ">
                <div class=" text-section">
                <div style="background-color: #efefef; padding: 20px 0px; border: 1px solid #333333; margin-top: 15px;">
              <h2 style="color: #1D5D7C; text-align: center; font-weight: bold; font-size: 18px;">¿PREGUNTAS?</h2>
              <p style="color: #1D5D7C; text-align: center; font-size: 28px;">1 (809) 792-6700</p>
            </div>
                </div>
            </div>
          </div>
      </div>
    </section>
</div>