<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container" style="margin-top:100px;">
    <div class="row">
        <div class="col-md-12">
            <h1>Preguntas Frecuentes</h1>
            <br/>
            <h3>Datos Generales</h3>
            <div class="accordion" id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        ¿Qué es Elicsis?
                        </button>
                    </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                    Elicsis es una plataforma online que conecta clientes y empresas con restaurantes y proveedores que ofrecen servicios de catering. Elicsis permite a los clientes ver el menú de catering que ofrecen los proveedores, realizar cotizaciones de manera automática, ordenar online y pagar vía tarjeta de crédito y débito.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        ¿Cuándo está disponible Elicsis?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                    Puedes realizar tus ordenes cualquier día de la semana,a  la hora que desees.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                        ¿Dónde está disponible Elicsis?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree3" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Elicsis está disponible actualmente en el Gran Santo Domingo y el Distrito Nacional.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree4" aria-expanded="false" aria-controls="collapseThree4">
                        ¿Cómo comienzo a usar Elicsis?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                        Ingresa a nuestra página web y elige tu dirección de entrega.
                        Elige entre los mejores restaurantes de Santo Domingo.
                        ¡Haz tu pedido y disfrutalo en grupo!
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree5" aria-expanded="false" aria-controls="collapseThree5">
                        ¿Me llevan mi orden a donde esté o tengo que buscarla?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Puedes elegir! Muchos de nuestros restaurantes ofrecen servicio de delivery. Si prefieres recogerlo tu mismo, simplemente indícalo al hacer tu orden.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree6" aria-expanded="false" aria-controls="collapseThree6">
                        ¿Cuál es el costo de envío de Elicsis?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    El costo de envío de Elicsis es un monto que le pagas al delivery por cada entrega, y que ayuda a cubrir los gastos operativos. El monto varia de restaurante en restaurante.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree7" aria-expanded="false" aria-controls="collapseThree7">
                        ¿Qué tipo de comida puedo pedir en Elicis?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Cualquier tipo que desees! Tenemos un gran rango de restaurantes conocidos y caterers.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree8" aria-expanded="false" aria-controls="collapseThree8">
                        ¿Qué pasa si soy alérgico a ciertos alimentos y necesito cambiar mi pedido?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Para la mayoría de las comidas que ordenes a través de Elicsis, podrás agregar instrucciones especiales para que los restaurantes sepan las modificaciones que quieras hacer.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree9" aria-expanded="false" aria-controls="collapseThree9">
                        ¿Qué hago si tengo un problema con mi pedido?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Estamos aquí para ayudarte. Una vez que hayas hecho tu pedido, solo contacta a nuestro equipo de soporte y te ayudaremos con cualquier problema.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree10" aria-expanded="false" aria-controls="collapseThree10">
                        ¿El repartidor traerá la comida a mi puerta o dentro de mi oficina?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    El repartidor llevará tu pedido a tu puerta.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree11" aria-expanded="false" aria-controls="collapseThree11">
                        ¿Tengo que darle propina a mi repartidor?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Las propinas no se incluyen, no se esperan, ni son obligatorias. Además, podrás calificar tu experiencia después de ordenar.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree12" aria-expanded="false" aria-controls="collapseThree12">
                        ¿Es Elicsis gratis?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree12" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Si! No debes pagar ninguna cuota por pedir por Elicsis, solo la que pagas por la comida que pidas. Te proporcionamos un desglose de la transacción.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree13" aria-expanded="false" aria-controls="collapseThree13">
                        Soy un restaurante y me interesa asociarme con Elicsis. ¿Cómo consigo más información?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree13" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    ¡Gracias por tu interés! Siempre buscamos entregar nuevos platos suculentos. Comienza dirigiéndose a nuestro espacio para restaurantes y encuentra allí las bases para iniciar tu proceso de ingreso a la plataforma.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree14" aria-expanded="false" aria-controls="collapseThree14">
                        ¿Puedo editar una orden después de hacerla?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree14" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Para hacer cambios a tu orden debes contactarte directamente con nuestro servicio al cliente. Cada restaurante tiene sus propias políticas de tiempo para poder hacer cambios.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree15" aria-expanded="false" aria-controls="collapseThree15">
                        ¿Existe una tarifa por cancelación?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree15" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    El cliente tiene la oportunidad de cancelar la orden siempre y cuando el restaurante no haya aceptado la solicitud. Luego de que el restaurante haya aceptado la orden no se permiten cancelaciones a pedidos. Para cambiar detalles de la orden debe contactarse con elicsis via chat o correo.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree16" aria-expanded="false" aria-controls="collapseThree16">
                        ¿Cómo cambio la información de mi cuenta?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree16" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Puedes editar tu información en tu perfil, o contactarnos para ayudarte con eso.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree17" aria-expanded="false" aria-controls="collapseThree17">
                        ¿Cómo Elicis hace dinero?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree17" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Elicsis recibe comisiones por órdenes de los restaurantes.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree18" aria-expanded="false" aria-controls="collapseThree18">
                        ¿Hay un cargo por delivery?
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree18" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    Algunos restaurantes tienen cargo por delivery, lo cual puedes ver en su perfil a la hora de ordenar.
                    </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
    </div>
</div>