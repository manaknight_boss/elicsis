<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="home">
    <style>
      #mapSection h3 {
        font-size: 24px;
        font-weight: normal;
      }
    </style>
    <header class="masthead position-relative overflow-hidden d-flex align-items-center  bg-light">

      <div class="hero-image no-pattern" style="background-image:url(/assets/image/vender-en-elicsis.jpg)"></div>

      <div class="col-md-9 mx-auto  position-relative text-center text-white">
          <h1 class="display-4 text-center mt-5 position-relative">Crece con nosotros </h1>
          <!-- <h2 class="text-center text-shadow mb-4 position-relative">Order catering from 60,504 restaurants nationwide.</h2> -->
          <!-- <button style="background: #ff6b1e; color: #ffffff; outline: none; border: 1px solid #e5601b; border-radius: 5px; padding: 8px 25px; margin-top: 15px;font-weight: 300;">Watch the video <i class="fas fa-play-circle"></i></button> -->
      </div>
    </header>

    <section id="benefitsSection" class="container mb-5">
    <div class="row ">
      <!-- col 1 -->
      <div class="col-md-6  pt-3 px-3 pt-md-5 px-md-5  overflow-hidden">
        <div class=" text-section">
          <h2 class="static-intro-header-2" style="color: #1D5D7C; padding-right: 20px;">Más pedidos.</h2>
          <p class="static-p-text" style="padding-right: 20px;">Elicsis representa la red de proveedores de catering más grande en Santo Domingo. Conectamos la demanda del mercado con nuestros socios de catering, en una relación ganar-ganar, donde los proveedores obtienen más pedidos y los clientes la mejor calidad.</p>
          <h2 class="static-intro-header-2" style="color: #1D5D7C; padding-right: 20px;">Más apoyo.</h2>
          <p class="static-p-text">Nos enfocamos en brindar el mejor servicio al cliente, además de garantizar su satisfacción. El restaurante no se debe preocupar por las llamadas telefónicas.</p>
          <h2 class="static-intro-header-2" style="color: #1D5D7C; padding-right: 20px;">Nuevos Clientes</h2>
          <p class="static-p-text">Cuando te unes a Elicsis y completas ordenes, no sólo satisfaces la necesidad del cliente, sino que también das a conocer tu marca a decenas de personas asociadas al cliente principal.</p>
        </div>
      </div>
      <!-- end col 1 -->

      <!-- col 2 -->
      <div class="col-sm-6 pt-3 px-3 pt-md-5   overflow-hidden">
        <div class="gutter " style=" padding: left 20px;">
          <div class=" text-section mb-5">
            <h2 class="static-bold-header" style="color: #1D5D7C; text-align: left; margin-bottom: 5px;">Pon tu plato en la mesa.</h2>
            <p class="static-p-text" style="color: #565a5c; margin-bottom: 0px !important;">Solo necesitamos unos cuantos detalles y listo.</p>
          </div>
          <div class="static-form text-section">
          <div class="row">
                <?php if (validation_errors()) : ?>
                  <div class="col-sm-8">
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors() ?>
                    </div>
                  </div>
                <?php endif; ?>
                <?php if (strlen($error) > 0) : ?>
                        <div class="col-sm-8">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                <?php endif; ?>
                <?php if (strlen($success) > 0) : ?>
                        <div class="col-sm-8">
                            <div class="alert alert-success" role="success">
                                <?php echo $success; ?>
                            </div>
                        </div>
                    <?php endif; ?></div>
            <?= form_open('', 'class="hs-form"') ?>
              <div class="row mb-3">
                <div class="col mobile-col">
                  <input type="text" class="form-control" name="business_name" placeholder="Nombre de la empresa">
                </div>
                <div class="col mobile-col">
                  <input type="text" class="form-control" name="zip" placeholder="Código Postal">
                </div>
              </div>
              <div class="row mb-3">
                <div class="col mobile-col">
                  <input type="text" class="form-control" name="first_name" placeholder="Nombres">
                </div>
                <div class="col mobile-col">
                  <input type="text" class="form-control" name="last_name" placeholder="Apellidos">
                </div>
              </div>
              <div class="row mb-3">
                <div class="col mobile-col">
                  <input type="email" class="form-control" name="email" placeholder="Correo Electrónico">
                </div>
                <div class="col mobile-col">
                  <input type="text" class="form-control" name="phone" placeholder="Teléfono">
                </div>
              </div>

              <input type="url" class="form-control mb-3" name="url" placeholder="Página Web de la empresa">

              <textarea class="form-control mb-3" name="comment" placeholder="Detalles" rows="5"></textarea>

              <input type="submit" value="Enviar" class="hs-button primary large">

            </form>
          </div>
        </div>
      </div>
      <!-- end col 2 -->
  </section>