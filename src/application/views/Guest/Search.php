<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
<?php
    echo 'function c () {';
    echo 'var config = ' . json_encode($data) . ';';
    echo 'var num_restaurants = ' . $num_restaurant . ';';
    echo 'return [config, num_restaurants];';
    echo '}';
    function am_or_pm ($n) {
      $minute_format = (is_numeric( $n ) && floor( $n ) != $n) ? ':30' : ':00';
      $integer_n = (int)floor($n);
      if ($integer_n == 24 || $integer_n == 0) {
          return '12' . $minute_format . ' AM';
      }
      if ($integer_n == 12) {
          return '12' . $minute_format . ' PM';
      }
      if ($integer_n < 12) {
          return $integer_n . $minute_format . ' AM';
      }

      if ($integer_n > 12) {
          return ($integer_n % 12) . $minute_format . ' PM';
      }
    }
?>
</script>
<script src="/assets/js/angular.min.js"></script>
<script src="/assets/js/spa.js"></script>
<div class="search page" ng-controller="SearchController">
<div id="navbarOrder" class="navbar navbar-expand-lg navbar-light bg-light  border-bottom fixed-top-search">
    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-filter"></i>
    </button> -->
    <a class="d-md-none" data-toggle="modal" data-target="#filterModal" aria-label="Toggle navigation">
      <small style="font-size:50%;" ng-bind-html="full_address_display">Santo Domingo</small style="font-size:50%;">
      <i class="fas fa-map-marker-alt"></i>
    </a>
    <button class="navbar-toggler" type="button" id="sideFiltersToggler" ng-click="toggleSideMenu()">
      <i class="fas fa-sliders-h"></i>
    </button>
    <div class="collapse navbar-collapse align-items-center" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item mr-4 my-3 my-md-0 d-flex flex-column justify-content-start">
        <label class="filter-label text-uppercase font-weight-bold text-primary mb-0">Ordenar Para</label>
        <div class="btn-group btn-group-sm" role="group">
            <button type="button" class="btn btn-outline-primary" ng-class="{active: filterObj.order_type == '1'}" ng-click="orderTypeChange('1')">Delivery</button>
            <button type="button" class="btn btn-outline-primary" ng-class="{active: filterObj.order_type == '2'}" ng-click="orderTypeChange('2')">Recoger</button>
        </div>
        </li>
        <li class="nav-item mr-3 mb-3 mb-md-0">
            <label class="text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
            Dirección
            </label>
            <span class="pointer d-block mt-1" data-toggle="modal" data-target="#filterModal" ng-cloak ng-if="full_address_display.length > 0"><span ng-bind-html="full_address_display"><?php echo $data['address_full'];?></span> <i class="fas fa-sort-down"></i></span>

            </label>
        </li>
        <li class="nav-item mr-3 mb-3 mb-md-0">
        <label class=" text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
        ¿Para Cuándo?
        </label>
        <span class="d-block mt-1 pointer" data-toggle="modal" data-target="#filterModal"><span ng-bind-html="filterObj.date"><?php echo $data['date'];?></span> a las <span ng-bind-html="am_or_pm(filterObj.time)"><?php echo am_or_pm($data['time']);?></span> <i class="fas fa-sort-down"></i></span>
        </li>

        <li class="nav-item mr-3 mb-3 mb-md-0">
            <label class="text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
            ¿Cuántas Personas?
            </label>
            <span class="d-block mt-1 pointer" data-toggle="modal" data-target="#filterModal"><span ng-bind-html="filterObj.serving"><?php echo $data['serving'];?></span> persona(s) <i class="fas fa-sort-down"></i></span>
        </li>

        </ul>

        <!-- <div>
        <button
        class="btn btn-outline-primary my-sm-0 text-uppercase font-weight-bold "
        id="filter-search" ng-click="filterResults()">Filtrar
        </button>
    </div> -->


    </div>
</div>


<main class="py-5 container" >
    <header class="my-5">
        <div class="d-flex flex-column flex-md-row justify-content-between align-items-center mb-3">
        <h1 class="font-weight-black h3 m-0" ng-cloak><span>{{num_restaurant}}</span> Restaurantes en Santo Domingo</h1>
        </div>

        <p class="text-gray font-italic text-special" ></p>
    </header>

    <nav class="nav-inline mt-5 mb-4">
      <ul class="nav justify-content-between">
        <li class="nav-item">
          <a class="nav-link p-0" ng-class="{active: filterObj.cuisine_id == 0}" ng-click="cuisineChange(0)" href="javascript:void(0)">Todas las Gastronomías</a>
        </li>
        <?php


        for ($i=0; $i < 4; $i++) {
          $id = $cuisines[$i]->id;
          $name = $cuisines[$i]->name;
          $active = (($data['cuisine_id'] == $id) ? 'active' : '');
          $cuisine_heredoc =<<<EOT
          <li class="nav-item">
          <a class="nav-link p-0" href="javascript:void(0)" ng-class="{active: filterObj.cuisine_id == $id}" ng-click="cuisineChange($id);">
            $name
          </a>
        </li>
EOT;
          echo $cuisine_heredoc;
        }
        ?>
        <li class="nav-item ">
          <div class="dropdown">
            <button class="nav-link p-0 dropdown-toggle" style="border:none;" type="button" id="mas" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Más
            </button>
            <div class="dropdown-mas dropdown-col dropdown-menu dropdown-menu-right" aria-labelledby="mas">
            <?php
            for ($i=4; $i < count($cuisines); $i++) {
              $id = $cuisines[$i]->id;
              $name = $cuisines[$i]->name;

              $cuisine_dropdown_heredoc =<<<EOT
              <button class="dropdown-item" type="button" ng-class="{active: filterObj.cuisine_id == $id}" ng-click="cuisineChange($id);">$name</button>
EOT;
              echo $cuisine_dropdown_heredoc;
            }
            ?>
            </div>
          </div>
        </li>
      </ul>
    </nav>

    <div class="row">
      <aside id="sideFilters" class="col-md-3 mb-5" ng-class="{'show': sidemenu}">
        <button id="closeSideFilters" class="close" ng-click="toggleSideMenu()" >&times;</button>
        <label class="font-weight-black text-uppercase">Buscar</label>
        <input type="text" class="form-control font-italic" ng-model="filterObj.search" ng-change="searchChange()" placeholder="Gastronomía, Preferencia, etc">

        <div class="my-5"></div>

        <label class="font-weight-black text-uppercase">PRECIO</label>
        <div class="custom-control custom-radio" ng-click="priceChange('')">
          <input type="radio" value="" name="price" ng-checked="filterObj.price == ''" class="custom-control-input" >
          <label class="custom-control-label" for="customRadio1" ng-click="priceChange('')">Todos</label>
        </div>
        <?php
          foreach ($pricing as $key => $value) {
              $pricing_heredoc =<<<EOT
              <div class="custom-control custom-radio my-2" ng-click="priceChange('$value')">
              <input type="radio" name="price" value="$value" ng-checked="filterObj.price == '$value'" class="custom-control-input"  />
              <label class="custom-control-label" for="customRadio" ng-click="priceChange('$value')">$value </label>
              </div>
EOT;
              echo $pricing_heredoc;
            }
            ?>

        <div class="my-5"></div>

        <label class="font-weight-black text-uppercase">PEDIDO MÍNIMO </label>
        <div class="custom-control custom-radio" ng-click="deliveryChange(0)">
          <input type="radio" value="0" name="delivery"  ng-checked="filterObj.delivery == 0"  class="custom-control-input" >
          <label class="custom-control-label" for="customRadio1">Todos</label>
        </div>
        <?php
          foreach ($delivery_minimum as $key => $value) {
              $delivery_minimum_heredoc =<<<EOT
              <div class="custom-control custom-radio my-2" ng-click="deliveryChange($value)">
              <input type="radio" name="delivery" ng-checked="filterObj.delivery == $value" value="$value" class="custom-control-input" />
              <label class="custom-control-label" for="customRadio" ng-click="deliveryChange($value)">DOP $value (o menos)</label>
              </div>
EOT;
              echo $delivery_minimum_heredoc;
            }
            ?>
        <div class="my-5"></div>

        <label class="font-weight-black text-uppercase">DISTANCIA</label>
        <div class="custom-control custom-radio"  ng-click="distanceChange(0)">
          <input type="radio" value="" name="distance"  ng-checked="filterObj.distance == 0"  class="custom-control-input" />
          <label class="custom-control-label" for="customRadio1">Todos</label>
        </div>
        <?php
          foreach ($distance as $key => $value) {
              $distance_heredoc =<<<EOT
              <div class="custom-control custom-radio my-2"  ng-click="distanceChange($value)">
              <input type="radio" name="distance" ng-checked="filterObj.distance == $value" value="$value" class="custom-control-input" />
              <label class="custom-control-label" for="customRadio" ng-click="distanceChange($value)">$value KM (o menos)</label>
              </div>
EOT;

              echo $distance_heredoc;
            }
            ?>
        <div class="my-5"></div>

        <label class="font-weight-black text-uppercase">CALIFICACIÓN</label>
        <div class="custom-control custom-radio" ng-click="ratingChange(0)">
          <input type="radio" value="" name="rating" ng-checked="filterObj.rating == 0"  class="custom-control-input" />
          <label class="custom-control-label" for="customRadio1">Todos</label>
        </div>
        <?php
          foreach ($rating as $key => $value) {
              $star = '';
              for ($i=0; $i < $value ; $i++) {
                $star = $star . '<i class="fas fa-star"></i>';
              }
              $rating_heredoc =<<<EOT
              <div class="custom-control custom-radio my-2"   ng-click="ratingChange($value)">
              <input type="radio" name="rating" ng-checked="filterObj.rating == $value" value="$value" class="custom-control-input"/>
              <label class="custom-control-label" for="customRadio" ng-click="ratingChange($value)">$star</label>
              </div>
EOT;

              echo $rating_heredoc;
            }
            ?>
      </aside>


    <section class="col-md-9 search-results" ng-cloak>
        <div ng-if="ready && items.length > 0">
          <div class="filter-search-box" ng-show="filterChanged">
            <div class="row" >
              <div class="col-md-8 col-sm-12 col-12">
                  <span>Mostrando
                  <span><strong>1-{{items.length}}</strong> de <strong>{{num_restaurant}} restaurantes</strong>
                  </span>
                  que coinciden con sus filtros
                </span>
              </div>
              <div class="col-md-4  col-sm-12 col-12 text-right">
              <button class="btn btn-info btn-sm" ng-click="clear()">Limpiar Filtros <i class="far fa-times-circle"></i></button>
              <div class="clearfix"></div>
              </div>
            </div>
           </div>
          <div ng-repeat="restaurant in items">
          <!-- card starts -->
          <article class="card flex-md-row rounded-0">
              <figure class="position-relative cater-thumbnail mb-0">
                  <img src="{{restaurant.image}}" class="mw-100 position-abolute object-fit-cover  px-0 rounded-0" alt="">
              </figure>
              <div class="card-body px-0 px-md-3 py-md-0 d-flex flex-column justify-content-between">
                  <header>
                      <h3 class="h5 font-weight-black"><a href="javascript:void(0)" ng-click="openRestaurant(restaurant)">{{restaurant.title}}</a></h3>
                      <p class="card-text">{{restaurant.description}} - {{restaurant.distance | number: 2}} km - {{restaurant.price_range}}</p>
                  </header>

                  <div class="">

                  <span class="d-inline-block mr-3"><b>DOP {{restaurant.food_minimum}}</b> Pedido mínimo </span>
                  <span class="d-inline-block mr-3"><b>DOP {{restaurant.delivery_fee}}</b> Delivery </span><br/>
                  <span class="d-inline-block mr-3"><b>{{restaurant.on_time_rating}}%</b> Entrega puntual </span>
                  </div>
              </div>
              <footer class="d-flex flex-row flex-md-column justify-content-between justify-content-md-start align-items-end">
                  <div class="mb-md-3">
                      <i class="far fa-thumbs-up mr-2 text-primary"></i>
                      <span class="text-primary">
                          <span ng-bind-html="restaurant.rating_star"></span>
                          <span class="text-primary font-weight-bold">{{restaurant.rating | number: 2}}</span>
                      </span>

                      <span class="text-primary d-block text-right" ng-if="restaurant.num_review > 1">({{restaurant.num_review}} Calificaciones)</span>
                  </div>
                  <button class="btn py-1 btn-secondary font-weight-bold" ng-click="openRestaurant(restaurant)">Seleccionar</button>
              </footer>
          </article>
          <!-- card ends -->
          </div>
          <nav class="mt-5" aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item"><a class="page-link" ng-if="hasPrev" ng-click="prev()" href="javascript:void(0)">Anterior</a></li>
            <li class="page-item" ng-class="{active: one == page}" ng-repeat="one in pageNumList">
              <a class="page-link" href="javascript:void(0)" ng-click="choosePage(one)">{{one + 1}}</a>
            </li>
            <li class="page-item"><a class="page-link" ng-if="hasNext" ng-click="next($scope.page)" href="javascript:void(0)">Próxima</a></li>
          </ul>
          </nav>
        </div>
        <div ng-if="ready && items.length == 0" class="text-center">
            <br/>
            <p>No encontramos Restaurantes disponibles en la zona</p>
            <br/>
        </div>
        <div>
        </div>
    </section>

    </div>
</main>
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filtrar Busqueda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item  mb-3 ">
                        <label class="text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
                            Dirección
                        </label>
                        <input class="address-nav-item form-control form-control-sm outline-0 font-italic" type="text" placeholder="Dirección de Entrega" id="address_string" ng-model="full_address_display" ng-change="addressChange()"/>
                    </li>

                    <li class="nav-item mb-3  d-flex flex-column justify-content-start">
                        <label class="filter-label text-uppercase font-weight-bold text-primary mb-0">Ordenar Para</label>
                        <div class="btn-group btn-group-sm" role="group">
                            <button type="button" class="btn btn-outline-primary"
                            ng-class="{active: filterObj.order_type == '1'}"
                            ng-click="orderTypeChange('1')">Delivery</button>
                            <button type="button" class="btn btn-outline-primary"
                            ng-class="{active: filterObj.order_type == '2'}"
                            ng-click="orderTypeChange('2')">Recoger</button>
                        </div>
                    </li>

                    <li class="nav-item mb-3 ">
                        <div class="row">
                            <div class="col-6">
                                <label class=" text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
                                    Fecha de Entrega
                                </label>
                                <input id="filter-date" class="form-control form-control-sm outline-0" type="text" placeholder="YYYY/MM/DD" value="" autocomplete="off"/>
                            </div>

                            <div class="col-6">
                                <label class=" text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
                                Hora
                                </label>
                                <select class="custom-select" ng-model="filterObj.time" ng-change="timeChange()">
                                <option class="tight" ng-repeat="hour in hour_key"
                                ng-selected="filterObj.time == hour"
                                value="{{hour}}">{{hour_value[$index]}}</option>
                                </select>
                            </div>

                            <div class="col-6 mt-3">
                                <label class="text-uppercase d-block mb-0 filter-label font-weight-bold text-primary">
                                    ¿Para Cuántas Personas?
                                </label>

                                <input class="form-control form-control-sm  outline-0" ng-model="filterObj.serving" ng-change="servingChange()" type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                            </div>


                        </div>
                    </li>

                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Almacenar Filtros</button>
            </div>
        </div>
    </div>
    </div>
</div>
