<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="home">
    <style>
      #mapSection h3 {
        font-size: 24px;
        font-weight: normal;
      }
    </style>
    <header class="masthead position-relative overflow-hidden d-flex align-items-center  bg-light">

      <div class="hero-image no-pattern" style="background-image:url(/assets/image/become-header2.jpg)"></div>

      <div class="col-md-9 mx-auto  position-relative text-center text-white">
          <h1 class="display-4 text-center mt-5 position-relative">Contactanos </h1>
          <!-- <h2 class="text-center text-shadow mb-4 position-relative">Order catering from 60,504 restaurants nationwide.</h2> -->
          <!-- <button style="background: #ff6b1e; color: #ffffff; outline: none; border: 1px solid #e5601b; border-radius: 5px; padding: 8px 25px; margin-top: 15px;font-weight: 300;">Watch the video <i class="fas fa-play-circle"></i></button> -->
      </div>
    </header>

  <section id="benefitsSection" class="container mb-5">
    <div class="row ">
      <!-- col 1 -->
      <div class="col-md-6  pt-3 px-3 pt-md-5 px-md-5  overflow-hidden">
        <div class=" text-section">
        <h2 class="static-intro-header-2" style="color: #3e90d6; padding-right: 20px;">Nuevos Clientes</h2>
          <p class="static-p-text">Cuando te unes a Elicsis y completas ordenes, no sólo satisfaces la necesidad del cliente, sino que también das a conocer tu marca a decenas de personas asociadas al cliente principal.</p>
        </div>
      </div>
      <!-- end col 1 -->

      <!-- col 2 -->
      <div class="col-sm-6 pt-3 px-3 pt-md-5   overflow-hidden">
        <div class="gutter " style=" padding: left 20px;">
            <div class=" text-section mb-5">
              <h2 class="static-bold-header" style="color: #88bb40; text-align: left; margin-bottom: 5px;">Tenenmos Agents listos para asistirle.</h2>
            </div>
            <div class="static-form text-section">
                <?php if (validation_errors()) : ?>
                  <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors() ?>
                    </div>
                  </div>
                <?php endif; ?>
                <?php if (strlen($error) > 0) : ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?= form_open('/contact','class="hs-form"') ?>
                <div class="row mb-3">
                  <div class="col">
                    <input type="text" class="form-control" name="first_name" placeholder="Nombres">
                  </div>
                  <div class="col">
                    <input type="text" class="form-control" name="last_name" placeholder="Apellidos">
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col">
                    <input type="email" class="form-control" name="email" placeholder="Correo Electrónico">
                  </div>
                  <div class="col">
                    <input type="text" class="form-control" name="phone" placeholder="Teléfono">
                  </div>
                </div>

                <textarea class="form-control mb-3" placeholder="Página Web de la empresa" name="comment" rows="5"></textarea>

                <input type="submit" value="Enviar" class="hs-button primary large">

              </form>
            </div>
        </div>
      </div>
    <!-- end col 2 -->
  </section>
    </div>