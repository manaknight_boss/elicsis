<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="home">
    <header class="masthead position-relative overflow-hidden d-flex align-items-center  bg-light">

        <div class="hero-image" style="background-image:url(/assets/image/nosotros1.jpg)"></div>

        <div class="col-md-9 mx-auto  position-relative text-center text-white">
            <h1 class="display-4 text-center mt-5 position-relative">Nosotros</h1>
            <!-- <h2 class="text-center text-shadow mb-4 position-relative">Order catering from 60,504 restaurants nationwide.</h2> -->
            <!-- <button style="background: #1D5D7C; color: #ffffff; outline: none; border: 1px solid #1D5D7C; border-radius: 5px; padding: 8px 25px; margin-top: 15px;font-weight: 300;">Watch the video <i class="fas fa-play-circle"></i></button> -->
        </div>
      </header>

      <section id="benefitsSection">
    <div class="row ">
      <!-- col 1 -->
      <div class="col-md-6  pt-3 px-3 pt-md-5 px-md-5  overflow-hidden">
        <div class="w-75 m-auto my-3 py-3">

          <h2 class="static-intro-header">Nuestra historia</h2>
          <p class="mb-5 lead"> Elicisis surge de una necesidad insatisfecha en nuestro país, donde la falta de información y tecnología aplicada te impedía visualizar y comparar los distintos proveedores existentes en el mercado. Fue fundada en febrero del 2019, con el objetivo de ser una fuente de información para una parte, y para otra una nueva oportunidad de crecer. </p>

          <p class="mb-5 lead"> Nos enfocamos en que el proceso de orden de comida para eventos y reuniones, así como también grupos corporativos, sea simple y rápido. En nuestra página tienes la oportunidad de hacer tus pedidos rutinarios para
            almorzar en la oficina, compartir en una festividad especial o cualquier otro tipo de velada, conectando a nuestros clientes con una red inmensa de proveedores de todo tipo de gastronomía. Además de un sitio web funcional, también
            contamos con un equipo de servicio al cliente de calidad insuperable, disponible para asistirte. </p>

          <p class="mb-5 lead"> Elicsis florece de la idea de dos estudiantes que lograron captar esta necesidad en el mercado, para ofrecer una solución factible para todo público, logrando desarrollar un sistema que mitigue cualquier situación fuera de la caja, con un servicio de calidad. </p>
        </div>
        <!-- <div class="bg-light shadow-sm mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div> -->
      </div>
      <!-- end col 1 -->

      <!-- col 2 -->
      <div class="col-md-6 d-md-flex align-items-center  overflow-hidden">
        <img src="/assets/image/nosotros2.jpg" alt="" class="w-100">

        <!-- <div class="bg-dark shadow-sm mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div> -->
      </div>
      <!-- end col 2 -->
  </section>

    <!-- <section id="mapSection" class="bg-light pt-5">
      <div class="col-md-7 mx-auto py-5">

        <div class=" text-section">
          <h2 class="static-bold-header">Community Stories</h2>
          <p class="static-p-text text-center mx-auto" style="max-width: 520px;">Learn how ezCater makes businesspeople more productive, and helps caterers and restaurants get more business.</p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <a href="#" class="hover-fade">
            <img class="mw-100" src="/assets/image/about-v1.jpg" alt="Testimonial">
          </a>
        </div>
        <div class="col-md-6">
          <a href="#" class="hover-fade">
            <img class="mw-100" src="/assets/image/about-v2.jpg" alt="Testimonial">
          </a>
        </div>
      </div>
    </section> -->
</div>