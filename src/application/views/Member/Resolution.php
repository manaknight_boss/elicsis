<?php defined('BASEPATH') OR exit('No direct script access allowed');
function get_time ($hour)
{
	$final_time = ($hour < 13) ? ($hour . ':00 AM') : ( ($hour % 12) . ':00 PM');
	if ($hour == 12)
	{
		$final_time = '12:00 PM';
	}

	if ($hour == 24)
	{
		$final_time = '12:00 AM';
	}

	return $final_time;
}
?>
<div class="container-fluid my-5">
        <div class="row ">
        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Resumen de Reclamación</h5>
                <div class="card-body">
				<h3>Restaurante de Origen</h3>
				<div><?php echo $restaurant->title;?><br/><br/></div>
				<h3>Motivo de Reclamación</h3>
				<div><?php echo $issue->notes;?><br/><br/></div>
				<h3>Resolución</h3>
				<div><?php echo $issue->resolution;?></div>
            </div>
        </div>
        </div>
		</div>
</div>