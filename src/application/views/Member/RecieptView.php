<?php
function get_time ($hour, $minute)
{
    if ($minute == 0) {
        $minute = '00';
    }

    $minute_format = ":{$minute}";

    if ($hour == 24 || $hour == 0) {
        return '12' . $minute_format . ' AM';
    }
    if ($hour == 12) {
        return '12' . $minute_format . ' PM';
    }
    if ($hour < 12) {
        return $hour . $minute_format . ' AM';
    }

    if ($hour > 12) {
        return ($hour % 12) . $minute_format . ' PM';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Comprobante</title>
    <link href="/assets/css/main.css" rel="stylesheet">
    <style>
        .table thead th {
            vertical-align: middle;
            /* border: none; */
        }

        body {
            margin: 0;
            padding: 0;
        }
        .collapse-mobile {
            display: table-cell;
        }
        .show-mobile {
            display: none;
        }
        @media screen and (max-width: 600px) {
            .collapse-mobile {
                display: none;
            }
            .show-mobile {
                display: block;
                border: none !important;
            }
        }

    </style>
</head>

<body>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <tr>
                        <th>
                            <img style="max-width:200px" src="/assets/image/Aplicaciones%20logo%20elicsis-07.png">
                            <br>
                            <small>Elcisis SRL.,</small>
                            <small>RNC#: 131-94566-1</small>
                            <small>Nicolás Penson 73, Santo Domingo</small>
                        </th>
                        <th colspan="4" scope="row">COMPROBANTE</th>

                    </tr>
                    <th class="collapse-mobile text-center" scope="col" colspan="">
                        Restaurante:
                        <div><?php echo $model->restaurant->title;?></div>
                        <small>RNC#: <?php echo $model->restaurant->rnc;?></small><br>
                        <small>Tel: <?php echo $model->restaurant->phone;?></small><br>
                        <small><?php echo $model->restaurant->address;?>, <?php echo $model->restaurant->city;?> <?php echo $model->restaurant->zip;?></small>

                    </th>

                    <th class="collapse-mobile text-center" scope="col" colspan="3">
                        Ubicacion de Entrega:
                        <div><?php echo $data['address']['name'];?></div>
                        <div><?php echo 'Apt/No. ' . $data['address']['suite'];?></div>
                        <small>Tel: <?php echo $model->phone;?></small><br/>
                        <?php echo (strlen($rnc) > 0) ? "<small>Comprobante fiscal al RNC: {$rnc} </small><br>" : '';?>
                        <small><?php echo $model->address;?>, <?php echo $model->city;?> <?php echo $model->zip;?></small>
                    </th>
                    <th class="collapse-mobile text-right">
                        Detalles de entrega:
                        <div>Orden #: <?php echo $model->id;?></div>
                        <div>Tipo: <?php echo $delivery_mapping[$model->is_delivery];?></div>
                        <div>Fecha: <?php echo $model->event_date_at;?></div>
                        <div>Hora: <?php echo get_time($model->event_hour, $model->event_minute);?></div><br/>
                    </th>

                    <th class="show-mobile text-left" scope="col" colspan="">
                        Restaurante:
                        <div><?php echo $model->restaurant->title;?></div>
                        <small>RNC#: <?php echo $model->restaurant->rnc;?></small><br>
                        <small>Tel: <?php echo $model->restaurant->phone;?></small><br>
                        <small><?php echo $model->restaurant->address;?>, <?php echo $model->restaurant->city;?> <?php echo $model->restaurant->zip;?></small>
                        <br/>
                        <br/>
                        Ubicacion de Entrega:
                        <div><?php echo $data['address']['name'];?></div>
                        <div><?php echo 'Apt/No. ' . $data['address']['suite'];?></div>
                        <small>Tel: <?php echo $model->phone;?></small>
                        <br>
                        <br>
                        <small><?php echo $model->address;?>, <?php echo $model->city;?> <?php echo $model->zip;?></small>
                        <br/>
                        <br/>
                        Detalles de entrega:
                        <div>Orden #: <?php echo $model->id;?></div>
                        <div>Tipo: <?php echo $delivery_mapping[$model->is_delivery];?></div>
                        <div>Fecha: <?php echo $model->event_date_at;?></div>
                        <div>Hora: <?php echo get_time($model->event_hour, $model->event_minute);?></div><br/>
                    </th>
                </tr>

                <tr>
                    <th colspan="4">Detalles:</th>

                    <th colspan="" class="text-right">Montos</th>
                </tr>
            </thead>



            <tbody>
                <tr>
                    <th colspan="4" scope="row">
                    <?php
                    foreach($data['items'] as $item)
                    {
                        echo '<div>' . $item['quantity'] . 'x ' . $item['title'] . '</div>';
                        echo ($item['specialInstruction'] != '') ? ('<small>Instrucciones especiales: ' . $item['specialInstruction'] . '</small>') : '';

                        if (!empty($item['addons']))
                        {
                            echo '<div>' . 'Añadir:' . '</div>'; //Translated addons: to Añadir if error restore
                            foreach($item['addons'] as $addon)
                            {
                                echo '<div> - ' . $addon['type'] . ' : ' . $addon['name'] . '</div>';
                            }
                        }
                    }
                    // Is this set here because you didn't want to set a function on line 120?
                    // Made delivery instructions smaller (Vic)
                    if (isset($data['address']['instructions']) && strlen($data['address']['instructions']) > 0) {
                        echo '<br/><br/><small><div>Instrucciones/Delivery: </small>'. $data['address']['instructions'] . '</div>';
                    }
                    ?>
                    </th>
                    <td class="text-right">DOP <?php echo number_format($data['total'], 2);?></td>
                </tr>
                <tr>
                    <th colspan="4" scope="row">Propinas</th>
                    <td class="text-right">DOP <?php echo number_format($data['real_tips'], 2);?></td>
                </tr>
                <tr>
                    <th colspan="4" scope="row" <?php echo ($data['is_delivery'] == '1' ? '' : 'style="display:none;"');?>>Delivery</th>
                    <td class="text-right">DOP <?php echo number_format($data['delivery_fee'], 2);?></td>
                </tr>
                <tr style="border-top:2px solid black;">
                    <th colspan="4" scope="row">Total Exc. Impuesto</th>
                    <td class="text-right" class="text-right">DOP <?php echo number_format($data['real_tips'] + $data['total'] + $data['delivery_fee'], 2);?></td>
                </tr>
                <tr>
                    <th colspan="4" scope="row">ITBIS</th>
                    <td class="text-right">DOP <?php echo number_format($data['tax'], 2);?></td>
                </tr>
                <tr style="border-top:2px solid black;">
                    <th style="font-size:1.5rem" colspan="4" scope="row">Total</th>
                    <td style="font-size:1.5rem" class="text-right font-weight-bold">DOP <?php echo number_format($data['real_tips'] + $data['total'] + $data['delivery_fee'] + $data['tax'], 2);?></td>
                </tr>
            </tbody>
        </table>
    </div>

</body>

</html>