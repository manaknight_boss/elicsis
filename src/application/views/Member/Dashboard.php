<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container my-5">
<form class="col-md-12 mx-auto mb-5 pb-5" action="/search">
<h2 class="text-center">Pide Catering cuando y donde quieras.</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="order-box input-icon">
        <div class="input-group mb-3 shadow-sm">
        <input type="text" id="address_string" class="form-control" placeholder="¿Donde te la entregamos?" aria-label="Introduzca una dirección" aria-describedby="button-addon2">
        <div class="input-group-append">
            <button type="submit" class="btn btn-primary text-uppercase px-4 " >
            <i class="fas fa-search d-md-none"></i>
            <span class="d-none d-md-inline">Buscar</span>
            </button>

        </div>
        </div>
    </div>
</form>
</div>