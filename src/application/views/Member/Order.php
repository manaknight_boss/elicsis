<?php defined('BASEPATH') OR exit('No direct script access allowed');
function get_time ($hour, $minute)
{
    if ($minute == 0) {
        $minute = '00';
    }

    $minute_format = ":{$minute}";

    if ($hour == 24 || $hour == 0) {
        return '12' . $minute_format . ' AM';
    }
    if ($hour == 12) {
        return '12' . $minute_format . ' PM';
    }
    if ($hour < 12) {
        return $hour . $minute_format . ' AM';
    }

    if ($hour > 12) {
        return ($hour % 12) . $minute_format . ' PM';
    }
}
?>
<div class="container-fluid my-5">
        <div class="row ">
        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12 mx-auto">
            <div class="card my-5">
                <h5 class="card-header">Pedidos</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <div>
				<small>Pedidos agendados para delivery o para recoger en tienda.<br/>
				Haz click a un pedido para ver, editar, cancelar o iniciar una reclamación.<br/>
				Todos tus pedidos serán agendado aquí.
				</small>
				<div class="table-responsive">
				<table class="table  table-hover table-condensed">
					<thead>
						<th class="no-bt">Pedido #</th>
						<th class="no-bt">Estado</th>
						<th class="no-bt">Tipo</th>
						<th class="no-bt">Fecha/Hora</th>
						<th class="no-bt">Ubicación</th>
						<th class="no-bt">Restaurante</th>
						<th class="no-bt">Acción</th>
					</thead>
					<tbody>
					<?php foreach ($list as $data) { ?>
						<?php
						echo '<tr>';
						echo '<td>' . $data->id  . '</td>';
						echo '<td>' . $mapping[$data->status] . '</td>';
						echo '<td>' . $delivery_mapping[$data->is_delivery] . '</td>';
						echo '<td>' . $data->event_date_at . '<br/>' . get_time($data->event_hour, $data->event_minute) . '</td>';
						echo '<td>' . $data->address . '<br/>' . $data->city . ', ' . $data->state . '<br/>' . $data->zip . '</td>';
						echo '<td>' . $data->restaurant_title . '</td>';
						echo '<td>';
						if ($data->is_review) {
							echo '<a class="btn btn-sm btn-primary my-1"  href="/member/orders/review/' . $data->id . '">Calificar</a><br/>';
						}
						if ($data->is_report && $data->status != 7 && $data->status != 6  && $data->status != 8 && $data->status != 9) {
							echo ' <a class="btn btn-sm btn-danger my-1"  href="/member/orders/issue/' . $data->id . '">Reclamar</a><br/>';
						}

						if ($data->status == 7 || $data->status == 9) {
							echo ' <a class="btn btn-sm btn-danger my-1"  href="/member/orders/resolution/' . $data->id . '">Resolución</a><br/>';
						}

						if ($data->is_cancel) {
							echo ' <a class="btn btn-sm btn-warning my-1"  href="/member/orders/cancel/' . $data->id . '">Cancelar</a><br/>';
						}
						if ($data->is_reorder && $data->status != 7 && $data->status != 9) {
							echo ' <a class="btn btn-sm btn-info my-1" target="__blank" href="/member/orders/reorder/' . $data->id . '">Reordenar</a>';
						}
						if ($data->is_editable) {
							echo ' <a class="btn btn-sm btn-primary my-1" target="__blank" href="/member/orders/edit/' . $data->id . '">Editar</a>';
						}
						echo '</td></tr>';
						?>
					<?php } ?>
					</tbody>
				</table>
				<p class="pagination_custom"><?php echo $links; ?></p>
				</div>
				<?php if (count($list) == 0) {
					echo '<small>No hay pedidos agendados.</small>';
				}?>
                </div>
			</div>
        </div>
			<!-- cart -->
			<div class="card my-5">
				<h5 class="card-header">Pedidos en Borrador</h5>
				<div class="card-body">
					<small>Los siguientes pedidos no han sido enviados. Haz click en uno para continuar el proceso.<br/>
					Cuando inicias un pedido y lo dejas sin terminar, sera almacenado aqui, hasta que lo envies o lo elimines.</small>
				<div class="table-responsive">
					<table class="table  table-hover table-condensed">
						<thead>
							<th class="no-bt">Fecha/Hora</th>
							<th class="no-bt">Ubicación</th>
							<th class="no-bt">Restaurante</th>
							<th class="no-bt">Total Exc. Impuesto</th>
							<th class="no-bt">ITBIS</th>
							<th class="no-bt">Total</th>
							<th class="no-bt">Acción</th>
						</thead>
						<tbody>
							<?php foreach ($cart as $data) { ?>
							<?php
								$json_data = json_decode($data->data, TRUE);
								$delivery_fee = ($json_data['is_delivery'] == '1') ? $json_data['delivery_fee'] : 0;
								echo '<tr>';
								if (isset($json_data['date'])){
									echo '<td>' . $json_data['date'] . ' ' . get_time($json_data['time'], $json_data['minute']) . '</td>';
								} else {
									echo '<td>N/A</td>';
								}
								echo '<td>' . $data->restaurant->address . ' ' . $data->restaurant->city . ' ' . $data->restaurant->state . ' ' . $data->restaurant->zip . '</td>';
								echo '<td>' . $data->restaurant->title . '</td>';
								echo '<td>' . number_format($json_data['total'], 2) . '</td>';
								echo '<td>' . number_format($json_data['tax'], 2) . '</td>';
								echo '<td>' . number_format($json_data['total'] + $json_data['tax'] + $delivery_fee + $json_data['real_tips'], 2) . '</td>';
								echo '<td>';
								echo '<a href="/checkout/';
								if ($data->is_day_time != 1) {
									echo '2';
								} else if ($data->is_address != 1) {
									echo '3';
								} else if ($data->is_payment != 1) {
									echo '4';
								} else {
									echo '4';
								}
								echo '/' . $data->id . '" class="btn btn-info btn-sm">Continuar</a>';
								echo '</td>';
								echo '</tr>';
								?>
							<?php } ?>
						</tbody>
					</table>
					<p class="pagination_custom">
						<?php echo $links; ?>
					</p>
					<?php if (count($list) == 0) {
					echo '<small>No hay pedidos en el borrador.</small>';
				}?>
				</div>
			</div>
			<!-- cart end -->
        </div>
		</div>
</div>