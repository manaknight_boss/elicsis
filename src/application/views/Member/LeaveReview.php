<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid my-5">
        <div class="row ">
        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">¿Cual fue su experiencia con este Restaurante?</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <div>
				<?= form_open(); ?>
					<div class="form-group">
						<label for="person_name">Calificación</label>
						<div id="rating"></div>
                        <input class="form-control form-control-lg" id="rating_value" type="hidden" name="rating" placeholder="" autocomplete="off" value="0">
                    </div>
                    <div class="form-group">
                        <label for="Comentario">Detalles o Comentarios</label>
                        <textarea class="form-control" id="comentario" name="comment" rows="2" col="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg">Enviar</button>
                </form>
                </div>
            </div>
        </div>
        </div>
		</div>
</div>