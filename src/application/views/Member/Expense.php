<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container my-5">
        <div class="row ">
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Reporte de consumo</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <div class="row">
					<div class="col-md-12">
						<small>Haz click en un pedido para ver su comprobante. Haz click en "Imprimir" para almacenar el comprobante.</small>
						<?= form_open() ?>
							<div class="form-group">
								<label for='name'>Fecha de Inicio</label>
								<input type='date' class='form-control' id='start_date' name='start_date' value=''/>
							</div>
							<div class="form-group">
								<label for='name'>Fecha Final</label>
								<input type='date' class='form-control' id='end_date' name='end_date' value=''/>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-primary" value="Enviar">
							</div>
						</form>
					</div>
                </div>
            </div>
        </div>
        </div>
		</div>
</div>