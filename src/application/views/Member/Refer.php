<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container my-5">
        <div class="row ">
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Invita tus amigos a Elicsis</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <div>
					<p>Comparte este enlace para invitar amigos y amigas:
						<a href="https://elicsis.com?affilate=<?php echo $code;?>">https://elicsis.com?affilate=<?php echo $code;?></a>
					</p>

					<p>Invitaciones</p>
					<div class="table-responsive">
					<table class="table  table-hover table-condensed">
					<thead>
						<th class="no-bt">Invitados</th>
						<th class="no-bt">Estado</th>
						<th class="no-bt">Fecha</th>
					</thead>
					<tbody>
					<?php foreach ($list as $data) { ?>
						<?php
						echo '<tr>';
						echo '<td>' . $data->refer_user . '</td>';
						echo '<td>' . $mapping[$data->status] . '</td>';
						echo '<td>' . date('Y-m-d', strtotime($data->created_at)) . '</td>';
						echo '</tr>';
						?>
					<?php } ?>
					</tbody>
				</table>
				<p class="pagination_custom"><?php echo $links; ?></p>
				</div>
                </div>
            </div>
        </div>
        </div>
		</div>
</div>