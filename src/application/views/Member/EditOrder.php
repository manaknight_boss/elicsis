<br/>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
                <h2>Editar Pedido</h2>
                <br/>
                <?php if (validation_errors()) : ?>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors() ?>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <?php if (strlen($error) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (strlen($success) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success" role="success">
                                <?php echo $success; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <?= form_open(); ?>
                    <div class="form-group">
                        <label for="Nombre del Local">Nombre de la Ubicación*</label>
                        <input class="form-control form-control-lg" id="name" type="text" name="name" placeholder="" autocomplete="off" value="<?php echo set_value('name', $data['address']['name']);?>">
                    </div>
                    <div class="form-group">
                        <label for="Dirección">Dirección*</label>
                        <input class="form-control form-control-lg" id="address" type="text" name="address" placeholder="" autocomplete="off" value="<?php echo set_value('address', $data['address']['address']);?>">
                    </div>
                    <div class="form-group">
                        <label for="Apartamento/Piso">Apartamento/Piso</label>
                        <input class="form-control form-control-lg" id="suite" type="text" name="suite" placeholder="" autocomplete="off" value="<?php echo set_value('suite', $data['address']['suite']);?>">
                    </div>
                    <div class="form-group">
                        <label for="Provincia">Ciudad*</label>
                        <input class="form-control form-control-lg" id="city" type="text" name="city" placeholder="" autocomplete="off" value="<?php echo set_value('city', $data['address']['city']);?>">
                    </div>
                    <!-- Remove Zip for now
                    <div class="form-group">
                        <label for="Codigo Postal">Codigo Postal</label>
                        <input class="form-control form-control-lg" id="zip" type="text" name="zip" placeholder="" autocomplete="off" value="<?php echo set_value('zip', $data['address']['zip']);?>"/>
                    </div>
                    -->
                    <div class="form-group">
                        <label for="person_name">¿A quien se le hará la entrega?</label>
                        <input class="form-control form-control-lg" id="person_name" type="text" name="person_name" placeholder="" autocomplete="off" value="<?php echo set_value('person_name', $data['address']['person_name']);?>">
                    </div>
                    <div class="form-group">
                        <label for="Número de teléfono">Número de teléfono*</label>
                        <input class="form-control form-control-lg" id="phone" type="text" readonly name="phone" placeholder="" autocomplete="off" value="<?php echo $model->phone;?>">
                    </div>
                    <div class="form-group">
                        <label for="Instrucciones para el delivery">Instrucciones para el delivery</label>
                        <textarea class="form-control" id="instructions" name="instructions" rows="2" col="10"><?php echo $data['address']['instructions'];?></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg">Siguiente</button>
                </form>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
