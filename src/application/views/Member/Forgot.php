<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Actualizar Contraseña</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/vendor.css">
    <link rel="stylesheet" href="/assets/css/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="/assets/css/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/assets/css/fonts/flag-icon-css/flag-icon.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }

	a:hover {
		color: black;
		text-decoration: underline;
	}
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
            <a href="/">
                <img src="/assets/image/Aplicaciones logo elicsis-07.png" alt="" srcset="" style="width:100px;">
            </a>
            <h3>Olvidé mi Contraseña</h3>
			</div>
			<?php if (validation_errors()) : ?>
			<br>
			<div class="alert alert-danger" role="alert">
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>
			<?php if (isset($error)) : ?>
				<br>
				<div class="alert alert-danger" role="alert">
					<?php echo $error; ?>
				</div>
			<?php endif; ?>
			<?php if (isset($success)) : ?>
				<br>
				<div class="alert alert-success" role="alert">
					<?php echo $success; ?>
				</div>
			<?php endif; ?>
            <div class="card-body">
				<?= form_open() ?>
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="email" type="email" name="email" placeholder="Correo" autocomplete="off">
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
                </form>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="/assets/js/vendor.js"></script>
    <script src="/assets/js/guest.js"></script>
</body>

</html>