<?php
function get_time ($hour, $minute)
{
    if ($minute == 0) {
        $minute = '00';
    }

    $minute_format = ":{$minute}";

    if ($hour == 24 || $hour == 0) {
        return '12' . $minute_format . ' AM';
    }
    if ($hour == 12) {
        return '12' . $minute_format . ' PM';
    }
    if ($hour < 12) {
        return $hour . $minute_format . ' AM';
    }

    if ($hour > 12) {
        return ($hour % 12) . $minute_format . ' PM';
    }
}
?>
<br/>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
                <h2>Tome un segundo para Revisar y Confirmar su Pedido</h2>
                <?php if (validation_errors()) : ?>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors() ?>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <?php if (strlen($error) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (strlen($success) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success" role="success">
                                <?php echo $success; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <br/>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <h3>Ubicación de Entrega <small><a href="/checkout/3/<?php echo $cart->id;?>?f=edit">Editar</a></small></h3>
                    <div><?php echo $data['address']['name'];?></div>
                    <div><?php echo $data['address']['address'] . (strlen($data['address']['suite']) > 0 ? (' Apt/No. ' . $data['address']['suite'] ) : '');?></div>
                    <div><?php echo $data['address']['city'] . ' ' . $data['address']['zip'];?></div>
                    <div><?php echo $data['address']['phone'];?></div>
                    <?php if (strlen($data['address']['instructions']) > 0) {
                         echo  '<p>Instrucciones: ' . $data['address']['instructions'] . '</p>';
                    }?>
                </div>
                <div class="col-md-6">
                    <h3>Fecha & Hora <small><a href="/checkout/2/<?php echo $cart->id;?>?f=edit">Editar</a></small></h3>
                    <div><?php echo date("F j, Y", strtotime($data['date'])) . ' ' . get_time($data['time'], $data['minute']);?></div>
                </div>
                <div class="col-md-6">
                    <br/>
                    <h3>¿A quien se le hara entrega? <small><a href="/checkout/3/<?php echo $cart->id;?>?f=edit">Editar</a></small></h3>
                    <div><?php echo strlen($data['address']['person_name']) > 0 ? ($data['address']['person_name']): 'N\A';?> </div>
                    <br/>
                </div>
                <div class="col-md-6">
                    <br/>
                    <h3>Método de Pago <small><a href="/checkout/4/<?php echo $cart->id;?>?f=edit">Editar</a></small></h3>
                    <div>Tarjeta</div>
                    <br/>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col" colspan="2">Detalles del Pedido:</th>
                            <th scope="col">Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row" colspan="2">
                            <?php
                            foreach($data['items'] as $item) {
                                echo '<div>' . $item['quantity'] . 'x ' . $item['title'] . '</div>';
                                echo ($item['specialInstruction'] != '') ? ('<small>Instrucciones: ' . $item['specialInstruction'] . '</small>') : '';
                            }
                            ?>
                            </th>
                            <td>DOP <?php echo number_format($data['total'], 2);?></td>
                        </tr>
                        <tr>
                            <th scope="row" colspan="2" >Propinas</th>
                            <td>DOP <?php echo number_format($data['real_tips'], 2);?></td>
                        </tr>
                        <tr <?php echo ($data['is_delivery'] == '1' ? '' : 'style="display:none;"');?>>
                                <th scope="row" colspan="2" >Delivery</th>
                                <td>DOP <?php echo number_format($data['delivery_fee'], 2);?></td>
                        </tr>
                        <tr <?php echo ($data['is_delivery'] == '0' ? '' : 'style="display:none;"');?>>
                                <th scope="row" colspan="2" >Recoger</th>
                                <td>DOP <?php echo number_format(0, 2);?></td>
                        </tr>
                        <tr style="border-top:2px solid black;">
                            <th scope="row" colspan="2" >Total Exc. Impuesto</th>
                            <td>DOP <?php echo number_format($data['real_tips'] + $data['total'] + $data['delivery_fee'], 2);?></td>
                        </tr>
                        <tr>
                            <th scope="row" colspan="2" >ITBIS</th>
                            <td>DOP <?php echo number_format($data['tax'], 2);?></td>
                        </tr>
                        <tr style="border-top:2px solid black;">
                            <th scope="row" colspan="2" >Total</th>
                            <td>DOP <?php echo number_format($data['real_tips'] + $data['total'] + $data['delivery_fee'] + $data['tax'], 2);?></td>
                        </tr>
                    </tbody>
                    </table>

                </div>
                <?= form_open(); ?>
                <button type="submit" value="submit" name="submit" class="btn btn-primary btn-lg">Siguiente</button>
                </form>
            </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
