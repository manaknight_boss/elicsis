<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container my-5">
        <div class="row ">
        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Calificaciones</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <div class="table-responsive">
					<small>Los restaurantes aparecerán aquí después que los pedidos sean entregados.</small>
					<table class="table  table-hover table-condensed">
					<thead>
						<th class="no-bt">Pedido #</th>
						<th class="no-bt">Restaurante</th>
						<th class="no-bt">Calificación</th>
						<th class="no-bt">Comentario</th>
					</thead>
					<tbody>
					<?php foreach ($list as $data) { ?>
						<?php
						echo '<tr>';
						echo '<td>' . $data->order_id . '</td>';
						echo '<td>' . $data->restaurant->title . '</td>';
						echo '<td>' . $data->rating . '</td>';
						echo '<td>' . $data->message . '</td>';
						echo '</tr>';
						?>
					<?php } ?>
					</tbody>
				</table>
				<p class="pagination_custom"><?php echo $links; ?></p>
				<?php if (count($list) == 0) {
					echo '<small>Tomar en cuenta que solo puedes calificar pedidos realizados en los últimos 45 días..</small>';
				}?>
                </div>
            </div>
        </div>
        </div>
		</div>
</div>