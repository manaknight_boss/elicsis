<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container my-5">
        <div class="row ">
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Métodos de Pago</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <div>
										<div class="table-responsive">
											<small>Para su conveniencia, todos sus métodos de pago se almacenan aquí utilizando la bóveda de datos Azul del Banco Popular. Si lo prefiere, puede eliminarlos una vez que se complete su pedido.</small>
											<table class="table  table-hover table-condensed">
											<thead>
												<th class="no-bt">Marca</th>
												<th class="no-bt">Número de tarjeta</th>
												<th class="no-bt">Fecha Exp.</th>
												<th class="no-bt">Creada</th>
												<th class="no-bt">Acción</th>
											</thead>
											<tbody>
											<?php foreach ($list as $data) { ?>
												<?php
												echo '<tr>';
												echo '<td>' . $data->brand . '</td>';
												echo '<td>' . $data->card_number . '</td>';
												echo '<td>' . $data->expire_date . '</td>';
												echo '<td>' . $data->create_at . '</td>';
												echo '<td><a class="btn btn-sm btn-primary my-1"  href="/member/card/remove/' . $data->id . '">Remover</a></td>';
												echo '</tr>';
												?>
											<?php } ?>
											</tbody>
										</table>
									</div>

                </div>
            </div>
        </div>
        </div>
		</div>
</div>