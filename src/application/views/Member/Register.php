
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Registrar Cuenta</title>
    <link href="/assets/css/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/vendor.css">
    <link rel="stylesheet" href="/assets/css/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="/assets/css/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/assets/css/fonts/flag-icon-css/flag-icon.min.css">
    <link rel="stylesheet" href="/assets/css/portal.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>
<!-- ============================================================== -->
<!-- signup form  -->
<!-- ============================================================== -->

<body>
    <!-- ============================================================== -->
    <!-- signup form  -->
    <!-- ============================================================== -->
	<?= form_open('', 'class="splash-container"') ?>
        <div class="card">
            <div class="card-header">
                <h3 class="mb-1">Registrar Cuenta</h3>
                <p>Por favor Introduzca un corréo y Contraseña.</p>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo validation_errors(); ?>
                </div>
                <?php endif; ?>
                <?php if (isset($error)) : ?>
                    <br>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>
                <div class="form-group">
                    <input class="form-control form-control-lg" type="email" name="email" required="" placeholder="Correo" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="password" name="password" type="password" required="" placeholder="Contraseña">
                </div>
                <div class="form-group">
                    <input type="text" id='first_name' name='first_name' class='form-control form-control-lg' placeholder="Nombres" value=""/>
                </div>
                <div class="form-group">
                    <input type="text" id='last_name' name='last_name' class='form-control form-control-lg' placeholder="Apellidos" value=""/>
                </div>
                <div class="form-group">
                    <input type="text" id='phone' name='phone' class='form-control form-control-lg' placeholder="Teléfono" value=""/>
                </div>
                <div class="form-group pt-2">
                    <button class="btn btn-block btn-primary" type="submit">Registrar</button>
                </div>
            </div>
            <div class="card-footer bg-white">
                <p>¿Ya tienes cuenta? <a href="/member/login" class="text-secondary">Iniciar Sesión.</a></p>
            </div>
        </div>
    </form>
</body>


</html>