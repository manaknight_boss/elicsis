<?php defined('BASEPATH') OR exit('No direct script access allowed');
function get_time ($hour)
{
	$final_time = ($hour < 13) ? ($hour . ':00 AM') : ( ($hour % 12) . ':00 PM');
	if ($hour == 12)
	{
		$final_time = '12:00 PM';
	}

	if ($hour == 24)
	{
		$final_time = '12:00 AM';
	}

	return $final_time;
}
?>
<div class="container-fluid my-5">
        <div class="row ">
        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Iniciar una reclamación</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <div>
				<?= form_open(); ?>
                    <div class="form-group">
                        <label for="problema">Queja/Reclamo</label>
                        <textarea class="form-control" id="problema" name="problem" rows="2" col="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg">Enviar</button>
                </form>
                </div>
            </div>
        </div>
        </div>
		</div>
</div>