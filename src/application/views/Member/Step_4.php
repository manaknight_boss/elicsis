<br/>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-6">
                <?php if (validation_errors()) : ?>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors() ?>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <?php if (strlen($error) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (strlen($success) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success" role="success">
                                <?php echo $success; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <div id="azhul-payment-form"></div>

                <?= form_open('', array('class' => 'd-none')); ?>
                    <input type="hidden" name="token" id="token" value=""/>
                    <input type="hidden" name="card" id="card" value=""/>
                    <button type="submit" id="save-token" class="hide">Save</button>
                </form>
        </div>
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col" colspan="2">Detalles del Pedido:</th>
                            <th scope="col">Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row" colspan="2">
                            <?php
                            foreach($data['items'] as $item) {
                                echo '<div>' . $item['quantity'] . 'x ' . $item['title'] . '</div>';
                                echo ($item['specialInstruction'] != '') ? ('<small>Instrucciones: ' . $item['specialInstruction'] . '</small>') : '';
                            }
                            ?>
                            </th>
                            <td>DOP <?php echo number_format($data['total'], 2);?></td>
                        </tr>
                        <tr>
                            <th scope="row" colspan="2" >Propinas</th>
                            <td>DOP <?php echo number_format($data['real_tips'], 2);?></td>
                        </tr>
                        <tr <?php echo ($data['is_delivery'] == '1' ? '' : 'style="display:none;"');?>>
                                <th scope="row" colspan="2" >Delivery</th>
                                <td>DOP <?php echo number_format($data['delivery_fee'], 2);?></td>
                        </tr>
                        <tr <?php echo ($data['is_delivery'] == '0' ? '' : 'style="display:none;"');?>>
                                <th scope="row" colspan="2" >Recoger</th>
                                <td>DOP <?php echo number_format(0, 2);?></td>
                        </tr>
                        <tr style="border-top:2px solid black;">
                            <th scope="row" colspan="2" >Total Exc. Impuesto</th>
                            <td>DOP <?php echo number_format($data['real_tips'] + $data['total'] + $data['delivery_fee'], 2);?></td>
                        </tr>
                        <tr>
                            <th scope="row" colspan="2" >ITBIS</th>
                            <td>DOP <?php echo number_format($data['tax'], 2);?></td>
                        </tr>
                        <tr style="border-top:2px solid black;">
                            <th scope="row" colspan="2" >Total</th>
                            <td>DOP <?php echo number_format($data['real_tips'] + $data['total'] + $data['delivery_fee'] + $data['tax'], 2);?></td>
                        </tr>
                    </tbody>
                    </table>

                </div>
            </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<script>
azhulForm({
    title: 'Checkout',
    id: 'azhul-payment-form',
    total: <?php echo $data['real_tips'] + $data['total'] + $data['delivery_fee'] + $data['tax'];?>,
    currency: 'DOP',
    url: <?php echo "'" . $payment_url . "'"; ?>,
    key: <?php echo "'" . $payment_key . "'"; ?>,
    cards: <?php

    if (count($cards) < 1) {
        echo '[]';
    } else {
        echo '[';
        foreach ($cards as $key => $card) {
            echo "{'brand': '{$card->brand}', 'card_number': '{$card->card_number}', 'id': {$card->id}}";
            if (count($cards) - 1 != $key)
            {
                echo ',';
            }
        }
        echo ']';
    }
    ?>,
},  function(err, data) {
    if (err) {
        return alert(data.message);
    } else {
        if (isNaN(data)) {
            document.getElementById('token').value = data;
            document.getElementById('save-token').click();
        } else {
            document.getElementById('token').value = "7b224272616e64223a202244756d6d79222c22436172644e756d626572223a202244756d6d79222c22446174615661756c74546f6b656e223a202244756d6d79222c2245787069726174696f6e223a202244756d6d79227d.3d33d088127e837f8db75424078b725d";
            document.getElementById('card').value = data;
            document.getElementById('save-token').click();
        }
    }
});
</script>