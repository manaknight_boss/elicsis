<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container my-5">
        <div class="row ">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Editar perfil</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <?= form_open() ?>
				<div class="form-group">
					<label class='col-form-label' for='email'>Correo </label>
						<input type='email' class='form-control' id='email' name='email' value='<?php echo set_value('email', $model->email); ?>'/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='password'>Contraseña </label>
						<input type='password' class='form-control' id='password' name='password' value=''/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='first_name'>Nombres </label>
						<input type="text" id='first_name' name='first_name' class='form-control' value="<?php echo set_value('first_name', $model->first_name); ?>"/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='last_name'>Apellidos </label>
					<input type="text" id='first_name' name='last_name' class='form-control' value="<?php echo set_value('last_name', $model->last_name); ?>"/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='business_name'>Nombre de la empresa </label>
					<input type="text" id='business_name' name='business_name' class='form-control' value="<?php echo set_value('last_name', $model->last_name); ?>"/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='type'>Tipo/Perfil </label>
					<input type="text" id='type' readonly="true" name='type' class='form-control' value="<?php echo set_value('profile_type', $model->profile_type); ?>"/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='phone'>Teléfono </label>
					<input type="text" id='phone' name='phone' class='form-control' value="<?php echo set_value('phone', $model->phone); ?>"/>
				</div>
				<div class="form-group">
					<label class='col-form-label' for='rnc'>RNC # </label>
					<input type="text" id='rnc' name='rnc' class='form-control' value="<?php echo set_value('rnc', $model->rnc); ?>"/>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
                </form>
            </div>
        </div>
        </div>
		</div>
</div>