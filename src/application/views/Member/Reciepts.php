<?php defined('BASEPATH') OR exit('No direct script access allowed');
function get_time ($hour, $minute)
{
    if ($minute == 0) {
        $minute = '00';
    }

    $minute_format = ":{$minute}";

    if ($hour == 24 || $hour == 0) {
        return '12' . $minute_format . ' AM';
    }
    if ($hour == 12) {
        return '12' . $minute_format . ' PM';
    }
    if ($hour < 12) {
        return $hour . $minute_format . ' AM';
    }

    if ($hour > 12) {
        return ($hour % 12) . $minute_format . ' PM';
    }
}
function order_row ($data) {
	$json_data = json_decode($data->data, TRUE);
	$html = '';
	$html .= '<div> Orden #: ' . $data->id . '</div>';
	$html .= '<div> Restaurante:<br/> ' . $data->restaurant->title . '</div>';
	$html .= '<div> Fecha: ' . $data->event_date_at . '</div>';
	$html .= '<div> Hora: ' . get_time($data->event_hour, $data->event_minute) . '</div>';
	if ($json_data['is_delivery'] == '1') {
		$html .= '<div>Delivery</div>';
	}
	else
	{
		$html .= '<div> Recoger</div>';
	}
	return $html;
}
?>
<div class="container my-5">
        <div class="row ">
        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Comprobantes</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<div class="table-responsive">
					<table class="table  table-hover table-condensed">
						<thead>
							<th class="no-bt">Pedidos</th>
							<th class="no-bt">Tipo</th>
							<th class="no-bt">Total Exc. Impuesto</th>
							<th class="no-bt">ITBIS</th>
							<th class="no-bt">Total</th>
							<th class="no-bt">Acción</th>
						</thead>
						<tbody>
							<?php foreach ($list as $data) { ?>
							<?php
								echo '<tr>';
								echo '<td>' . order_row($data) . '</td>';
								echo '<td>' . $delivery_mapping[$data->is_delivery] . '</td>';
								echo '<td>' . number_format($data->amount, 2) . '</td>';
								echo '<td>' . number_format($data->tax, 2) . '</td>';
								echo '<td>' . number_format($data->total, 2) . '</td>';
								echo '<td> <a href="/member/reciepts/view/' . $data->id . '" class="btn btn-info">Detalles</a></td>';
								echo '</tr>';
								?>
							<?php } ?>
						</tbody>
					</table>
							<p class="pagination_custom">
								<?php echo $links; ?>
							</p>
				</div>
            </div>
        </div>
        </div>
		</div>
</div>