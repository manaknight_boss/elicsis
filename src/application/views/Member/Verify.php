<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container my-5">
        <div class="row ">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Verificar</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<p>No reconocemos este dispositivo.</p>
				<p>Necesitamos confirmar que en realidad eres tú antes de permitir cambios a tus datos personales.
					Introduce tu numero de teléfono para recibir un código de confirmación. <small>*Recuerda incluir 1+ el número ej: 18298882222</small>
				</p>
                <?= form_open() ?>
				<div class="form-group">
					<label class='col-form-label' for='phone'>Teléfono </label>
					<input type="text" id='phone' name='phone' class='form-control' value="<?php echo set_value('phone', $model->phone); ?>"/>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Verificar">
				</div>
                </form>
            </div>
        </div>
        </div>
		</div>
</div>