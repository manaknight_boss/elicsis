<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container my-5">
        <div class="row ">
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 mx-auto">
            <div class="card">
                <h5 class="card-header">Sugerencias</h5>
                <div class="card-body">
				<?php if (validation_errors()) : ?>
					<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
						<div class="alert alert-danger" role="alert">
							<?= validation_errors() ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($error) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-danger" role="alert">
								<?php echo $error; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php if (strlen($success) > 0) : ?>
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="alert alert-success" role="success">
								<?php echo $success; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
                <div class="row">
					<div class="col-md-12">
						<small>¿Conoces un Restaurante que te gustaría ver aquí? Cuéntanos y haremos todo lo posible para traerlo a Elicsis.</small>
						<?= form_open() ?>
							<div class="form-group">
								<label for='business_name'>Nombre del Restaurante </label>
									<input type='text' class='form-control' id='business_name' name='name' value=''/>
									</div>
							<div class="form-group">
								<label for='location'>Ubicación </label>
									<input type='text' class='form-control' id='location' name='location' value=''/>
									</div>
							<div class="form-group">
								<label for='phone'>Teléfono </label>
									<input type='text' class='form-control' id='phone' name='phone' value=''/>
									</div>
							<div class="form-group">
								<label for='note'>Detalles </label>
									<textarea id='note' name='note' class='form-control' rows='10'></textarea>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-primary" value="Enviar">
							</div>
						</form>
					</div>
                </div>
            </div>
        </div>
        </div>
		</div>
</div>