<br/>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-8">
                <h2>Confirmar Fecha y Hora de Entrega</h2>
                <br/>
                <?php if (validation_errors()) : ?>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-danger" role="alert">
                      <?php echo 'La Fecha es requerida para continuar'; ?>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <?php if (strlen($error) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (strlen($success) > 0) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success" role="success">
                                <?php echo $success; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <?= form_open(); ?>
                    <div class="form-group">
                        <label for="Event Date">Fecha de Entrega</label>
                        <input class="form-control form-control-lg" id="date" type="date" name="date" placeholder="date" autocomplete="off" min="<?php echo date('Y-m-d');?>" value="<?php echo (strlen($date) > 0) ? $date : date('Y-m-d', strtotime('+1 day'));?>">
                    </div>
                    <div class="form-group">
                        <label for="Event Date">Hora de Entrega</label>
                        <select id="time" name='time' class="custom-select custom-select-lg mb-3" >
                            <?php for ($i=1; $i < 25; $i++) {
								$v = ($i < 13) ? ($i . ':00 AM') : ( ($i % 12) . ':00 PM');
								$v2 = ($i < 13) ? ($i . ':30 AM') : ( ($i % 12) . ':30 PM');
								if ($i == 12) {
									$v = '12:00 PM';
									$v2 = '12:30 PM';
								}
								if ($i == 24) {
									$v = '12:00 AM';
								}
								$selected = '';

                                if ($time != 0 && $i == $time)
                                {
                                    $selected = 'selected';
                                }
								echo "<option value='{$i}' {$selected}> {$v} </option>";
								if ($i != 24) {
									$selected_30 = ($time != 0 && $i == $time) ? 'selected' : '';
									echo "<option value='" . ($i + 0.5) . "' {$selected_30}> {$v2} </option>";
								}
							}?>
							</select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg">Siguiente</button>
                </form>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
