<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Actualizar Contraseña</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }

	a:hover {
		color: black;
		text-decoration: underline;
	}
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
            <a href="/">
                <img src="/assets/image/Aplicaciones logo elicsis-07.png" alt="" srcset="" style="width:100px;">
            </a>
            <h3>Actualizar Contraseña</h3>
			</div>
			<?php if (validation_errors()) : ?>
			<br>
			<div class="alert alert-danger" role="alert">
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>
			<?php if (isset($error)) : ?>
				<br>
				<div class="alert alert-danger" role="alert">
					<?php echo $error; ?>
				</div>
			<?php endif; ?>
			<?php if (isset($success)) : ?>
				<br>
				<div class="alert alert-success" role="alert">
					<?php echo $success; ?>
				</div>
			<?php endif; ?>
            <div class="card-body">
				<?= form_open() ?>
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="password" type="password" name="password" placeholder="Contraseña" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="confirm_password" type="password" name="confirm_password" placeholder="Confirmar Contraseña" autocomplete="off">
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
                </form>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
</body>

</html>