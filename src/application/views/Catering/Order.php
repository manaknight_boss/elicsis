<?php defined('BASEPATH') OR exit('No direct script access allowed');
function get_time ($hour, $minute)
{
    if ($minute == 0) {
        $minute = '00';
    }

    $minute_format = ":{$minute}";

    if ($hour == 24 || $hour == 0) {
        return '12' . $minute_format . ' AM';
    }
    if ($hour == 12) {
        return '12' . $minute_format . ' PM';
    }
    if ($hour < 12) {
        return $hour . $minute_format . ' AM';
    }

    if ($hour > 12) {
        return ($hour % 12) . $minute_format . ' PM';
    }
}
?>
<h2>Pedidos</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Filtrar Pedidos</h5>
            <div class="card-body">
				<?= form_open() ?>
				<div class="row">
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='order_id'># Pedido </label>
							<input type='number' class='form-control' id='order_id' name='order_id' value='<?php echo $order_id;?>'/>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='date'>Fecha </label>
							<input type='date' class='form-control' id='event_date_at' name='event_date_at' value='<?php echo $event_date_at;?>'/>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='phone'>Teléfono </label>
							<input type='text' class='form-control' id='phone' name='phone' value='<?php echo $phone;?>'/>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='status'>Estado </label>
							<select name='status' class='form-control'>
								<option value=''>Todos</option>
							<?php
							foreach ($mapping as $key => $value) {
								echo "<option value='{$key}' " . (($status == $key && $status != '') ? 'selected' : '') . "> {$value} </option>";
							}
							?>
							</select>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Enviar">
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
</div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Pedidos</h5>
            <div class="card-body">
			<div class="table-responsive">
            <table class="table  table-hover table-condensed">
					<thead>
						<th class="no-bt"># Pedido</th>
						<th class="no-bt">Estado</th>
						<th class="no-bt">Tipo</th>
						<th class="no-bt">Fecha/Hora</th>
						<th class="no-bt">Ubicación</th>
						<th class="no-bt">Total</th>
						<th class="no-bt">Acción</th>
					</thead>
					<tbody>
					<?php foreach ($list as $data) { ?>
						<?php
						echo '<tr>';
						echo '<td>' . $data->id  . '</td>';
						echo '<td>' . $mapping[$data->status] . '</td>';
						echo '<td>' . $delivery_mapping[$data->is_delivery] . '</td>';
						echo '<td>' . $data->event_date_at . '<br/>' . get_time($data->event_hour, $data->event_minute) . '</td>';
						echo '<td>' . $data->address . '<br/>' . $data->city . ', ' . $data->state . '<br/>' . $data->zip . '<br/>' . $data->phone . '</td>';
						echo '<td>' . number_format($data->total, 2, ".", ",")  . ' DOP</td>';
                        echo '<td>';
                        /**
                         * Steps:
                         * 1.If Order is New -> Change to Processing
                         * 2.If Order is New -> Change to Cancel
                         * 2.If Order is Processing, before delivering, change to delivering
                         * 3.If Order is Delivering, change status to delivered.
                         */
						if ($data->status == 1) {
							echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/accept/' . $data->id . '">Confirmado</a><br/>';
						}
                        if ($data->status == 1) {
							echo ' <a class="btn btn-sm btn-warning my-1" href="/catering/orders/cancel/' . $data->id . '">Cancelado</a><br/>';
                        }
						if ($data->status == 2) {
							echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/process/' . $data->id . '">Procesando</a><br/>';
						}
						if ($data->status == 3) {
							echo '<a class="btn btn-sm btn-info my-1" href="/catering/orders/delivery/' . $data->id . '">En Camino</a><br/>';
						}
						if ($data->status == 4) {
							echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/delivered/' . $data->id . '">Entregado</a><br/>';
                        }
                        echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/view/' . $data->id . '">Ver</a><br/>';
						echo '</td></tr>';
						?>
					<?php } ?>
					</tbody>
				</table>
				<p class="pagination_custom"><?php echo $links; ?></p>
			</div>
  </div>
  </div>
  </div>
</div>