<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Perfil </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/catering/dashboard" class="breadcrumb-link">Panel de control</a></li>
						<li class="breadcrumb-item active" aria-current="page">Perfil</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
</div>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	<div class="card">
		<h5 class="card-header">Perfil</h5>
			<div class="card-body">
				<?= form_open() ?>
					<div class="form-group">
						<label class="col-form-label" for="email">Correo </label>
						<input type="email" class="form-control" id="email" name="email" placeholder="ejemplo@gmail.com" value="<?php echo set_value('email', $model->email); ?>"/>
					</div>
					<div class="form-group">
						<label class="col-form-label" for="password">Contraseña </label>
						<input type="password" class="form-control" id="password" name="password" placeholder="" value=""/>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Submit">
					</div>
				</form>
			</div>
		</div><!-- .row -->
		</h5>
	</div>
	</div>
</div>