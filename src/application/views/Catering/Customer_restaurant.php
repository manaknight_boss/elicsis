<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Clientes</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Filtrar Clientes</h5>
            <div class="card-body">
				<?= form_open() ?>
				<div class="row">
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='email'>Correo </label>
							<input type='email' class='form-control' id='email' name='email' value='<?php echo $email;?>'/>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<label for='number'>Numero de Ordenes </label>
							<input type='number' class='form-control' id='num_order' name='num_order' value='<?php echo $num_order;?>'/>
						</div>
					</div>
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Enviar">
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
</div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Clientes</h5>
            <div class="card-body">
            <div class="table-responsive">
  <table class="table  table-hover table-condensed">
    <thead>
		<th class="no-bt">Cliente</th>
		<th class="no-bt">Num. de Pedidos</th>
		<th class="no-bt">Primer Pedido</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
		echo '<td>' . $data->user->email . '<br/>' . $data->user->first_name . ' ' . $data->user->last_name . '<br/>' . $data->user->phone . '</td>';
		echo '<td>' . $data->num_order . '</td>';
		echo '<td>' . $data->first_order_date . '</td>';
        echo '</tr>';
        ?>
    <?php } ?>
    </tbody>
</table>
<p class="pagination_custom"><?php echo $links; ?></p>
            </div>
  </div>
  </div>
  </div>
</div>