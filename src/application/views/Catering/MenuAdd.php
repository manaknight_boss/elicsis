<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Menú </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/catering/dashboard" class="breadcrumb-link">Panel De Control</a></li>
						<li class="breadcrumb-item"><a href="/catering/menu" class="breadcrumb-link">Menú</a></li>
						<li class="breadcrumb-item active" aria-current="page">Añadir Menú</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
</div>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	<div class="card">
		<h5 class="card-header">Añadir Menú</h5>
            <div class="card-body">
			<?= form_open() ?>
				<div class="form-group">
					<label for='name'>Nombre del Plato</label>
						<input type='text' class='form-control' id='name' name='name' value=''/>
						</div>
				<div class="form-group">
					<label for='description'>Descripción </label>
						<textarea id='description' name='description' class='form-control' rows='10'></textarea>
						</div>
				<div class="form-group">
					<label for='catering_menu_id'>Tipo de Menu </label>
					<select id="catering_menu_id" name='catering_menu_id' class='form-control'>
					<?php
						foreach ($catering_menu as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}
					?>
					</select>
				</div>
				<div class="form-group">
					<label class="custom-control custom-checkbox">
							<input type="checkbox" value="1" name="most_ordered" class="custom-control-input"><span class="custom-control-label">Más pedido?</span>
					</label>
				</div>
				<div class="form-group">
					<label class="custom-control custom-checkbox">
							<input type="checkbox" value="1" name="popular" class="custom-control-input"><span class="custom-control-label">Popular?</span>
					</label>
				</div>
				<div class="form-group">
					<label for='quantity_min'>Cantidad Mínima </label>
						<input type='number' class='form-control' id='quantity_min' name='quantity_min' min="1" step="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value='1'/>
						</div>
				<div class="form-group">
					<label for='quantity_max'>Cantidad Máxima</label>
						<input type='number' class='form-control' id='quantity_max' name='quantity_max' min="1" step="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value=''/>
						</div>
				<div class="form-group">
					<label for='serving'>No. Porciones x1</label>
						<input type='number' class='form-control' id='serving' name='serving' value='' min="1" step="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
						</div>
				<div class="form-group">
					<label for='price'>Precio </label>
						<input type='number' class='form-control' id='price' name='price' value='' />
						</div>
				<div class="form-group">
					<label for='order'>Orden de Aparición </label>
						<input type='number' class='form-control' id='order' name='order' value='' min="1" step="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />
				</div>
				<div class="form-group">
						<label for='order'>Preferencia </label>
						<select id="preference" name='preference[]' multiple="multiple" class='form-control'>
						<?php foreach ($preferences as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
						</select>
				</div>

				<div class="form-group">
						<label for='order'>Se Vende por </label>
						<select id="sold_by" name='sold_by' class='form-control'>
						<?php foreach ($sold_by_mapping as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
						</select>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Enviar">
				</div>
			</form>
		</div>
    </div>
</div>
</div>