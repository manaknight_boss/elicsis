<?php defined('BASEPATH') OR exit('No direct script access allowed');
function get_time ($hour)
{
	$final_time = ($hour < 13) ? ($hour . ':00 AM') : ( ($hour % 12) . ':00 PM');
	if ($hour == 12)
	{
		$final_time = '12:00 PM';
	}

	if ($hour == 24)
	{
		$final_time = '12:00 AM';
	}

	return $final_time;
}
?>
<h2> PANEL DE CONTROL</h2>
<br>
<div class="row">
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
        <h4>Confirmación %: <?php echo $on_time_accept_rating;?></h4>
        <div id='chart_on_time_accept' data-graph="<?php echo str_replace('"', '\'', json_encode($on_time_accept));?>"></div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center">
        <h4>Entrega a Tiempo: <?php echo $on_time_delivery_rating;?></h4>
        <div id='chart_on_time_delivery' data-graph="<?php echo str_replace('"', '\'', json_encode($on_time_delivery));?>"></div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
        <h4>Total Ventas: <?php echo $total_sales;?></h4>
        <h4>Total Pedidos: <?php echo $total_orders;?></h4>
        <h4> Avg. Precio/Orden: <?php echo $avg_order_value;?></h4>
        <h4>Avg. Calificación: <?php echo $avg_review;?></h4>
        <h4>Clientes Habituales: <?php echo $repeat_customer;?></h4>

    </div>
</div>
<br/>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Pedidos: Acción Inmediata</h5>
                <div class="card-body">
                <div class="table-responsive">
                <table class="table  table-hover table-condensed">
                        <thead>
                            <th class="no-bt">Orden #</th>
                            <th class="no-bt">Estado</th>
                            <th class="no-bt">Tipo</th>
                            <th class="no-bt">Fecha/Hora</th>
                            <th class="no-bt">Ubicación</th>
                            <th class="no-bt">Total</th>
                            <th class="no-bt">Acción</th>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $data) { ?>
                            <?php
                            echo '<tr>';
                            echo '<td>' . $data->id  . '</td>';
                            echo '<td>' . $mapping[$data->status] . '</td>';
                            echo '<td>' . $delivery_mapping[$data->is_delivery] . '</td>';
                            echo '<td>' . $data->event_date_at . '<br/>' . get_time($data->event_hour) . '</td>';
                            echo '<td>' . $data->address . '<br/>' . $data->city . ', ' . $data->state . '<br/>' . $data->zip . '<br/>' . $data->phone . '</td>';
                            echo '<td>' . number_format($data->total, 2, ".", ",")  . ' DOP</td>';
                            echo '<td>';
                            /**
                             * Steps:
                             * 1.If Order is New -> Change to Processing
                             * 2.If Order is New -> Change to Cancel
                             * 2.If Order is Processing, before delivering, change to delivering
                             * 3.If Order is Delivering, change status to delivered.
                             */
                            if ($data->status == 1) {
                                echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/accept/' . $data->id . '">Confirmada</a><br/>';
                            }
                            if ($data->status == 1) {
                                echo ' <a class="btn btn-sm btn-warning my-1" href="/catering/orders/cancel/' . $data->id . '">Cancelar</a><br/>';
                            }
                            if ($data->status == 2) {
                                echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/process/' . $data->id . '">Preparando</a><br/>';
                            }
                            if ($data->status == 3) {
                                echo '<a class="btn btn-sm btn-info my-1" href="/catering/orders/delivery/' . $data->id . '">En Camino</a><br/>';
                            }
                            if ($data->status == 4) {
                                echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/delivered/' . $data->id . '">Entregada</a><br/>';
                            }
                            echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/view/' . $data->id . '">Ver</a><br/>';
                            echo '</td></tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </div>
  <script>
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {};
    xhr.open('GET', '/v1/api/payments?restaurant_id=<?php echo $restaurant_id?>');
    xhr.send();
  </script>