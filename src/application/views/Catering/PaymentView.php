<?php defined('BASEPATH') OR exit('No direct script access allowed');
function get_time ($hour)
{
	$final_time = ($hour < 13) ? ($hour . ':00 AM') : ( ($hour % 12) . ':00 PM');
	if ($hour == 12)
	{
		$final_time = '12:00 PM';
	}

	if ($hour == 24)
	{
		$final_time = '12:00 AM';
	}

	return $final_time;
}
?>
<h2>Pedidos a pagar</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">

	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
		<h5 class="card-header">Detalles del Pago
		</h5>
            <div class="card-body">
				<h6>Pago #: &nbsp; <?php echo $model->id;?></h6>
				<h6>Fecha/Corte: &nbsp; <?php echo $model->payment_date;?></h6>
				<h6>Numero de Pedidos: &nbsp; <?php echo $model->num_orders;?></h6>
				<h6>Estado: &nbsp; <?php echo $mapping[$model->status];?></h6>
				<h6>Total Comida: &nbsp; <?php echo number_format($model->amount, 2) . ' DOP';?></h6>
				<h6>Total Delivery: &nbsp; <?php echo number_format($model->delivery, 2) . ' DOP';?></h6>
				<h6>Total Propinas: &nbsp; <?php echo number_format($model->tips, 2) . ' DOP';?></h6>
				<h6>Total Impuestos: &nbsp; <?php echo number_format($model->tax, 2) . ' DOP';?></h6>
				<h6>Total Bruto: &nbsp; <?php echo number_format($model->total, 2) . ' DOP';?></h6>
				<h6>Comisíon: &nbsp; <?php echo number_format($model->commission, 2) . ' DOP';?></h6>
				<h6>Ajustes: &nbsp; <?php echo number_format($model->adjustment, 2) . ' DOP';?></h6>
				<h6>Total Pedidos No pagados: &nbsp; <?php echo number_format($model->not_paid, 2) . ' DOP';?></h6>
				<h6>Total Pedidos/Reclamaciones: &nbsp; <?php echo number_format($model->problem_order, 2) . ' DOP';?></h6>
				<h6>Pago Final: &nbsp; <?php echo number_format(($model->total - $model->commission + $model->adjustment), 2) . ' DOP';?></h6>
				<?php
				if (strlen($model->notes) > 0) {
					echo '<h6>Notas de ajuste:</h6>';
					echo  "<p>{$model->notes}</p>";
				}?>
			</div>
		</div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
		<h5 class="card-header">Pedidos Entregados entre <?php echo $previous_payout_day . ' Y ' . $payout_day;?></h5>
            <div class="card-body">
            <table class="table  table-hover table-condensed">
					<thead>
						<th class="no-bt"># Pedido</th>
						<th class="no-bt">Estado</th>
						<th class="no-bt">Tipo</th>
						<th class="no-bt">Fecha/Hora</th>
						<th class="no-bt">Ubicación</th>
						<th class="no-bt">Total</th>
						<th class="no-bt">Acción</th>
					</thead>
					<tbody>
					<?php foreach ($list as $data) { ?>
						<?php
						echo '<tr>';
						echo '<td>' . $data->id  . '</td>';
						echo '<td>' . $mapping[$data->status] . '</td>';
						echo '<td>' . $delivery_mapping[$data->is_delivery] . '</td>';
						echo '<td>' . $data->event_date_at . '<br/>' . get_time($data->event_hour) . '</td>';
						echo '<td>' . $data->address . '<br/>' . $data->city . ', ' . $data->state . '<br/>' . $data->zip . '<br/>' . $data->phone . '</td>';
						echo '<td>' . $data->total  . ' DOP</td>';
                        echo '<td>';
                        echo '<a class="btn btn-sm btn-primary my-1" href="/catering/orders/view/' . $data->id . '">Ver</a><br/>';
						echo '</td></tr>';
						?>
					<?php } ?>
					</tbody>
				</table>
  </div>
  </div>
  </div>
</div>