<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Calificaciones</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Calificaciones</h5>
            <div class="card-body">
  <table class="table  table-hover table-condensed">
    <thead>
    	<th class="no-bt">Usuario</th>
		<th class="no-bt">Orden</th>
		<th class="no-bt">Calificación</th>
		<th class="no-bt">Mensaje</th>
		<th class="no-bt">Fecha</th>
    </thead>
    <tbody>
    <?php foreach ($list as $data) { ?>
        <?php
        echo '<tr>';
        echo '<td>' . $data->user->email . '<br/>' . $data->user->first_name . ' ' . $data->user->last_name . '<br/>' . $data->user->phone . '</td>';
        echo '<td>';
        $json_data = json_decode($data->order->data, TRUE);

        foreach($json_data['items'] as $item)
        {
            echo '<div>' . $data->order->event_date_at . '</div>';
            echo '<div>' . $item['quantity'] . 'x ' . $item['title'] . '</div>';
            echo ($item['specialInstruction'] != '') ? ('<small>Instrucciones especiales: ' . $item['specialInstruction'] . '</small>') : '';

            if (!empty($item['addons']))
            {
                echo '<div>' . 'Addons:' . '</div>';
                foreach($item['addons'] as $addon)
                {
                    echo '<div> - ' . $addon['type'] . ' : ' . $addon['name'] . '</div>';
                }
            }
        }
        echo '</td>';
		echo '<td>' . $data->rating . '</td>';
		echo '<td>' . $data->message . '</td>';

        echo '<td>';
        echo ' <a class="btn btn-primary" target="__blank" href="/catering/order/view/' . $data->id . '">Ver Pedido</a>';
        echo '</td></tr>';
        ?>
    <?php } ?>
    </tbody>
  </table>
  <p class="pagination_custom"><?php echo $links; ?></p>
  </div>
  </div>
  </div>
</div>