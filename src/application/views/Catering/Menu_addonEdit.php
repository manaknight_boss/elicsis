<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Complemento de Menú </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/catering/dashboard" class="breadcrumb-link">Panel De Control</a></li>
						<li class="breadcrumb-item active" aria-current="page">Comp. Menu</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
        </div>
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Complemento</h5>
                <div class="card-body">
				<?= form_open() ?>
					<div class="form-group">
						<label for='name'>Nombre </label>
							<input type='text' class='form-control' id='name' name='name' value='<?php echo set_value('name', $model->name); ?>'/>
							</div>
					<div class="serving-form-component form-group d-none">
						<label for='serving'>Porciones </label>
							<input type='number' class='form-control' id='serving' name='serving' value='<?php echo set_value('serving', $model->serving); ?>'/>
							</div>
					<div class="price-form-component form-group d-none">
						<label for='price'>Precio </label>
							<input type='text' class='form-control' id='price' name='price' value='<?php echo set_value('price', $model->price); ?>' data-inputmask="'alias': 'decimal', 'radixpoint': '.'"/>
							</div>
					<div class="info-form-component form-group d-none">
						<label for='info'>Detalles </label>
							<input type='text' class='form-control' id='info' name='info' value='<?php echo set_value('info', $model->info); ?>'/>
							</div>
					<div class="preference-form-component form-group d-none">

						<label for='order'>Preferencias </label>
						<select id="preference" name='preference[]' multiple="multiple" class='form-control'>
						<?php foreach ($preferences as $key => $value) {
							$selected = '';
							foreach ($processed_preference as $preference_id) {
								if ($preference_id == $key) {
									$selected = ' selected ';
								}
							}
							echo "<option value='{$key}' $selected> {$value} </option>";
						}?>
						</select>

					</div>
					<div class="form-group">
					<label class='col-form-label' for='status'>Estado </label>
						<select name='status' class='form-control'>
						<option value='1' <?php echo ($model->status == 'Active') ? 'selected' : '';?>> Activo </option>
						<option value='0' <?php echo ($model->status == 'Inactive') ? 'selected' : '';?>> Inactivo </option>
						</select>
					</div>
					<div class="form-group">
						<input type="hidden" id="menu_property_id" name="menu_property_id" value="<?php echo set_value('menu_property_id', $model->menu_property_id); ?>"/>
						<input type="hidden" id="menu_addon_edit" name="menu_addon_edit" value="1"/>
						<input type="hidden" id="processed_preference" name="processed_preference" value='<?php echo $processed_preference; ?>'>
						<input type="submit" class="btn btn-primary" value="Enviar">
					</div>
					<script>
						<?php echo 'var menu_property_list = ' . json_encode($menu_property_list) . ';';?>
					</script>
				</form>
            </div>
        </div>
        </div>