<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Complemento de Menú </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/catering/dashboard" class="breadcrumb-link">Panel de control</a></li>
						<li class="breadcrumb-item"><a href="/catering/menu" class="breadcrumb-link">Menu</a></li>
						<li class="breadcrumb-item active" aria-current="page">Complemento</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (strlen($error) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-danger" role="alert">
		                <?php echo $error; ?>
		            </div>
		        </div>
		    </div>
		<?php endif; ?>
		<?php if (strlen($success) > 0) : ?>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="alert alert-success" role="success">
		                <?php echo $success; ?>
		            </div>
		        </div>
		    </div>
        <?php endif; ?>
</div>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	<div class="card">
		<h5 class="card-header">Añadir Complemento</h5>
            <div class="card-body">
			<label for="">Elije un Tipo  </label>
			<select name="menu_property_list" class="form-control" id="menu_property_list">
			<?php
			$selected = $menu_property_list[0]->id;
			foreach ($menu_property_list as $key => $value) {
				echo "<option value='{$value->id}'>{$value->name}</option>";
			}
			?>
			</select>
			<script>
			<?php echo 'var menu_property_list = ' . json_encode($menu_property_list) . ';';?>
			</script>
			<div>
				<?= form_open() ?>
					<div class="form-group">
						<label for='name'>Nombre </label>
							<input type='text' class='form-control' id='name' name='name' value=''/>
							</div>
					<div class="serving-form-component form-group d-none">
						<label for='serving'>Porciones </label>
							<input type='number' class='form-control' id='serving' name='serving' value=''/>
							</div>
					<div class="price-form-component form-group d-none">
						<label for='price'>Precio </label>
							<input type='text' class='form-control' id='price' name='price' value='' data-inputmask="'alias': 'decimal', 'radixpoint': '.'"/>
							</div>
					<div class="info-form-component form-group d-none">
						<label for='info'>Detalles </label>
							<input type='text' class='form-control' id='info' name='info' value=''/>
							</div>
					<div class="preference-form-component form-group d-none">
						<label for='order'>Preferencia </label>
						<select id="preference" name='preference[]' multiple="multiple" class='form-control'>
						<?php foreach ($preferences as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
						</select>
					</div>
					<div class="form-group">
						<input type="hidden" id="menu_property_id" name="menu_property_id" value="<?php echo $selected;?>"/>
						<input type="submit" class="btn btn-primary" value="Enviar">
					</div>
				</form>
			</div>
		</div>
    </div>
</div>
</div>