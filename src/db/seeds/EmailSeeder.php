<?php


use Phinx\Seed\AbstractSeed;

class EmailSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'slug'  => 'register',
                'subject'  => 'Registrar Cuenta',
                'tags' => 'email',
                'html'  => "Hola {{{email}}},<br/>Bienvenido a Elicsis, Gracias por registrar tu cuenta. <br/>Gracias,<br/> La administración"
            ],
            [
                'id'    => 2,
                'slug'  => 'guest-register',
                'subject'  => 'Bienvenida a Elicsis',
                'tags' => 'email,password',
                'html'  => "Hola {{{email}}},<br/>Te damos la bienvenida, Gracias por registrar tu cuenta. <br/>Para acceder por favor uitiliza los siguinetes credenciales:<br/>Email: {{{email}}}<br/>Password: {{{password}}}<br/>Gracis,<br/> Elicsis"
            ],
            [
                'id'    => 3,
                'slug'  => 'reset-password',
                'subject'  => 'Actualizar Contraseña',
                'tags' => 'email,reset_token,link',
                'html'  => "Hola {{{email}}},<br/>Hemos recibído una solicitud para cambiar tu contraseña, para proceder haz click al enlace<br/><a href=\"{{{link}}}/{{{reset_token}}}\">Aquí</a>. <br/>Gracias,<br/> La Administración"
            ],
            [
                'id'    => 4,
                'slug'  => 'reciept',
                'subject'  => 'Elicsis Comprobante #{{{id}}}',
                'tags' => 'title,rnc,address,state,city,zip,id,event_date_at,event_hour,items,sub_total,tips,delivery,total,tax,final_total,phone',
                'html'  => '<!DOCTYPE html><html lang="en"><head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <meta http-equiv="X-UA-Compatible" content="ie=edge"> <title>Recibo</title></head><body><div>Recibo</div><div>Elcisis SRL.</div><div>Nicolás Penson 73</div><div>Gazcue</div><div>Santon Domingo</div><br/><div>Restaurante:</div><div>{{{title}}}</div><div><div>Teléfono #: {{{phone}}}</div><div>RNC#: {{{rnc}}}</div><div>{{{address}}}</div><div>{{{state}}}</div><div>{{{city}}}</div><div>{{{zip}}}</div></div><br/><div class="table-responsive"><table class="table table-bordered" style="width:100%"><thead> <tr> <th scope="col" >Detalles:</th> <th scope="col">Monto</th> </tr></thead><tbody> <tr> <td scope="row" > <div>Orden #:{{{id}}}</div><div>Teléfono #: {{{phone}}}</div><div>Fecha:{{{event_date_at}}}</div><div>Hora:{{{event_hour}}}</div><br/> <div>Detalles: </div><div>{{{items}}}</div></td> <td>DOP{{{sub_total}}}</td></tr><tr> <td scope="row"  >Propinas</td> <td>DOP{{{tips}}}</td></tr>{{{delivery}}}<tr style="border-top:2px solid black;"> <td scope="row"  >Total Exc. Impuesto</td> <td>DOP{{{total}}}</td></tr><tr> <td scope="row"  >ITBIS</td> <td>DOP{{{tax}}}</td></tr><tr style="border-top:2px solid black;"> <td scope="row"  >Total</td> <td>DOP{{{final_total}}}</td></tr></tbody></table></div></body></html>'
            ],
            [
                'id'    => 5,
                'slug'  => 'confirm-password',
                'subject'  => 'Confirmar Contraseña',
                'tags' => 'email,confirm_token',
                'html'  => "Hola {{{email}}},<br/>Haz click al enlace para confirmar la contraseña.<br/><a href=\"/confirm/{{{confirm_token}}}\">link</a>Gracias,<br/> La administración"
            ],
            [
                'id'    => 6,
                'slug'  => 'accept',
                'subject'  => 'Pedido {{{id}}} Aceptado',
                'html'  => 'Confirmamos que su pedido # {{{id}}} ha sido aceptado por el restaurante {{{title}}}',
                'tags' => 'id,title'
            ],
            [
                'id'    => 7,
                'slug'  => 'process',
                'subject'  => 'Pedido {{{id}}} Procesando',
                'html'  => 'Confirmamos que el restaurante {{{title}}} está preparando su pedido # {{{id}}} y estará listo para entrega a las {{{event_time_at}}',
                'tags' => 'id,title'
            ],
            [
                'id'    => 8,
                'slug'  => 'delivering',
                'subject'  => 'Pedido {{{id}}} en camino',
                'html'  => 'Su pedido  {{{id}}} ha sido recogido por el delivery y va en camino desde {{{title}}}',
                'tags' => 'id,title'
            ],
            [
                'id'    => 9,
                'slug'  => 'delivered',
                'subject'  => 'Pedido {{{id}}} Entregado',
                'html'  => 'Su pedido  {{{id}}} ha sido entregado por {{{title}}}, si no lo ha recibido por favor iniciar una reclamación o contátecnos de inmediato.',
                'tags' => 'id,title'
            ],
            [
                'id'    => 10,
                'slug'  => 'cancel',
                'subject'  => 'Pedido {{{id}}} Cancelado',
                'html'  => 'Su pedido {{{id}}} ha sido cancelado, su pedido venía de {{{title}}}, si usted no canceló por favor contáctenos.',
                'tags' => 'id,title'
            ],
            [
                'id'    => 11,
                'slug'  => 'resolution',
                'subject'  => 'Pedido {{{id}}} Resolución',
                'html'  => 'Hemos llegado a una resolución para el reclamo de su pedido {{{id}}}, puede acceder a su cuenta y mirar los detalles',
                'tags' => 'id'
            ],
            [
                'id'    => 12,
                'slug'  => 'refund',
                'subject'  => 'Pedido {{{id}}} Reembolsado',
                'html'  => 'Su pedido {{{id}}} ha sido reembolsado, es nuestra meta proveer un servicio satisfactorio. Gracias por confiar en nosotros,',
                'tags' => 'id'
            ],
            [
                'id'    => 13,
                'slug'  => 'verify',
                'subject'  => 'Verificación de Elicsis',
                'html'  => 'Su numero de verificación de Elicsis es {{{num}}}',
                'tags' => 'num'
            ]
        ];

        $roles = $this->table('emails');
        $roles->insert($rows)->save();
    }
}
