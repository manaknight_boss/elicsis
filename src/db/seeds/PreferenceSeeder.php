<?php


use Phinx\Seed\AbstractSeed;

class PreferenceSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            ['name' => 'Vegetariana', 'icon' => '<i class="fas fa-leaf tooltips"   title="Vegetarian"></i>', 'status' => 1],
            ['name' => 'Sin-Gluten', 'icon' => '<i class="fas fa-bread-slice tooltips"   title="Gluten-Free"></i>', 'status' => 1],
            ['name' => 'Pescado', 'icon' => '<i class="fas fa-fish tooltips"   title="Fish"></i>', 'status' => 1],
            ['name' => 'Cerdo', 'icon' => '<i class="fas fa-piggy-bank tooltips"   title="Pork"></i>', 'status' => 1],
            ['name' => 'Res', 'icon' => '<i class="fas fa-hamburger tooltips"   title="Beef"></i>', 'status' => 1],
            ['name' => 'Lechería', 'icon' => '<i class="fas fa-mug-hot tooltips"   title="Dairy"></i>', 'status' => 1],
            ['name' => 'Halal', 'icon' => '<i class="fab fa-hire-a-helper tooltips"   title="Halal"></i>', 'status' => 1],
            ['name' => 'Picante', 'icon' => '<i class="fas fa-pepper-hot tooltips"   title="Spicy"></i>', 'status' => 1],
            ['name' => 'Kosher', 'icon' => '<i class="fab fa-kickstarter-k tooltips"   title="Kocher"></i>', 'status' => 1],
            ['name' => 'Cruda', 'icon' => '<i class="fas fa-democrat tooltips"   title="Raw"></i>', 'status' => 1],
            ['name' => 'BBQ', 'icon' => '<span class="tooltips"   title="BBQ">BBQ</span>', 'status' => 1],
            ['name' => 'Sin-Grasa', 'icon' => '<span class="tooltips"   title="Fat-Free">F</span>', 'status' => 1],
            ['name' => 'Azucarada', 'icon' => '<i class="fas fa-cookie-bite tooltips"   title="Sugar"></i>', 'status' => 1]
            // ['name' => 'Vegetarian', 'icon' => '<i class="fas fa-leaf"  data-toggle="tooltip" data-placement="top" title="Vegetarian"></i>', 'status' => 1],
            // ['name' => 'Gluten-Free', 'icon' => '<i class="fas fa-bread-slice"  data-toggle="tooltip" data-placement="top" title="Gluten-Free"></i>', 'status' => 1],
            // ['name' => 'Fish', 'icon' => '<i class="fas fa-fish"  data-toggle="tooltip" data-placement="top" title="Fish"></i>', 'status' => 1],
            // ['name' => 'Pork', 'icon' => '<i class="fas fa-piggy-bank"  data-toggle="tooltip" data-placement="top" title="Pork"></i>', 'status' => 1],
            // ['name' => 'Beef', 'icon' => '<i class="fas fa-hamburger"  data-toggle="tooltip" data-placement="top" title="Beef"></i>', 'status' => 1],
            // ['name' => 'Dairy', 'icon' => '<i class="fas fa-mug-hot"  data-toggle="tooltip" data-placement="top" title="Dairy"></i>', 'status' => 1],
            // ['name' => 'Halal', 'icon' => '<i class="fab fa-hire-a-helper"  data-toggle="tooltip" data-placement="top" title="Halal"></i>', 'status' => 1],
            // ['name' => 'Spicy', 'icon' => '<i class="fas fa-pepper-hot"  data-toggle="tooltip" data-placement="top" title="Spicy"></i>', 'status' => 1],
            // ['name' => 'Kocher', 'icon' => '<i class="fab fa-kickstarter-k"  data-toggle="tooltip" data-placement="top" title="Kocher"></i>', 'status' => 1],
            // ['name' => 'Raw', 'icon' => '<i class="fas fa-democrat"  data-toggle="tooltip" data-placement="top" title="Raw"></i>', 'status' => 1],
            // ['name' => 'BBQ', 'icon' => '<span  data-toggle="tooltip" data-placement="top" title="BBQ">BBQ</span>', 'status' => 1],
            // ['name' => 'Fat-Free', 'icon' => '<span  data-toggle="tooltip" data-placement="top" title="Fat-Free">F</span>', 'status' => 1],
            // ['name' => 'Sugar', 'icon' => '<i class="fas fa-cookie-bite"  data-toggle="tooltip" data-placement="top" title="Sugar"></i>', 'status' => 1]
        ];
        $users = $this->table('preferences');
        $users->insert($data)->save();
    }
}
