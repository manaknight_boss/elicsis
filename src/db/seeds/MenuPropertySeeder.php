<?php


use Phinx\Seed\AbstractSeed;

class MenuPropertySeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            ['name' => 'SELECCIONAR', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 0, 'status' => 1],
            ['name' => 'SELECCIONAR FRUTA', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 1, 'status' => 1],
            ['name' => 'SELECCIONAR TOSTADA', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 1, 'status' => 1],
            ['name' => 'AVISANOS SI NECESITAS', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 0, 'status' => 1],
            ['name' => 'SELECCIONAR PAN', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 1, 'status' => 1],
            ['name' => 'SELECCIONAR SANDWICH O TIPO DE WRAP', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 0, 'status' => 1],
            ['name' => 'AGREGAR', 'is_serving' => 0, 'is_price' => 1, 'is_info' => 0, 'is_preference' => 1, 'status' => 1],
            ['name' => 'SELECCIONAR PASTA', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 1, 'status' => 1],
            ['name' => 'SELECCIONAR ENSALADA', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 1, 'status' => 1],
            ['name' => 'SELECCIONAR LASAGNA', 'is_serving' => 0, 'is_price' => 0, 'is_info' => 0, 'is_preference' => 1, 'status' => 1],
        ];
        $users = $this->table('menu_property');
        $users->insert($data)->save();
    }
}
