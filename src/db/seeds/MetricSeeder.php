<?php


use Phinx\Seed\AbstractSeed;

class MetricSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'num_order' => 0,
                'num_user' => 0,
                'num_catering' => 0,
                'total_sale' => 0,
                'total_delivered' => 0,
                'total_pending' => 0,
                'total_cancel' => 0,
                'total_refund' => 0,
                'total_issue' => 0,
            ]
        ];
        $users = $this->table('metric');
        $users->insert($data)->save();
    }
}
