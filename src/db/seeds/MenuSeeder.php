<?php


use Phinx\Seed\AbstractSeed;

class MenuSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            ['name' => 'La Granja Festiva', 'image' => '', 'restaurant_id' => 1, 'catering_menu_id' => 2, 'most_ordered' => 0, 'popular' => 1, 'quantity_min' => 1, 'quantity_max' => 100, 'preference' => '[]', 'serving' => 10, 'price' => 10, 'description' => 'Include una mezcla de vegetales con Ensalada de Faro (Con/ Espagueti Squash), hummus de frijoles negros, grano agrios, zanaorias al sartén, Ensalada estilo Alemana con papas rojas y moradas, squash invernal (con/ healing spices), ensalada brassica, Gouda ahumado, cheddar en chipotle, nueces y semillas, pasas & cranberries, pollo asado, salsa honey mustard, adereso ranch, y vinagre de sherry. Todo lo que necesitas. Lo sentimos, no ofrecemos Postres o Bebidas.', 'data'=> '[]', 'status' => 1, 'order' => 1, 'sold_by' => 1],
            ['name' => 'Combo de Pasta', 'image' => '', 'restaurant_id' => 2, 'catering_menu_id' => 8, 'most_ordered' => 1, 'popular' => 0, 'quantity_min' => 1, 'quantity_max' => 100, 'preference' => '[]', 'serving' => 20, 'price' => 5, 'description' => 'Cada combo incluye una selección de pasta y ensalada con un roll de ajo. Agrega la bebida o el postre y todo listo.', 'data'=> '[]', 'status' => 1, 'order' => 2, 'sold_by' => 2],
            ['name' => 'Combo de Lasagna', 'image' => '', 'restaurant_id' => 2, 'catering_menu_id' => 8, 'most_ordered' => 0, 'popular' => 1, 'quantity_min' => 1, 'quantity_max' => 100, 'preference' => '[]', 'serving' => 20, 'price' => 2, 'description' => 'Cada combo incluye una selección de pasta y ensalada con un roll de ajo. Agrega la bebida o el postre y todo listo.', 'data'=> '[]', 'status' => 1, 'order' => 1, 'sold_by' => 3]
        ];
        $users = $this->table('menu');
        $users->insert($data)->save();
    }
}
