<?php


use Phinx\Seed\AbstractSeed;

class CuisinesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            ['name' => 'Sandwiches', 'status' => 1],
            ['name' => 'Comida Caliente', 'status' => 1],
            ['name' => 'Mejicana', 'status' => 1],
            ['name' => 'Vegetariana Friendly', 'status' => 1],
            ['name' => 'Italiana', 'status' => 1],
            ['name' => 'BBQ', 'status' => 1],
            ['name' => 'Mediterranea', 'status' => 1],
            ['name' => 'Asiática', 'status' => 1],
            ['name' => 'Golosinas', 'status' => 1],
            ['name' => 'Sin - Gluten', 'status' => 1],
            ['name' => 'India', 'status' => 1],
            ['name' => 'Thai', 'status' => 1],
            ['name' => 'Americana', 'status' => 1],
            ['name' => 'China', 'status' => 1],
            ['name' => 'Sushi', 'status' => 1],
            ['name' => 'Ensalada', 'status' => 1],
            ['name' => 'Hors d \'Oeuvres', 'status' => 1],
            ['name' => 'Pollo', 'status' => 1],
            ['name' => 'Medio Oriente', 'status' => 1],
            ['name' => 'Vegan Friendly', 'status' => 1],
            ['name' => 'Kosher', 'status' => 1],
            ['name' => 'Griega', 'status' => 1],
            ['name' => 'Saludable', 'status' => 1],
            ['name' => 'Deli', 'status' => 1],
            ['name' => 'Sureña', 'status' => 1],
            ['name' => 'Sureña / America Latina', 'status' => 1],
            ['name' => 'Hamburguesas', 'status' => 1],
            ['name' => 'Halal', 'status' => 1],
            ['name' => 'Mariscos', 'status' => 1],
            ['name' => 'Sopas', 'status' => 1],
            ['name' => 'Bebidas Especiales', 'status' => 1],
            ['name' => 'Francesa', 'status' => 1],
            ['name' => 'Africana', 'status' => 1],
            ['name' => 'Alemana', 'status' => 1],
            ['name' => 'Española / Tapas', 'status' => 1],
            ['name' => 'Cenas', 'status' => 1],
            ['name' => 'Europa Oriental / Rusa', 'status' => 1]
        ];
        $users = $this->table('cuisines');
        $users->insert($data)->save();
    }
}
