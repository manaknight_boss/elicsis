<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'username'    => 'admin',
                'email'       => 'mpulseinc1@gmail.com',
                'password'    => password_hash('a123456', PASSWORD_BCRYPT),
                'image'       => '',
                'verify'    => 1,
                'type'        => 'n',
                'profile_type' => 'Regular',
                'business_name' => '',
                'rnc' => '',
                'first_name'  => 'vic',
                'last_name'  => 'perez',
                'reset_token' => '',
                'stripe_id'  => '',
                'phone'  => '6477839183',
                'role_id'  => 3,
                'status'  => 1,
                'refer'  => uniqid(),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],[
                'username'    => 'member',
                'email'       => 'getelicsis@gmail.com',
                'password'    => password_hash('a123456', PASSWORD_BCRYPT),
                'image'       => '',
                'verify'    => 1,
                'type'        => 'n',
                'business_name' => '',
                'rnc' => '',
                'profile_type' => 'Regular',
                'reset_token' => '',
                'first_name'  => 'Jose',
                'last_name'  => 'perez',
                'phone'  => '6477839183',
                'stripe_id'  => '',
                'role_id'  => 2,
                'status'  => 1,
                'refer'  => '5ca9f2fd3346d',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ]
        ];
        $users = $this->table('users');
        $users->insert($data)->save();
    }
}
