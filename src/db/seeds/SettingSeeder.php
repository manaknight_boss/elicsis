<?php


use Phinx\Seed\AbstractSeed;

class SettingSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'key'  => 'maintenance',
                'value' => '0'
            ],
            [
                'id'    => 2,
                'key'  => 'version',
                'value' => '1.0.0'
            ],
            [
                'id'    => 3,
                'key'  => 'email',
                'value' => 'mailtrap'
            ],
            [
                'id'    => 4,
                'key'  => 'cache',
                'value' => '0'
            ],
            [
                'id'    => 5,
                'key'  => 'site_title',
                'value' => 'Elicsis'
            ],
            [
                'id'    => 6,
                'key'  => 'site_logo',
                'value' => '/assets/image/Aplicaciones logo elicsis-07.png'
            ],
            [
                'id'    => 7,
                'key'  => 'copyright',
                'value' => 'Copyright © 2019 Manaknightdigital Inc. All rights reserved.'
            ],
            [
                'id'    => 8,
                'key'  => 'price',
                'value' => json_encode([
                    '$', '$$', '$$$', '$$$$'
                ])
            ],
            [
                'id'    => 9,
                'key'  => 'delivery_minimum',
                'value' => json_encode([
                    25,50,75,100
                ])
            ],
            [
                'id'    => 10,
                'key'  => 'distance',
                'value' => json_encode([
                    10,20,50
                ])
            ],
            [
                'id'    => 11,
                'key'  => 'rating',
                'value' => json_encode([
                    5,4,3
                ])
            ],
            [
                'id'    => 12,
                'key'  => 'tax',
                'value' => '0'
            ],
            [
                'id'    => 13,
                'key'  => 'tips',
                'value' => json_encode([
                    10, 15, 20
                ])
            ],
            [
                'id'    => 14,
                'key'  => 'payment_options',
                'value' => json_encode([
                    'cash_on_delivery'
                ])
            ],
            [
                'id'    => 15,
                'key'  => 'payout_day_of_week',
                'value' => '0'
            ],
            [
                'id'    => 16,
                'key'  => 'earning_cut',
                'value' => '10'
            ]
        ];

        $settings = $this->table('settings');
        $settings->insert($rows)->save();
    }
}
