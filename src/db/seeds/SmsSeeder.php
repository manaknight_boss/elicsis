<?php


use Phinx\Seed\AbstractSeed;

class SmsSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'id'    => 1,
                'slug'  => 'accept',
                'content'  => 'Su pedido # {{{id}}} del restaurante {{{title}}} ha sido Confirmado y será entregado el {{{event_date_at}}}.',
                'tags' => 'id,title,event_date_at'
            ],
            [
                'id'    => 2,
                'slug'  => 'process',
                'content'  => 'Su pedido # {{{id}}} del restaurante {{{title}}} esta en proceso y será entregado a tiempo a las {{{event_time}}}, hoy.',
                'tags' => 'id,title,event_date_at,event_time'
            ],
            [
                'id'    => 3,
                'slug'  => 'delivering',
                'content'  => 'Su pedido # {{{id}}} ha sido recogido en {{{title}}} y está en camino para entrega a las {{{event_time}}} en {{{event_location}}}, hoy.',
                'tags' => 'id,title,event_date_at,event_time,event_location'
            ],
            [
                'id'    => 4,
                'slug'  => 'delivered',
                'content'  => 'Su pedido # {{{id}}} del restaurante {{{title}}} ha sido entregado hoy a las {{{event_time}}} en {{{event_location}}}. Por favor ayudenos a mejorar el servicio con una calificaión, Gracias por preferirnos.',
                'tags' => 'id,title,event_date_at,event_time,event_location'
            ],
            [
                'id'    => 5,
                'slug'  => 'cancel',
                'content'  => 'Confirmamos que su pedido # {{{id}}} del restaurante {{{title}}} ha sido cancelado.',
                'tags' => 'id,title,event_date_at'
            ],
            [
                'id'    => 6,
                'slug'  => 'resolution',
                'content'  => 'Hemos emitido una resolución a su reclamación para pedido # {{{id}}}, puede acceder a su cuenta para mas detalles',
                'tags' => 'id'
            ],
            [
                'id'    => 7,
                'slug'  => 'refund',
                'content'  => 'Hemos reembolsado su pedido # {{{id}}}, por favor disculpe cualquier inconveniente que esta experiencia alla causado.',
                'tags' => 'id'
            ],
            [
                'id'    => 8,
                'slug'  => 'verify',
                'content'  => 'Su numero de verificación de Elicsis es {{{num}}}',
                'tags' => 'num'
            ],
            [
                'id'    => 9,
                'slug'  => 'new_order',
                'content'  => 'Tienes un nuevo pedido {{{id}}}.',
                'tags' => 'id'
            ]
        ];

        $roles = $this->table('sms');
        $roles->insert($rows)->save();
    }
}
