<?php


use Phinx\Seed\AbstractSeed;

class CateringMenuSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            ['name' => 'Desayunos', 'status' => 1],
            ['name' => 'Catering', 'status' => 1],
            ['name' => 'Ensaladas', 'status' => 1],
            ['name' => 'Guarniciones', 'status' => 1],
            ['name' => 'Bebidas', 'status' => 1],
            ['name' => 'Entradas', 'status' => 1],
            ['name' => 'Postres', 'status' => 1],
            ['name' => 'Almuersos', 'status' => 1],
            ['name' => 'Cena', 'status' => 1],
            ['name' => 'Aperitivos', 'status' => 1]
        ];
        $users = $this->table('catering_menu');
        $users->insert($data)->save();
    }
}
