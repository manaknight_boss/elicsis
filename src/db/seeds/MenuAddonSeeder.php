<?php


use Phinx\Seed\AbstractSeed;

class MenuAddonSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            ['name' => 'Extra Pollo', 'menu_id' => 1, 'menu_property_id' => 7, 'serving' => 0, 'price' =>3, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Salmon Atlantico', 'menu_id' => 1, 'menu_property_id' => 7, 'serving' => 0, 'price' =>4, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Steak - Alimentado a grama', 'menu_id' => 1, 'menu_property_id' => 7, 'serving' => 0, 'price' =>5, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Halal Pollo', 'menu_id' => 1, 'menu_property_id' => 7, 'serving' => 0, 'price' =>6, 'info'=> '', 'preference'=> '[7]', 'status' => 1],
            ['name' => 'Espaguetis', 'menu_id' => 2, 'menu_property_id' => 8, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[1]', 'status' => 1],
            ['name' => 'Más Popular', 'menu_id' => 2, 'menu_property_id' => 8, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Macarron & Queso', 'menu_id' => 2, 'menu_property_id' => 8, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[1]', 'status' => 1],
            ['name' => 'Pasta Ajo & Aceite', 'menu_id' => 2, 'menu_property_id' => 8, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[1]', 'status' => 1],
            ['name' => 'Ziti Horneada', 'menu_id' => 2, 'menu_property_id' => 8, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[1]', 'status' => 1],
            ['name' => 'Más Popular', 'menu_id' => 2, 'menu_property_id' => 9, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Caesar Salad', 'menu_id' => 2, 'menu_property_id' => 9, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Ensalada Griega', 'menu_id' => 2, 'menu_property_id' => 9, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[1,2]', 'status' => 1],
            ['name' => 'Ensalada Verde', 'menu_id' => 2, 'menu_property_id' => 9, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[1,2]', 'status' => 1],
            ['name' => 'Albongidas', 'menu_id' => 2, 'menu_property_id' => 7, 'serving' => 0, 'price' =>1.5, 'info'=> '', 'preference'=> '[]', 'status' => 1],

            ['name' => 'Más Popular', 'menu_id' => 3, 'menu_property_id' => 10, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Lasagna de Carne', 'menu_id' => 3, 'menu_property_id' => 10, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Spinach Lasagna', 'menu_id' => 3, 'menu_property_id' => 10, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Más Popular', 'menu_id' => 3, 'menu_property_id' => 9, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Ensalada Cesar', 'menu_id' => 3, 'menu_property_id' => 9, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[]', 'status' => 1],
            ['name' => 'Ensalada Griega', 'menu_id' => 3, 'menu_property_id' => 9, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[1,2]', 'status' => 1],
            ['name' => 'Ensalada Verde', 'menu_id' => 3, 'menu_property_id' => 9, 'serving' => 0, 'price' =>0, 'info'=> '', 'preference'=> '[1,2,3]', 'status' => 1]
        ];
        $users = $this->table('menu_addon');
        $users->insert($data)->save();
    }
}
