<?php


use Phinx\Migration\AbstractMigration;

class Cards extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('cards');
        $table->addColumn('brand', 'string', ['limit' => 25])
        ->addColumn('card_number', 'string', ['limit' => 50])
        ->addColumn('token', 'string', ['limit' => 255])
        ->addColumn('expire_date', 'string', ['limit' => 20])
        ->addColumn('user_id', 'float')
        ->addColumn('created_at', 'date')
        ->addColumn('update_at', 'date')
        ->create();
    }
}