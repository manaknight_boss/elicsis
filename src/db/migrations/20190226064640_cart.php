<?php


use Phinx\Migration\AbstractMigration;

class Cart extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('cart');
        $table->addColumn('is_login', 'integer')
        ->addColumn('is_day_time', 'integer')
        ->addColumn('is_address', 'integer')
        ->addColumn('is_payment', 'integer')
        ->addColumn('is_complete', 'integer')
        ->addColumn('user_id', 'integer')
        ->addColumn('restaurant_id', 'integer')
        ->addColumn('data', 'text')
        ->addColumn('payment_data', 'text')
        ->addColumn('created_at', 'date')
        ->create();
    }
}
