<?php
use Phinx\Migration\AbstractMigration;

class Orders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('orders');
        $table->addColumn('amount', 'float')
        ->addColumn('currency', 'string', ['limit' => 5])
        ->addColumn('event_date_at', 'date')
        ->addColumn('event_hour', 'integer')
        ->addColumn('event_minute', 'integer')
        ->addColumn('restaurant_id', 'integer')
        ->addColumn('phone', 'string', ['limit' => 20])
        ->addColumn('address', 'text', ['null' => true])
        ->addColumn('lat', 'float')
        ->addColumn('long', 'float')
        ->addColumn('tips', 'float')
        ->addColumn('tax', 'float')
        ->addColumn('is_delivery', 'integer')
        ->addColumn('delivery_fee', 'float')
        ->addColumn('total', 'float')
        ->addColumn('discount', 'float')
        ->addColumn('payment_method', 'string', ['limit' => 10])
        ->addColumn('zip', 'string', ['limit' => 10])
        ->addColumn('city', 'string', ['limit' => 30])
        ->addColumn('state', 'string', ['limit' => 30])
        ->addColumn('data', 'text', ['null' => true])
        ->addColumn('payment_data', 'text', ['null' => true])
        ->addColumn('notes', 'text', ['null' => true])
        ->addColumn('status', 'integer')
        ->addColumn('user_id', 'integer')
        ->addColumn('on_time_rating', 'integer')
        ->addColumn('on_delivery_rating', 'integer')
        ->addColumn('start_on_time_at', 'datetime', ['null' => true])
        ->addColumn('end_on_time_at', 'datetime', ['null' => true])
        ->addColumn('start_on_delivery_at', 'datetime', ['null' => true])
        ->addColumn('end_on_delivery_at', 'datetime', ['null' => true])
        ->addColumn('created_at', 'date')
        ->create();
    }
}
