<?php


use Phinx\Migration\AbstractMigration;

class Restaurants extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('restaurants');
        $table->addColumn('title', 'string', ['limit' => 200])
        ->addColumn('description', 'text')
        ->addColumn('about', 'text')
        ->addColumn('u', 'integer')
        ->addColumn('m', 'integer')
        ->addColumn('t', 'integer')
        ->addColumn('w', 'integer')
        ->addColumn('r', 'integer')
        ->addColumn('f', 'integer')
        ->addColumn('s', 'integer')
        ->addColumn('u_open', 'integer')
        ->addColumn('u_close', 'integer')
        ->addColumn('m_open', 'integer')
        ->addColumn('m_close', 'integer')
        ->addColumn('t_open', 'integer')
        ->addColumn('t_close', 'integer')
        ->addColumn('w_open', 'integer')
        ->addColumn('w_close', 'integer')
        ->addColumn('r_open', 'integer')
        ->addColumn('r_close', 'integer')
        ->addColumn('f_open', 'integer')
        ->addColumn('f_close', 'integer')
        ->addColumn('s_open', 'integer')
        ->addColumn('s_close', 'integer')
        ->addColumn('u_open_min', 'integer')
        ->addColumn('u_close_min', 'integer')
        ->addColumn('m_open_min', 'integer')
        ->addColumn('m_close_min', 'integer')
        ->addColumn('t_open_min', 'integer')
        ->addColumn('t_close_min', 'integer')
        ->addColumn('w_open_min', 'integer')
        ->addColumn('w_close_min', 'integer')
        ->addColumn('r_open_min', 'integer')
        ->addColumn('r_close_min', 'integer')
        ->addColumn('f_open_min', 'integer')
        ->addColumn('f_close_min', 'integer')
        ->addColumn('s_open_min', 'integer')
        ->addColumn('s_close_min', 'integer')
        ->addColumn('u_open_total', 'integer')
        ->addColumn('u_close_total', 'integer')
        ->addColumn('m_open_total', 'integer')
        ->addColumn('m_close_total', 'integer')
        ->addColumn('t_open_total', 'integer')
        ->addColumn('t_close_total', 'integer')
        ->addColumn('w_open_total', 'integer')
        ->addColumn('w_close_total', 'integer')
        ->addColumn('r_open_total', 'integer')
        ->addColumn('r_close_total', 'integer')
        ->addColumn('f_open_total', 'integer')
        ->addColumn('f_close_total', 'integer')
        ->addColumn('s_open_total', 'integer')
        ->addColumn('s_close_total', 'integer')
        ->addColumn('rating', 'float')
        ->addColumn('food_minimum', 'integer')
        ->addColumn('delivery_fee', 'float')
        ->addColumn('cuisines_id', 'integer')
        ->addColumn('price_range', 'string', ['limit' => 6])
        ->addColumn('type', 'integer')
        ->addColumn('status', 'integer')
        ->addColumn('serving', 'integer')
        ->addColumn('reset_token', 'string', ['limit' => 100])
        ->addColumn('address', 'text')
        ->addColumn('city', 'string', ['limit' => 20])
        ->addColumn('state', 'string', ['limit' => 20])
        ->addColumn('zip', 'string', ['limit' => 20])
        ->addColumn('lat', 'float')
        ->addColumn('long', 'float')
        ->addColumn('email', 'string', ['limit' => 100])
        ->addColumn('password', 'string', ['limit' => 255])
        ->addColumn('phone', 'string', ['limit' => 20])
        ->addColumn('notes', 'text')
        ->addColumn('rnc', 'string', ['limit' => 200])
        ->addColumn('logo', 'string', ['limit' => 200])
        ->addColumn('image', 'string', ['limit' => 200])
        ->addColumn('banner_image', 'string', ['limit' => 200])
        ->addColumn('on_time_rating', 'integer')
        ->addColumn('on_delivery_rating', 'integer')
        ->addColumn('num_review', 'integer')
        ->addColumn('created_at', 'date')
        ->addColumn('started_at', 'date')
        ->create();
    }
}
