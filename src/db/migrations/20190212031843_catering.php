<?php


use Phinx\Migration\AbstractMigration;

class Catering extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('new_catering');
        $table->addColumn('first_name', 'string', ['limit' => 100])
        ->addColumn('last_name', 'string', ['limit' => 100])
        ->addColumn('business_name', 'string', ['limit' => 200])
        ->addColumn('email', 'string', ['limit' => 100])
        ->addColumn('phone', 'string', ['limit' => 50])
        ->addColumn('type', 'integer')
        ->addColumn('comment', 'text')
        ->addColumn('zip', 'string', ['limit' => 10])
        ->addColumn('url', 'string', ['limit' => 200])
        ->addColumn('created_at', 'datetime')
        ->create();
    }
}
