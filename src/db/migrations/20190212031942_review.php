<?php


use Phinx\Migration\AbstractMigration;

class Review extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('review');
        $table->addColumn('user_id', 'integer')
        ->addColumn('restaurant_id', 'integer')
        ->addColumn('name', 'string', ['limit' => 100])
        ->addColumn('order_id', 'integer')
        ->addColumn('status', 'integer')
        ->addColumn('rating', 'float')
        ->addColumn('message', 'text')
        ->addColumn('created_at', 'datetime')
        ->create();
    }
}
