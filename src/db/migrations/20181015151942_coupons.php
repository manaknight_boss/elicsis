<?php
use Phinx\Migration\AbstractMigration;

class Coupons extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //coupon_code
        $table = $this->table('coupons');
        $table->addColumn('coupon_code', 'string', ['limit' => 50])
        ->addColumn('discount_type', 'string')
        ->addColumn('coupon_amount', 'integer')
        ->addColumn('plan_id', 'integer', ['null' => true])
        ->addColumn('usage_limit', 'integer', ['null' => true])
        ->addColumn('usage_total', 'integer', ['null' => true])
        ->addColumn('status', 'integer')
        ->addColumn('stripe_coupon_code', 'string', ['limit' => 50])
        ->addColumn('expiry_date', 'date')
        ->addColumn('created_at', 'date')
        ->create();
    }
}