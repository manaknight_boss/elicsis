<?php


use Phinx\Migration\AbstractMigration;

class Payments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('payments');
        $table->addColumn('payment_date', 'date')
        ->addColumn('amount', 'float')
        ->addColumn('delivery', 'float')
        ->addColumn('tips', 'float')
        ->addColumn('tax', 'float')
        ->addColumn('total', 'float')
        ->addColumn('commission', 'float')
        ->addColumn('adjustment', 'float')
        ->addColumn('not_paid', 'float')
        ->addColumn('problem_order', 'float')
        ->addColumn('restaurant_id', 'integer')
        ->addColumn('num_orders', 'integer')
        ->addColumn('status', 'integer')
        ->addColumn('notes', 'text')
        ->addColumn('created_at', 'date')
        ->create();
    }
}
