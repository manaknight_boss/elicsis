<?php


use Phinx\Migration\AbstractMigration;

class Report extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('report');
        $table->addColumn('name', 'string', ['limit' => 100])
        ->addColumn('type', 'integer')
        ->addColumn('restaurant_id', 'integer')
        ->addColumn('url', 'text')
        ->addColumn('query', 'text')
        ->addColumn('start_at', 'date')
        ->addColumn('end_at', 'date')
        ->addColumn('create_at', 'date')
        ->create();
    }
}
