<?php


use Phinx\Migration\AbstractMigration;

class Metric extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('metric');
        $table->addColumn('num_order', 'integer')
        ->addColumn('num_user', 'integer')
        ->addColumn('num_catering', 'integer')
        ->addColumn('total_sale', 'float')
        ->addColumn('total_delivered', 'float')
        ->addColumn('total_pending', 'float')
        ->addColumn('total_cancel', 'float')
        ->addColumn('total_refund', 'float')
        ->addColumn('total_issue', 'float')
        ->create();
    }
}
