<?php


use Phinx\Migration\AbstractMigration;

class MenuAddon extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('menu_addon');
        $table->addColumn('name', 'string', ['limit' => 100])
        ->addColumn('menu_id', 'integer')
        ->addColumn('menu_property_id', 'integer')
        ->addColumn('serving', 'integer')
        ->addColumn('price', 'float')
        ->addColumn('info', 'text')
        ->addColumn('preference', 'text')
        ->addColumn('status', 'integer')
        ->create();
    }
}
