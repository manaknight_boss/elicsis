<?php


use Phinx\Migration\AbstractMigration;

class Menu extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('menu');
        $table->addColumn('name', 'string', ['limit' => 100])
        ->addColumn('image', 'string', ['limit' => 100])
        ->addColumn('restaurant_id', 'integer')
        ->addColumn('catering_menu_id', 'integer')
        ->addColumn('most_ordered', 'integer')
        ->addColumn('popular', 'integer')
        ->addColumn('quantity_min', 'integer')
        ->addColumn('quantity_max', 'integer')
        ->addColumn('preference', 'text')
        ->addColumn('serving', 'integer')
        ->addColumn('price', 'integer')
        ->addColumn('description', 'text')
        ->addColumn('data', 'text')
        ->addColumn('sold_by', 'integer')
        ->addColumn('status', 'integer')
        ->addColumn('order', 'integer')
        ->create();
    }
}
